import React from 'react';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import { useDispatch , useSelector } from 'react-redux';
import { setFilters} from "../../features/search/searchSlice";
import { useEffect } from 'react';
import StyledSelect from '../../atoms/form/select/StyledSelect';
export default function MySelect({items , type , data , setData , label}) {
    let dispatch = useDispatch();
    let selector = useSelector((state) => state.search.filters);
    const [filter, setFilter] = React.useState('');
    const [val , setVal] = React.useState('');
    useEffect(() => {
        if(type === 'Edit'){
            setVal(data.major)
        }
        else {
            setFilter(selector[type])
        }
    }, [data , selector , type])
    const handleChange = (event) => {
        if(type !== 'Edit'){
        if(type !== 'Edit'){
        setFilter(event.target.value);
        dispatch(setFilters({filter: type, value: event.target.value}));
        }
        else {
            setData({...data, major: event.target.value})
            setVal(event.target.value)
            console.log(event.target.value)
        }
        dispatch(setFilters({filter: type, value: event.target.value}));
        }
        else {
            setData({...data, major: event.target.value})
            setVal(event.target.value)
            console.log(event.target.value)
        }
        console.log(selector)
    };
    return (
        <StyledSelect
        labelId="demo-select"
        id="demo-select-small"
        value={type !== 'Edit' ? filter : val}
        label={label}
        onChange={handleChange}
        >
            {items.map((item) => {
                return (
                    <MenuItem value={item}>{item}</MenuItem>
                );
            })}
        </StyledSelect>
    );
}