import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";

export const StyledSelectMenu = styled(Box)(({ theme }) => ({
  flex: "1",
  display: "flex",
  minWidth: "120px",
  margin: "10px",
  width: "100%",
  height: "100%",
}));

export default StyledSelectMenu;
