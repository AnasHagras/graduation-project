import FormControl from "@mui/material/FormControl";
import React from "react";
import { Label } from "../../../atoms/form/label/Label";
import Select from "../../../atoms/form/select/Select";
import StyledSelectMenu from "./StyledSelectMenu";
import { makeStyles } from "@mui/styles";
const useStyles = makeStyles((theme) => ({
  selectMenuRoot: {
    direction: "rtl !important",
    "& legend": { textAlign: "right" },
    // border: "1px solid green",
    "& .MuiInputBase-input": {
      textAlign: "right",
      direction: "rtl",
      // marginRight: "10px",
      // margin: "0px",
    },
    "& label": {
      transformOrigin: "right !important",
      left: "inherit !important",
      right: "1.80rem !important",
      color: "#807D7B",
      fontWeight: "bolder",
      overflow: "unset",
      // whiteSpace: "nowrap",
      // paddingLeft: "10px",
    },
    "& .MuiSelect-icon": {
      right: "auto", // Set the right position to auto
      left: "8px", // Set the left position to adjust the arrow
    },
    "& .MuiSelect-select, & .MuiSelect-outlined.MuiInputBase-input.MuiOutlinedInput-input":
      {
        paddingRight: "10px", // Update the paddingRight to 10px
      },
    "& .Mui-focused .MuiInputLabel-root": {
      marginRight: "0px",
    },
  },
}));
const SelectMenu = ({
  label,
  choices,
  value,
  setValue,
  type,
  disabled,
  apply,
}) => {
  const classes = useStyles();
  return (
    <StyledSelectMenu
      sx={{ minWidth: 120, flex: 1 }}
      disabled={disabled}
      className={apply && classes.selectMenuRoot}
    >
      <FormControl fullWidth style={{ flex: 1 }}>
        <Label label={label} label_id="label_idxyz" />
        <Select
          labelId="label_idxyz"
          label={label}
          choices={choices}
          type={type}
          value={value}
          setValue={setValue}
          disabled={disabled}
        />
      </FormControl>
    </StyledSelectMenu>
  );
};
export default SelectMenu;
