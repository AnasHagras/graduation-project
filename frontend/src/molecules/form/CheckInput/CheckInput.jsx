import StyledCheckInput from "./StyledCheckInput";
import Checkbox from "@mui/material/Checkbox";

export const CheckInput = ({ label, checked, setChecked }) => {
  return (
    <StyledCheckInput>
      <Checkbox checked={checked} onChange={setChecked} />
      {label}
    </StyledCheckInput>
  );
};
export default CheckInput;
