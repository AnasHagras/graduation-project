import React from "react";
import StyledFormInput from "./StyledFormInput";
import Input from "../../../atoms/form/input/Input";

const FormInput = ({
  type,
  value,
  setValue,
  label,
  disabled = false,
  required = false,
  style,
}) => {
  return (
    <StyledFormInput>
      <Input
        style={style}
        required={required}
        type={type}
        label={label}
        value={value}
        setValue={setValue}
        disabled={disabled}
      />
    </StyledFormInput>
  );
};
export default FormInput;
