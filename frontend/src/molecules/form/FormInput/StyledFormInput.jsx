import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";

export const StyledFormInput = styled(Box)(({ theme }) => ({
  margin: "10px",
  flex: "1",
  width: "100%",
  position: "relative",
}));

export default StyledFormInput;
