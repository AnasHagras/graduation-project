import { Box, Typography } from "@mui/material";
import { useState } from "react";
import FormInput from "../form/FormInput/FormInput";
import StyledSearchPickModal from "./StyledSearchPickModal";
const style = {
  position: "absolute",
  margin: "auto",
  transform: "translate(-50%, -50%)",
  width: 800,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
  top: "50%",
  left: "50%",
  borderRadius: "5px",
};
export const SearchPickModal = ({
  open,
  handleClose,
  items,
  pickedItems,
  setPickedItems,
}) => {
  const [searchText, setSearchText] = useState("");

  return (
    <StyledSearchPickModal open={open} onClose={handleClose}>
      <Box sx={style}>
        <FormInput
          label="بحث"
          placeholder="بحث"
          type="text"
          name="search"
          value={searchText}
          setValue={(searchText) => setSearchText(searchText)}
        />
        <Box
          sx={{
            maxHeight: "300px",
            overflowY: "auto",
            border: "1px solid #000",
            borderRadius: "5px",
          }}
        >
          {items
            ?.filter((item) => item.name.includes(searchText))
            ?.filter((item) => !pickedItems.includes(item.value))
            ?.map((item, idx) => (
              <Box
                key={idx}
                className="item-row"
                onClick={() => setPickedItems([...pickedItems, item.value])}
              >
                <Typography>{item.name}</Typography>
              </Box>
            ))}
        </Box>
      </Box>
    </StyledSearchPickModal>
  );
};
export default SearchPickModal;
