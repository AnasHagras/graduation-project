import { styled } from "@mui/material/styles";
import Modal from "@mui/material/Modal";

export const StyledSearchPickModal = styled(Modal)(({ theme }) => ({
  // ".modal-box": {
  //   position: "absolute",
  //   margin: "auto",
  //   transform: "translate(-50%, -50%)",
  //   maxWidth: "400px",
  //   maxHeight: "400px",
  //   bgcolor: "background.paper",
  //   border: "2px solid #000",
  //   boxShadow: 24,
  //   p: 4,
  //   top: "50%",
  //   left: "50%",
  // },
  ".item-row": {
    borderBottom: "1px solid #000",
    padding: "10px",
    margin: "5px",
    cursor: "pointer",
    display: "flex",
    alignItems: "center",
    "&:hover": {
      backgroundColor: "#000",
      color: "#fff",
    },
  },
}));

export default StyledSearchPickModal;
