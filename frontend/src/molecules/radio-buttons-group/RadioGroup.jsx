import React from "react";
import RadioGroup from "@mui/material/RadioGroup";
import { RadioFormController } from "../../atoms";
import { useDispatch } from "react-redux";
import {
  setRadioValue,
  setSearchTerm,
} from "../../features/search/searchSlice";
import {
  setEditRadioValue
} from "../../features/major edit/editSlice";
function MyRadioGroup({ dirction , radio_fields , type}) {
  let [value, setValue] = React.useState(null);
  let dispatch = useDispatch();
  let handleChange = (event) => {
    setValue(event.target.value);
    if(type === "majoredit"){
      dispatch(setEditRadioValue(event.target.value));
    }
    else if(type === "search"){
    dispatch(setRadioValue(event.target.value));
    dispatch(setSearchTerm(""));
    }
  };
  return (
    <RadioGroup
      row={dirction === "row" ? true : false}
      aria-labelledby="demo-row-radio-buttons-group-label"
      name="row-radio-buttons-group"
      value={value}
      onChange={handleChange}
    >
      {radio_fields.map((radio_field, index) => (
        <RadioFormController type={radio_field} value={index} />
      ))}
    </RadioGroup>
  );
}

export default MyRadioGroup;
