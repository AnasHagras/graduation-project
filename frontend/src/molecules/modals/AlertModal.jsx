import styled from "@emotion/styled";
import { Box, Dialog } from "@mui/material";
import React from "react";

export const AlertModalContainer = styled(Box)(({ theme }) => ({
  borderRadius: "20px",
  padding: "35px",
  display: "flex",
  flexDirection: "column",
  width: "370px",
  gap: "15px",
  justifyContent: "center",
  alignItems: "center",
  margin: "auto",
  backgroundColor: "#fff",
  [theme.breakpoints.down("sm")]: {
    width: "fit-content",
  },
}));

export const AlertModalTitle = styled("p")(({ theme }) => ({
  fontWeight: "bolder",
  color: "black",
  fontSize: "22px",
  [theme.breakpoints.down("sm")]: {
    fontSize: "18px",
  },
}));

export const AlertModalContent = styled("p")(({ theme }) => ({
  display: "flex",
  width: "100%",
  // border: "1px solid green",
  justifyContent: "center",
  alignItems: "center",
  fontSize: "18px",
  textAlign: "center",
  [theme.breakpoints.down("sm")]: {
    fontSize: "16px",
  },
}));

export const AlertModalButton = styled("div")(() => ({
  fontSize: "17px",
  padding: "10px",
  width: "100%",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  backgroundColor: "rgb(117 72 122)",
  "&:hover": {
    cursor: "pointer",
  },
  color: "white",
  borderRadius: "20px",
}));

const AlertModal = ({ title, content, buttonText, open, setOpen }) => {
  const handleClose = (e, reason) => {
    if (reason !== "backdropClick") {
      setOpen(false);
    }
  };
  return (
    <Dialog
      open={open}
      sx={{
        width: "fit-content",
        ".MuiPaper-root": {
          borderRadius: "30px",
        },
        margin: "auto",
      }}
      onClose={handleClose}
    >
      <AlertModalContainer>
        <AlertModalTitle>{title}</AlertModalTitle>
        <AlertModalContent>{content}</AlertModalContent>
        <AlertModalButton onClick={(_, reason) => handleClose(_, reason)}>
          {buttonText}
        </AlertModalButton>
      </AlertModalContainer>
    </Dialog>
  );
};

export default AlertModal;
