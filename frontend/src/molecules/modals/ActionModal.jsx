import styled from "@emotion/styled";
import { Dialog, IconButton } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import { CTextField } from "../../atoms";
import DateInput from "../../atoms/form/dateInput/DateInput";
import DateRangeIcon from "@mui/icons-material/DateRange";
// import AddChildModal from "./AddChildModal"
import {
  AlertModalButton,
  AlertModalContainer,
  AlertModalContent,
  AlertModalTitle,
} from "./AlertModal";

const AlertModalActionButton = styled(AlertModalButton)(() => ({
  paddingLeft: "8px",
  paddingRight: "8px",
}));

const AlertModalCancelButton = styled(AlertModalButton)(() => ({
  paddingLeft: "10px",
  paddingRight: "10px",
  backgroundColor: "transparent",
  color: "purple",
  border: "1px solid purple",
}));

const AlertButtonsContainer = styled("div")(() => ({
  display: "flex",
  width: "100%",
  justifyContent: "space-between",
  gap: "15px",
}));

const ActionModal = ({
  title,
  content,
  actionText,
  action,
  cancelText,
  open,
  setOpen,
  dates,
  collegeDate,
  universityDate,
  departmentDate,
  reason,
  setReason,
  setCollegeDate,
  setUniversityDate,
  setDepartmentDate,
}) => {
  const handleClose = (e, reason) => {
    if (reason !== "backdropClick") setOpen(false);
  };
  const handleAction = (e, reason) => {
    console.log("action called");
    if (reason !== "backdropClick") {
      action();
      handleClose(e, reason);
    }
  };
  const getTypingDirection = () => {
    if (reason) {
      const firstCharUnicode = reason.charCodeAt(0);

      // Arabic characters range (RTL)
      if (firstCharUnicode >= 0x0600 && firstCharUnicode <= 0x06ff) {
        return "rtl";
      }
    }

    // Default to LTR
    return "ltr";
  };

  return (
    <>
      <Dialog
        open={open}
        sx={{
          width: "fit-content",
          ".MuiPaper-root": {
            borderRadius: "30px",
          },
          margin: "auto",
        }}
        onClose={handleClose}
      >
        <AlertModalContainer>
          <AlertModalTitle>{title}</AlertModalTitle>
          <AlertModalContent>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                gap: "15px",
                justifyContent: "center",
                alignItems: "center",
                width: "100%",
              }}
            >
              <>{content}</>
              {reason !== undefined && (
                <CTextField
                  multiline
                  sx={{
                    width: "100%",
                    margin: "0px",
                    direction: `${getTypingDirection()}  !important`,
                  }}
                  // dir={getTypingDirection()}
                  label="السبب"
                  value={reason}
                  name="reason"
                  onChange={(e) => setReason(e.target.value)}
                ></CTextField>
              )}
              {dates && (
                <>
                  <DateInput
                    apply
                    date={universityDate}
                    setDate={setUniversityDate}
                    label="موافقة مجلس الجامعة"
                  />
                  <DateInput
                    apply
                    date={collegeDate}
                    setDate={setCollegeDate}
                    label="موافقة مجلس الكلية"
                  />
                  <DateInput
                    apply
                    date={universityDate}
                    setDate={setDepartmentDate}
                    label="موافقة مجلس القسم"
                  />
                </>
              )}
            </Box>
          </AlertModalContent>
          <AlertButtonsContainer>
            <AlertModalCancelButton
              onClick={(_, reason) => handleClose(_, reason)}
            >
              {cancelText}
            </AlertModalCancelButton>
            <AlertModalActionButton
              onClick={(_, reason) => handleAction(_, reason)}
            >
              {actionText}
            </AlertModalActionButton>
          </AlertButtonsContainer>
        </AlertModalContainer>
      </Dialog>
    </>
  );
};

export default ActionModal;
