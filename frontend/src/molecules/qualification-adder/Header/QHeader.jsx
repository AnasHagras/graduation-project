import StyledQHeader from "./StyledQHeader";

export const QHeader = () => {
  return (
    <StyledQHeader>
      <p style={{ flex: 1 }}> </p>
      <p style={{ flex: 4 }}>المؤهل</p>
      <p style={{ flex: 4 }}>جهة التخرج</p>
      <p style={{ flex: 2 }}>التقدير</p>
      <p style={{ flex: 2 }}>تاريخ التخرج</p>
      <p style={{ flex: 1 }}>حذف </p>
    </StyledQHeader>
  );
};
export default QHeader;
