import { styled } from "@mui/material/styles";
import { Box } from "@mui/material";
export const StyledQHeader = styled(Box)(({ theme }) => ({
  flex: 1,
  display: "flex",
  backgroundColor: "black",
  height: "50px",
  maxHeight: "50px",
  color: "white",
  padding: "10px",
  display: "flex",
  //add styles to <p> tags
  "& p": {
    color: "white",
    fontSize: "17px",
    fontWeight: "bold",
  },
}));

export default StyledQHeader;
