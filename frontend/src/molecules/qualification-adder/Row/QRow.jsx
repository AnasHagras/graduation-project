import Button from "@mui/material/Button";
import DeleteIcon from "@mui/icons-material/Delete";
import StyledQRow from "./StyledQRow";

export const QRow = ({ qualification, handleDelete }) => {
  return (
    <StyledQRow>
      <p style={{ flex: 1 }}> </p>
      <p style={{ flex: 4 }}>{qualification.degree}</p>
      <p style={{ flex: 4 }}>{qualification.university}</p>
      <p style={{ flex: 2 }}>{qualification.grade}</p>
      <p style={{ flex: 2 }}>{qualification.date}</p>
      <Button
        style={{ flex: 1 }}
        onClick={handleDelete}
        variant="outlined"
        startIcon={<DeleteIcon />}
      >
        حذف
      </Button>
    </StyledQRow>
  );
};
export default QRow;
