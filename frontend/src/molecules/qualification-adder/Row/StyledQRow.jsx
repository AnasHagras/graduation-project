import { styled } from "@mui/material/styles";
import { Box } from "@mui/material";
export const StyledQRow = styled(Box)(({ theme }) => ({
  flex: 1,
  display: "flex",
  backgroundColor: "white",
  height: "50px",
  maxHeight: "50px",
  color: "black",
  padding: "10px",
  display: "flex",
  marginBottom: "2px",
}));

export default StyledQRow;
