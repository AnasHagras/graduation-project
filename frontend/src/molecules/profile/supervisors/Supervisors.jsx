import React, { useEffect } from "react";
import getSupervision from "../../../services/supervision/getSupervision";
import { useSelector } from "react-redux";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

function Supervisors({ setPageTitle , student }) {
  console.log(student)
  const navigation = useNavigate() ;
  const [filteredSupervisors , setFilteredSupervisors] = useState([]) ;
  const [filteredIndexes , setFilteredIndexes] = useState([]) ;
  const {supervisors} = useSelector((state) => state.supervisors) ;
  // console.log(supervisors)
  useEffect(() => {
    console.log(student?.thesis?.id)
    getSupervision({thesis : student?.thesis?.id}).then((res) => {
      console.log("res", res);

      const filteredSupervisors = res?.map((item) =>
        supervisors.find((supervisor) => supervisor.user.id === item.id)
      );
    
      const filteredIndexes = res?.map((item) =>
        supervisors.findIndex((supervisor) => supervisor.user.id === item.id)
      );
      Promise.all([filteredSupervisors, filteredIndexes]).then(([supervisorsArray, indexesArray]) => {
        setFilteredSupervisors(supervisorsArray);
        setFilteredIndexes(indexesArray);
      });
    });
    setPageTitle("المشرفين");
  }, [student]);
  return (
    <table class="table table-striped table-hover">
      <tbody>
        {
          filteredSupervisors?.map((supervisor , index) => (
            <tr>
              <td style={{
                cursor : "pointer",
                textAlign : "center",
                fontWeight : "bold",
                fontSize : "20px"
              }}
              onClick = {() => navigation(`/main_info/supervisor_profile/${filteredIndexes[index]}`)}
              >{supervisor?.user?.arabicName}</td>
            </tr>
          ))
        }
      </tbody>
    </table>
  );
}

export default Supervisors;
