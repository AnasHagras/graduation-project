import React, { useEffect, useState } from "react";
import InputFieldwithEdit from "../components/InputFieldwithEdit";
import SelectFieldwithEdit from "../components/SelectFieldwithEdit";
import DateFieldwithEdit from "../components/DateFieldwithEdit";
import { useSelector } from "react-redux";
function PersonalInfoPage({ setPageTitle, student }) {
  const { organization, cities, military } = useSelector(
    (state) => state.enums
  );
  console.log("ORGANIZATION", organization);
  const DegreeChoices = [
    { id: 0, name: "بكالوريوس", display_name: "بكالوريوس" },
    { id: 1, name: "ماجستير", display_name: "ماجستير" },
    { id: 2, name: "دكتوراه", display_name: "دكتوراه" },
  ];
  const stateChoices = [
    { id: 0, name: "مستمر", display_name: "مستمر" },
    { id: 1, name: "ملغي", display_name: "ملغي" },
    { id: 2, name: "منحت", display_name: "منحت" },
  ];
  const major_choices = [
    { name: "جوهري", display_name: "جوهري" },
    { name: "غير جوهري", display_name: "غير جوهري" },
    { name: "undefined", display_name: "غير محدد" },
  ];
  const [data, setData] = useState(student);
  const [editing, setEditing] = useState(false);
  const [showButton, setShowButton] = useState(true);
  const handleEdit = () => {
    setEditing(true);
  };

  console.log("Test : ", data?.last_qualification?.department);

  const handleSave = () => {
    setEditing(false);
    setShowButton(true);
    console.log("DATA", data);
  };

  useEffect(() => {
    setPageTitle("البيانات الشخصية");
    setData(student);
  }, [student]);
  const s = 12;
  return (
    <div
      className="col-10"
      style={{
        direction: "rtl",
      }}
    >
      <table
        class="table table-striped table-hover my_table"
        style={{
          marginBottom: "15rem",
        }}
      >
        <tbody>
          <tr>
            <th
              colspan="3"
              style={{
                textAlign: "center",
              }}
            >
              الاسم بالعربي{" "}
            </th>
            <td colspan={s - 2}>
              <InputFieldwithEdit
                editing={editing}
                value={
                  data?.user?.arabicName && !editing
                    ? data?.user?.arabicName
                    : !editing && !data?.user?.arabicName
                    ? "ـــــــــــــ"
                    : data?.user?.arabicName
                }
                setValue={(e) =>
                  setData({ ...data, user: { ...data?.user, arabicName: e } })
                }
                handleSave={handleSave}
                handleEdit={handleEdit}
              />
            </td>
          </tr>
          <tr>
            <th
              colspan="3"
              style={{
                textAlign: "center",
              }}
            >
              الاسم بالانجليزي{" "}
            </th>
            <td colspan={s - 2}>
              <InputFieldwithEdit
                editing={editing}
                value={
                  data?.user?.englishName && !editing
                    ? data?.user?.englishName
                    : !editing && !data?.user?.englishName
                    ? "ـــــــــــــ"
                    : data?.user?.englishName
                }
                setValue={(e) =>
                  setData({ ...data, user: { ...data?.user, englishName: e } })
                }
                handleSave={handleSave}
                handleEdit={handleEdit}
              />
            </td>
          </tr>
          <tr>
            <th
              colspan="3"
              style={{
                textAlign: "center",
              }}
            >
              البريد الالكتروني{" "}
            </th>
            <td colspan={s - 2}>
              <InputFieldwithEdit
                editing={editing}
                value={
                  data?.user?.email && !editing
                    ? data?.user?.email
                    : !editing && !data?.user?.email
                    ? "ـــــــــــــ"
                    : data?.user?.email
                }
                setValue={(e) =>
                  setData({ ...data, user: { ...data?.user, email: e } })
                }
                handleSave={handleSave}
                handleEdit={handleEdit}
                disabled={true}
              />
            </td>
          </tr>
          <tr>
            <th
              colspan="3"
              style={{
                textAlign: "center",
              }}
            >
              الدرجة
            </th>
            <td colspan={s - 2}>
              <>{data?.thesis?.degree}</>
            </td>
          </tr>
          <tr>
            <th
              colspan="3"
              style={{
                textAlign: "center",
              }}
            >
              الحالة
            </th>
            <td colspan={s - 2}>
              {
                <SelectFieldwithEdit
                  value={
                    data?.thesis?.state && !editing
                      ? data?.thesis?.state
                      : !editing
                      ? "ـــــــــــــ"
                      : data?.thesis?.state
                  }
                  editing={editing}
                  setValue={(e) =>
                    setData({ ...data, thesis: { ...data?.thesis, state: e } })
                  }
                  handleSave={handleSave}
                  handleEdit={handleEdit}
                  options={stateChoices}
                  label={"الحالة"}
                  disabled={true}
                />
              }
            </td>
          </tr>
          <tr>
            <th
              colspan="3"
              style={{
                textAlign: "center",
              }}
            >
              تاريخ التسجيل{" "}
            </th>
            <td colspan={s - 2}>{<>{data?.thesis?.registration_period}</>}</td>
          </tr>
          <tr>
            <th
              colspan="3"
              style={{
                textAlign: "center",
              }}
            >
              تعديل جوهري
            </th>
            <td colspan={s - 2}>
              <>{data?.thesis?.state === "جوهري" ? "نعم" : "لا"}</>
            </td>
          </tr>
          <tr>
            <th
              colspan="3"
              style={{
                textAlign: "center",
              }}
            >
              رقم الملف
            </th>
            <td colspan={s - 2}>
              <InputFieldwithEdit
                editing={editing}
                value={
                  data?.file_number && !editing
                    ? data?.file_number
                    : !editing && !data?.file_number
                    ? "ـــــــــــــ"
                    : data?.file_number
                }
                setValue={(e) => setData({ ...data, file_number: e })}
                handleSave={handleSave}
                handleEdit={handleEdit}
                disabled={true}
              />
            </td>
          </tr>
          <tr>
            <th
              colspan="3"
              style={{
                textAlign: "center",
              }}
            >
              الجنسية{" "}
            </th>
            <td colspan={s - 2}> مصري </td>
          </tr>
          <tr>
            <th
              colspan="3"
              style={{
                textAlign: "center",
              }}
            >
              القسم{" "}
            </th>
            <td colspan={s - 2}>
              <SelectFieldwithEdit
                editing={editing}
                value={data?.last_qualification?.department?.name}
                setValue={(e) =>
                  setData({
                    ...data,
                    last_qualification: {
                      ...data?.last_qualification,
                      department: e,
                    },
                  })
                }
                handleSave={handleSave}
                handleEdit={handleEdit}
                options={organization?.departments?.Engineering}
                label={"القسم"}
              />
            </td>
          </tr>
          <tr>
            <th
              colspan="3"
              style={{
                textAlign: "center",
              }}
            >
              الشعبة{" "}
            </th>
            <td colspan={s - 2}>
              <SelectFieldwithEdit
                editing={editing}
                value={data?.last_qualification?.branch?.name}
                setValue={(e) =>
                  setData({
                    ...data,
                    last_qualification: {
                      ...data?.last_qualification,
                      branch: e.name,
                    },
                  })
                }
                handleSave={handleSave}
                handleEdit={handleEdit}
                options={
                  organization?.branches[
                    data?.last_qualification?.department?.name
                  ]
                }
                label={"الشعبة"}
              />
            </td>
          </tr>
          <tr>
            <th
              colspan="3"
              style={{
                textAlign: "center",
              }}
            >
              التخصص{" "}
            </th>
            <td colspan={s - 2}>
              <SelectFieldwithEdit
                editing={editing}
                value={data?.last_qualification?.specialization?.name}
                setValue={(e) =>
                  setData({
                    ...data,
                    last_qualification: {
                      ...data?.last_qualification,
                      specialization: e,
                    },
                  })
                }
                handleSave={handleSave}
                handleEdit={handleEdit}
                options={
                  organization?.specializations[
                    data?.last_qualification?.branch
                  ]
                    ? organization?.specializations[
                        data?.last_qualification?.branch?.name
                      ]
                    : []
                }
                label={"التخصص"}
              />
            </td>
          </tr>
          <tr>
            <th
              colSpan="3"
              style={{
                textAlign: "center",
              }}
            >
              الرقم القومي{" "}
            </th>
            <td colspan={s - 2}>
              <InputFieldwithEdit
                editing={editing}
                value={
                  data?.user?.nationalID && !editing
                    ? data?.user?.nationalID
                    : !editing && !data?.user?.nationalID
                    ? "ـــــــــــــ"
                    : data?.user?.nationalID
                }
                setValue={(e) =>
                  setData({ ...data, user: { ...data?.user, nationalID: e } })
                }
                handleSave={handleSave}
                handleEdit={handleEdit}
                disabled={true}
              />
            </td>
          </tr>
          <tr>
            <th
              colspan="3"
              style={{
                textAlign: "center",
              }}
            >
              النوع
            </th>
            <td colspan={s - 2}>
              <>{data?.user?.gender}</>
            </td>
          </tr>
          <tr>
            <th
              colspan="3"
              style={{
                textAlign: "center",
              }}
            >
              تاريخ الميلاد{" "}
            </th>
            <td colspan={s - 2}>
              <>
                {data?.user?.date_of_birth && !editing
                  ? data?.user?.date_of_birth
                  : !editing && !data?.user?.date_of_birth
                  ? ""
                  : data?.user?.date_of_birth}
              </>
            </td>
          </tr>
          <tr>
            <th
              colspan="3"
              style={{
                textAlign: "center",
              }}
            >
              محل الاقامة
            </th>
            <td colspan={s - 2}>
              <SelectFieldwithEdit
                editing={editing}
                value={data?.user?.city}
                setValue={(e) =>
                  setData({ ...data, user: { ...data?.user, city: e } })
                }
                handleSave={handleSave}
                handleEdit={handleEdit}
                options={cities}
                label={"محل الاقامة"}
              />
            </td>
          </tr>
          <tr>
            <th
              colspan="3"
              style={{
                textAlign: "center",
              }}
            >
              رقم الهاتف
            </th>
            <td colspan={s - 2}>
              <InputFieldwithEdit
                editing={editing}
                value={
                  data?.user?.phoneNumber && !editing
                    ? data?.user?.phoneNumber
                    : !editing && !data?.user?.phoneNumber
                    ? "ـــــــــــــ"
                    : data?.user?.phoneNumber
                }
                setValue={(e) =>
                  setData({ ...data, user: { ...data?.user, phoneNumber: e } })
                }
                handleSave={handleSave}
                handleEdit={handleEdit}
              />
            </td>
          </tr>
          <tr>
            <th
              colspan="3"
              style={{
                textAlign: "center",
              }}
            >
              موقف التجنيد{" "}
            </th>
            <td colspan={s - 2}>
              <SelectFieldwithEdit
                editing={editing}
                value={data?.user?.militaryStatus}
                setValue={(e) =>
                  setData({
                    ...data,
                    user: { ...data?.user, militaryStatus: e },
                  })
                }
                handleSave={handleSave}
                handleEdit={handleEdit}
                options={military}
                label={"موقف التجنيد"}
              />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default PersonalInfoPage;
