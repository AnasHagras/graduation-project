import "./ExpensesPage.css";

import React, { useEffect } from "react";

function ExpensesPage({ setPageTitle, student }) {
  useEffect(() => {
    setPageTitle("المصاريف");
  }, []);
  return (
    <table className="table table-striped table-hover my-table">
      <tbody>
        <tr>
          <th>أخر ايصال</th>
          <td
            className={`${
              student.attachments.lastPill ? "bg-success" : "bg-danger"
            } text-white`}
          >
            {student.attachments.lastPill ? "تم الدفع" : "لم يتم الدفع"}
          </td>
        </tr>
        <tr>
          <th>تاريخ اخر ايصال</th>
          <td className="bg-dark text-white">
            {student.attachments.lastPillDate}
          </td>
        </tr>
      </tbody>
    </table>
  );
}

export default ExpensesPage;
