import React from "react";
import SideBarItem from "../../../atoms/profile/sidebar-item/SideBarItem";
import StyledSideBar from "./SideBarStyles";

const sideBarItems = [
  ["بيانات شخصية", "personal_info"],
  ["بيانات الوظيفة", "Job_info"],
  ["المشرفين", "supervisors"],
  ["الطلبات", "orders"],
  ["المصاريف", "expenses"],
];

function SideBar({ setPage }) {
  return (
    <StyledSideBar>
      {sideBarItems.map((item, index) => (
        <SideBarItem
          setPage={setPage}
          id={index}
          title={item[0]}
          link={item[1]}
        />
      ))}
    </StyledSideBar>
  );
}
export default SideBar;
