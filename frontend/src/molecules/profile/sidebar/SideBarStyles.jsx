import { List } from "@mui/material";
import { styled } from "@mui/material/styles";
import { display } from "@mui/system";

export const StyledSideBar = styled(List)(({ theme }) => ({
  background: "grey",
  width: "100%",
}));

export default StyledSideBar;
