import React, { useEffect } from "react";

function JobInfo({ setPageTitle, student }) {
  useEffect(() => {
    setPageTitle("بيانات الوظيفة");
  }, []);
  return (
    <table class="table table-striped table-hover">
      <tbody>
        <tr>
          <th>الوظيفة الحالية</th>
          <td>{student.job}</td>
        </tr>
        <tr>
          <th>جهة العمل</th>
          <td>{student.jobPlace}</td>
        </tr>
        <tr>
          <th>هاتف العمل</th>
          <td>{student.jobPhone}</td>
        </tr>
      </tbody>
    </table>
  );
}

export default JobInfo;
