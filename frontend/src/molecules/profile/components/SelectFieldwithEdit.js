import React from "react";
import SelectMenu from "../../../molecules/form/SelectMenu/SelectMenu";
import EditIcon from "@mui/icons-material/Edit";
import SaveIcon from "@mui/icons-material/Save";

function SelectFieldwithEdit({
  editing,
  value,
  handleEdit,
  handleSave,
  setValue,
  options,
  label,
  disabled,
}) {
  // console.log("SelectFieldwithEdit.js: ", value)
  return (
    <div
      style={{
        direction: "rtl",
      }}
    >
      <div
        className="section"
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <div className="section" style={{ width: "75%" }}>
          <SelectMenu
            label={label}
            choices={options}
            value={value ? value : ""}
            disabled={!editing || disabled}
            setValue={setValue}
            apply={true}
          />
        </div>
        {editing ? (
          <SaveIcon
            onClick={handleSave}
            style={{
              cursor: "pointer",
              marginLeft: "8px",
              display: disabled ? "none" : "block",
            }}
          />
        ) : (
          <EditIcon
            onClick={handleEdit}
            style={{
              cursor: "pointer",
              marginLeft: "8px",
              display: disabled ? "none" : "block",
            }}
          />
        )}
      </div>
    </div>
  );
}

export default SelectFieldwithEdit;
