import React from 'react'
import DateInput from "../../../atoms/form/dateInput/DateInput";
import EditIcon from "@mui/icons-material/Edit";
import SaveIcon from "@mui/icons-material/Save";

function DateFieldwithEdit({ editing , value , handleEdit , handleSave , setValue , label , disabled}) {
    // console.log("SelectFieldwithEdit.js: ", value)
  return (
    <div
    style={{
        direction: "rtl",
    }}
    >
    <div
    className="section"
    style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
    }}
    >
    <div 
    className="section" 
    style={{ width: "75%"}}>
        <DateInput
        label={label}
        date={value}
        setDate={setValue}
        disabled={!editing || disabled}
        apply={true}
        />
    </div>
    {editing ? (
        <SaveIcon
        onClick={handleSave}
        style={{ cursor: "pointer", marginLeft: "8px", display : disabled ? "none" : "block"}}
        />
    ) : (
        <EditIcon
        onClick={handleEdit}
        style={{ cursor: "pointer", marginLeft: "8px" , display : disabled ? "none" : "block"}}
        />
    )}
    </div>
    </div>
  )
}

export default DateFieldwithEdit