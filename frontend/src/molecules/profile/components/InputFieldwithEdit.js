import React from 'react'
import EditIcon from "@mui/icons-material/Edit";
import SaveIcon from "@mui/icons-material/Save";

function InputFieldwithEdit({editing , value , handleEdit , handleSave , setValue , disabled}) {
  // console.log("HERE" , value)
  return (
    <div>
        {
                editing ?
                <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
                >
                <input
                style={{
                  flex: 1,
                  border: "none",
                  background: "transparent",
                  color: "inherit",
                }}
                value={value}
                onChange={
                    (e) => setValue(e.target.value)
                } />
                <SaveIcon onClick={handleSave}
                style={{ cursor: "pointer", marginLeft: "8px", display : disabled ? "none" : "block"}}
                />
                </div> : 
                <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
                >
                {value}
                <div>
                </div>
                <EditIcon onClick={handleEdit} 
                style={{ cursor: "pointer", marginLeft: "8px", display : disabled ? "none" : "block"}}
                />
                </div>
              }
    </div>
  )
}

export default InputFieldwithEdit