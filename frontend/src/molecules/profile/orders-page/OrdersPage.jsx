import { useEffect } from "react";

function OrdersPage({ setPageTitle }) {
  useEffect(() => {
    setPageTitle("الطلبات");
  }, []);
  return (
    <>
      <table className="table table-striped table-hover my-table">
        <tbody>
          <tr>
            <th>طلب تغيير عنوان</th>
            <td className="bg-success text-white">تمت الموافقة</td>
          </tr>
          <tr>
            <th>طلب تغيير مشرف</th>
            <td className="bg-success text-white">تمت الموافقة </td>
          </tr>
          <tr>
            <th>طلب مد</th>
            <td className="bg-dark text-white">لم يتم تقديم طلب</td>
          </tr>
          <tr>
            <th>طلب تشكيل</th>
            <td className="bg-danger text-white">تم رفض الطلب</td>
          </tr>
          <tr>
            <th>طلب الغاء </th>
            <td className="bg-warning text-white">لم يتم الموافقة بعد </td>
          </tr>
        </tbody>
      </table>
    </>
  );
}
export default OrdersPage;
