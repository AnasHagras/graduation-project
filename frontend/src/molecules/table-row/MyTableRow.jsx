import React from "react";
import StyledTableRow from "./TableRowStyles.jsx";
import TableCell from "../../atoms/table-cell/TableCell.jsx";

function MyTableRow({ cells, onClick }) {
  console.log(cells);

  return (
    <StyledTableRow>
      {cells.slice(0, cells.length - (cells.length === 6)).map((data) => {
        return <TableCell data={data} onClick={onClick}></TableCell>;
      })}
    </StyledTableRow>
  );
}

export default MyTableRow;
