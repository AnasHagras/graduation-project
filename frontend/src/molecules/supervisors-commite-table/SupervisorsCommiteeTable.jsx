import Button from "@mui/material/Button";
import DeleteIcon from "@mui/icons-material/Delete";
import { MenuItem, Select } from "@mui/material";
import StyledSupervisorsCommiteeTable from "./StyledSupervisorsCommiteeTable";
import { useState } from "react";

export const SupervisorsCommiteeTable = ({
  supervisors,
  setSupervisors,
  supervisionTypes,
  setSupervisionTypes,
  disabled,
  loading,
}) => {
  return (
    <StyledSupervisorsCommiteeTable>
      <div className="supervisors-commitee-header">
        <p style={{ flex: 1 }}> </p>
        <p style={{ flex: 4 }}>اسم المشرف</p>
        <p style={{ flex: 2 }}>نوع الإشراف</p>
        <p style={{ flex: 2 }}>حذف</p>
      </div>

      {
        loading ? 
        <div className="spinner-border"
        style={{
          color: "#5e72e4",
          width: "3rem",
          height: "3rem",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          textAlign: "center",
          margin: "auto",
        }}
        >
        </div>
        :
        supervisors.map((supervisor, index) => (
          <div className="supervisors-commitee-row" key={index}>
            <p style={{ flex: 1 }}>{index + 1}</p>
            <p style={{ flex: 4 }}>{supervisor.user.arabicName}</p>
            <div style={{ flex: 2 }}>
              <Select
                style={{ height: "30px", marginBottom: "10px" }}
                value={supervisionTypes[index]}
                onChange={(e) => {
                  console.log(e.target.value);
                  let newSupervisors = [...supervisors];
                  // newSupervisors[index].supervisionDegree = e.target.value;
                  setSupervisors(newSupervisors);
                  setSupervisionTypes({
                    ...supervisionTypes,
                    [index] : e.target.value,
                  });
                }}
                disabled = { disabled }
              >
                <MenuItem value={"مشرف رئيسي"}>مشرف رئيسي</MenuItem>
                <MenuItem value={"مشرف مشارك"}>مشرف مشارك</MenuItem>
              </Select>
            </div>
            <p style={{ flex: 2 }}>
              <Button
                style={{ flex: 1 }}
                onClick={() => {
                  const newSupervisors = [...supervisors];
                  setSupervisionTypes({ ...supervisionTypes, index: null });
                  newSupervisors.splice(index, 1);
                  setSupervisors(newSupervisors);
                }}
                variant="outlined"
                startIcon={<DeleteIcon />}
                disabled={disabled}
              >
                حذف
              </Button>
            </p>
          </div>
        ))
      }
    </StyledSupervisorsCommiteeTable>
  );
};
export default SupervisorsCommiteeTable;
