import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";

export const StyledSupervisorsCommiteeTable = styled(Box)(({ theme }) => ({
  width: "850px",
  minHeight: "200px",
  border: "1px solid #000",
  borderRadius: "5px",
  backgroundColor: "lightgrey",
  display: "flex",
  flexDirection: "column",
  margin: "auto",
  ".supervisors-commitee-header": {
    flex: 1,
    display: "flex",
    backgroundColor: "black",
    height: "50px",
    maxHeight: "50px",
    color: "white",
    padding: "10px",
    display: "flex",
    //add styles to <p> tags
    "& p": {
      color: "white",
      fontSize: "17px",
      fontWeight: "bold",
    },
  },
  ".supervisors-commitee-row": {
    flex: 1,
    display: "flex",
    backgroundColor: "white",
    height: "50px",
    maxHeight: "50px",
    color: "black",
    padding: "10px",
    display: "flex",
    marginBottom: "2px",
  },
}));

export default StyledSupervisorsCommiteeTable;
