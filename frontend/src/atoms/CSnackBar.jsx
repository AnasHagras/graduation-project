import * as React from "react";
import Button from "@mui/material/Button";
import Snackbar from "@mui/material/Snackbar";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import MuiAlert from "@mui/material/Alert";
import { Typography } from "@mui/material";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const CSnackBar = (props) => {
  return (
    <Snackbar
      open={props.open}
      autoHideDuration={4000}
      onClose={() => props.setOpen(false)}
      anchorOrigin={{ vertical: "top", horizontal: "center" }}
    >
      <Alert
        icon={false}
        sx={{
          direction: "ltr",
          width: "100%",
        }}
        severity={props.severity}
        onClose={() => props.setOpen(false)}
      >
        {props.message}
      </Alert>
    </Snackbar>
  );
};

export default CSnackBar;
