import { Button, CircularProgress, Typography } from "@mui/material";
import React from "react";

/**
 *
 * takes: normal content , loading content , onClick , disabled
 *
 * @returns Button
 */
function CLoadingButton(props) {
  return (
    <Button
      variant="contained"
      disableRipple={true}
      sx={props.sx}
      size="large"
      onClick={props.onClick}
      disabled={props.disabled}
    >
      {props.disabled && (
        <CircularProgress
          color="inherit"
          size={16}
          sx={{ marginRight: "5px " }}
        />
      )}
      {props.disabled ? (
        <Typography sx={{ fontWeight: "bolder" }}>
          {props.loadingContent}
        </Typography>
      ) : (
        <Typography sx={{ fontWeight: "bolder" }}>
          {props.normalContent}
        </Typography>
      )}
    </Button>
  );
}

export default CLoadingButton;
