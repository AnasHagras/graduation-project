import { styled, Typography } from "@mui/material";
import React from "react";

const StyledTextLink = styled(Typography)(({ theme }) => ({
  color: "gray",
  textDecoration: "none",
  "&:hover": {
    color: "gray",
  },
}));

const TextLink = (props) => {
  return <StyledTextLink component="a" {...props}></StyledTextLink>;
};

export default TextLink;
