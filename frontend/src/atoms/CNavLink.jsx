import { Button, styled } from "@mui/material";
import React from "react";
import { NavLink } from "react-router-dom";

const StyledNavLink = styled(NavLink)(({ theme }) => ({
  display: "block",
  padding: "5px",
  position: "relative",
  // fontSize:""
  "&.active , &:hover": {
    color: "#31efbd",
    fontWeight: "bold",
    "&:after": {
      width: "100%",
      borderBottom: `1px solid #31efbd`,
    },
  },
  "&:after": {
    content: `""`,
    position: "absolute",
    width: "0px",
    right: "0px",
    bottom: "0px",
    transitionProperty: "width",
    transitionDuration: "0.75s",
  },
  color: "white",
  fontSize: "19px",
}));

const CNavLink = (props) => {
  const { link } = props;
  return (
    <StyledNavLink
      // {...props}
      to={link.to}
      style={{ textDecoration: "none" }}
    >
      {link.name}
    </StyledNavLink>
  );
};

export default CNavLink;
