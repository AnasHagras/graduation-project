import StyledInput from "./StyledInput";

const Input = ({
  label,
  value,
  setValue,
  type,
  disabled = false,
  required = false,
  style,
}) => {
  return (
    <StyledInput
      style={style}
      required={required}
      disabled={disabled}
      InputLabelProps={{ className: "textfield_label" }}
      label={label}
      type={type}
      value={value}
      onChange={(e) => {
        setValue(e.target.value);
      }}
    />
  );
};
export default Input;
