import { styled } from "@mui/material/styles";
import { TextField } from "@mui/material";
export const StyledInput = styled(TextField)(({ theme }) => ({
  width: "100%",
}));
export default StyledInput;
