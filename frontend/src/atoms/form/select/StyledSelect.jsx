import { styled } from "@mui/material/styles";
import { Select } from "@mui/material";
const StyledSelect = styled(Select)(({ theme }) => ({
  flex: 1,
  width: "100%",
  height: "100%",
}));
export default StyledSelect;
