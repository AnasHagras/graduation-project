import StyledSelect from "./StyledSelect";
import MenuItem from "@mui/material/MenuItem";

const Select = ({ label, choices, value, setValue, type, disabled }) => {
  const handleChange = (event) => {
    setValue(event.target.value);
  };
  console.log("FFF", typeof choices, choices);

  return (
    <StyledSelect
      label={label}
      value={value}
      disabled={disabled}
      onChange={handleChange}
    >
      {type === "org"
        ? Array.isArray(choices) &&
          choices?.map((choice, id) => {
            return (
              <MenuItem key={id} value={choice}>
                {choice.display_name}
              </MenuItem>
            );
          })
        : Array.isArray(choices) &&
          choices?.map((choice, id) => {
            return (
              <MenuItem key={id} value={choice.name}>
                {choice.display_name}
              </MenuItem>
            );
          })}
    </StyledSelect>
  );
};
export default Select;
