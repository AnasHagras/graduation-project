import InputLabel from "@mui/material/InputLabel";

export const Label = ({ label, label_id }) => {
  return <InputLabel id={label_id}>{label}</InputLabel>;
};
