import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";

export const StyledDateInput = styled(Box)(({ theme }) => ({
  flex: "1",
  display: "flex",
  minWidth: "120px",
  margin: "10px",
  width: "100%",
  alignSelf: "bottom",
  ".MuiFormControl-root": {
    width: "100%",
  },
}));

export default StyledDateInput;
