import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import StyledDateInput from "./StyledDateInput";
import { makeStyles } from "@mui/styles";
import dayjs from "dayjs";
const useStyles = makeStyles((theme) => ({
  datePickerRoot: {
    direction: "rtl !important",
    "& legend": { textAlign: "right" },
    // border: "1px solid green",
    "& .MuiInputBase-input": {
      textAlign: "right",
      direction: "rtl",
    },
    "& label": {
      transformOrigin: "right !important",
      left: "inherit !important",
      right: "1.80rem !important",
      color: "#807D7B",
      fontWeight: "bolder",
      overflow: "unset",
    },
  },
}));
export const DateInput = ({ label, date, setDate, disabled, apply }) => {
  const classes = useStyles();
  const handleDateChanged = (newDate) => {
    const day = newDate.date();
    const month = newDate.month() + 1;
    const year = newDate.year();
    const newDateStr = `${year}-${month}-${day}`;
    setDate(newDateStr);
  };
  // date = dayjs("2022-04-17");
  // console.log("Date Info : ", typeof date);
  return (
    <StyledDateInput>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <DatePicker
          // renderInput={renderInput}
          format="YYYY-MM-DD"
          label={label}
          value={date}
          onChange={handleDateChanged}
          disabled={disabled}
          className={apply && classes.datePickerRoot}
        />
      </LocalizationProvider>
    </StyledDateInput>
  );
};
export default DateInput;
