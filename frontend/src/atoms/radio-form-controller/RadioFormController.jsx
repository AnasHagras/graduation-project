import React from 'react'
import FormControlLabel from '@mui/material/FormControlLabel';
import { Radio } from '@mui/material';

function RadioFormController({type , value}) {
  return (
    <FormControlLabel
        value={`button ${value}`}
        control={<Radio />}
        label={type}
        />
  )
}

export default RadioFormController