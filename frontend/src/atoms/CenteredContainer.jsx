import styled from "@emotion/styled";
import { Box } from "@mui/system";
import React from "react";

const Container = styled(Box)(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  alignContent: "center",
  height: "100vh",
  backgroundColor: "white",
  flexDirection: "row",
  flexWrap: "wrap",
  boxShadow: "1px 1px 1px 1px black",
}));

const CenteredContainer = (props) => {
  return <Container {...props} />;
};

export default CenteredContainer;
