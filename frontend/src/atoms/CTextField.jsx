import { styled, TextField } from "@mui/material";
import React from "react";
const CustomTextField = styled(TextField)(({ theme }) => ({
  "& label": {
    transformOrigin: "right !important",
    left: "inherit !important",
    right: "1.80rem !important",
    color: "#807D7B",
    fontWeight: "bolder",
    overflow: "unset",
  },
  "& legend": { textAlign: "right" },
  "& .MuiOutlinedInput-root:hover": {},
  direction: "ltr",
  width: "100%",
}));
const CTextField = (props) => {
  return <CustomTextField {...props}></CustomTextField>;
};

export default CTextField;
