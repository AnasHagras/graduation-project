import InputLabel from '@mui/material/InputLabel';

export default function MyInputLabel({label}) {
    return (
        <InputLabel id="demo-select"> {label} </InputLabel>
    );
}