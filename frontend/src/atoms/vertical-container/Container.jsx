import React from 'react'
import { StyledContainer } from './ContainerStyle'
function Container({children}) {
  return (
    <StyledContainer>{children}</StyledContainer>
  )
}

export default Container