import React from 'react'
import {InputAdornment} from "@mui/material" 
import SearchIcon from "@mui/icons-material/Search"
function SearchInputProps() {
  return (
    <InputAdornment position="end">
      <SearchIcon />
    </InputAdornment>
  )
}

export default SearchInputProps