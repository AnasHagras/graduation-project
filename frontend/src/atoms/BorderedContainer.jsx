import { Grid } from "@mui/material";
import { styled } from "@mui/system";
import React from "react";

const Container = styled(Grid)(({ theme }) => ({
  [theme.breakpoints.up("xs")]: {
    margin: "auto",
    width: "100%",
  },
  [theme.breakpoints.up("sm")]: {
    width: "500px",
    border: "1px solid #dddddd",
    borderRadius: "10px",
  },
  padding: "30px",
  paddingTop: "0px",
  backgroundColor: "white",
}));

const BorderedContainer = (props) => {
  return <Container container {...props}></Container>;
};

export default BorderedContainer;
