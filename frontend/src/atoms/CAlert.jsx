import { Alert } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";

function CAlert(props) {
  return (
    <Alert sx={props.sx} {...props} icon={false}>
      {props.message}
    </Alert>
  );
}

export default CAlert;
