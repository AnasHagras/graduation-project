import { styled } from "@mui/material/styles";
import { TextField } from "@mui/material";

export const StyledTextField = styled(TextField)(({ theme }) => ({
        width: "50%",
        marginBottom: "1rem",
}));

