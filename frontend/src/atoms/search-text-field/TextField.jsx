import React from "react";
import { StyledTextField } from "./TextFieldStyle";
import { useDispatch } from "react-redux";
import { SearchInputProps } from "../../atoms";
import { setSearchTerm } from "../../features/search/searchSlice";
import { makeStyles } from "@mui/styles";
const useStyles = makeStyles((theme) => ({
  textFieldRoot: {
    direction: "rtl !important",
    "& legend": { textAlign: "right" },
    // border: "1px solid green",
    "& .MuiInputBase-input": {
      textAlign: "right",
      direction: "rtl",
      // marginRight: "10px",
      // margin: "0px",
    },
    "& label": {
      transformOrigin: "right !important",
      left: "inherit !important",
      right: "1.80rem !important",
      color: "#807D7B",
      fontWeight: "bolder",
      overflow: "unset",
    },
  },}));
function TextField() {
  const classes = useStyles();
  const [preSearchTerm, setPreSearchTerm] = React.useState("");
  let dispatch = useDispatch();
  const handleChange = (event) => {
    setPreSearchTerm(event.target.value);
  };

  const handleSearch = () => {
    dispatch(setSearchTerm(preSearchTerm));
  };
  return (
    <StyledTextField
      label="بحث"
      // variant="outlined"
      onChange={handleChange}
      onKeyPress={(ev) => {
        if (ev.key === "Enter") {
          console.log(preSearchTerm);
          handleSearch();
        }
      }}
      InputProps={{ endAdornment: <SearchInputProps /> }}
      className={classes.textFieldRoot}
    ></StyledTextField>
  );
}

export default TextField;
