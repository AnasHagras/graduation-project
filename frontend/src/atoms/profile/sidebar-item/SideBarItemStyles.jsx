import { Button } from "@mui/material";
import { styled } from "@mui/material/styles";

export const StyledSideBarItem = styled(Button)(({ theme }) => ({
    margin: "5px",
    color:"white",
    "&:hover":{
        cursor: "pointer"
    },
    width: "100%",
    fontSize: 20,
}));

export default StyledSideBarItem;
