import React from "react";
import StyledSideBarItem from "./SideBarItemStyles";

function SideBarItem({ title, id, setPage }) {
  return (
    <StyledSideBarItem
      onClick={() => {
        setPage(id);
      }}
    >
      {title}
    </StyledSideBarItem>
  );
}
export default SideBarItem;
