import React from "react";
import StyledTableCell from "./TableCellStyles.jsx";
import ThreeDotsButton from "../../pages/students-table/ThreeDotsActions.js";
function TableCell({ data, onClick }) {

  return onClick === null || onClick === undefined ? <StyledTableCell 
  sx = {{cursor: "default"}}
  >{data}</StyledTableCell> :
  <StyledTableCell onClick={onClick}>{data}</StyledTableCell>;
}

export default TableCell;
