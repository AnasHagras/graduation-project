import { Navigate, Outlet, useLocation } from "react-router-dom";

import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { refreshAuth } from "../features/authentication/authSlice";
import homePage from "../data/Config";

function RequireAuth() {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(refreshAuth({ token: localStorage.getItem("token") }));
  }, []);
  const location = useLocation();
  return (
    <>
      {auth.refreshPending ? (
        <div>Loading</div>
      ) : auth.isLoggedIn ? (
        <Outlet></Outlet>
      ) : (
        <Navigate to={"/login"} state={{ from: location }} replace />
      )}
    </>
  );
}

export default RequireAuth;
