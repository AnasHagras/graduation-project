import * as React from "react";
import Box from "@mui/material/Box";
import Link from "@mui/material/Link";
import AppBar from "./AppBar";
import Toolbar from "./ToolBar";
import withRoot from "../../themes/WithRoots";
import { Button } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../../features/authentication/authSlice";
const rightLink = {
  fontSize: 16,
  color: "common.white",
  ml: 3,
};

function Navbar(props) {
  const { isLoggedIn } = useSelector((state) => state.auth);
  const authDispatch = useDispatch();
  const handleLogout = (e) => {
    e.preventDefault();
    authDispatch(logout());
  };
  return (
    <div>
      <AppBar position="fixed">
        <Toolbar sx={{ justifyContent: "space-between" }}>
          <Box sx={{ flex: 1 }} />
          <Link
            variant="h6"
            underline="none"
            color="inherit"
            href="/home"
            sx={{ fontSize: 24 }}
          >
            {"Graduation Post Management System"}
          </Link>
          <Box sx={{ flex: 1, display: "flex", justifyContent: "flex-end" }}>
            {!isLoggedIn ? (
              <>
                <Link
                  color="inherit"
                  variant="h6"
                  underline="none"
                  href="/signin"
                  sx={rightLink}
                >
                  {"Sign In"}
                </Link>
                <Link
                  variant="h6"
                  underline="none"
                  href="/signup"
                  sx={{ ...rightLink, color: "secondary.main" }}
                >
                  {"Sign Up"}
                </Link>
              </>
            ) : (
              <Link
                color="inherit"
                variant="h6"
                underline="none"
                href=""
                onClick={handleLogout}
                sx={rightLink}
              >
                {"Logout"}
              </Link>
            )}
          </Box>
        </Toolbar>
      </AppBar>
      <Toolbar />
    </div>
  );
}

export default withRoot(Navbar);
