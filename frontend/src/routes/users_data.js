import { Route } from "react-router-dom";
import CSidebar from "../organisms/CSidebar";
import AddSupervisor from "../pages/users-data/add-supervisor/AddSupervisor";
import AddUser from "../pages/users-data/add-user/AddUser";
import PersonAddIcon from "@mui/icons-material/PersonAdd";
const items3 = [
  { name: "إضافة مستخدم", to: "", icon: <PersonAddIcon></PersonAddIcon> },
  {
    name: "إضافة مشرف",
    to: "add_supervisor",
    icon: <PersonAddIcon></PersonAddIcon>,
  },
];

const UsersDataRoutes = (
  <Route
    path="/users_data"
    element=<CSidebar items={items3} title={"users"}></CSidebar>
  >
    <Route path="/users_data" element=<AddUser />></Route>
    <Route
      path="/users_data/add_supervisor"
      element=<AddSupervisor></AddSupervisor>
    ></Route>
    <Route path="/users_data/third" element=<h1>الثالثة</h1>></Route>
  </Route>
);

export default UsersDataRoutes;
