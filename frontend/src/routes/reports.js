import { Typography } from "@mui/material";
import React from "react";
import { Route } from "react-router-dom";
import CSidebar from "../organisms/CSidebar";
import NumberOfThesis from "../pages/Reports_tabs/NumberOfThesis/NumberOfThesis";
import TeachingMembers from "../pages/Reports_tabs/NumberOfThesis/TeachingMembers";
import InventoryTable from "../pages/Reports_tabs/InventoryReport/InventoryTable";
import { InventoryReport } from "../pages/Reports_tabs/InventoryReport/InventoryReport";
import ThesisNames from "../pages/Reports_tabs/ThesisNames/ThesisNames";
import ThesisNamesTable from "../pages/Reports_tabs/ThesisNames/ThesisNamesTable";
import RegistrationReports from "../pages/Reports_tabs/RegistrationReports/RegistrationReports";
import { ResearcherInformation } from "../pages/Reports_tabs/RegistrationReports/ResearcherInformation";
import StatementReport from "../pages/Reports_tabs/StatementReport/StatementReport";
import StudentsDetails from "../pages/Reports_tabs/StatementReport/StudentsDetails";
import ScientificReport from "../pages/Reports_tabs/ScientificReport/ScientificReport";
import ResearcherDocuments from "../pages/Reports_tabs/ResearcherDocuments/ResearcherDocuments";
import RequestApprovals from "../pages/Reports_tabs/RequestApprovals/RequestApprovals";
import StudentDetails from "../pages/Reports_tabs/ScientificReport/StudentDetails";
import FileCopyIcon from "@mui/icons-material/FileCopy";
import SummarizeIcon from "@mui/icons-material/Summarize";
import ArticleIcon from "@mui/icons-material/Article";
import InventoryIcon from "@mui/icons-material/Inventory";
import HowToRegIcon from "@mui/icons-material/HowToReg";
import BeenhereIcon from "@mui/icons-material/Beenhere";
import PsychologyAltIcon from "@mui/icons-material/PsychologyAlt";
import ApprovalIcon from "@mui/icons-material/Approval";
const reportsTabs = [
  {
    name: "عدد الرسائل",
    to: "",
    icon: <SummarizeIcon></SummarizeIcon>,
  },
  {
    name: "مستندات باحث",
    to: "researcher_document",
    icon: <FileCopyIcon></FileCopyIcon>,
  },
  {
    name: "اسماء الرسائل",
    to: "name_of_messages",
    icon: <ArticleIcon></ArticleIcon>,
  },
  {
    name: "تقرير حصر",
    to: "inventory_report",
    icon: <InventoryIcon></InventoryIcon>,
  },
  {
    name: "تقارير التسجيل",
    to: "registration_reports",
    icon: <HowToRegIcon></HowToRegIcon>,
  },
  { name: "إفادة", to: "statement", icon: <BeenhereIcon></BeenhereIcon> },
  {
    name: "تقرير علمي",
    to: "scientific_report",
    icon: <PsychologyAltIcon></PsychologyAltIcon>,
  },
  {
    name: "طلب موافقات",
    to: "request_approvals",
    icon: <ApprovalIcon></ApprovalIcon>,
  },
];
const ReportsRoutes = (
  <Route
    path="/reports"
    element=<CSidebar items={reportsTabs} title={"reports"}></CSidebar>
  >
    <Route
      path="/reports"
      element=<NumberOfThesis TeachingMembers={TeachingMembers} />
    ></Route>
    <Route
      path="/reports/researcher_document"
      element=<ResearcherDocuments />
    ></Route>
    <Route
      path="/reports/researcher_document/:id"
      element=<ResearcherDocuments />
    ></Route>
    <Route
      path="/reports/name_of_messages"
      element=<ThesisNames ThesisNamesTable={ThesisNamesTable} />
    ></Route>
    <Route
      path="/reports/inventory_report"
      element=<InventoryReport InventoryTable={InventoryTable} />
    ></Route>
    <Route
      path="/reports/registration_reports/:id"
      element=<RegistrationReports
        ResearcherInformation={ResearcherInformation}
      />
    ></Route>
    <Route
      path="/reports/statement/"
      element=<StatementReport StudentsDetails={StudentsDetails} />
    ></Route>
    <Route
      path="/reports/statement/:id"
      element=<StatementReport StudentsDetails={StudentsDetails} />
    ></Route>
    <Route
      path="/reports/scientific_report"
      element=<ScientificReport StudentDetails={StudentDetails} />
    ></Route>
    <Route
      path="/reports/scientific_report/:id"
      element=<ScientificReport StudentDetails={StudentDetails} />
    ></Route>
    <Route
      path="/reports/request_approvals"
      element=<RequestApprovals />
    ></Route>
    <Route
      path="/reports/request_approvals/:id"
      element=<RequestApprovals />
    ></Route>
  </Route>
);
export default ReportsRoutes;
