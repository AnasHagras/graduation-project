import { Route, useNavigate } from "react-router-dom";
import CSidebar from "../organisms/CSidebar";
import {
  StudentRegistraionForm as StudentRegistrationForm,
  Students,
  Supervisor,
} from "../pages";
import ThesisListActions from "../pages/main_info_tabs/thesis_list_actions/ThesisListActions";
import SingleThesisCancel from "../pages/main_info_tabs/thesis_cancel/SingleThesisCancel";
import SingleThesisGrant from "../pages/main_info_tabs/thesis_grant/SingleThesisGrant";
import ResearcherProfile from "../pages/ResearcherProfile/ResearcherProfile";
import SupervisorProfile from "../pages/supervisor_profile/SupervisorProfile";
import { MajorChangePage } from "../pages";
import AddIcon from "@mui/icons-material/Add";
import SupervisorAccountIcon from "@mui/icons-material/SupervisorAccount";
import SchoolIcon from "@mui/icons-material/School";
import EditIcon from "@mui/icons-material/Edit";
import PersonIcon from "@mui/icons-material/Person";
import CancelIcon from "@mui/icons-material/Cancel";
import HistoryEduIcon from "@mui/icons-material/HistoryEdu";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import ChangeCircleIcon from "@mui/icons-material/ChangeCircle";
const mainInfoTabs = [
  {
    name: "بيانات الطلاب",
    to: "",
    icon: <SchoolIcon></SchoolIcon>,
  },
  {
    name: "بيانات المشرفين والمحكمين",
    to: "supervisors_data",
    icon: <SupervisorAccountIcon></SupervisorAccountIcon>,
  },
  { name: "اضافة رسالة", to: "thesis_create", icon: <AddIcon></AddIcon> },
  {
    name: "تعديل بيانات الطلاب",
    to: "edit_researchers_data",
    icon: <EditIcon></EditIcon>,
  },
  {
    name: "بيانات المشرف",
    to: "supervisor_profile",
    icon: <PersonIcon></PersonIcon>,
  },
  {
    name: "إلغاء رسالة",
    to: "message_cancel",
    icon: <CancelIcon></CancelIcon>,
  },
  {
    name: "تعديل إالغاء رسالة",
    to: "edit_message_cancel",
    icon: <HistoryEduIcon></HistoryEduIcon>,
  },
  {
    name: "منح رسالة",
    to: "message_grant",
    icon: <CheckCircleIcon></CheckCircleIcon>,
  },
  {
    name: "تعديل منح رسالة",
    to: "edit_message_grant",
    icon: <HistoryEduIcon></HistoryEduIcon>,
  },
  {
    name: "تغيير جوهري للرسالة",
    to: "change_message",
    icon: <ChangeCircleIcon></ChangeCircleIcon>,
  },
];
const MainInfoRoutes = (
  <Route path="/main_info" element=<CSidebar items={mainInfoTabs}></CSidebar>>
    <Route path="/main_info/supervisors_data" element={<Supervisor />}></Route>
    <Route path="/main_info/" element={<Students />}></Route>
    <Route
      path="/main_info/edit_researchers_data/:id"
      element=<ResearcherProfile />
    ></Route>
    <Route
      path="/main_info/thesis_create"
      element=<StudentRegistrationForm />
    ></Route>
    <Route
      path="/main_info/supervisor_profile/:id"
      element=<SupervisorProfile />
    ></Route>
    <Route
      path="/main_info/message_cancel/:id"
      element=<SingleThesisCancel />
    ></Route>
    <Route
      path="/main_info/edit_message_cancel"
      element=<ThesisListActions type="cancel" />
    ></Route>
    <Route
      path="/main_info/message_grant/:id"
      element=<SingleThesisGrant />
    ></Route>
    <Route
      path="/main_info/edit_message_grant"
      element=<ThesisListActions type="grant" />
    ></Route>
    <Route
      path="/main_info/change_message/:id"
      element=<MajorChangePage />
    ></Route>
    <Route
      path="/main_info/change_message/:id"
      element=<MajorChangePage />
    ></Route>
  </Route>
);

export default MainInfoRoutes;
