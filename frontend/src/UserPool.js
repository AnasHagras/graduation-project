import { CognitoUserPool } from "amazon-cognito-identity-js";

const poolData = {
  UserPoolId: "us-east-1_uVVpyHGh4",
  ClientId: "246rsgsbfc9l0kdmhnuvebhks8",
};

export default new CognitoUserPool(poolData);
