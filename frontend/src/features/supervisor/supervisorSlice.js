import { createSlice, current, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { supervisorServices } from "../../services/supervisors";

const listSupervisors = createAsyncThunk(
  "supervisors/list",
  async ({ token }, thunkAPI) => {
    try {
      const response = await supervisorServices.getSupervisors({
        token: token,
      });
      return response;
    } catch (err) {
      let message = "error";
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const supervisorSlice = createSlice({
  name: "supervisors",

  initialState: {
    supervisors: null,
    supervisorPending: false,
    errorMessage: "",
    successMessage: "",
    initialLoading: true,
  },
  reducers: {},

  extraReducers: (builder) => {
    // list Supervisors actions
    builder.addCase(listSupervisors.pending, (state) => {
      state.errorMessage = "";
      state.successMessage = "";
    });
    builder.addCase(listSupervisors.fulfilled, (state, { payload }) => {
      console.log("Fulfilled  ", payload);
      state.supervisors = payload;
      state.initialLoading = false;
      state.supervisorPending = false;
    });
    builder.addCase(listSupervisors.rejected, (state, { payload }) => {
      state.initialLoading = false;
      state.errorMessage = payload;
      state.successMessage = "";
      state.supervisorPending = false;
    });
  },
});

export const { register } = supervisorSlice.actions;

export default supervisorSlice.reducer;

export { listSupervisors };
