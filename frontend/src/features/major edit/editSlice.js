import { createSlice } from "@reduxjs/toolkit";

const editSlice = createSlice({
  name: "majorEdit",
  initialState: {
    major: "",
  },
  reducers: {
    setEditRadioValue: (state, action) => {
      state.major = action.payload;
    },
  },
});

export const { setEditRadioValue } = editSlice.actions;

export default editSlice.reducer;
