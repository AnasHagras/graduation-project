import { createSlice, current, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { studentServices } from "../../services/students";

const listStudents = createAsyncThunk(
  "students/list",
  async ({ token }, thunkAPI) => {
    try {
      const response = await studentServices.listStudents({
        token: token,
      });
      return response;
    } catch (err) {
      let message = "error";
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const studentSlice = createSlice({
  name: "students",

  initialState: {
    students: null,
    studentPending: false,
    errorMessage: "",
    successMessage: "",
    initialLoading: true,
  },
  reducers: {},

  extraReducers: (builder) => {
    // list Supervisors actions
    builder.addCase(listStudents.pending, (state) => {
      state.errorMessage = "";
      state.successMessage = "";
    });
    builder.addCase(listStudents.fulfilled, (state, { payload }) => {
      console.log("Fulfilled  ", payload);
      state.students = payload;
      state.initialLoading = false;
      state.studentPending = false;
    });
    builder.addCase(listStudents.rejected, (state, { payload }) => {
      state.initialLoading = false;
      state.errorMessage = payload;
      state.successMessage = "";
      state.studentPending = false;
    });
  },
});

export const { register } = studentSlice.actions;

export default studentSlice.reducer;

export { listStudents };
