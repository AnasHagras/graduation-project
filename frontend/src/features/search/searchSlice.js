import { createSlice } from "@reduxjs/toolkit";

const searchSlice = createSlice({
  name: "search",
  initialState: {
    searchTerm: "",
    SearchType: "",
    filters: {
      Department: "",
      Status: "",
      Degree: "",
    },
  },
  reducers: {
    setSearchTerm: (state, action) => {
      state.searchTerm = action.payload;
    },
    setRadioValue: (state, action) => {
      state.SearchType = action.payload;
    },
    setFilters: (state, action) => {
      state.filters[action.payload.filter] = action.payload.value;
    },
    clearFilters: (state) => {
      state.searchTerm = "";
      state.SearchType = "";
      state.filters = {
        Department: "",
        Status: "",
        Degree: "",
      };
    },
  },
});

export const { setSearchTerm, setRadioValue, setFilters , clearFilters } = searchSlice.actions;

export default searchSlice.reducer;
