import { createSlice, current, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { useDispatch } from "react-redux";
import { services } from "../../services";

const login = createAsyncThunk("auth/login", async ({ state }, thunkAPI) => {
  try {
    console.log(state);
    const response = await services.login(state);
    return response;
  } catch (err) {
    let message = "حدث خطأ برجاء المحاولة في وقت لاحق";
    if (err.response?.status === 400) {
      message = "البريد الإلكتروني او كلمة المرور غير صحيحة";
    }
    return thunkAPI.rejectWithValue(message);
  }
});

const logout = createAsyncThunk("auth/logout", async ({ token }, thunkAPI) => {
  try {
    const response = await services.logout({ token: token });
    return response;
  } catch (err) {
    console.log(err);
    let message = "حدث خطأ برجاء المحاولة في وقت لاحق";
    if (err.response?.status === 401) {
      message = "لا يمكنك اجراء هذه العملية";
    }
    return thunkAPI.rejectWithValue(message);
  }
});

const refreshAuth = createAsyncThunk(
  "auth/refresh",
  async ({ token }, thunkAPI) => {
    try {
      const response = await services.getCurrentUser({ token: token });
      return response;
    } catch (err) {
      return thunkAPI.rejectWithValue(err.response.status);
    }
  }
);

const logoutAction = (state) => {
  localStorage.removeItem("token");
  state.pending = false;
  state.refreshPending = false;
  state.user = null;
  state.token = "";
  state.isLoggedIn = false;
  state.errorMessage = "";
  console.log("FROM LOGOUT ACTION");
};

export const authSlice = createSlice({
  name: "auth",

  initialState: {
    isLoggedIn: localStorage.getItem("token") !== null,
    user: null,
    token: localStorage.getItem("token") || null,
    pending: false,
    refreshPending: false,
    initialLoading: true,
    errorMessage: "",
    successMessage: "",
  },

  extraReducers: (builder) => {
    // login actions
    builder.addCase(login.pending, (state, { payload }) => {
      state.refreshPending = true;
      state.pending = true;
      state.errorMessage = "";
    });
    builder.addCase(login.fulfilled, (state, { payload }) => {
      localStorage.setItem("token", payload.token);
      state.pending = false;
      state.refreshPending = false;
      state.user = payload.user;
      state.token = payload.token;
      state.isLoggedIn = true;
      state.errorMessage = "";
      state.successMessage = "Welcome!" + payload.user.first_name;
    });
    builder.addCase(login.rejected, (state, { payload }) => {
      state.pending = false;
      state.refreshPending = false;
      // console.log(action);
      state.errorMessage = payload;
    });

    // logout actions
    builder.addCase(logout.pending, (state) => {
      console.log("Logout called");
      state.pending = true;
      state.errorMessage = "";
      state.successMessage = "";
    });
    builder.addCase(logout.fulfilled, (state, { payload }) => {
      console.log("Action");
      logoutAction(state);
    });
    builder.addCase(logout.rejected, (state, { payload }) => {
      state.pending = false;
      state.errorMessage = payload;
      state.successMessage = "";
      console.log("object", payload);
    });
    // refresh auth actions
    builder.addCase(refreshAuth.pending, (state) => {
      state.refreshPending = true;
      state.errorMessage = "";
      state.successMessage = "";
    });
    builder.addCase(refreshAuth.fulfilled, (state, { payload }) => {
      return {
        ...state,
        refreshPending: false,
        user: payload.user,
        initialLoading: false,
      };
    });
    builder.addCase(refreshAuth.rejected, (state, { payload }) => {
      console.log("INSIDE REFRESH AUTH REJECTED : ");
      state.refreshPending = false;
      state.initialLoading = false;
      if (payload === 401) {
        state.errorMessage = payload;
        logoutAction(state);
      }
    });
  },
});

export const { register } = authSlice.actions;

export default authSlice.reducer;

export { login, logout, refreshAuth };
