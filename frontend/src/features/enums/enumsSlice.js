import { createSlice, current, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { enumsServices } from "../../services/enums";

const listCities = createAsyncThunk(
  "enums/city",
  async ({ token }, thunkAPI) => {
    try {
      const response = await enumsServices.getCities({ token: token });
      return response;
    } catch (err) {
      let message = "error";
      return thunkAPI.rejectWithValue(message);
    }
  }
);

const listMilitary = createAsyncThunk(
  "enums/military",
  async ({ token }, thunkAPI) => {
    try {
      const response = await enumsServices.getMilitary({ token: token });
      return response;
    } catch (err) {
      let message = "error";
      return thunkAPI.rejectWithValue(message);
    }
  }
);

const listOrganization = createAsyncThunk(
  "enums/organization",
  async ({ token }, thunkAPI) => {
    try {
      const response = await enumsServices.getOrganization({ token: token });
      return response;
    } catch (err) {
      let message = "error";
      return thunkAPI.rejectWithValue(message);
    }
  }
);

const listGrades = createAsyncThunk(
  "enums/grades",
  async ({ token }, thunkAPI) => {
    try {
      const response = await enumsServices.getGrades({ token: token });
      return response;
    } catch (err) {
      let message = "error";
      return thunkAPI.rejectWithValue(message);
    }
  }
);

const checkLoading = (state) => {
  console.log(
    "FROM CHECK PENDING : ",
    state.militaryPending,
    state.organizationPending,
    state.citiesPending,
    state.gradesPending
  );
  return (
    state.militaryPending ||
    state.organizationPending ||
    state.citiesPending ||
    state.gradesPending
  );
};

export const enumsSlice = createSlice({
  name: "enums",

  initialState: {
    cities: null,
    military: null,
    citiesPending: false,
    errorMessage: "",
    successMessage: "",
    initialLoading: true,
    militaryPending: false,
    organization: null,
    organizationPending: false,
    grades: null,
    gradesPending: false,
  },
  reducers: {},

  extraReducers: (builder) => {
    // list cities actions
    builder.addCase(listCities.pending, (state) => {
      console.log("FETCH CITIES PENDING");
      state.citiesPending = true;
      state.errorMessage = "";
      state.successMessage = "";
    });
    builder.addCase(listCities.fulfilled, (state, { payload }) => {
      state.cities = payload;
      state.citiesPending = false;
      state.initialLoading = checkLoading(state);
    });
    builder.addCase(listCities.rejected, (state, { payload }) => {
      state.errorMessage = payload;
      state.successMessage = "";
      state.citiesPending = false;
      state.initialLoading = checkLoading(state);
    });
    // list military actions
    builder.addCase(listMilitary.pending, (state) => {
      console.log("FETCH MILITARY PENDING");
      state.errorMessage = "";
      state.militaryPending = true;
      state.successMessage = "";
    });
    builder.addCase(listMilitary.fulfilled, (state, { payload }) => {
      state.military = payload;
      state.militaryPending = false;
      state.initialLoading = checkLoading(state);
    });
    builder.addCase(listMilitary.rejected, (state, { payload }) => {
      state.errorMessage = payload;
      state.successMessage = "";
      state.militaryPending = false;
      state.initialLoading = checkLoading(state);
    });
    //  list organization
    builder.addCase(listOrganization.pending, (state) => {
      console.log("FETCH ORGANIZATION PENDING");
      state.errorMessage = "";
      state.organizationPending = true;
      state.successMessage = "";
      state.initialLoading = checkLoading(state);
    });
    builder.addCase(listOrganization.fulfilled, (state, { payload }) => {
      console.log("ORG FULFILLED ");
      state.organization = payload;
      state.organizationPending = false;
      state.initialLoading = checkLoading(state);
    });
    builder.addCase(listOrganization.rejected, (state, { payload }) => {
      console.log("ORG REJECTED");
      state.errorMessage = payload;
      state.successMessage = "";
      state.organizationPending = false;
      state.initialLoading = checkLoading(state);
    });
    //  list grades
    builder.addCase(listGrades.pending, (state) => {
      console.log("FETCH GRADES PENDING");
      state.errorMessage = "";
      state.gradesPending = true;
      state.successMessage = "";
    });
    builder.addCase(listGrades.fulfilled, (state, { payload }) => {
      state.grades = payload;
      state.gradesPending = false;
      state.initialLoading = checkLoading(state);
    });
    builder.addCase(listGrades.rejected, (state, { payload }) => {
      state.gradesPending = false;
      state.initialLoading = checkLoading(state);
      state.errorMessage = payload;
      state.successMessage = "";
    });
  },
});

export const { register } = enumsSlice.actions;

export default enumsSlice.reducer;

export { listCities, listMilitary, listOrganization, listGrades };
