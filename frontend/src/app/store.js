import { configureStore } from "@reduxjs/toolkit";
import authenticationReducer from "../features/authentication/authSlice";
import searchReducer from "../features/search/searchSlice";
import supervisorsReducer from "../features/supervisor/supervisorSlice";
import enumsReducer from "../features/enums/enumsSlice";
import studentSlice from "../features/students/studentSlice";
import editSlice from "../features/major edit/editSlice";
export default configureStore({
  reducer: {
    auth: authenticationReducer,
    search: searchReducer,
    supervisors: supervisorsReducer,
    enums: enumsReducer,
    major: editSlice,
    students: studentSlice,
  },
});
