import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { listSupervisors } from "../features/supervisor/supervisorSlice";
import getBranches from "../services/enums/getBranches";
import getCities from "../services/enums/getCities";
import getDepartments from "../services/enums/getDepartments";
import getFaculties from "../services/enums/getFaculties";
import getGrades from "../services/enums/getGrades";
import getMilitary from "../services/enums/getMilitary";
import getSpecializations from "../services/enums/getSpecializations";
import getStudentSupervisors from "../services/supervisors/getSupervisors";
import getSupervisors from "../services/supervisors/listSupervisors";
import updateThesis from "../services/thesis/updateThesis";

const TestServices = () => {
  const { supervisors, supervisorPending } = useSelector(
    (state) => state.supervisors
  );
  const { cities, military, organization, grades } = useSelector(
    (state) => state.enums
  );
  useEffect(() => {
    updateThesis(2, {
      state: "منحت",
    })
      .then((thesis) => console.log("thesis: ", thesis))
      .catch((err) => console.log(err));
  }, []);
  console.log("supervisors , ", supervisors);
  console.log("organization : ", organization);
  return (
    <>
      {supervisors.map((sup) => {
        return <h1>{sup.branch.display_name}</h1>;
      })}
      <h1>-</h1>
      {cities.map((city) => {
        return <h1>{city.display_name}</h1>;
      })}
      <h1>-</h1>
      {military.map((city) => {
        return <h1>{city.display_name}</h1>;
      })}
      <h1>-</h1>
      {grades.map((city) => {
        return <h1>{city.display_name}</h1>;
      })}
    </>
  );
};

export default TestServices;
