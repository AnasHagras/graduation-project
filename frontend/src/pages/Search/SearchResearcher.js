import React from "react";
import { Box } from "@mui/material";
import DisplayResearchers from "./DisplayResearchers";
import {SearchBox} from "../../molecules"
function SearchResearcher() {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        mt: "5rem",
      }}
    >
      <SearchBox />
      <DisplayResearchers />
    </Box>
  );
}

export default SearchResearcher;
