import React from "react";
import ResearcherTable from "./components/Researcher/ResearcherTable";
function DisplayResearchers() {
  return <ResearcherTable type="Res" />;
}

export default DisplayResearchers;
