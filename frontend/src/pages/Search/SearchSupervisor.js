import React from "react";
import { Box } from "@mui/material";
import DisplaySupervisors from "./DisplaySupervisors";
import Searchbox from "./components/Searchbox";
function SearchSupervisor() {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        mt: "5rem",
      }}
    >
      <Searchbox />
      <DisplaySupervisors />
    </Box>
  );
}

export default SearchSupervisor;
