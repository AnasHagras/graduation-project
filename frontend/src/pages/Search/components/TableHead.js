import React from "react";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { StyledTableCell } from "./styles/StyledTableCell";

function ResearchTableHead(props) {
  let [fields, setFields] = React.useState([]);
  React.useEffect(() => {
    setFields(props.fields);
  }, [props.fields]);
  return (
    <TableHead>
      <TableRow>
        {fields.map((field) =>
          field === "Researcher_Name" || field === "Supervisor_Name" ? (
            <StyledTableCell key={field}>{field}</StyledTableCell>
          ) : (
            <StyledTableCell key={field} align="right">
              {field}
            </StyledTableCell>
          )
        )}
      </TableRow>
    </TableHead>
  );
}

export default ResearchTableHead;
