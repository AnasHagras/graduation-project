import React from "react";
import { StyledTableRow } from "../styles/StyledTableRow";
import { StyledTableCell } from "../styles/StyledTableCell";
function ResearcherData(props) {
  let row = props.row;
  return (
    <StyledTableRow>
      <StyledTableCell component="th" scope="row">
        {row.Researcher_Name}
      </StyledTableCell>
      <StyledTableCell align="right">{row.Degree}</StyledTableCell>
      <StyledTableCell align="right">{row.State}</StyledTableCell>
      <StyledTableCell align="right">{row.Date_of_entry}</StyledTableCell>
      <StyledTableCell align="right">{row.Period_of_entry}</StyledTableCell>
      <StyledTableCell align="right">{row.Payment}</StyledTableCell>
      <StyledTableCell align="right">
        {row.Substantial_amendment}
      </StyledTableCell>
      <StyledTableCell align="right">{row.File_number}</StyledTableCell>
    </StyledTableRow>
  );
}

export default ResearcherData;
