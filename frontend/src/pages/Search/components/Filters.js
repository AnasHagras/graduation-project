import React from 'react'
import { Box } from '@mui/material'
import { DropBox } from '../../../organisms'
import data from './FiltersData.json'
import ClearFiltersButton from '../../../organisms/clear filters button/ClearFilters'
function Filters() {
  return (
    <Box
        sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        }}
    >
        {
        data.map((item) => (
            <DropBox
                key={item.id}
                items={item.items}
                type={item.type}
            />
        ))
        }
        <ClearFiltersButton />
    </Box>
  )
}

export default Filters