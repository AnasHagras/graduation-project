export function createResearcherData(
  Researcher_Name,
  Degree,
  State,
  Date_of_entry,
  Period_of_entry,
  Payment,
  Substantial_amendment,
  File_number
) {
  return {
    Researcher_Name,
    Degree,
    State,
    Date_of_entry,
    Period_of_entry,
    Payment,
    Substantial_amendment,
    File_number,
  };
}
