import React from "react";
import { StyledTableRow } from "../styles/StyledTableRow";
import { StyledTableCell } from "../styles/StyledTableCell";
function SupervisorData(props) {
  let row = props.row;
  return (
    <StyledTableRow>
      <StyledTableCell component="th" scope="row">
        {row.Supervisor_Name}
      </StyledTableCell>
      <StyledTableCell align="right">{row.Degree}</StyledTableCell>
      <StyledTableCell align="right">{row.Department}</StyledTableCell>
      <StyledTableCell align="right">{row.Division}</StyledTableCell>
      <StyledTableCell align="right">{row.Specialize}</StyledTableCell>
      <StyledTableCell align="right">{row.Specialization}</StyledTableCell>
      <StyledTableCell align="right">{row.Institution}</StyledTableCell>
    </StyledTableRow>
  );
}

export default SupervisorData;
