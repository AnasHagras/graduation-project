import * as React from "react";
import Table from "@mui/material/Table";
import TableContainer from "@mui/material/TableContainer";
import Paper from "@mui/material/Paper";
import TableHead from "../TableHead";
import TableBody from "../TableBody";
import myJson from "./Supervisor.json";
import Fields from "./SuperFields.json";
function SupervisiorTable(props) {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 700 }} aria-label="customized table">
        <TableHead fields={Fields} />
        <TableBody rows={myJson} type={props.type} />
      </Table>
    </TableContainer>
  );
}

export default SupervisiorTable;
