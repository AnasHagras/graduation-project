import React from "react";
import { TextField, InputAdornment} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import { useDispatch } from "react-redux";
import { setSearchTerm } from "../../../features/search/searchSlice";
function Searchbox() {
  const [preSearchTerm, setPreSearchTerm] = React.useState("");
  let dispatch = useDispatch();
  const handleChange = (event) => {
    setPreSearchTerm(event.target.value);
  };

  const handleSearch = () => {
    dispatch(setSearchTerm(preSearchTerm));
  };

  return (
    <TextField
      sx={{
        width: "50%",
      }}
      label="Search"
      variant="outlined"
      value={preSearchTerm}
      onChange={handleChange}
      onKeyPress={(event) => {
        if (event.key === "Enter") {
          handleSearch();
        }
      }}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <SearchIcon />
          </InputAdornment>
        ),
      }}
    />
  );
}

export default Searchbox;
