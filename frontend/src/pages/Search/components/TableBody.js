import React from "react";
import { TableBody } from "@mui/material";
import { useSelector } from "react-redux";
import ResearcherData from "./Researcher/ResearcherData";
import SupervisorData from "./Supervisor/SupervisorData";
function ResearchTableBody(props) {
  let [rows, setRows] = React.useState([]);
  let row = props.rows;
  let searchTerm = useSelector((state) => state);
  React.useEffect(() => {
    console.log(searchTerm);
    let filteredData = [];
    if (searchTerm === "" || searchTerm === undefined) {
      setRows(row);
      return;
    }
    if (props.type === "Res") {
      filteredData = row.filter((item) => {
        // return item.Researcher_Name?.toLowerCase().includes(
        //   searchTerm?.toLowerCase()
        // );
      });
    } else {
      filteredData = row.filter((item) => {
        // return (
        //   item.Supervisor_Name &&
        //   item.Supervisor_Name.toLowerCase().includes(searchTerm?.toLowerCase())
        // );
      });
    }
    setRows(filteredData);
  }, [row, searchTerm, props.type]);
  return (
    <TableBody>
      {props.type === "Res"
        ? rows.map((row) => (
            <ResearcherData key={row.Researcher_Name} row={row} />
          ))
        : rows.map((row) => (
            <SupervisorData key={row.Supervisor_Name} row={row} />
          ))}
    </TableBody>
  );
}

export default ResearchTableBody;
