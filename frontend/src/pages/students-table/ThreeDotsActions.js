import React, { useState } from "react";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { useNavigate } from "react-router-dom";

const ThreeDotsButton = ({ items, id }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const navigate = useNavigate();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleAction = (path) => {
    navigate(path.replace(":id", id));
    handleClose();
  };

  return (
    <>
      <Button onClick={handleClick}>
        <span
          style={{
            fontSize: "1.5rem",
            fontWeight: "bold",
            color: "#000",
          }}
        >
          . . .
        </span>
      </Button>
      <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleClose}>
        {items.map((item) => {
          return (
            <MenuItem onClick={() => handleAction(`/${item.to}/:id`)}>
              {item.title}
            </MenuItem>
          );
        })}
        {/* تعديل بيانات الطالب
        <MenuItem onClick={() => handleAction("/main_info/message_grant/:id")}>
          منح الرسالة
        </MenuItem>
        <MenuItem onClick={() => handleAction("/main_info/message_cancel/:id")}>
          إلغاء الرسالة
        </MenuItem>
        <MenuItem onClick={() => handleAction("/main_info/change_message/:id")}>
          تغيير جوهري للرسالة
        </MenuItem> */}
      </Menu>
    </>
  );
};

export default ThreeDotsButton;
