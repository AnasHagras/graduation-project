import React from "react";
import MyTable from "../../organisms/table/MyTable.jsx";
import header from "./Header.json";
import body from "./Body.json";
import { useSelector } from "react-redux";
import ThreeDotsButton from "./ThreeDotsActions.js";
let searchType = {
  "": 0,
  "button 0": 6,
  "button 1": 0,
};
const extract = (result, searchTerm, SearchType, filters) => {
  const filter = [];
  console.log("Results is : ", searchType, " | ", searchType[SearchType]);
  result.forEach((element) => {
    if (
      element[searchType[SearchType]]
        .toString()
        .toLowerCase()
        .trim()
        .includes(searchTerm.toString().toLowerCase()) &&
      (filters["Degree"] === "" ||
        element[1].toLowerCase().includes(filters["Degree"].toLowerCase())) &&
      (filters["Department"] === "" ||
        element[7]
          .toLowerCase()
          .includes(filters["Department"].toLowerCase())) &&
      (filters["Status"] === "" ||
        element[2].toLowerCase().includes(filters["Status"].toLowerCase()))
    ) {
      filter.push(element);
    }
  });
  return filter;
};

const thesisActions = [
  { to: "main_info/edit_researchers_data", title: "عرض الملف الشخصي" },
  { to: "main_info/message_cancel", title: "الغاء رسالة" },
  { to: "main_info/message_grant", title: "منح رسالة" },
  { to: "main_info/change_message", title: "تغير جوهري للرسالة" },
];

// Docs
// Statement
// Scientific
// Request approval
const reportActions = [
  { to: "reports/researcher_document", title: "مستندات" },
  { to: "reports/registration_reports", title: "تقرير التسجيل" },
  { to: "reports/statement", title: "افادة" },
  { to: "reports/scientific_report", title: "تقرير علمي" },
  { to: "reports/request_approvals", title: "طلب موافقات" },
];
function StudentsTable() {
  let { searchTerm, SearchType } = useSelector((state) => state.search);
  let filters = useSelector((state) => state.search.filters);
  const { students, initialLoading: studentsLoading } = useSelector(
    (state) => state.students
  );
  console.log(students);
  const result = [];
  if (students) {
    students.map((element, index) => {
      result.push([
        element.user?.arabicName ? element?.user?.arabicName : "ــــــ",
        element.thesis?.degree ? element.thesis?.degree : "_______",
        element.thesis?.state ? element?.thesis?.state : "ــــــ",
        element.registration_date ? element?.registration_date : "ــــــ",
        element.attachments?.lastPill ? "دفع" : "لم يدفع",
        element.thesis?.edit_type === "جوهري" ? "نعم" : "لا",
        element.file_number ? element.file_number : "ــــــ",
        element.last_qualification?.department?.display_name
          ? element.last_qualification?.department?.display_name
          : "ــــــ",

        element.last_qualification?.grade
          ? element?.last_qualification?.grade?.display_name
          : "ــــــ",
        <ThreeDotsButton items={thesisActions} id={[index]} />,
        <ThreeDotsButton items={reportActions} id={[index]} />,
      ]);
    });
  }
  console.log(result);
  return (
    <MyTable
      loading={studentsLoading}
      header={header}
      body={extract(result, searchTerm, SearchType, filters)}
    />
  );
}

export default StudentsTable;
