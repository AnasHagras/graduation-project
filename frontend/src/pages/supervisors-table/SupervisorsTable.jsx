import React from "react";
import MyTable from "../../organisms/table/MyTable.jsx";
import header from "./Header.json";
import body from "./Body.json";
import { useSelector } from "react-redux";
const extract = (body, searchTerm) => {
  const result = [];
  body.forEach((element) => {
    result.push(Object.values(element));
  });
  const filter = [];
  result.forEach((element) => {
    if (
      element[0].toLowerCase().includes(searchTerm.toString().toLowerCase())
    ) {
      filter.push(element);
    }
  });
  return filter;
};

function SupervisorsTable() {
  const { initialLoading: supervisorLoading } = useSelector(
    (state) => state.supervisors
  );
  const searchTerm = useSelector((state) => state.search.searchTerm);
  const supervisors = useSelector((state) => state.supervisors.supervisors);
  let table_body = [];
  for (let i = 0; i < supervisors?.length; i++) {
    table_body.push({
      Supervisor_Name:
        supervisors[i].user.arabicName !== null &&
        supervisors[i].user.arabicName !== ""
          ? supervisors[i].user.arabicName
          : "ـــ",
      Degree:
        supervisors[i].degree !== null && supervisors[i].degree !== ""
          ? supervisors[i].degree
          : "ـــ",
      Department:
        supervisors[i].department.display_name !== null &&
        supervisors[i].display_name !== ""
          ? supervisors[i].department.display_name
          : "ـــ",
      // "Division": "علوم الحاسوب",
      Specialize:
        supervisors[i].specialization !== null &&
        supervisors[i].specialization !== ""
          ? supervisors[i].specialization
          : "ـــ",
      // "Specialization": "الذكاء الاصطناعي",
      Institution:
        supervisors[i].organization !== null &&
        supervisors[i].organization !== ""
          ? supervisors[i].organization
          : "ـــ",
      ID: supervisors[i]?.user?.id,
    });
  }
  console.log(supervisors);
  return (
    <MyTable
      header={header}
      body={extract(table_body, searchTerm)}
      loading={supervisorLoading}
    />
  );
}

export default SupervisorsTable;
