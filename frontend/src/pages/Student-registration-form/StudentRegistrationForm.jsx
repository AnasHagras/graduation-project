import React, { useCallback, useEffect } from "react";
import { useState } from "react";
import FormInput from "../../molecules/form/FormInput/FormInput";
import StyledForm from "./StyledForm";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import rtlPlugin from "stylis-plugin-rtl";
import { prefixer } from "stylis";
import { CacheProvider } from "@emotion/react";
import createCache from "@emotion/cache";
import SelectMenu from "../../molecules/form/SelectMenu/SelectMenu";
import Button from "@mui/material/Button";

import {
  degree_choices,
  education_system_choices,
  department_choices,
  attachments,
  councils,
} from "./constants";
import { CheckGroup } from "../../organisms/form/CheckGroup/CheckGroup";
import QualificationAdder from "../../organisms/qualification-adder/QualificationAdder";
import SupervisorsCommitee from "../../organisms/supervisors-commitee/SupervisorsCommitee";
import { useSelector } from "react-redux";
import { useOutletContext } from "react-router-dom";
import studentThesisRegister from "../../services/students/studentThesisRegister";
import AlertModal from "../../molecules/modals/AlertModal";

const theme = createTheme({
  direction: "rtl", // Both here and <body dir="rtl">
});
const cacheRtl = createCache({
  key: "muirtl",
  stylisPlugins: [prefixer, rtlPlugin],
});

const StudentRegistraionForm = () => {
  const [_, setTitle] = useOutletContext();
  useEffect(() => {
    setTitle("إضافة رسالة");
  }, []);

  const [data, setData] = useState({
      student_data: {},
      student_profile_data: {},
      thesis_data:{},
      qualification_data:{},
      checked_attachments: [],
      qualifications: [],
      checked_councils: [],
      supervisors: [],
      supervisionTypes: [],
  });

  const [activeTab, setActiveTab] = useState(0);
  const { organization, military, grades, cities } = useSelector(
    (state) => state.enums
  );

  useEffect(()=> {
    console.log(data)
  }, [data]);

  const [alertModalTitle, setAlertModalTitle] = useState("");
  const [alertModalContent, setAlertModalContent] = useState("");
  const [alertModalButtonText, setAlertModalButtonText] = useState("");
  const [alertModalOpen, setAlertModalOpen] = useState(false);

  const handleAlertModal = ({ title, content, buttonText }) => {
    setAlertModalTitle(title);
    setAlertModalContent(content);
    setAlertModalButtonText(buttonText);
    setAlertModalOpen(true);
  };

  const handleSubmit = async () => {
    await studentThesisRegister({
      data: data, 
      token: localStorage.getItem("token")
    })
    .then((response) => {
      setData({
        student_data: {},
        student_profile_data: {},
        thesis_data:{},
        qualification_data:{},
        checked_attachments: [],
        qualifications: [],
        checked_councils: [],
        supervisors: [],
        supervisionTypes: [],
      });
      setActiveTab(0);
      handleAlertModal({
        title: "نجحت عملية التسجيل",
        content: "تم تسجيل الطالب بنجاح",
        buttonText: "حسنا",
      });
    })
    .catch((err) => {
      handleAlertModal({
        title: "عفوا",
        content: "حدث خطأ أثناء تسجيل الطلب",
        buttonText: "حسنا",
      });
    });
  }


  return (
    <CacheProvider value={cacheRtl}>
      <ThemeProvider theme={theme}>
        <StyledForm>
          {/* Main Info Tab */}
          <AlertModal
            open={alertModalOpen}
            setOpen={setAlertModalOpen}
            title={alertModalTitle}
            content={alertModalContent}
            buttonText={alertModalButtonText}
          ></AlertModal>
          {activeTab === 0 && (
            <div>
              {/* Section is a line of inputs */}
              {/* First Tab First Section */}
              <div className="section">
                <SelectMenu
                  label="الدرجة"
                  choices={degree_choices}
                  value={data.degree}
                  setValue={(degree) => setData({ ...data, degree: degree })}
                />
                <SelectMenu
                  label="نظام التعليم"
                  choices={education_system_choices}
                  value={data.student_profile_data.azharian}
                  setValue={(azharian) =>
                    setData({ ...data, student_profile_data:{
                      ...data.student_profile_data, azharian: azharian
                    } })
                  }
                />
                <FormInput
                  value={data.student_data.nationality}
                  setValue={(nationality) => {
                      let new_data = data;
                      new_data.student_data.nationality= nationality;
                      setData(new_data);
                    }
                  }
                  label="الجنسية "
                />
                <FormInput
                  value={data.student_profile_data.file_number}
                  type="number"
                  setValue={(file_number) =>
                    setData({ ...data, student_profile_data: {
                      ...data.student_profile_data, file_number: file_number
                    } })
                  }
                  label="رقم الملف "
                />
              </div>
              {/* First Tab Second Section */}
              <div className="section">
                <SelectMenu
                  label="الكلية"
                  choices={organization["faculties"]}
                  value={data.faculty}
                  setValue={(faculty) => {
                    setData({
                      ...data,
                      faculty: faculty,
                      department: "",
                      branch: "",
                      specialization: "",
                    });
                  }}
                />
                <SelectMenu
                  label="القسم"
                  choices={
                    organization["departments"][data.faculty]
                      ? organization["departments"][data.faculty]
                      : []
                  }
                  value={data.department}
                  setValue={(department) =>
                    setData({
                      ...data,
                      department: department,
                      branch: "",
                      specialization: "",
                    })
                  }
                />
                <SelectMenu
                  label="الشعبة"
                  choices={
                    organization["branches"][data.department]
                      ? organization["branches"][data.department]
                      : []
                  }
                  value={data.branch}
                  setValue={(branch) =>
                    setData({ ...data, branch: branch, specialization: "" })
                  }
                />
                <SelectMenu
                  label="التخصص"
                  choices={
                    organization["specializations"][data.branch]
                      ? organization["specializations"][data.branch]
                      : []
                  }
                  value={data.specialization}
                  setValue={(specialization) =>
                    setData({ ...data, specialization: specialization })
                  }
                />
              </div>
              {/* First Tab Third Section */}
              <div className="section">
                <FormInput
                  value={data.student_data.arabicName}
                  setValue={(arabic_name) => {
                      setData({ ...data, student_data : {
                        ...data.student_data, arabicName: arabic_name
                      } })
                    }
                  }
                  label="الإسم بالعربي"
                />
                <FormInput
                  value={data.student_data.englishName}
                  setValue={(english_name) => {
                    setData({ ...data, student_data : {
                      ...data.student_data, englishName: english_name
                    } })
                  }
                }
                  label="الإسم بالإنجليزي"
                />
                <FormInput
                  value={data.student_data.email}
                  setValue={(email) => {
                      setData({...data, student_data:{
                        ...data.student_data, email: email
                      }})
                    }
                  }
                  label="البريد الإلكتروني"
                />
              </div>
              {/* First Tab Fourth Section */}
              <div className="section">
                <FormInput
                  value={data.student_data.nationalID}
                  setValue={(national_id) => {
                    setData({ ...data, student_data : {
                      ...data.student_data, nationalID: national_id
                    } })
                  }
                }
                  label="الرقم القومي"
                />
                <FormInput
                  value={data.issue_date}
                  setValue={(issue_date) =>
                    setData({ ...data, issue_date: issue_date })
                  }
                  label="تاريخ الصدور"
                />
                <FormInput
                  value={data.issue_place}
                  setValue={(issue_place) =>
                    setData({ ...data, issue_place: issue_place })
                  }
                  label="جهة الصدور"
                />

                <SelectMenu
                  label="الجنس"
                  choices={[
                    { name: "ذكر", display_name: "ذكر" },
                    { name: "أنثى", display_name: "أنثى" },
                  ]}
                  value={data.student_data.gender}
                  setValue={(gender) => {
                      let new_data = data;
                      new_data.student_data.gender= gender;
                      setData(new_data);
                    }
                  }
                   />
              </div>
              {/* First Tab Fifth Section */}
              <div className="section">
                <FormInput
                  value={data.student_data.date_of_birth}
                  setValue={(date_of_birth) => {
                      setData({ ...data, student_data : {
                        ...data.student_data, date_of_birth: date_of_birth
                      } })
                    }
                  }
                  label="تاريخ الميلاد "
                />
                <FormInput
                  value={data.student_data.birthPlace}
                  setValue={(birth_place) =>
                    setData({ ...data, student_data: {
                      ...data.student_data, birthPlace: birth_place
                    } })
                  }
                  label="جهة الميلاد"
                />
                <SelectMenu
                  label="المحافظة"
                  choices={cities}
                  value={data.student_data.city}
                  setValue={(city) => {
                    setData({ ...data, student_data:{
                      ...data.student_data, city: cities.find((c) => c.name === city)?.id || 0
                    } })
                  }}
                />
              </div>
              {/* First Tab Sixth Section */}
              <div className="section">
                <FormInput
                  value={data.student_data.currentPlace}
                  setValue={(mahl) => setData({ ...data, student_data: {
                    ...data.student_data, currentPlace: mahl
                  } })}
                  label="محل الإقامة"
                />
              </div>
              {/* First Tab Seventh Section */}
              <div className="section">
                <FormInput
                  value={data.home_phone}
                  setValue={(home_phone) =>
                    setData({ ...data, home_phone: home_phone })
                  }
                  label="تليفون المنزل"
                />
                <FormInput
                  value={data.student_data.phone_number}
                  setValue={(personal_phone) =>{
                      setData({ ...data, student_data: {
                        ...data.student_data, phoneNumber: personal_phone, phone_number: personal_phone
                      } 
                    })
                    }
                  }
                  label="المحمول"
                />
                <FormInput
                  value={data.student_profile_data.jobPhone}
                  setValue={(jobPhone) =>{

                    setData({ ...data, student_profile_data: {
                      ...data.student_profile_data, jobPhone: jobPhone
                    } })
                    
                    console.log(data);
                  }
                }
                  label=" تليفون العمل"
                />
              </div>
              {/* First Tab Eighth Section */}
              <div className="section">
                <SelectMenu
                  label={"موقف التجنيد"}
                  choices={military}
                  value={data.student_profile_data.military}
                  setValue={(x) => {
                    setData({ ...data, student_profile_data:{
                      military: military.find((m) => m.name === x)?.id || 0
                    } })

                    console.log(data);
                  }}
                />
              </div>
              <h5 style={{ marginTop: "20px", marginBottom: "20px" }}>
                بيانات الوظيفة
              </h5>
              {/* First Tab Last Section */}
              <div className="section">
                <SelectMenu
                  label="حالة العمل"
                  choices={[
                    { name: 0, display_name: "يعمل" },
                    { name: 1, display_name: "لا يعمل" },
                  ]}
                  value={data.work_status}
                  setValue={(work_status) =>
                    setData({ ...data, work_status: work_status })
                  }
                />
                <FormInput
                  disabled={data.work_status !== 0}
                  value={data.student_profile_data.job}
                  setValue={(job) =>
                    setData({ ...data, student_profile_data:{
                      ...data.student_profile_data, job: job
                    } })
                  }
                  label="الوظيفة الحالية "
                />
                <FormInput
                  disabled={data.work_status !== 0}
                  value={data.student_profile_data.jobPlace}
                  setValue={(jobPlace) =>
                    setData({ ...data, student_profile_data:{
                      ...data.student_profile_data, jobPlace: jobPlace
                    } })
                  }
                  label="جهة العمل "
                />
              </div>
            </div>
          )}
          {/* Qualification && Attachments Tab */}
          {activeTab === 1 && (
            <div className="tab">
              <h5 style={{ marginTop: "20px", marginBottom: "20px" }}>
                بيانات المؤهل
              </h5>
              <div className="section">
                <SelectMenu
                  label="المؤهل "
                  choices={[
                    { name: "بكالوريوس", display_name: "بكالوريوس" },
                    { name: "ماجستير", display_name: "ماجستير" },
                    { name: "دكتوراه", display_name: "دكتوراه" },
                  ]}
                  value={data.qualification_data.degree}
                  setValue={(degree) =>
                    setData({ ...data, 
                      qualification_data: {
                      ...data.qualification_data, degree: degree
                    }})
                  }
                />
                <FormInput
                  label="الجامعة"
                  value={data.qualification_data.university}
                  setValue={(university) =>
                    setData({
                      ...data,
                      qualification_data: {
                        ...data.qualification_data, university: university
                      }})
                  }
                />
                <FormInput
                  label="الكلية"
                  value={data.qualification_data.faculty}
                  setValue={(faculty) =>
                    setData({
                      ...data,
                      qualification_data: {
                        ...data.qualification_data, faculty: faculty
                      }})
                  }
                />
                <FormInput
                  label="تاريخ المؤهل"
                  value={data.qualification_data.date}
                  setValue={(date) =>
                    setData({
                      ...data,
                      qualification_data: {
                        ...data.qualification_data, date: date
                      }})
                  }
                />
                <SelectMenu
                  label="التقدير "
                  choices={grades}
                  value={data.qualification_data.grade}
                  setValue={(grade) =>
                    setData({
                      ...data,
                      qualification_data: {
                        ...data.qualification_data, grade: grade
                      }})
                  }
                />
              </div>
              <h5 style={{ marginTop: "20px", marginBottom: "20px" }}>
                قم بإضافة المؤهلات الأخرى
              </h5>
              <QualificationAdder
                qualifications={data.qualifications}
                setQualifications={(qualifications) =>
                  setData({ ...data, qualifications: qualifications })
                }
              />
              <h5 style={{ marginTop: "20px", marginBottom: "20px" }}>
                المرفقات
              </h5>
              <div className="section">
                <CheckGroup
                  choices={attachments}
                  values={data.checked_attachments}
                  setValues={(checked_attachments) =>
                    setData({
                      ...data,
                      checked_attachments: checked_attachments,
                    })
                  }
                />
              </div>
            </div>
          )}
          {/* Thesis and Supervisors */}
          {activeTab === 2 && (
            <div className="tab">
              <div className="section">
                <div className="column-section">
                  <FormInput
                    label="تاريخ دورة التسجيل"
                    value={data.student_profile_data.registration_date}
                    setValue={(registration_date) =>
                      setData({ ...data, student_profile_data: {
                        ...data.student_profile_data, registration_date: registration_date
                      }})
                    }
                  />
                  <FormInput
                    label="تاريخ مجلس القسم"
                    date={data.department_council_date}
                    setValue={(department_council_date) =>
                      setData({
                        ...data,
                        department_council_date: department_council_date,
                      })
                    }
                  />
                  <FormInput
                    label="تاريخ مجلس الكلية"
                    value={data.college_council_date}
                    setValue={(college_council_date) =>
                      setData({
                        ...data,
                        college_council_date: college_council_date,
                      })
                    }
                  />

                  <FormInput
                    label="تاريخ مجلس الجامعة"
                    value={data.university_council_date}
                    setValue={(university_council_date) =>
                      setData({
                        ...data,
                        university_council_date: university_council_date,
                      })
                    }
                  />
                </div>

                <SupervisorsCommitee
                  supervisors={data.supervisors}
                  setSupervisors={(supervisors) =>
                    setData({ ...data, supervisors: supervisors })
                  }
                  supervisionTypes={data.supervisionTypes}
                  setSupervisionType={(supervisionTypes) =>
                    setData({ ...data, supervisionTypes: supervisionTypes })
                  }
                />
              </div>
              <div className="section">
                <CheckGroup
                  choices={councils}
                  values={data.checked_councils}
                  setValues={(checked_councils) =>
                    setData({ ...data, checked_councils: checked_councils })
                  }
                />
              </div>
              <div className="thesis-title">
                <FormInput
                  label="عنوان الرسالة بالعربي"
                  value={data.thesis_data.arabic_title}
                  setValue={(arabic_title) =>
                    setData({ ...data, thesis_data:{
                      ...data.thesis_data, arabic_title: arabic_title
                    } })
                  }
                />
              </div>
              <div className="thesis-title">
                <FormInput
                  label="عنوان الرسالة بالإنجليزي"
                  value={data.thesis_data.english_title}
                  setValue={(english_title) =>
                    setData({ ...data, thesis_data:{
                      ...data.thesis_data, english_title: english_title
                    } })
                  }
                />
              </div>
            </div>
          )}
          <div className="control-buttons">
            <Button
              onClick={() => {
                if (activeTab > 0) {
                  setActiveTab(activeTab - 1);
                }
              }}
            >
              السابق
            </Button>
            <Button
              onClick={() => {
                if (activeTab < 2) {
                  setActiveTab(activeTab + 1);
                }else{
                  handleSubmit();
                }
              }}
            >
              التالي
            </Button>
          </div>
        </StyledForm>
      </ThemeProvider>
    </CacheProvider>
  );
};

export default StudentRegistraionForm;
