import { styled } from "@mui/material/styles";
import Form from "@mui/material/FormGroup";

export const StyledForm = styled(Form)(({ theme }) => ({
  padding: "20px",
  paddingBottom: "300px",
  width: "80%",
  margin: "auto",
  marginTop: "20px",
  // border: "1px solid #000",
  ".section": {
    display: "flex",
    flexDirection: "row",
    maxHeight: "fit-content",
    minHeight: "fit-content",
  },
  ".column-section": {
    width: "30%",
    flexDirection: "column",
    marginBottom: "10px",
    padding: "10px",
    minHeight: "fit-content",
  },
  ".thesis-title": {
    minHeight: "100px",
    width: "100%",
    marginBottom: "10px",
    padding: "10px",
  },
  ".control-buttons": {
    display: "flex",
    flexDirection: "row",
    width: "100%",
    marginTop: "20px",
    justifyContent: "space-between",
  },
  ".control-buttons button": {
    width: "400px",
    height: "40px",
    fontSize: "15px",
    fontWeight: "bold",
    color: "white",
    backgroundColor: "#000",
    border: "1px solid #000",
    borderRadius: "5px",
    "&:hover": {
      backgroundColor: "white",
      color: "#000",
    },
  },
}));

export default StyledForm;
