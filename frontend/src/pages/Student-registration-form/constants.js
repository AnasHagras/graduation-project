export const degree_choices = [
  { name: "دكتوراه", display_name: "دكتوراه" },
  { name: "ماجستير", display_name: "ماجستير" },
];

export const education_system_choices = [
  { name: "true", display_name: "أزهري" },
  { name: "false", display_name: "غير أزهري" },
];

export const department_choices = [
  { name: "0", display_name: "قسم الهندسة الميكانيكية" },
  { name: "1", display_name: "قسم الهندسة الكهربائية" },
  { name: "2", display_name: "قسم هندسة النظم و الحاسبات" },
  { name: "3", display_name: "قسم الهندسة المدنية" },
  { name: "4", display_name: "قسم هندسة البترول و التعدين" },
  { name: "5", display_name: "قسم هندسة العمارة" },
];

export const major_choices = [
  [
    { name: "0", display_name: "الشعبة 1" },
    { name: "1", display_name: "الشعبة 2" },
    { name: "2", display_name: "الشعبة 3" },
    { name: "3", display_name: "الشعبة 4" },
    { name: "4", display_name: "الشعبة 5" },
  ],
  [
    { name: "0", display_name: "الشعبة 1" },
    { name: "1", display_name: "الشعبة 2" },
    { name: "2", display_name: "الشعبة 3" },
    { name: "3", display_name: "الشعبة 4" },
    { name: "4", display_name: "الشعبة 5" },
  ],
  [
    { name: "0", display_name: "الشعبة 1" },
    { name: "1", display_name: "الشعبة 2" },
    { name: "2", display_name: "الشعبة 3" },
    { name: "3", display_name: "الشعبة 4" },
    { name: "4", display_name: "الشعبة 5" },
  ],
  [
    { name: "0", display_name: "الشعبة 1" },
    { name: "1", display_name: "الشعبة 2" },
    { name: "2", display_name: "الشعبة 3" },
    { name: "3", display_name: "الشعبة 4" },
    { name: "4", display_name: "الشعبة 5" },
  ],
  [
    { name: "0", display_name: "الشعبة 1" },
    { name: "1", display_name: "الشعبة 2" },
    { name: "2", display_name: "الشعبة 3" },
    { name: "3", display_name: "الشعبة 4" },
    { name: "4", display_name: "الشعبة 5" },
  ],
  [
    { name: "0", display_name: "الشعبة 1" },
    { name: "1", display_name: "الشعبة 2" },
    { name: "2", display_name: "الشعبة 3" },
    { name: "3", display_name: "الشعبة 4" },
    { name: "4", display_name: "الشعبة 5" },
  ],
];

export const attachments = [
  { name: "0", display_name: "الرقم القومي" },
  { name: "1", display_name: "شهادة البكالريوس" },
  { name: "2", display_name: "شهادة الدبلومة" },
  { name: "3", display_name: "شهادة الجيش" },
  { name: "4", display_name: "إيصال المصروفات" },
  { name: "5", display_name: "تعهد بعدم القيد بأي جامعة" },
  { name: "6", display_name: "شهادة الماجستير" },
  { name: "7", display_name: "إقرار بعدم العمل (موافقة جهة العمل)" },
];

export const councils = [
  { name: "0", display_name: "موافقة مجلس القسم" },
  { name: "1", display_name: "موافقة مجلس الكلية" },
  { name: "2", display_name: "موافقة مجلس الجامعة" },
];

export default { degree_choices, education_system_choices, attachments };
