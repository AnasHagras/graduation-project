import React, { useEffect, useState } from "react";
import { SearchBox } from "../../molecules";
import { StudentsTable } from "../";
import { Container } from "../../atoms";
import { RadioBox } from "../../organisms";
import { Filters } from "../../pages";
import { useLocation, useOutletContext } from "react-router-dom";
import AlertModal from "../../molecules/modals/AlertModal";
function Students(props) {
  const [_, setTitle] = useOutletContext();
  const location = useLocation();
  const [modalOpen, setModalOpen] = useState(location.state?.modalOpen);
  console.log("STUDENTS : ", modalOpen);
  useEffect(() => {
    setTitle("بيانات الطلاب");
  }, []);

  return (
    <>
      <AlertModal
        buttonText={"حسنا"}
        content="من فضلك اختر طالب"
        open={modalOpen}
        setOpen={setModalOpen}
        title={"عذرا"}
      ></AlertModal>
      <Container>
        <RadioBox />
        <SearchBox />
        <Filters />
        <StudentsTable />
      </Container>
    </>
  );
}

export default Students;
