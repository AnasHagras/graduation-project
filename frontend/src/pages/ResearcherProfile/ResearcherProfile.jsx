import "./Profile.css";
import ExpensesPage from "../../molecules/profile/expenses-page/ExpensesPage";
import OrdersPage from "../../molecules/profile/orders-page/OrdersPage";
import PersonalInfoPage from "../../molecules/profile/personal-info-page/PersonalInfoPage";
import JobInfo from "../../molecules/profile/job-info/JobInfo";
import SideBar from "../../molecules/profile/sidebar/SideBar";
import { useEffect, useState } from "react";
import Supervisors from "../../molecules/profile/supervisors/Supervisors";
import { useOutletContext, useParams } from "react-router-dom";
import { useSelector } from "react-redux";

function ResearcherProfile() {
  const [page, setPage] = useState(0);
  const [pageTitle, setPageTitle] = useState("الملف الشخصي");
  const id = useParams().id;
  const { students } = useSelector((state) => state.students);
  const [student, setStudent] = useState({});
  useEffect(() => {
    setStudent(students ? students[id] : {});
  }, [students, id]);
  const [_, setTitle] = useOutletContext();
  useEffect(() => {
    console.log("STUDENT : ", student);
    setTitle(
      " الملف الشخصي" +
        " " +
        (student?.user?.arabicName ? ": " + student?.user?.arabicName : "")
    );
  }, [student]);
  return (
    <div>
      <h1 className="bg-secondary text-light p-3 text-center">{pageTitle}</h1>

      <div className="row">
        <div className="col-2 ">
          <SideBar setPage={setPage} />
        </div>

        <div className="col-10">
          {page === 0 && (
            <PersonalInfoPage setPageTitle={setPageTitle} student={student} />
          )}
          {page === 1 && (
            <JobInfo setPageTitle={setPageTitle} student={student} />
          )}
          {page === 2 && (
            <Supervisors setPageTitle={setPageTitle} student={student} />
          )}
          {page === 3 && (
            <OrdersPage setPageTitle={setPageTitle} student={student} />
          )}
          {page === 4 && (
            <ExpensesPage setPageTitle={setPageTitle} student={student} />
          )}
        </div>
      </div>
    </div>
  );
}
export default ResearcherProfile;
