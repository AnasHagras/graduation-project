import React, { useEffect } from "react";
import { SearchBox } from "../../molecules";
import { SupervisorsTable } from "..";
import { Container } from "../../atoms";
import { useOutletContext } from "react-router-dom";
function Supervisors() {
  const [_, setTitle] = useOutletContext();
  useEffect(() => {
    setTitle("بيانات المشرفين والمحكمين");
  }, []);
  return (
    <Container>
      <SearchBox />
      <SupervisorsTable />
    </Container>
  );
}

export default Supervisors;
