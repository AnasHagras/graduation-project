import React, { useEffect } from "react";
import ResearcherName from "./components/ResearcherName";
import RadioBox from "./components/RadioBox";
import ArabicTextField from "./components/ArabicTextField";
import SupervisorsCommitee from "../../organisms/supervisors-commitee/SupervisorsCommitee";
import { useSelector, useDispatch } from "react-redux";
import StyledForm from "../Student-registration-form/StyledForm";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { Button } from "@mui/material";
import SelectMenu from "../../molecules/form/SelectMenu/SelectMenu";
import updateThesis from "../../services/thesis/updateThesis";
import { useOutletContext, useParams } from "react-router-dom";
import getSupervision from "../../services/supervision/getSupervision";
import AlertModal from "../../molecules/modals/AlertModal";
import { listStudents } from "../../features/students/studentSlice";
import { listSupervisors } from "../../features/supervisor/supervisorSlice";
// import { updateStudent } from "../../features/students/studentSlice";
function MajorChangePage() {
  const dispatch = useDispatch();
  const [_, setSideTitle] = useOutletContext();
  useEffect(() => {
    setSideTitle("تغيير جوهري للرسالة");
  }, []);
  const student_id = useParams().id;
  const students = useSelector((state) => state.students.students);
  const supervisorsList = useSelector((state) => state.supervisors.supervisors);
  const [supervision, setSupervision] = React.useState();
  const [student, setStudent] = React.useState(null);
  const [supervisionTypes, setSupervisionTypes] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [title, setTitle] = React.useState("");
  const [content, setContent] = React.useState("");
  const theme = createTheme({
    direction: "rtl", // Both here and <body dir="rtl">
  });
  const major = useSelector((state) => state.major.major);
  const [supervisors, setSupervisors] = React.useState([]);
  const major_choices = [
    { name: "جوهري", display_name: "جوهري" },
    { name: "غير جوهري", display_name: "غير جوهري" },
    { name: "undefined", display_name: "غير محدد" },
  ];

  const fetchSupervisors = async () => {
    try {
      setLoading(true);
      const response = await getSupervision({ thesis: student?.thesis.id });
      console.log("response: ", response);
      const newValue = supervisorsList
        .map((supervisor) => {
          console.log("supervisor: ", supervisor.user.id);
          console.log(supervision);
          const matchingSupervision = response.find(
            (supervision) => supervision.id === supervisor.user.id
          );
          console.log("matchingSupervision: ", matchingSupervision);
          if (matchingSupervision) {
            console.log(matchingSupervision.supervising_type);
            setSupervisionTypes((prev) =>
              prev.concat(matchingSupervision.supervising_type)
            );
            return {
              ...supervisor,
              supervisionType: {
                name: matchingSupervision.supervising_type,
                display_name: matchingSupervision.supervising_type,
              },
            };
          }
          return supervisorsList[matchingSupervision?.id]
            ? supervisorsList[matchingSupervision?.id]
            : null;
        })
        .filter((value) => value !== null);

      console.log("newValue: ", newValue);
      setSupervisors(newValue);
      console.log("supervisors: ", supervisors);
      console.log("supervisionTypes: ", supervisionTypes);
      setLoading(false);
    } catch (error) {
      // Handle any errors that occurred during the fetch
      setLoading(false);
      setTitle("فشل في تحميل البيانات");
      setContent("حاول مرة أخرى");
      setOpen(true);
    }
  };

  useEffect(() => {
    const newStudent = students && students[student_id];
    console.log("newStudent", newStudent);
    setStudent(newStudent);
    setData({
      arabic_title: newStudent?.thesis.arabic_title,
      english_title: newStudent?.thesis.english_title,
      edit_type: newStudent?.thesis.edit_type,
      supervisors: [],
    });
    if (student) {
      fetchSupervisors();
    }
  }, [students, student]);

  console.log("supervision: ", supervisors);
  console.log("supervisionTypes: ", supervisionTypes);
  const [data, setData] = React.useState({
    arabic_title: student?.thesis.arabic_title,
    english_title: student?.thesis.english_title,
    edit_type: student?.thesis.edit_type,
    supervisors: [],
  });
  function showData() {
    let last_supervisors = [];
    for (let i = 0; i < supervisors.length; i++) {
      last_supervisors.push({
        id: supervisors[i].user.id,
        supervising_type: supervisionTypes[i],
      });
    }
    const updateThesisWithData = async () => {
      try {
        await setData({
          ...data,
          supervisors: last_supervisors,
          edit_type: "جوهري",
        });
        console.log("DATA: ", data);
        const updatedData = {
          ...data,
          supervisors: last_supervisors,
          edit_type: "جوهري",
        };
        await updateThesis({
          token: localStorage.getItem("token"),
          thesisID: 1,
          thesis: updatedData,
        });
        setTitle("تم تعديل الرسالة بنجاح");
        setContent("تم تعديل الرسالة بنجاح");
        setOpen(true);
        const token = localStorage.getItem("token");
        dispatch(listStudents({ token: token }));
        dispatch(listSupervisors({ token: token }));
      } catch (error) {
        console.log("Failed to update thesis:", error);
        setTitle("فشل في تعديل الرسالة");
        setContent("حاول مرة أخرى");
        setOpen(true);
      }
    };

    updateThesisWithData();
  }
  if (!student) {
    // Render a loading state or return null if the student data is not available yet
    return <div>Loading...</div>;
  }
  return (
    <ThemeProvider theme={theme}>
      <StyledForm>
        <>
          <AlertModal
            open={open}
            setOpen={setOpen}
            title={title}
            content={content}
            buttonText={"حسناً"}
          />
          <ResearcherName
            label={"أسم الباحث"}
            value={student?.user.arabicName}
          />
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <RadioBox />
          </div>
          <ArabicTextField
            label={"عنوان الرساله عربي (جديد)"}
            type={major}
            disabled={"button 0"}
            value={data.arabic_title}
            multiline={true}
            direction={"rtl"}
            setValue={(arabic_title) =>
              setData({ ...data, arabic_title: arabic_title })
            }
          />
          <ArabicTextField
            label={"عنوان الرساله إنجليزي (جديد)"}
            type={major}
            disabled={"button 0"}
            value={data.english_title}
            multiline={true}
            direction={"ltr"}
            setValue={(english_title) =>
              setData({ ...data, english_title: english_title })
            }
          />
          <div className="tab">
            <div className="section">
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  width: "30%",
                }}
              >
                <div style={{ width: "80%", paddingTop: "3rem" }}>
                  <SelectMenu
                    label="نوع التعديل"
                    choices={major_choices}
                    value={data.edit_type}
                    setValue={(edit_type) =>
                      setData({ ...data, edit_type: edit_type })
                    }
                    disabled={true}
                    apply={true}
                  />
                </div>
              </div>
              <SupervisorsCommitee
                supervisors={supervisors}
                setSupervisors={(supervisors) => setSupervisors(supervisors)}
                supervisionTypes={supervisionTypes}
                setSupervisionType={(supervisionTypes) =>
                  setSupervisionTypes(supervisionTypes)
                }
                disabled={major !== "button 1" ? true : false}
                loading={loading}
              />
            </div>
          </div>
          <Button
            style={{
              margin: "10px",
              height: "55px",
              fontSize: "17px",
              fontWeight: "bold",
            }}
            disabled={
              major !== "button 0" && major !== "button 1" ? true : false
            }
            variant="outlined"
            onClick={() => showData()}
          >
            حفظ التغييرات
          </Button>
        </>
      </StyledForm>
    </ThemeProvider>
  );
}

export default MajorChangePage;
