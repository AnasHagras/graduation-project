import { makeStyles } from "@mui/styles";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { useEffect, useState } from "react";
const useStyles = makeStyles((theme) => (
  {
  root: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: theme.spacing(2),
  },
  textField: {
    marginRight: theme.spacing(2),
    width: "85%",
  },
  label: {
    fontSize: "1.2rem",
    fontWeight: "bold",
    marginRight: theme.spacing(2),
  },
  textFieldRoot: (dir) => (
    // console.log(dir) ,
    {
    direction: `${dir} !important`,
    "& legend": { textAlign: `right` },
    // border: "1px solid green",
    "& .MuiInputBase-input": {
      textAlign: `${dir} !important`,
      direction: `${dir} !important`,
      // marginRight: "10px",
      // margin: "0px",
    },
    "& label": {
      transformOrigin: "right !important",
      left: "inherit !important",
      right: "1.80rem !important",
      color: "#807D7B",
      fontWeight: "bolder",
      overflow: "unset",
    },
  }),
}));

function ArabicTextField({ label, type, disabled, value, multiline, direction, setValue }) {
  const classes = useStyles({dir : direction});
  const handleChange = (e) => {
    setValue(e.target.value);
  };

  return (
    <Box className={classes.root}>
      {label && (
        <Typography className={classes.label}>{label}</Typography>
      )}
      <TextField
      label={label}
        className={`${classes.textField} ${classes.textFieldRoot}`}
        size="small"
        disabled={disabled !== type}
        value={value}
        dir={direction}
        multiline={multiline}
        rows={4}
        variant={disabled !== type ? "filled" : "outlined"}
        onChange={handleChange}
      />
    </Box>
  );
}

export default ArabicTextField;
