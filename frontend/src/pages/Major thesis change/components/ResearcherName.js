import React from 'react';
import { makeStyles } from '@mui/styles';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';


const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
  label: {
    fontWeight: 'bold',
    paddingRight: theme.spacing(2),
  },
  value: {
    color: theme.palette.text.secondary,
  },
}));

const ResearcherName = ({ label, value }) => {
  const classes = useStyles();

  return (
    <TableRow className={classes.root}>
      <TableCell className={classes.label} component="th" scope="row">
        {label}
      </TableCell>
      <TableCell className={classes.value} align="left">
        {value}
      </TableCell>
    </TableRow>
  );
}

export default ResearcherName;
