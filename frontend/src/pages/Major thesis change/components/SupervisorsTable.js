import React, { useState } from "react";
import { makeStyles } from "@mui/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Button,
} from "@mui/material";
import { styled } from "@mui/material/styles";
import ArabicTextField from "./ArabicTextField";
import { useSelector } from "react-redux";

const useStyles = makeStyles({
  tableContainer: {
    margin: "20px",
    maxWidth: "800px",
  },
});

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  textAlign: "right",
}));

const initialData = [
  { name: "John Doe", type: "Admin" },
  { name: "Jane Smith", type: "Manager" },
  { name: "Bob Johnson", type: "Supervisor" },
];

const SupervisorsTable = ({ type, disabled }) => {
  const { initialLoading: supervisorLoading } = useSelector(
    (state) => state.supervisors
  );
  const classes = useStyles();
  const [data, setData] = useState(initialData);

  const handleDelete = (index) => {
    setData(data.filter((_, i) => i !== index));
  };
  const handleAdd = () => {
    setData([...data, { name: "", type: "" }]);
  };
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <TableContainer component={Paper} className={classes.tableContainer}>
        {supervisorLoading ? (
          <div
            className="spinner-border"
            style={{
              color: "#5e72e4",
              width: "3rem",
              height: "3rem",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              textAlign: "center",
              margin: "auto",
            }}
          ></div>
        ) : (
          <Table aria-label="styled table">
            <TableHead>
              <TableRow dir="rtl">
                <StyledTableCell dir="rtl">أسم المشرف</StyledTableCell>
                <StyledTableCell dir="rtl">نوع المشرف</StyledTableCell>
                <StyledTableCell>حذف</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((row, index) => (
                <TableRow key={index} dir="rtl">
                  <StyledTableCell>
                    <ArabicTextField
                      label={""}
                      type={type}
                      disapled={disabled}
                      value={row.name}
                      multilines={false}
                      dirc={"rtl"}
                    />
                  </StyledTableCell>
                  <StyledTableCell>
                    <ArabicTextField
                      label={""}
                      type={type}
                      disapled={disabled}
                      value={row.type}
                      multilines={false}
                      dirc={"rtl"}
                    />
                  </StyledTableCell>
                  <StyledTableCell>
                    <Button
                      variant="contained"
                      color="secondary"
                      onClick={() => handleDelete(index)}
                      disabled={disabled != type ? true : false}
                    >
                      حذف
                    </Button>
                  </StyledTableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        )}
      </TableContainer>
      <Button
        variant="contained"
        disableElevation
        sx={{ m: 2 }}
        onClick={() => handleAdd()}
        disabled={disabled !== type ? true : false}
      >
        إضافة مشرف
      </Button>
    </div>
  );
};

export default SupervisorsTable;
