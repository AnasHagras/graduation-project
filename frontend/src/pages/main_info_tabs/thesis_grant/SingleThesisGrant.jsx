import styled from "@emotion/styled";
import { CircularProgress } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { refreshAuth } from "../../../features/authentication/authSlice";
import { listStudents } from "../../../features/students/studentSlice";
import { studentMapping } from "../../../mappings/studentMapping";
import ActionModal from "../../../molecules/modals/ActionModal";
import AlertModal from "../../../molecules/modals/AlertModal";
import getStudent from "../../../services/students/getStudent";
import fetchStudents from "../../../services/students/listStudents";
import updateThesis from "../../../services/thesis/updateThesis";
import { SaveChangesButton } from "../../users-data/add-user/AddUser";

const ListContainer = styled("div")(({ theme }) => ({
  backgroundColor: "#288750",
  width: "80%",
  margin: "auto",
  borderRadius: "10px",
  padding: "10px 6px",
}));

const ListItem = styled("div")(({ theme }) => ({
  transition: "background-color .20s linear ",
  "&:hover": {
    backgroundColor: "#89c58b8c",
  },
  color: "#fff",
  padding: "10px",
  marginBottom: "5px",
  marginTop: "5px",
  display: "flex",
  justifyContent: "space-between",
  borderRadius: "10px",
  [theme.breakpoints.down("md")]: {
    flexDirection: "column",
  },
}));

const Divider = styled("div")(({ theme }) => ({
  borderBottom: "1px solid #ffffff85",
  marginLeft: "10px",
  marginRight: "10px",
}));

const ListItemKey = styled("div")(({ theme }) => ({
  fontWeight: "bold",
  borderRadius: "10px",
}));

const ListItemValue = styled("div")(({ theme }) => ({
  borderRadius: "10px",
  width: "50%",
}));

const SaveButton = styled("div")(({ theme }) => ({
  borderRadius: "10px",
  color: "white",
  width: "fit-content",
  padding: "5px",
  paddingLeft: "70px",
  paddingRight: "70px",
  ":hover": {
    cursor: "pointer",
    backgroundColor: "#185f364f",
  },
  backgroundColor: "#13502d",
  border: "2px solid white",
  margin: "auto",
  marginTop: "30px",
  marginBottom: "20px",
  transition:
    "color .20s ease-in-out, background-color .20s ease-in-out, border-color .20s ease-in-out ",
}));

const SingleThesisGrant = () => {
  const studentID = useParams()["id"];
  const [student, setStudent] = useState(null);
  const { token } = useSelector((state) => state.auth);
  const [confirmModalOpen, setConfirmModalOpen] = useState(false);
  const [alertModalOpen, setAlertModalOpen] = useState(false);
  const [loading, setLoading] = useState(true);
  const [thesis, setThesis] = useState(null);
  const [alertModalHandler, setAlertModalHandler] = useState({});
  const [collegeDate, setCollegeDate] = useState();
  const [universityDate, setUniversityDate] = useState();
  const [departmentDate, setDepartmentDate] = useState();
  const dispatch = useDispatch();
  useEffect(() => {
    // TODO : replaced with just Get User
    fetchStudents({ token: token })
      .then((res) => {
        console.log("RES : ", res);
        const currentStudent = res.filter((student, idx) => idx == studentID);
        res.map((id, idx) => console.log("ID: ", idx));
        setStudent(currentStudent[0]);
        console.log("Logs : ", currentStudent[0]);
        setThesis(currentStudent[0].thesis);
        setLoading(false);
      })
      .catch((err) => {
        console.log("Error : ", err);
      });
    //  get user this update the states
  }, [confirmModalOpen]);
  const handleAlertModal = (alertModalHandler) => {
    setAlertModalHandler({
      title: alertModalHandler["title"],
      content: alertModalHandler["content"],
      buttonText: alertModalHandler["buttonText"],
    });
    setAlertModalOpen(true);
  };
  const handleAction = () => {
    if (!departmentDate || !collegeDate || !universityDate) {
      handleAlertModal({
        title: "فشل",
        content: "فشل منح هذه الرسالة",
        buttonText: "حسنا",
      });
      return;
    }
    updateThesis({
      token: token,
      thesisID: thesis.id,
      thesis: {
        state: "منحت",
        department_cancel_date: departmentDate,
        college_cancel_date: collegeDate,
        university_cancel_date: universityDate,
      },
    })
      .then((thesis) => {
        console.log("thesis: ", thesis);
        handleAlertModal({
          title: "تم",
          content: "تم منح الرسالة بنجاح",
          buttonText: "حسنا",
        });
        dispatch(listStudents({ token: localStorage.getItem("token") }));
      })
      .catch((err) => {
        handleAlertModal({
          title: "فشل",
          content: "فشل منح هذه الرسالة",
          buttonText: "حسنا",
        });
      });
  };
  return (
    <>
      <ActionModal
        open={confirmModalOpen}
        setOpen={setConfirmModalOpen}
        action={handleAction}
        dates={true}
        collegeDate={collegeDate}
        universityDate={universityDate}
        departmentDate={departmentDate}
        setCollegeDate={setCollegeDate}
        setUniversityDate={setUniversityDate}
        setDepartmentDate={setDepartmentDate}
        title={"تأكيد"}
        content={"هل انت متأكد من منح هذه الرسالة"}
        actionText={"حسنا"}
        cancelText={"الرجوع"}
      ></ActionModal>
      <AlertModal
        open={alertModalOpen}
        setOpen={setAlertModalOpen}
        title={alertModalHandler.title}
        content={alertModalHandler.content}
        buttonText={alertModalHandler.buttonText}
      ></AlertModal>
      <>
        {loading ? (
          <CircularProgress></CircularProgress>
        ) : thesis ? (
          thesis["state"] == "مستمر" ? (
            <ListContainer>
              <ListItem>
                <ListItemKey>اسم الطالب</ListItemKey>
                <ListItemValue>
                  {student.user.arabicName} | {student.user.englishName}
                </ListItemValue>
              </ListItem>
              <Divider></Divider>
              <ListItem>
                <ListItemKey>القسم</ListItemKey>
                <ListItemValue>
                  {student.last_qualification.department
                    ? student.last_qualification.department?.display_name
                    : "غير محدد"}
                </ListItemValue>
              </ListItem>
              <Divider></Divider>
              <ListItem>
                <ListItemKey>الشعبة</ListItemKey>
                <ListItemValue>
                  {student.last_qualification.branch?.display_name
                    ? student.last_qualification.branch?.display_name
                    : "غير محدد"}
                </ListItemValue>
              </ListItem>
              <Divider></Divider>
              <ListItem>
                <ListItemKey>التخصص</ListItemKey>
                <ListItemValue>
                  {student.last_qualification?.specialization?.display_name
                    ? student.last_qualification?.specialization?.display_name
                    : "غير محدد"}
                </ListItemValue>
              </ListItem>
              <Divider></Divider>
              <ListItem>
                <ListItemKey>عنوان الرسالة بالعربي</ListItemKey>
                <ListItemValue>{thesis["arabic_title"]}</ListItemValue>
              </ListItem>
              <Divider></Divider>
              <ListItem>
                <ListItemKey>عنوان الرسالة بالإنجليزي </ListItemKey>
                <ListItemValue>{thesis["english_title"]}</ListItemValue>
              </ListItem>
              <Divider></Divider>
              <ListItem>
                <ListItemKey>دورة التسجيل</ListItemKey>
                <ListItemValue>{thesis["registration_period"]}</ListItemValue>
              </ListItem>
              <Divider></Divider>
              <ListItem>
                <ListItemKey>مجلس القسم</ListItemKey>
                <ListItemValue>{thesis["department_agree_date"]}</ListItemValue>
              </ListItem>
              <Divider></Divider>
              <ListItem>
                <ListItemKey>مجلس الكلية</ListItemKey>
                <ListItemValue>{thesis["college_agree_date"]}</ListItemValue>
              </ListItem>
              <Divider></Divider>
              <ListItem>
                <ListItemKey>مجلس الجامعة</ListItemKey>
                <ListItemValue>{thesis["university_agree_date"]}</ListItemValue>
              </ListItem>
              <Divider></Divider>
              <ListItem>
                <ListItemKey>ملاحظات</ListItemKey>
                <ListItemValue>
                  {thesis["notes"] ? thesis["notes"] : "لا يوجد"}
                </ListItemValue>
              </ListItem>
              <SaveButton onClick={() => setConfirmModalOpen(true)}>
                منح الرسالة
              </SaveButton>
            </ListContainer>
          ) : thesis["state"] == "ملغي" ? (
            <>عفوا لا يمكن منح هذه الرسالة لأنه تم الغاؤها بالفعل</>
          ) : (
            <>عفوا لا يمكن منح هذه الرسالة لأنه تم منحها</>
          )
        ) : (
          <>عفوا لا يوجد رسالة لهذا الطالب</>
        )}
      </>
    </>
  );
};
export default SingleThesisGrant;
