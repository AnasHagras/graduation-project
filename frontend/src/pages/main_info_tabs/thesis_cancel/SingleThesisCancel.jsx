import styled from "@emotion/styled";
import { Checkbox, CircularProgress } from "@mui/material";
import React, { useEffect } from "react";
import { useState } from "react";
import { studentMapping } from "../../../mappings/studentMapping";
import ActionModal from "../../../molecules/modals/ActionModal";
import AlertModal from "../../../molecules/modals/AlertModal";
import getStudent from "../../../services/students/getStudent";
import updateThesis from "../../../services/thesis/updateThesis";
import { useDispatch, useSelector } from "react-redux";
import { useOutletContext } from "react-router-dom";
import { useParams } from "react-router-dom";
import fetchStudents from "../../../services/students/listStudents";
import { listStudents } from "../../../features/students/studentSlice";
// import AlertModal from "../../../molecules/modals/AlertModal";
// normal div styling
const RedDiv = styled("div")(({ theme }) => ({
  border: `1px solid ${theme.palette.primary.light}`,
  // display: "flex",
  // flexDirection: "row",
  "&.bahaa": {
    border: "1px solid green",
  },
  "&.z": {
    border: "1px solid babyblue",
  },
  "&.bahaa:hover": {
    border: "1px solid blue",
  },
  "&:hover": {
    border: "1px solid red",
  },
  [theme.breakpoints.up("x")]: {
    backgroundColor: "red",
    width: "50%",
    // flexDirection: "column",
  },
}));

// styling onther component
const BorderRaduisRedDiv = styled(RedDiv)(({ theme }) => ({
  borderRadius: "50px",
}));

const ListContainer = styled("div")(({ theme }) => ({
  backgroundColor: "#bc2929",
  width: "80%",
  //   height: "600px",
  borderRadius: "10px",
  paddingTop: "10px",
  paddingBottom: "20px",
  paddingLeft: "6px",
  paddingRight: "6px",
  margin: "auto",
}));

const ListItem = styled("div")(({ theme }) => ({
  "&:hover": {
    backgroundColor: "#c58989",
  },
  transition: "background-color .20s linear ",
  transitionProperty: "background-color",
  transitionTimingFunction: "step-start(1,start)",
  borderRadius: "7px",
  padding: "10px",
  marginBottom: "5px",
  marginTop: "5px",
  color: "rgb(255, 255, 255)",
  display: "flex",
  justifyContent: "space-between",
  [theme.breakpoints.down("md")]: {
    flexDirection: "column",
    gap: "5px",
  },
}));

const Divider = styled("div")(({ theme }) => ({
  borderBottom: "1px solid #ffffff6b",
  marginLeft: "10px",
  marginRight: "10px",
}));

const ListItemKey = styled("div")(({ theme }) => ({
  fontWeight: "bold",
  borderRadius: "10px",
}));

const ListItemValue = styled("div")(({ theme }) => ({
  borderRadius: "10px",
  width: "50%",
  [theme.breakpoints.down("md")]: {
    width: "100%",
  },
}));

const SaveChangesButton = styled("div")(({ theme }) => ({
  borderRadius: "10px",
  color: "white",
  width: "fit-content",
  padding: "5px",
  paddingLeft: "70px",
  paddingRight: "70px",
  ":hover": {
    cursor: "pointer",
    backgroundColor: "#8c0d0d00",
  },
  backgroundColor: "#8c0d0d",
  border: "2px solid white",
  margin: "auto",
  marginTop: "25px",
  transition:
    "color .20s ease-in-out, background-color .20s ease-in-out, border-color .20s ease-in-out ",
}));

// call the endpoint to update the thesis and make it's canceled boolean to true

const SingleThesisCancel = () => {
  const [_, setTitle] = useOutletContext();
  useEffect(() => {
    setTitle("الغاء رسالة");
  }, []);
  const studentID = useParams()["id"];
  const dispatch = useDispatch();
  const [student, setStudent] = useState(null);
  const { token } = useSelector((state) => state.auth);
  const [confirmModalOpen, setConfirmModalOpen] = useState(false);
  const [alertModalOpen, setAlertModalOpen] = useState(false);
  const [loading, setLoading] = useState(true);
  const [thesis, setThesis] = useState(null);
  const [alertModalHandler, setAlertModalHandler] = useState({});
  const [reason, setReason] = useState(null);
  const [collegeDate, setCollegeDate] = useState();
  const [universityDate, setUniversityDate] = useState();
  const [departmentDate, setDepartmentDate] = useState();
  console.log("STUDENT ID : ", studentID);
  useEffect(() => {
    // TODO : replaced with just Get User
    fetchStudents({ token: token })
      .then((res) => {
        console.log("RES : ", res);
        const currentStudent = res.filter((student, idx) => idx == studentID);
        res.map((id, idx) => console.log("ID: ", idx));
        setStudent(currentStudent[0]);
        console.log("Logs : ", currentStudent[0]);
        setThesis(currentStudent[0].thesis);
        setLoading(false);
      })
      .catch((err) => {
        console.log("Error : ", err);
      });
    //  get user this update the states
  }, [confirmModalOpen]);
  const handleAlertModal = (alertModalHandler) => {
    setAlertModalHandler({
      title: alertModalHandler["title"],
      content: alertModalHandler["content"],
      buttonText: alertModalHandler["buttonText"],
    });
    setAlertModalOpen(true);
  };
  const handleAction = () => {
    if (!departmentDate || !collegeDate || !universityDate) {
      handleAlertModal({
        title: "فشل",
        content: "فشل منح هذه الرسالة",
        buttonText: "حسنا",
      });
      return;
    }
    updateThesis({
      token: token,
      thesisID: student.thesis.id,
      thesis: {
        state: "ملغي",
        cancel_reason: reason,
        department_cancel_date: departmentDate,
        college_cancel_date: collegeDate,
        university_cancel_date: universityDate,
      },
    })
      .then((thesis) => {
        console.log("thesis: ", thesis);
        handleAlertModal({
          title: "تم",
          content: "تم الغاء الرسالة بنجاح",
          buttonText: "حسنا",
        });
        dispatch(listStudents({ token: localStorage.getItem("token") }));
      })
      .catch((err) => {
        console.log("ERROR : ", err);
        handleAlertModal({
          title: "فشل",
          content: "فشل الغاء هذه الرسالة",
          buttonText: "حسنا",
        });
      });
  };

  return (
    <>
      <ActionModal
        open={confirmModalOpen}
        setOpen={setConfirmModalOpen}
        action={handleAction}
        reason={reason}
        setReason={setReason}
        dates={true}
        collegeDate={collegeDate}
        universityDate={universityDate}
        departmentDate={departmentDate}
        setCollegeDate={setCollegeDate}
        setUniversityDate={setUniversityDate}
        setDepartmentDate={setDepartmentDate}
        title={"تأكيد"}
        content={"هل انت متأكد من الغاء هذه الرسالة"}
        actionText={"حسنا"}
        cancelText={"الرجوع"}
      ></ActionModal>
      <AlertModal
        open={alertModalOpen}
        setOpen={setAlertModalOpen}
        title={alertModalHandler.title}
        content={alertModalHandler.content}
        buttonText={alertModalHandler.buttonText}
      ></AlertModal>
      <>
        {loading ? (
          <CircularProgress></CircularProgress>
        ) : thesis ? (
          thesis["state"] == "مستمر" ? (
            <ListContainer>
              <ListItem>
                <ListItemKey>اسم الطالب</ListItemKey>
                <ListItemValue>
                  {student.user.arabicName} | {student.user.englishName}
                </ListItemValue>
              </ListItem>
              <Divider></Divider>
              <ListItem>
                <ListItemKey>القسم</ListItemKey>
                <ListItemValue>
                  {student.last_qualification.department?.display_name
                    ? student.last_qualification.department?.display_name
                    : "غير محدد"}
                </ListItemValue>
              </ListItem>
              <Divider></Divider>
              <ListItem>
                <ListItemKey>الشعبة</ListItemKey>
                <ListItemValue>
                  {student.last_qualification.branch?.display_name
                    ? student.last_qualification.branch?.display_name
                    : "غير محدد"}
                </ListItemValue>
              </ListItem>
              <Divider></Divider>
              <ListItem>
                <ListItemKey>التخصص</ListItemKey>
                <ListItemValue>
                  {student.last_qualification.specialization?.display_name
                    ? student.last_qualification.specialization?.display_name
                    : "غير محدد"}
                </ListItemValue>
              </ListItem>
              <Divider></Divider>
              <ListItem>
                <ListItemKey>عنوان الرسالة بالعربي</ListItemKey>
                <ListItemValue>{thesis["arabic_title"]}</ListItemValue>
              </ListItem>
              <Divider></Divider>
              <ListItem>
                <ListItemKey>عنوان الرسالة بالإنجليزي </ListItemKey>
                <ListItemValue>{thesis["english_title"]}</ListItemValue>
              </ListItem>
              <Divider></Divider>
              <ListItem>
                <ListItemKey>دوررة التسجيل</ListItemKey>
                <ListItemValue>{thesis["registration_period"]}</ListItemValue>
              </ListItem>
              <Divider></Divider>
              {/* <ListItem>
                <ListItemKey>دورة التسجيل</ListItemKey>
                <ListItemValue>{thesis["creation_date"]}</ListItemValue>
              </ListItem>
              <Divider></Divider> */}
              {/* TODO : to be added after DR reply */}
              <ListItem>
                <ListItemKey>مجلس القسم</ListItemKey>
                <ListItemValue>{thesis["department_agree_date"]}</ListItemValue>
              </ListItem>
              <Divider></Divider>
              <ListItem>
                <ListItemKey>مجلس الكلية</ListItemKey>
                <ListItemValue>{thesis["college_agree_date"]}</ListItemValue>
              </ListItem>
              <Divider></Divider>
              <ListItem>
                <ListItemKey>مجلس الجامعة</ListItemKey>
                <ListItemValue>{thesis["university_agree_date"]}</ListItemValue>
              </ListItem>
              <Divider></Divider>
              <ListItem>
                <ListItemKey>ملاحظات</ListItemKey>
                <ListItemValue>
                  {thesis["notes"] ? thesis["notes"] : "لا يوجد"}
                </ListItemValue>
              </ListItem>
              <SaveChangesButton onClick={() => setConfirmModalOpen(true)}>
                الغاء الرسالة
              </SaveChangesButton>
            </ListContainer>
          ) : thesis["state"] == "ملغي" ? (
            <>عفوا لا يمكن الغاء هذه الرسالة لأنه تم الغاؤها بالفعل</>
          ) : (
            <>عفوا لا يمكن الغاء هذه الرسالة لأنه تم منحها</>
          )
        ) : (
          <>عفوا لا يوجد رسالة لهذا الطالب</>
        )}
      </>
    </>
  );
};

export default SingleThesisCancel;
