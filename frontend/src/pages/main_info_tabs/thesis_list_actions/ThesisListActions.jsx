import styled from "@emotion/styled";
import { CircularProgress } from "@mui/material";
import { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useOutletContext } from "react-router-dom";
import { listStudents } from "../../../features/students/studentSlice";
import ActionModal from "../../../molecules/modals/ActionModal";
import AlertModal from "../../../molecules/modals/AlertModal";
import getCanceledTheses from "../../../services/thesis/getCanceledTheses";
import getGrantedTheses from "../../../services/thesis/getGrantedTheses";
// import getStudentThesis from "../../../services/thesis/getStudentThesis";
import updateThesis from "../../../services/thesis/updateThesis";
import {
  ClearChangesButton,
  SaveChangesButton,
} from "../../users-data/add-user/AddUser";

const Container = styled("div")(({ theme }) => ({
  width: "100%",
  maxHeight: "100vh",
  overflowY: "auto",
  scrollCollapse: true,
  "&.MuiBox-root": {
    margin: "0px",
    width: "60%",
    border: "1px solid green",
  },
}));
const CancelStyledTable = styled("table")(({ theme }) => ({
  borderCollapse: "collapse",
  width: "99%",
  border: "1px solid #e4e5e5",
  "& :nth-of-type(even) tr": {
    backgroundColor: "#ffb3b370",
  },
  "&.MuiBox-root": {
    margin: "0px",
    width: "60%",
    border: "1px solid green",
  },
}));
const GrantStyledTable = styled(CancelStyledTable)(({ theme }) => ({
  "& :nth-of-type(even) tr": {
    backgroundColor: "#b3ffd5b8",
  },
}));
const GrantRow = styled(CancelStyledTable)(({ theme }) => ({
  "& :nth-of-type(even) tr": {
    backgroundColor: "#b3ffd5b8",
  },
}));

const CancelTableHead = styled("th")(({ theme }) => ({
  color: "#fff",
  backgroundColor: "#cd4e4e",
  padding: "10px",
  "&.Action": {
    textAlign: "center",
  },
}));
const GrantTableHead = styled(CancelTableHead)(({ theme }) => ({
  backgroundColor: "#2d9759",
}));
const TableData = styled("td")(({ theme }) => ({
  borderLeft: "1px solid #e4e5e5",
  padding: "10px",
}));

const CancelButton = styled(ClearChangesButton)(({ theme }) => ({
  textAlign: "center",
  width: "100%",
  color: "#dc3545",
  margin: "0",
  "&:hover": {
    backgroundColor: "#cd4e4e",
  },
}));
const GrantButton = styled(SaveChangesButton)(({ theme }) => ({
  textAlign: "center",
  width: "100%",
  color: "#2d9759",
  border: "2px solid #2d9759",
  margin: "0",
  "&:hover": {
    backgroundColor: "#2d9759",
  },
}));

const ThesisListActions = ({ type }) => {
  const { token } = useSelector((state) => state.auth);
  const [alertModalHandler, setAlertModalHandler] = useState({});
  const [alertModalOpen, setAlertModalOpen] = useState(false);
  const [confirmModalOpen, setConfirmModalOpen] = useState(false);
  const [selectedID, setSelectedID] = useState();
  const [_, setTitle] = useOutletContext();
  const dispatch = useDispatch();
  useEffect(() => {
    setTitle("الرسائل " + (type == "cancel" ? "الملغية" : "الممنوحة"));
  }, [type]);
  const handleAlertModal = (alertModalHandler) => {
    setAlertModalHandler({
      title: alertModalHandler["title"],
      content: alertModalHandler["content"],
      buttonText: alertModalHandler["buttonText"],
    });
    setAlertModalOpen(true);
  };
  const handleRevert = () => {
    // getStudentThesis(selectedID).then((res) => {
    //   console.log("HERE : ", selectedID);
    //   console.log("OK", res);
    // });
    updateThesis({
      token: token,
      thesisID: selectedID,
      thesis: {
        state: "مستمر",
      },
    })
      .then((thesis) => {
        handleAlertModal({
          title: "تم",
          content:
            type === "grant" ? "تم حذف المنح بنجاج" : "تم حذف الإلغاء بنجاح",
          buttonText: "حسنا",
        });
        dispatch(listStudents({ token: localStorage.getItem("token") }));
      })
      .catch((err) =>
        handleAlertModal({
          title: "فشل",
          content:
            type === "grant"
              ? "فشل الغاء منح هذه الرسالة"
              : "فشل حذف الغاء هذه الرسالة",
          buttonText: "حسنا",
        })
      );
  };
  const [objects, setObjects] = useState([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setObjects([]);
  }, [type]);
  useEffect(() => {
    if (type === "grant") {
      setLoading(true);
      getGrantedTheses({
        token: token,
      })
        .then((objects) => {
          setLoading(false);
          console.log("OBJECTS FROM LIST : ", objects.results);
          setObjects(objects.results);
          // setLoading(false);
        })
        .catch((err) => {
          setLoading(false);
          alert(err);
        });
    } else {
      setLoading(true);
      getCanceledTheses({
        token: token,
      })
        .then((objects) => {
          console.log("OBJECTS FROM LIST : ", objects.results);
          setObjects(objects.results);
          setLoading(false);
        })
        .catch((err) => {
          setLoading(false);
          alert(err);
        });
    }
  }, [alertModalOpen, type]);
  return (
    <>
      <ActionModal
        open={confirmModalOpen}
        setOpen={setConfirmModalOpen}
        action={handleRevert}
        title={"تأكيد"}
        content={
          type === "grant"
            ? "هل انت متأكد من حذف منح هذه الرسالة"
            : "هل انت متأكد من حذف الغاء هذه الرسالة"
        }
        actionText={"حسنا"}
        cancelText={"الرجوع"}
      ></ActionModal>
      <AlertModal
        open={alertModalOpen}
        setOpen={setAlertModalOpen}
        title={alertModalHandler.title}
        content={alertModalHandler.content}
        buttonText={alertModalHandler.buttonText}
      ></AlertModal>
      {loading ? (
        <CircularProgress></CircularProgress>
      ) : objects.length === 0 ? (
        <>لا يوجد عناصر هنا</>
      ) : (
        <>
          <Container>
            {/* condition to choose  green style to grant and the red style to cancel  */}
            {type === "grant" ? (
              <GrantStyledTable>
                <thead>
                  <tr>
                    <GrantTableHead>اسم الطالب</GrantTableHead>
                    <GrantTableHead>موافقة مجلس الجامعة</GrantTableHead>
                    <GrantTableHead>موافقة مجلس الكلية</GrantTableHead>
                    <GrantTableHead>موافقة مجلس القسم</GrantTableHead>
                    <GrantTableHead>ملاحظات</GrantTableHead>
                    <GrantTableHead className="Action">حذف</GrantTableHead>
                  </tr>
                </thead>
                {objects.map((obj, i) => {
                  return (
                    <Fragment key={i}>
                      <tbody>
                        <tr>
                          <TableData>{obj.student.user.arabicName}</TableData>
                          <TableData>{obj.university_agree_date}</TableData>
                          <TableData>{obj.college_agree_date}</TableData>
                          <TableData>{obj.department_agree_date}</TableData>
                          <TableData>
                            {obj.notes
                              ? obj.notes
                              : obj.reason
                              ? obj.reason
                              : "______"}
                          </TableData>
                          <TableData>
                            <GrantButton
                              onClick={() => {
                                setSelectedID(obj.id);
                                setConfirmModalOpen(true);
                              }}
                            >
                              حذف المنح
                            </GrantButton>
                          </TableData>
                        </tr>
                      </tbody>
                    </Fragment>
                  );
                })}
              </GrantStyledTable>
            ) : (
              <CancelStyledTable>
                <thead>
                  <tr>
                    <CancelTableHead>اسم الطالب</CancelTableHead>
                    <CancelTableHead>موافقة مجلس الجامعة</CancelTableHead>
                    <CancelTableHead>موافقة مجلس الكلية</CancelTableHead>
                    <CancelTableHead>موافقة مجلس القسم</CancelTableHead>
                    <CancelTableHead>ملاحظات</CancelTableHead>
                    <CancelTableHead className="Action">حذف</CancelTableHead>
                  </tr>
                </thead>
                {objects.map((obj, i) => {
                  return (
                    <Fragment key={i}>
                      <tbody>
                        <tr>
                          <TableData>{obj.student.user.arabicName}</TableData>
                          <TableData>{obj.university_agree_date}</TableData>
                          <TableData>{obj.college_agree_date}</TableData>
                          <TableData>{obj.department_agree_date}</TableData>
                          <TableData>
                            {obj.notes
                              ? obj.notes
                              : obj.reason
                              ? obj.reason
                              : "______"}
                          </TableData>
                          <TableData>
                            <CancelButton
                              onClick={() => {
                                setSelectedID(obj.id);
                                setConfirmModalOpen(true);
                              }}
                            >
                              حذف الإلغاء
                            </CancelButton>
                          </TableData>
                        </tr>
                      </tbody>
                    </Fragment>
                  );
                })}
              </CancelStyledTable>
            )}
          </Container>
        </>
      )}
    </>
  );
};

export default ThesisListActions;
