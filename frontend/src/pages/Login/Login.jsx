import { Grid, styled, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { Box } from "@mui/system";
import React from "react";
import CenteredContainer from "../../atoms/CenteredContainer";
import LoginForm from "../../organisms/LoginForm";

function Login() {
  return (
    <CenteredContainer>
      <LoginForm></LoginForm>
    </CenteredContainer>
  );
}

export default Login;
