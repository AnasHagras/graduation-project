import Login from "./Login/Login";
import TestServices from "./TestServices";
import Students from "./students/Students";
import StudentsTable from "./students-table/StudentsTable";
import SupervisorsTable from "./supervisors-table/SupervisorsTable";
import Supervisor from "./supervisors/Supervisor";
import StudentRegistraionForm from "./Student-registration-form/StudentRegistrationForm";
import Filters from "./Search/components/Filters";
import MajorChangePage from "./Major thesis change/MajorChangePage";
export {
  Login,
  TestServices,
  Students,
  StudentsTable,
  SupervisorsTable,
  Supervisor, StudentRegistraionForm,
  Filters,
  MajorChangePage,
};
