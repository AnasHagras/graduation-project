import * as React from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import { Link } from "react-router-dom";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import CopyRights from "../Login/components/CopyRights";
import { useContext } from "react";
import AuthContext from "../../context/AuthContext";
import { Navigate } from "react-router-dom";
function Signup() {
  const { registerUser, error, setError } = useContext(AuthContext);
  const theme = createTheme({
    typography: {
      error: {
        color: "red",
      },
      sucess: {
        color: "green",
      },
    },
  });
  return (
    <>
      {localStorage.getItem("authToken") === null ? (
        <ThemeProvider theme={theme}>
          <Container component="main" maxWidth="xs">
            <CssBaseline />
            <Box
              sx={{
                marginTop: 8,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Sign up
              </Typography>
              <Box
                component="form"
                noValidate
                sx={{ mt: 3 }}
                onSubmit={registerUser}
              >
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      autoComplete="given-name"
                      name="first_name"
                      required
                      fullWidth
                      id="first_name"
                      label="First Name"
                      autoFocus
                      error={
                        error !== null && error.hasOwnProperty("first_name")
                      }
                      helperText={
                        error !== null && error.hasOwnProperty("first_name")
                          ? error["first_name"]
                          : ""
                      }
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      required
                      fullWidth
                      id="last_name"
                      label="Last Name"
                      name="last_name"
                      autoComplete="family-name"
                      error={
                        error !== null && error.hasOwnProperty("last_name")
                      }
                      helperText={
                        error !== null && error.hasOwnProperty("last_name")
                          ? error["last_name"]
                          : ""
                      }
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      required
                      fullWidth
                      id="email"
                      label="Email Address"
                      name="email"
                      autoComplete="email"
                      error={error !== null && error.hasOwnProperty("email")}
                      helperText={
                        error !== null && error.hasOwnProperty("email")
                          ? error["email"]
                          : ""
                      }
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      required
                      fullWidth
                      name="password"
                      label="Password"
                      type="password"
                      id="password"
                      autoComplete="new-password"
                      error={error !== null && error.hasOwnProperty("password")}
                      helperText={
                        error !== null && error.hasOwnProperty("password")
                          ? error["password"]
                          : ""
                      }
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      required
                      fullWidth
                      name="password2"
                      label="Confirm Password"
                      type="password"
                      id="password2"
                      autoComplete="new-password"
                      error={
                        error !== null && error.hasOwnProperty("password2")
                      }
                      helperText={
                        error !== null && error.hasOwnProperty("password2")
                          ? error["password2"]
                          : ""
                      }
                    />
                  </Grid>
                </Grid>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2 }}
                >
                  Sign Up
                </Button>
                <Grid container justifyContent="center" sx={{ mb: 2 }}>
                  <Grid item>
                    <Link
                      to="/signin"
                      variant="body2"
                      onClick={() => setError(null)}
                    >
                      Already have an account? Sign in
                    </Link>
                  </Grid>
                </Grid>
              </Box>
            </Box>
            <CopyRights sx={{ mt: 5 }} />
          </Container>
        </ThemeProvider>
      ) : (
        <Navigate to="/home" />
      )}
    </>
  );
}

export default Signup;
