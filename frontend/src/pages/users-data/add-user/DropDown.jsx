import * as React from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import styled from '@emotion/styled';

const userType = ["Admin" , "مدخل البيانات" , "مدير الدراسات" , "قسم النظم" ] ; 




console.log()

const NewFormControl = styled(FormControl)(({ theme }) => ({
    m : 1,
    margin : 0 ,
    width: '60%' ,
    [theme.breakpoints.down("lg")]: {
      minWidth: "100%",
    },
    
}));


export default function SelectAutoWidth() {
  const [userTypes, setUserTypes] = React.useState('');

  const handleChange = (SelectChangeEvent) => {
    setUserTypes(SelectChangeEvent.target.value);
  };

  return (
    <div>
      <FormControl  >
        <InputLabel id="demo-simple-select-autowidth-label">نوع المستخدم</InputLabel>
        <Select
          labelId="demo-simple-select-autowidth-label"
          id="demo-simple-select-autowidth"
          MenuProps={{ style: { width: '100%' } }}
          value={userTypes}
          onChange={handleChange}
          label="نوع المستخدم"
        >
        {userType.map ((user , i)=>{
            return (
                <MenuItem value={i}>{user}</MenuItem>
            )
        })} 
        </Select>
      </FormControl>
    </div>
  );
}
