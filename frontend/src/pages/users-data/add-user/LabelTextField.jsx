import { TextField } from "@mui/material";

const LabelTextField = (props) => {
  return (
    <>
      <TextField
        required
        id="outlined-required"
        label={props.labelName}
        type={props.type}
      />
    </>
  );
};
export default LabelTextField;
