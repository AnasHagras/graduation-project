import * as React from "react";
import Box from "@mui/material/Box";

import styled from "@emotion/styled";
import { useState } from "react";
import AlertModal from "../../../molecules/modals/AlertModal";
import FormInput from "../../../molecules/form/FormInput/FormInput";
import SelectMenu from "../../../molecules/form/SelectMenu/SelectMenu";
import { useSelector } from "react-redux";
import addSupervisor from "../../../services/supervisors/addSupervisor";
import { useOutletContext } from "react-router-dom";
const BoxLabel = styled("div")(({ theme }) => ({
  backgroundColor: "#1ab2f11f	",
  borderRadius: "10px",
  width: "80%",
  padding: "20px",
  margin: "auto",
}));
const BoxLabelALL = styled("div")(({ theme }) => ({
  display: "flex",
  // width: "100%",
  // border: "1px solid red",
  // justifyContent: "space-between",
  alignItems: "center",
  flexWrap: "wrap",
  // width: "100%",
  padding: "5px 20px",
  [theme.breakpoints.down("lg")]: {
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    gap: "5px",
  },
}));

const BoxLabelRight = styled("div")(({ theme }) => ({
  fontWeight: "bold",
  padding: "15px",
  width: "160px",
  marginBottom: "0",
}));

const NewBox = styled(Box)(({ theme }) => ({
  "& .MuiTextField-root": { m: 1, width: "60%" },
  [theme.breakpoints.down("lg")]: {
    "& .MuiTextField-root": { m: 1, width: "100%" },
  },
}));

const ButtonsBox = styled("div")(({ theme }) => ({
  marginTop: "20px",
  display: "flex",
  padding: "0px 26px",

  justifyContent: "center",
  gap: "15px",
  [theme.breakpoints.down("lg")]: {
    paddingLeft: "12px",
    paddingRight: "27px",
    // marginTop: "30px",
    flexDirection: "column",
  },
}));
export const SaveChangesButton = styled("div")(({ theme }) => ({
  backgroundColor: "transparent",
  fontWeight: "bold",
  width: "50%",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  // margin: "auto",
  marginTop: "15px",
  marginBottom: "15px",
  color: "#198754",
  padding: "13px 5px",
  borderRadius: "8px",
  border: "2px solid #198754",
  transition:
    "color .20s ease-in-out, background-color .20s ease-in-out, border-color .20s ease-in-out ",
  "&:hover": {
    backgroundColor: "#198754",
    color: "#FFF",
    cursor: "pointer",
  },
  [theme.breakpoints.down("lg")]: {
    width: "100%",
    // margin: "0px",
    // paddingLeft: "0px",
    // paddingRight: "0px",
    // gap: "5px",
    margin: "auto",
  },
}));
export const ClearChangesButton = styled(SaveChangesButton)(({ theme }) => ({
  color: "#dc3545",
  borderColor: "#dc3545",
  "&:hover": {
    backgroundColor: "#dc3545",
    color: "#FFF",
    cursor: "pointer",
  },
}));

const degreeChoices = [
  { name: "معيد", display_name: "معيد" },
  { name: "باحث دراسات عليا", display_name: "باحث دراسات عليا" },
  { name: "مدرس", display_name: "مدرس" },
  { name: "مدرس مساعد", display_name: "مدرس مساعد" },
  { name: "مدرس متفرغ", display_name: "مدرس متفرغ" },
  { name: "استاذ", display_name: "استاذ" },
  { name: "استاذ مساعد", display_name: "استاذ مساعد" },
  { name: "استاذ مساعد متفرغ", display_name: "استاذ مساعد متفرغ" },
  { name: "استاذ متفرغ", display_name: "استاذ متفرغ" },
];

const AddSupervisor = () => {
  const [_, setTitle] = useOutletContext();
  React.useEffect(() => {
    setTitle("تسجيل مشرف رسالة");
  }, []);
  const [data, setData] = useState({
    email: "",
    password: "",
    passwordConfirm: "",
    arabicName: "",
    englishName: "",
    gender: null,
    degree: null,
    faculty: null,
    department: null,
    organization: "",
  });

  const { organization, initialLoading } = useSelector((state) => state.enums);
  console.log("TEST : ", organization);
  console.log("FACULTY : ", data.faculty);

  const [alertModalHandler, setAlertModalHandler] = useState({});
  const [alertModalOpen, setAlertModalOpen] = useState(false);
  const handleAlertModal = (alertModalHandler) => {
    setAlertModalHandler({
      title: alertModalHandler["title"],
      content: alertModalHandler["content"],
      buttonText: alertModalHandler["buttonText"],
    });
    setAlertModalOpen(true);
  };

  const clearData = () => {
    setData({
      email: "",
      password: "",
      passwordConfirm: "",
      arabicName: "",
      englishName: "",
      gender: null,
      degree: null,
      faculty: null,
      department: null,
      organization: "",
    });
  };

  const handleSubmit = () => {
    addSupervisor({ data: data, token: localStorage.getItem("token") })
      .then((res) => {
        handleAlertModal({
          title: "تم",
          content: "تم تسجيل المشرف بنجاح",
          buttonText: "حسنا",
        });
        clearData();
      })
      .catch((err) => {
        console.log("err: ", err);
        handleAlertModal({
          title: "فشل",
          content: err.message,
          buttonText: "حسنا",
        });
      });
  };
  return (
    <>
      <AlertModal
        open={alertModalOpen}
        setOpen={setAlertModalOpen}
        title={alertModalHandler.title}
        content={alertModalHandler.content}
        buttonText={alertModalHandler.buttonText}
      ></AlertModal>
      <NewBox component="form" noValidate autoComplete="off">
        <BoxLabel>
          <BoxLabelALL>
            <BoxLabelRight> البريد الإلكتروني :</BoxLabelRight>
            <FormInput
              style={{ width: "100%" }}
              value={data.email}
              setValue={(email) => setData({ ...data, email: email })}
              // label="البريد الإلكتروني "
            />
          </BoxLabelALL>
          <BoxLabelALL>
            <BoxLabelRight> الأسم بالعربي:</BoxLabelRight>
            <FormInput
              style={{ width: "100%" }}
              value={data.arabicName}
              setValue={(arabicName) =>
                setData({ ...data, arabicName: arabicName })
              }
              // label="البريد الإلكتروني "
            />
          </BoxLabelALL>
          <BoxLabelALL>
            <BoxLabelRight> الأسم بالإنجليزي:</BoxLabelRight>
            <FormInput
              style={{ width: "100%" }}
              value={data.englishName}
              setValue={(englishName) =>
                setData({ ...data, englishName: englishName })
              }
              // label="البريد الإلكتروني "
            />
          </BoxLabelALL>
          <BoxLabelALL>
            <BoxLabelRight> الجنس :</BoxLabelRight>
            <SelectMenu
              // label="الدرجة"
              choices={[
                { name: "ذكر", display_name: "ذكر" },
                { name: "أنثي", display_name: "أنثي" },
              ]}
              value={data.gender}
              setValue={(gender) => setData({ ...data, gender: gender })}
            />
          </BoxLabelALL>
          <BoxLabelALL>
            <BoxLabelRight> المنظمة : </BoxLabelRight>
            <FormInput
              style={{ width: "100%" }}
              value={data.organization}
              setValue={(organization) =>
                setData({ ...data, organization: organization })
              }
              // label="البريد الإلكتروني "
            />
          </BoxLabelALL>
          <BoxLabelALL>
            <BoxLabelRight> الكلية :</BoxLabelRight>
            <SelectMenu
              // label="الدرجة"
              type={"org"}
              choices={initialLoading ? [] : organization?.faculties}
              value={data.faculty}
              setValue={(faculty) => setData({ ...data, faculty: faculty })}
            />
          </BoxLabelALL>
          <BoxLabelALL>
            <BoxLabelRight> القسم :</BoxLabelRight>
            <SelectMenu
              type={"org"}
              // label="الدرجة"
              choices={
                initialLoading
                  ? []
                  : organization?.departments[data.faculty?.name]
              }
              value={data.department}
              setValue={(department) =>
                setData({ ...data, department: department })
              }
            />
          </BoxLabelALL>
          <BoxLabelALL>
            <BoxLabelRight> الدرجة :</BoxLabelRight>
            <SelectMenu
              // label="الدرجة"
              choices={degreeChoices}
              value={data.degree}
              setValue={(degree) => setData({ ...data, degree: degree })}
            />
          </BoxLabelALL>
          <BoxLabelALL>
            <BoxLabelRight> كلمة المرور :</BoxLabelRight>
            <FormInput
              type={"password"}
              style={{ width: "100%" }}
              value={data.password}
              setValue={(password) => setData({ ...data, password: password })}
              // label="البريد الإلكتروني "
            />
          </BoxLabelALL>
          <BoxLabelALL>
            <BoxLabelRight> تأكيد كلمة المرور :</BoxLabelRight>
            <FormInput
              type={"password"}
              style={{ width: "100%" }}
              value={data.passwordConfirm}
              setValue={(passwordConfirm) =>
                setData({ ...data, passwordConfirm: passwordConfirm })
              }
              // label="البريد الإلكتروني "
            />
          </BoxLabelALL>
          <ButtonsBox>
            <SaveChangesButton onClick={handleSubmit}>
              تسجيل المشرف
            </SaveChangesButton>
            <ClearChangesButton onClick={clearData}>
              تفريغ الحقول
            </ClearChangesButton>
          </ButtonsBox>
        </BoxLabel>
      </NewBox>
    </>
  );
};

export default AddSupervisor;
