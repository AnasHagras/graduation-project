import styled from "@emotion/styled";
import React, { useEffect, useState } from "react";
import { Navigate, useOutletContext, useParams } from "react-router-dom";
import AlertModal from "../../../molecules/modals/AlertModal";
import { useSelector } from "react-redux";
import getSupervision from "../../../services/supervision/getSupervision";

const Container = styled("div")(({ theme }) => ({
  width: "90%",
  border: "solid 1px black",
  position: "relative",
  fontWeight: "bold",
  padding: "20px",
}));

const CenterDiv = styled("div")(({ theme }) => ({
  width: "fit-content",
  margin: "auto",
  marginTop: "30px",
  fontSize: "20px",
}));

const TwoLines = styled("div")(({ theme }) => ({
  borderBottom: "solid 2px black",
  borderTop: "solid 2px black",
  margin: "20px 0 ",
}));
const Devider = styled("div")(({ theme }) => ({
  borderBottom: "solid 2px black",
  borderTop: "solid 2px black",
  width: "30%",
  margin: "auto",
  marginTop: "20px",
  marginBottom: "20px",
}));
const InfoDiv = styled("div")(({ theme }) => ({
  display: "flex",
  justifyContent: "space-between",
  width: "90%",
  margin: "auto",
  margin: "20px",
}));
const AnswerDiv = styled("div")(({ theme }) => ({
  width: "70%",
}));

const StyledTable = styled("table")(({ theme }) => ({
  textAlign: "center",
  width: "99%",
  border: "2px solid black",
}));
const TableHeader = styled("th")(({ theme }) => ({
  border: "1px solid black",
  borderBottom: "2px solid black",
  backgroundColor: "#e4e5e5",
  padding: "7px",
}));
const TableData = styled("td")(({ theme }) => ({
  border: "1px solid black",
  padding: "7px",
}));
const ResearcherDocuments = () => {
  const id = useParams().id;
  const { students } = useSelector((state) => state.students);
  const student = students[id];
  const [_, setTitle] = useOutletContext();
  const [filteredSupervisors, setFilteredSupervisors] = useState([]);
  const [filteredRole, setFilteredRole] = useState([]);
  const { supervisors } = useSelector((state) => state.supervisors);
  const boolean = (check) => {
    return !check ? "x" : "\u2713";
  };
  console.log(boolean(true));
  console.log(student);
  useEffect(() => {
    setTitle("مستندات باحث");
    getSupervision({ thesis: student?.thesis?.id }).then((res) => {
      console.log("res", res);

      const filteredSupervisors = res?.map((item) =>
        supervisors.find((supervisor) => supervisor.user.id === item.id)
      );

      const filteredRole = res?.map((item) => item.supervising_type);
      Promise.all([filteredSupervisors, filteredRole]).then(
        ([supervisorsArray, indexesArray]) => {
          setFilteredSupervisors(supervisorsArray);
          setFilteredRole(indexesArray);
          console.log("supervisorsArray", supervisorsArray);
          console.log("indexesArray", indexesArray);
        }
      );
    });
  }, [student]);

  if (id == null) {
    return (
      <Navigate to="/main_info/" state={{ modalOpen: true }} replace></Navigate>
    );
  }

  return (
    <>
      <Container>
        <div>جامعة الازهر</div>
        <div>الدراسات العليا</div>
        <CenterDiv>بيانات مستند باحث </CenterDiv>
        <TwoLines />
        <InfoDiv>
          <div> الاسم :</div>
          <AnswerDiv>
            {student?.user?.arabicName
              ? student?.user?.arabicName
              : "ـــــــــــــــــ"}
          </AnswerDiv>
        </InfoDiv>
        <InfoDiv>
          <div> الرقم القومي :</div>
          <AnswerDiv>
            {student?.user?.nationalID
              ? student?.user?.nationalID
              : "ـــــــــــــــــ"}
          </AnswerDiv>
        </InfoDiv>
        <InfoDiv>
          <div> الدرجة :</div>
          <AnswerDiv>
            {student?.thesis?.degree
              ? student?.thesis?.degree
              : "ـــــــــــــــــ"}
          </AnswerDiv>
        </InfoDiv>
        <InfoDiv>
          <div> القسم :</div>
          <AnswerDiv>
            {student?.last_qualification?.department?.display_name
              ? student?.last_qualification?.department?.display_name
              : "ـــــــــــــــــ"}
          </AnswerDiv>
        </InfoDiv>
        <InfoDiv>
          <div> الشعبة :</div>
          <AnswerDiv>
            {student?.last_qualification?.branch?.display_name
              ? student?.last_qualification?.branch?.display_name
              : "ـــــــــــــــــ"}
          </AnswerDiv>
        </InfoDiv>
        <InfoDiv>
          <div> عنوان الرسالة :</div>
          <AnswerDiv>
            {student?.thesis?.arabic_title
              ? student?.thesis?.arabic_title
              : "ـــــــــــــــــ"}
          </AnswerDiv>
        </InfoDiv>

        <CenterDiv>المشرفين</CenterDiv>
        <Devider />

        <StyledTable>
          <thead>
            <TableHeader>الرقم</TableHeader>
            <TableHeader>المشرف</TableHeader>
            <TableHeader>نوع الاشراف</TableHeader>
          </thead>
          <tbody>
            {filteredSupervisors?.map((supervisor, index) => {
              return (
                <tr>
                  <TableData>{index + 1}</TableData>
                  <TableData
                    style={{
                      width: "50%",
                      textAlign: "center",
                      border: "1px solid black",
                    }}
                  >
                    {supervisor?.user?.arabicName
                      ? supervisor?.user?.arabicName
                      : "ـــــــــــــــــ"}
                  </TableData>
                  <TableData>
                    {filteredRole[index]
                      ? filteredRole[index]
                      : "ـــــــــــــــــ"}
                  </TableData>
                </tr>
              );
            })}
          </tbody>
        </StyledTable>
        <CenterDiv>مستندات التقديم</CenterDiv>
        <Devider />
        <StyledTable>
          <tbody>
            <tr>
              <TableHeader>شهادة البكالريوس</TableHeader>
              <TableData>
                {boolean(student?.attachments?.bcsCertification)}
              </TableData>
              <TableHeader>شهادة الدبلومة </TableHeader>
              <TableData>
                {boolean(student?.attachments?.deplomaCertification)}
              </TableData>
            </tr>
            <tr>
              <TableHeader>رقم قومي</TableHeader>
              <TableData>{boolean(student?.attachments?.nationalID)}</TableData>
              <TableHeader> شهادة ماجستير</TableHeader>
              <TableData>
                {boolean(student?.attachments?.masterCertification)}
              </TableData>
            </tr>
            <tr>
              <TableHeader> اقرار بعدم العمل </TableHeader>
              <TableData>
                {boolean(student?.attachments?.jobAcceptance)}
              </TableData>
              <TableHeader> اخر ايصال سداد الرسوم</TableHeader>
              <TableData>{boolean(student?.attachments?.lastPill)}</TableData>
            </tr>
            <tr>
              <TableHeader> اقرر موافقة الجيش </TableHeader>
              <TableData>
                {boolean(student?.attachments?.masterCertification)}
              </TableData>
              <TableHeader> تعهد بعدم القيد بأي جامعة أخري</TableHeader>
              <TableData>{boolean(student?.attachments?.stdPromise)}</TableData>
            </tr>
          </tbody>
        </StyledTable>
      </Container>
    </>
  );
};

export default ResearcherDocuments;
