import styled from "@emotion/styled";
import React, { useEffect, useState } from "react";
import { Navigate, useOutletContext, useParams } from "react-router-dom";
import CancelReport from "./Reports/CancelReport";
import ChangeTitleReport from "./Reports/ChangeTitleReport";
import FormationRulingReport from "./Reports/FormationRulingReport";
import ModifySupervisorsReport from "./Reports/ModifySupervisorsReport";
import RequestExtendReport from "./Reports/RequestExtendReport";
import StudentsDetails from "./StudentsDetails";

const RadioContainerDiv = styled("div")(({ theme }) => ({
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  fontWeight: "bold",
  width: "90%",
  margin: "auto",
  padding: "20px",
  // backgroundColor: "#9f65a51f",
  borderRadius: "20px",
}));
const RadioDiv = styled("label")(({ theme }) => ({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
}));
const ModifyLabel = styled("label")(({ theme }) => ({
  marginLeft: "20px",
}));

const RequestApprovals = () => {
  const [report, setReport] = useState(null);
  const [title, setTitle] = useOutletContext();
  useEffect(() => {
    setTitle("طلب موافقة");
  }, []);
  const id = useParams()["id"];
  if (id == null) {
    return (
      <Navigate to="/main_info/" state={{ modalOpen: true }} replace></Navigate>
    );
  }
  const handleRadioChange = (event) => {
    setReport(event.target.value);
  };

  let reportComponent;
  if (report === "changeTitle") {
    reportComponent = <ChangeTitleReport StudentsDetails={StudentsDetails} />;
  } else if (report === "cancel") {
    reportComponent = <CancelReport StudentsDetails={StudentsDetails} />;
  } else if (report === "modifySupervisors") {
    reportComponent = (
      <ModifySupervisorsReport StudentsDetails={StudentsDetails} />
    );
  } else if (report === "formationRuling") {
    reportComponent = (
      <FormationRulingReport StudentsDetails={StudentsDetails} />
    );
  } else if (report === "requestExtend") {
    reportComponent = <RequestExtendReport StudentsDetails={StudentsDetails} />;
  }

  const Print = () => {
    //console.log('print');
    let printContents = document.getElementById("printablediv").innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    window.location.reload(false);
  };

  return (
    <>
      <RadioContainerDiv>
        <RadioDiv>
          <ModifyLabel for="changeTitle">تغيير عنوان </ModifyLabel>
          <input
            type="radio"
            id="changeTitle"
            value="changeTitle"
            name="RequestApprovals"
            onChange={handleRadioChange}
          />
        </RadioDiv>
        <RadioDiv>
          <ModifyLabel for="cancel"> إلغاء </ModifyLabel>
          <input
            type="radio"
            id="cancel"
            value="cancel"
            name="RequestApprovals"
            onChange={handleRadioChange}
          />
        </RadioDiv>
        <RadioDiv>
          <ModifyLabel for="modifySupervisors">
            {" "}
            تعديل لجنة الإشراف{" "}
          </ModifyLabel>
          <input
            type="radio"
            id="modifySupervisors"
            value="modifySupervisors"
            name="RequestApprovals"
            onChange={handleRadioChange}
          />
        </RadioDiv>
        <RadioDiv>
          <ModifyLabel for="formationRuling"> تشكيل لجنة حكم </ModifyLabel>
          <input
            type="radio"
            id="formationRuling"
            value="formationRuling"
            name="RequestApprovals"
            onChange={handleRadioChange}
          />
        </RadioDiv>
        <RadioDiv>
          <ModifyLabel for="requestExtend"> طلب مد </ModifyLabel>
          <input
            type="radio"
            id="requestExtend"
            value="requestExtend"
            name="RequestApprovals"
            onChange={handleRadioChange}
          />
        </RadioDiv>
      </RadioContainerDiv>
      {reportComponent}
    </>
  );
};

export default RequestApprovals;
