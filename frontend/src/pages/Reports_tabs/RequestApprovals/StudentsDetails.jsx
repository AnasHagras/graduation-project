const StudentsDetails = {

    name: "بهاء محمد فتحي داغر ",
    nationality : "سوداني",
    college: "الهندسة",
    department: "هندسة الحاسبات والنظم",
    specification: "نظم معلومات",
    degree: "ماجستير",
    thesisArabicName: "تصميم وتطوير نظام إدارة الرسائل العلمية",
    thesisEnglishName: "Design and development of a scientific message management system",
    RegistrationDate: "2020/10/10",
    collegeCouncil: "2018/8/10",
    universityCouncil: "2018/9/1",
    expiration_date_Legal_period : "2020/10/10",
    end_date_last_extension : "2020/10/10",
    mainSupervisor: " محمد عبد الله",
    mainSupervisorDegree: "أستاذ ",
    mainSupervisorPlace: "هندسة القاهرة",
    coSupervisor: "  ابراهيم  العادلي ",
    coSupervisorDegree: "أستاذ مساعد",
    coSupervisorPlace: "هندسة الازهر",


}
export default StudentsDetails