import React, { useState } from "react";
import styled from "@emotion/styled";
import Footer from "./Footer";
import { Container } from "./Footer";
import { LineDiv } from "./Footer";
import { SignatureDiv } from "./Footer";
import { RightDiv } from "./Footer";
import { LeftDiv } from "./Footer";
import { CenteredDiv } from "./Footer";
import { InputDate } from "./Footer";
import { PrintButton } from "./Footer";
import { BoldDiv } from "./Footer";
import { MessageSubject } from "./Footer";
import { DivThesis } from "./Footer";
import { MarginDiv } from "./Footer";
import { ButtonDev } from "./Footer";
import { StyledInput } from "./Footer";
import { StatementSubmittedDiv } from "./Footer";
import AlertModal from "../../../../molecules/modals/AlertModal";
import DateInput from "../../../../atoms/form/dateInput/DateInput";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
const CancelReport = ({ StudentsDetails }) => {
  const [showReport, setShowReport] = useState(false);
  const [title, setTitle] = useState("");
  const { students } = useSelector((state) => state.students);
  const id = useParams().id;
  const student = students ? students[id] : {};

  const handleInputChange = (event) => {
    setTitle(event.target.value);
  };

  function convertDateFormat(dateString) {
    if (!dateString) return dateString;
    var parts = dateString?.split("-");
    var day = parts[0];
    var month = parts[1];
    var year = parts[2];

    // Concatenate the parts in the desired format
    var convertedDate = year + "-" + month + "-" + day;

    return convertedDate;
  }

  const Print = () => {
    //console.log('print');
    let printContents = document.getElementById("printablediv").innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    window.location.reload(false);
  };
  const [collegeDate, setcollegeDate] = useState();
  const [departmentDate, setdepartmentDate] = useState();
  const [value, setValue] = useState("");
  const [finalCollegeDate, setFinalCollegeDate] = useState();
  const [finalDepartmentDate, setFinalDepartmentDate] = useState();
  const [open, setOpen] = useState(false);
  const [content, setContent] = useState("");
  const [tit, setTit] = useState("");

  const show = () => {
    if (title !== "" && collegeDate && departmentDate) {
      setShowReport(true);
      setValue(title);
      setFinalCollegeDate(collegeDate);
      setFinalDepartmentDate(departmentDate);
    } else {
      setOpen(true);
      setTit("خطأ");
      setContent("من فضلك أدخل جميع البيانات");
    }
  };

  return (
    <>
      <AlertModal
        open={open}
        setOpen={setOpen}
        title={title}
        content={content}
        buttonText={"حسناً"}
      />
      <StatementSubmittedDiv>
        <div> أسباب الفصل</div>
        <StyledInput type="text" onChange={handleInputChange} />
      </StatementSubmittedDiv>
      <SignatureDiv className="margin">
        {/* <CenteredDiv> */}
        {/* <BoldDiv>مجلس الكلية</BoldDiv> */}
        <DateInput
          date={convertDateFormat(collegeDate)}
          setDate={(e) => setcollegeDate(e)}
          apply={true}
          label={"موافقة مجلس القسم على تعديل الرسالة"}
          // disabled={true}
        />
        {/* </CenteredDiv> */}
        {/* <CenteredDiv> */}
        <DateInput
          date={convertDateFormat(departmentDate)}
          setDate={(e) => setdepartmentDate(e)}
          label={"موافقة مجلس الكلية على تعديل الرسالة"}
          apply={true}
          // disabled={true}
        />
        {/* </CenteredDiv> */}
      </SignatureDiv>
      <ButtonDev onClick={show}>عرض</ButtonDev>
      {showReport && (
        <div>
          <PrintButton onClick={Print}>طباعه</PrintButton>
          <div id="printablediv">
            <Container>
              <LineDiv className="marginBottom">
                <RightDiv className="center">
                  <div> جامعة الأزهر</div>
                  <div>كلية الهندسة بنين - القاهرة</div>
                  <div>ادارة الدراسات العليا </div>
                </RightDiv>
              </LineDiv>
              <CenteredDiv>
                طلب الموافقة على الغاء قيد / تسجيل درجة{" "}
                {student?.thesis?.degree}{" "}
              </CenteredDiv>
              <LineDiv>
                <RightDiv>اسم الباحث / {student?.user.arabicName}</RightDiv>
                <LeftDiv>الجنسية / {student?.user?.nationality}</LeftDiv>
              </LineDiv>
              <LineDiv>
                <RightDiv>
                  القسم /{" "}
                  {student?.last_qualification?.department?.display_name}
                </RightDiv>
                <LeftDiv>
                  الشعبة / {student?.last_qualification?.branch?.display_name}
                </LeftDiv>
              </LineDiv>
              <MessageSubject>
                <div> عنوان الرسالة : </div>
                <DivThesis> {student?.thesis?.arabic_title}</DivThesis>
              </MessageSubject>
              <LineDiv className="margin">
                <RightDiv>
                  تاريخ تسجيل الكلية : {student?.thesis?.college_agree_date}
                </RightDiv>
                <LeftDiv>
                  تاريخ تسجيل الجامعة : {student?.thesis?.university_agree_date}
                </LeftDiv>
              </LineDiv>
              <MessageSubject>
                <div> سبب الفصل : </div>
                <DivThesis> {title}</DivThesis>
              </MessageSubject>
              <MarginDiv>
                موافقة مجلس القسم على الالغاء : {finalCollegeDate}
              </MarginDiv>
              <MarginDiv>
                موافقة مجلس الكلية على الالغاء : {finalDepartmentDate}
              </MarginDiv>

              <CenteredDiv className="Line">
                السيد الاستاذ الدكتور / نائب رئيس الجامعة
              </CenteredDiv>
              <CenteredDiv className="Line">
                بعد التحيـــــــــــــة ،،،،،،،،،،،
              </CenteredDiv>
              <MarginDiv>
                برجاء التكرم بالموافقة نحو اعتماد الغاء القيد / تسجيل الطالب
                المذكور بعاليه من الدراسات العليا
              </MarginDiv>
              <SignatureDiv className="margin Signature">
                <div>الدراسات العليا </div>
                <div>مدير عام الكلية </div>
                <div> يعتمد أ.د / وكيل الكلية للدراسات العليا </div>
              </SignatureDiv>
              <Footer></Footer>
            </Container>
          </div>
        </div>
      )}
    </>
  );
};

export default CancelReport;
