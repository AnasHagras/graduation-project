import React, { useState } from "react";
import "react-datepicker/dist/react-datepicker.css";
import Footer, { BlankSpan } from "./Footer";
import { Container } from "./Footer";
import { LineDiv } from "./Footer";
import { SignatureDiv } from "./Footer";
import { RightDiv } from "./Footer";
import { LeftDiv } from "./Footer";
import { CenteredDiv } from "./Footer";
import { InputDate } from "./Footer";
import { PrintButton } from "./Footer";
import { BoldDiv } from "./Footer";
import { MessageSubject } from "./Footer";
import { DivThesis } from "./Footer";
import { MarginDiv } from "./Footer";
import { ButtonDev } from "./Footer";
import { StyledInput } from "./Footer";
import { StatementSubmittedDiv } from "./Footer";
import { StyledTable } from "./Footer";
import { TableHeader } from "./Footer";
import { TableData } from "./Footer";
import AlertModal from "../../../../molecules/modals/AlertModal";
import DateInput from "../../../../atoms/form/dateInput/DateInput";
import { useSelector } from "react-redux";
import getSupervision from "../../../../services/supervision/getSupervision";
import { useParams } from "react-router-dom";
const RequestExtendReport = ({ StudentsDetails }) => {
  const [title, setTitle] = useState("");
  const [open, setOpen] = useState(false);
  const [content, setContent] = useState("");
  const id = useParams().id;
  const { students } = useSelector((state) => state.students);
  const student = students ? students[id] : {};
  const [filteredSupervisors, setFilteredSupervisors] = useState([]);
  const [filteredRole, setFilteredRole] = useState([]);
  const { supervisors } = useSelector((state) => state.supervisors);
  React.useEffect(() => {
    getSupervision({ thesis: student?.thesis?.id }).then((res) => {
      console.log("res", res);

      const filteredSupervisors = res?.map((item) =>
        supervisors.find((supervisor) => supervisor.user.id === item.id)
      );

      const filteredRole = res?.map((item) => item.supervising_type);
      Promise.all([filteredSupervisors, filteredRole]).then(
        ([supervisorsArray, indexesArray]) => {
          setFilteredSupervisors(supervisorsArray);
          setFilteredRole(indexesArray);
          console.log("supervisorsArray", supervisorsArray);
          console.log("indexesArray", indexesArray);
        }
      );
    });
  }, [student]);
  const Print = () => {
    //console.log('print');
    let printContents = document.getElementById("printablediv").innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    window.location.reload(false);
  };
  const [collegeDate, setcollegeDate] = useState();
  const [departmentDate, setdepartmentDate] = useState();

  const [startExtension, setstartExtention] = useState();
  const [endExtension, setendExtention] = useState();

  const [finalCollegeDate, setfinalCollegeDate] = useState();
  const [finalDepartmentDate, setfinalDepartmentDate] = useState();

  const [finalStartExtension, setfinalStartExtention] = useState();
  const [finalEndExtension, setfinalEndExtention] = useState();

  const [showReport, setshowReport] = useState(false);

  const show = () => {
    console.log("collegeDate", collegeDate);
    console.log("departmentDate", departmentDate);
    console.log("startExtension", startExtension);
    console.log("endExtension", endExtension);
    if (collegeDate && departmentDate && startExtension && endExtension) {
      setshowReport(true);
      setfinalCollegeDate(collegeDate);
      setfinalDepartmentDate(departmentDate);
      setfinalStartExtention(startExtension);
      setfinalEndExtention(endExtension);
    } else {
      setOpen(true);
      setTitle("خطأ");
      setContent("من فضلك املأ جميع الحقول");
    }
  };

  return (
    <>
      <AlertModal
        open={open}
        setOpen={setOpen}
        title={title}
        content={content}
        buttonText={"حسناً"}
      />
      <SignatureDiv className="margin">
        <DateInput
          date={collegeDate}
          setDate={(e) => setcollegeDate(e)}
          label="موافقة مجلس الكلية على المد"
          apply={true}
        />
        <DateInput
          date={departmentDate}
          setDate={(e) => setdepartmentDate(e)}
          label="موافقة مجلس القسم على المد"
          apply={true}
        />
        <DateInput
          date={startExtension}
          setDate={(e) => setstartExtention(e)}
          label="بداية المد الجديد"
          apply={true}
        />
        <DateInput
          date={endExtension}
          setDate={(e) => setendExtention(e)}
          label="نهاية المد الجديد"
          apply={true}
        />
      </SignatureDiv>
      <ButtonDev onClick={show}>عرض</ButtonDev>

      {showReport && (
        <div>
          <PrintButton onClick={Print}>طباعه</PrintButton>
          <div id="printablediv">
            <Container className="smallFont">
              <LineDiv className="marginBottom">
                <RightDiv className="center">
                  <div> جامعة الأزهر</div>
                  <div>كلية الهندسة بنين - القاهرة</div>
                  <div>ادارة الدراسات العليا </div>
                </RightDiv>
              </LineDiv>
              <CenteredDiv>
                {" "}
                طلب الموافقة على مد لجنة الاشراف لدرجة :{" "}
                {student?.thesis?.degree}{" "}
              </CenteredDiv>
              <LineDiv>
                <RightDiv>اسم الباحث / {student?.user?.arabicName}</RightDiv>
                <LeftDiv>الجنسية / {student?.user?.nationality}</LeftDiv>
              </LineDiv>
              <LineDiv>
                <RightDiv>
                  القسم /{" "}
                  {student?.last_qualification?.department?.display_name}
                </RightDiv>
                <LeftDiv>
                  الشعبة / {student?.last_qualification?.branch?.display_name}
                </LeftDiv>
              </LineDiv>
              <MessageSubject>
                <div>موضوع الرسالة : </div>
                <DivThesis> {student?.thesis.arabic_title}</DivThesis>
              </MessageSubject>
              <LineDiv className="margin">
                <RightDiv>
                  موافقة مجلس الكلية على التسجيل :{" "}
                  {student?.thesis?.college_agree_date}
                </RightDiv>
                <LeftDiv>
                  موافقة مجلس الجامعة على التسجيل :{" "}
                  {student?.thesis?.university_agree_date}
                </LeftDiv>
              </LineDiv>
              <MarginDiv className="Line Big">
                لجنة الاشراف المعتمدة :
              </MarginDiv>
              <StyledTable>
                <thead>
                  <TableHeader>الرقم</TableHeader>
                  <TableHeader>لجنة الاشراف</TableHeader>
                  <TableHeader>الدرجة</TableHeader>
                  <TableHeader>الجهة </TableHeader>
                </thead>
                <tbody>
                  {filteredSupervisors.map((supervisor, index) => (
                    <tr>
                      <TableData>{index + 1}</TableData>
                      <TableData> {supervisor?.user?.arabicName}</TableData>
                      <TableData>{filteredRole[index]}</TableData>
                      <TableData> {supervisor?.organization} </TableData>
                    </tr>
                  ))}
                </tbody>
              </StyledTable>
              <LineDiv className="margin">
                <RightDiv>
                  بداية المد الجديد <BlankSpan></BlankSpan> يبدأ من :{" "}
                  {finalStartExtension}
                </RightDiv>
                <LeftDiv>وينتهي في : {finalEndExtension}</LeftDiv>
              </LineDiv>
              <LineDiv className="margin">
                <RightDiv>
                  موافقة مجلس القسم على المد : {finalCollegeDate}
                </RightDiv>
                <LeftDiv>
                  موافقة مجلس الكلية على المد : {finalDepartmentDate}
                </LeftDiv>
              </LineDiv>
              <CenteredDiv>
                السيد الاستاذ الدكتور / نائب رئيس الجامعة للدراسات العليا
              </CenteredDiv>
              <CenteredDiv>
                {" "}
                الرجو اتخاذ اللازم نحو اعتماد المد المشار اليه{" "}
              </CenteredDiv>
              <CenteredDiv className="Line">
                {" "}
                وتفضلوا بقبول وافر الاحترام ،،،،،،،،،،،{" "}
              </CenteredDiv>
              <SignatureDiv className="margin Signature">
                <div>الدراسات العليا </div>
                <div>مدير عام الكلية </div>
                <div> يعتمد أ.د / وكيل الكلية للدراسات العليا </div>
              </SignatureDiv>
              <Footer />
            </Container>
          </div>
        </div>
      )}
    </>
  );
};

export default RequestExtendReport;
