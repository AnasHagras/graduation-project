import React, { useState } from "react";
import "react-datepicker/dist/react-datepicker.css";
import Footer from "./Footer";
import { Container } from "./Footer";
import { LineDiv } from "./Footer";
import { SignatureDiv } from "./Footer";
import { RightDiv } from "./Footer";
import { LeftDiv } from "./Footer";
import { CenteredDiv } from "./Footer";
import { InputDate } from "./Footer";
import { PrintButton } from "./Footer";
import { BoldDiv } from "./Footer";
import { MessageSubject } from "./Footer";
import { DivThesis } from "./Footer";
import { MarginDiv } from "./Footer";
import { ButtonDev } from "./Footer";
import { StyledInput } from "./Footer";
import { StatementSubmittedDiv } from "./Footer";
import { StyledTable } from "./Footer";
import { TableHeader } from "./Footer";
import { TableData } from "./Footer";
import AlertModal from "../../../../molecules/modals/AlertModal";
import DateInput from "../../../../atoms/form/dateInput/DateInput";
import { useSelector } from "react-redux";
import getSupervision from "../../../../services/supervision/getSupervision";
import { useParams } from "react-router-dom";

const ChangeTitleReport = ({ StudentsDetails }) => {
  function convertDateFormat(dateString) {
    if (!dateString) return dateString;
    var parts = dateString?.split("-");
    var day = parts[0];
    var month = parts[1];
    var year = parts[2];

    // Concatenate the parts in the desired format
    var convertedDate = year + "-" + month + "-" + day;

    return convertedDate;
  }
  const id = useParams().id;
  const [showReport, setShowReport] = useState(false);
  const { students } = useSelector((state) => state.students);
  const student = students ? students[id] : {};
  const [filteredSupervisors, setFilteredSupervisors] = useState([]);
  const [filteredRole, setFilteredRole] = useState([]);
  const { supervisors } = useSelector((state) => state.supervisors);
  const [title, setTitle] = useState("");
  const [open, setOpen] = useState(false);
  const [content, setContent] = useState("");
  const [tit, setTit] = useState("");
  React.useEffect(() => {
    setdepartmentDate(student?.thesis?.department_agree_date);
    setcollegeDate(student?.thesis?.college_agree_date);
    console.log(student?.thesis?.department_agree_date);
    console.log(student?.thesis?.department_agree_date);
    console.log("DEPART", departmentDate);
    console.log("COL", collegeDate);
    getSupervision({ thesis: student?.thesis?.id }).then((res) => {
      console.log("res", res);

      const filteredSupervisors = res?.map((item) =>
        supervisors.find((supervisor) => supervisor.user.id === item.id)
      );

      const filteredRole = res?.map((item) => item.supervising_type);
      Promise.all([filteredSupervisors, filteredRole]).then(
        ([supervisorsArray, indexesArray]) => {
          setFilteredSupervisors(supervisorsArray);
          setFilteredRole(indexesArray);
          console.log("supervisorsArray", supervisorsArray);
          console.log("indexesArray", indexesArray);
        }
      );
    });
  }, [student]);

  const handleInputChange = (event) => {
    setTitle(event.target.value);
  };
  const show = () => {
    if (title !== "" && collegeDate && departmentDate) {
      setShowReport(true);
      setValue(title);
      setFinalCollegeDate(collegeDate);
      setFinalDepartmentDate(departmentDate);
    } else {
      setOpen(true);
      setTit("خطأ");
      setContent("من فضلك أدخل موضوع الرسالة المفترح");
    }
  };

  const Print = () => {
    //console.log('print');
    let printContents = document.getElementById("printablediv").innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    window.location.reload(false);
  };
  const [collegeDate, setcollegeDate] = useState();
  const [departmentDate, setdepartmentDate] = useState();
  const [value, setValue] = useState("");
  const [finalCollegeDate, setFinalCollegeDate] = useState();
  const [finalDepartmentDate, setFinalDepartmentDate] = useState();
  return (
    <>
      <AlertModal
        open={open}
        setOpen={setOpen}
        title={tit}
        content={content}
        buttonText={"حسناً"}
      />

      <StatementSubmittedDiv>
        <div>موضوع الرسالة المقترح</div>
        <StyledInput type="text" onChange={handleInputChange} />
      </StatementSubmittedDiv>
      <SignatureDiv className="margin">
        {/* <CenteredDiv> */}
        {/* <BoldDiv>مجلس الكلية</BoldDiv> */}
        <DateInput
          date={convertDateFormat(collegeDate)}
          setDate={(e) => setcollegeDate(e)}
          apply={true}
          label={"موافقة مجلس القسم على تعديل الرسالة"}
          // disabled={true}
        />
        {/* </CenteredDiv> */}
        {/* <CenteredDiv> */}
        <DateInput
          date={convertDateFormat(departmentDate)}
          setDate={(e) => setdepartmentDate(e)}
          label={"موافقة مجلس الكلية على تعديل الرسالة"}
          apply={true}
          // disabled={true}
        />
        {/* </CenteredDiv> */}
      </SignatureDiv>
      <ButtonDev onClick={show}>عرض</ButtonDev>

      {showReport && (
        <div>
          <PrintButton onClick={Print}>طباعه</PrintButton>
          <div id="printablediv">
            <Container>
              <LineDiv className="marginBottom">
                <RightDiv className="center">
                  <div> جامعة الأزهر</div>
                  <div>كلية الهندسة بنين - القاهرة</div>
                  <div>ادارة الدراسات العليا </div>
                </RightDiv>
              </LineDiv>
              <CenteredDiv>طلب الموافقة على تعديل عنوان الرسالة </CenteredDiv>
              <LineDiv>
                <RightDiv>اسم الباحث / {student?.user?.arabicName}</RightDiv>
                <LeftDiv>الجنسية / {student?.user?.nationality}</LeftDiv>
              </LineDiv>
              <LineDiv>
                <RightDiv>
                  القسم / {student?.last_qualification?.department.display_name}
                </RightDiv>
                <LeftDiv>
                  الشعبة / {student?.last_qualification?.branch.display_name}
                </LeftDiv>
              </LineDiv>
              <MessageSubject>
                <div>موضوع الرسالة المعتمد : </div>
                <DivThesis> {student?.thesis?.arabic_title}</DivThesis>
              </MessageSubject>
              <LineDiv className="margin">
                <RightDiv>
                  موافقة مجلس الكلية على التسجيل :{" "}
                  {student?.thesis?.college_agree_date}
                </RightDiv>
                <LeftDiv>
                  موافقة مجلس الجامعة على التسجيل :{" "}
                  {student?.thesis?.university_agree_date}
                </LeftDiv>
              </LineDiv>
              <MessageSubject>
                <div>موضوع الرسالة المقترح : </div>
                <DivThesis> {value}</DivThesis>
              </MessageSubject>
              <MarginDiv className="Line Big">
                لجنة الاشراف المعتمدة :
              </MarginDiv>
              <StyledTable>
                <thead>
                  <TableHeader>الرقم</TableHeader>
                  <TableHeader>الاسم</TableHeader>
                  <TableHeader>نوع الإشراف</TableHeader>
                  <TableHeader>الدرجة</TableHeader>
                  <TableHeader>جهة العمل </TableHeader>
                </thead>
                <tbody>
                  {filteredSupervisors.map((supervisor, index) => (
                    <tr>
                      <TableData>{index + 1}</TableData>
                      <TableData> {supervisor?.user?.arabicName}</TableData>
                      <TableData> {filteredRole[index]}</TableData>
                      <TableData>{supervisor?.degree}</TableData>
                      <TableData> {supervisor?.organization} </TableData>
                    </tr>
                  ))}
                </tbody>
              </StyledTable>
              <LineDiv className="margin">
                <RightDiv>
                  موافقة مجلس القسم على تعديل الرسالة : {finalCollegeDate}
                </RightDiv>
                <LeftDiv>
                  موافقة مجلس الكلية على تعديل الرسالة : {finalDepartmentDate}
                </LeftDiv>
              </LineDiv>
              <CenteredDiv className="Line">
                السيد الاستاذ الدكتور / نائب رئيس الجامعة للدراسات العليا
              </CenteredDiv>
              <CenteredDiv className="Line">
                برجاء التكرم بالموافقة نحو التعديل المشار اليه ،،،،،،،،،،،{" "}
              </CenteredDiv>
              <SignatureDiv className="margin Signature">
                <div>الدراسات العليا </div>
                <div>مدير عام الكلية </div>
                <div>أ.د / عميد الكلية </div>
              </SignatureDiv>
              <Footer />
            </Container>
          </div>
        </div>
      )}
    </>
  );
};

export default ChangeTitleReport;
