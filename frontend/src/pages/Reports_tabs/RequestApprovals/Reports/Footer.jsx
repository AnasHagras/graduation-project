import styled from "@emotion/styled";
import React from "react";

export const StatementSubmittedDiv = styled("div")(({ theme }) => ({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  fontWeight: "bold",
  margin: "10px 0",
}));
export const ButtonDev = styled("button")(({ theme }) => ({
  backgroundColor: "purple",
  color: "white",
  display: "block",
  margin: "0 auto",
  marginTop: "2rem",
  marginBottom: "2rem",
  borderRadius: "4rem",
  width: "15%",
  height: "3.5rem",
}));
export const StyledInput = styled("input")(({ theme }) => ({
  width: "80%",
  height: "3.5rem",
  border: "2px solid black",
  borderRadius: "4rem",
  marginRight: "1rem",
  marginLeft: "1rem",
  // textAlign: "center",
  padding: "0 1rem",
  fontSize: "1.2rem",
  // fontWeight: "bold",
  "&:focus": {
    outline: "none",
  },
}));

export const Container = styled("div")(({ theme }) => ({
  border: "2px solid black",
  padding: "20px",
  margin: "auto",
  fontWeight: "bold",
  width: "95%",
  fontSize: "14px",
  "&.smallFont": {
    fontSize: "12px",
  },
  "&.NotBold": {
    fontWeight: "normal",
  },
}));
export const LineDiv = styled("div")(({ theme }) => ({
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  marginBottom: "5px",
  "&.margin": {
    margin: "20px 0",
  },
  "&.Signature": {
    marginBottom: "40px",
  },
  "&.marginBottom": {
    marginBottom: "10px",
  },
  "&.marginTop": {
    marginTop: "30px",
  },
}));
export const SignatureDiv = styled(LineDiv)(({ theme }) => ({
  justifyContent: "space-around",
}));
export const RightDiv = styled("div")(({ theme }) => ({
  "&.center": {
    textAlign: "center",
  },
}));
export const LeftDiv = styled("div")(({ theme }) => ({
  width: "40%",
}));
export const MessageSubject = styled("div")(({ theme }) => ({
  width: "90%",
  border: "solid 2px black",
  margin: "auto",
  marginTop: "10px",
  padding: "10px",
}));
export const Span = styled("span")(({ theme }) => ({
  marginRight: "40px",
}));
export const MarginDiv = styled("div")(({ theme }) => ({
  margin: "20px 0 ",
  width: "fit-content",
  "&.Line": {
    borderBottom: "2px solid black",
  },
  "&.Big": {
    fontSize: "15px",
  },
  "&.small": {
    margin: "5px 0 ",
  },
}));
export const StyledTable = styled("table")(({ theme }) => ({
  textAlign: "center",
  width: "90%",
  border: "2px solid black",
  margin: "5px auto",
}));
export const TableHeader = styled("th")(({ theme }) => ({
  border: "1px solid black",
  borderBottom: "2px solid black",
  backgroundColor: "#e4e5e5",
  padding: "4px",
}));
export const TableData = styled("td")(({ theme }) => ({
  border: "1px solid black",
  padding: "4px",
}));
export const CenteredDiv = styled("div")(({ theme }) => ({
  margin: "5px auto",
  textAlign: "center",
  padding: "3px",
  "&.Line": {
    borderBottom: "2px solid black",
  },
  "&.smallDistance": {
    margin: "auto",
    padding: "1px",
  },
  width: "fit-content",
}));
export const PrintButton = styled("button")(({ theme }) => ({
  backgroundColor: "purple",
  color: "white",
  display: "block",
  margin: "0 auto",
  marginTop: "2rem",
  marginBottom: "2rem",
  borderRadius: "4rem",
  width: "15%",
  height: "3.5rem",
}));

export const DivThesis = styled("div")(({ theme }) => ({
  // fontSize: "13px",
  textAlign: "center",
}));

export const InputDate = styled("input")(({ theme }) => ({
  padding: "10px",
  margin: "5px",
  backgroundColor: "#9f65a51f",
  borderRadius: "10px",
}));
export const BoldDiv = styled("div")(({ theme }) => ({
  fontWeight: "bold",
}));

export const BlankSpan = styled("span")(({ theme }) => ({
  margin: "0 30px",
}));

const Footer = () => {
  return (
    <>
      <CenteredDiv className="Line smallDistance">
        الادارة العامة للدراسات العليا والبحوث بالجامعه
      </CenteredDiv>
      <CenteredDiv className="Line smallDistance">
        روجعت البيانات والاجراءات صحيحة واللوائح مطابقة
      </CenteredDiv>
      <SignatureDiv className="margin Signature">
        <div> الموظف المختص </div>
        <div>رئيس القسم </div>
        <div>مدير الإدارة </div>
        <div>المدير العام </div>
      </SignatureDiv>

      <SignatureDiv className="Signature">
        <RightDiv>
          <CenteredDiv>
            <div>موافقة :</div>
            <div>(أ.د / نائب رئيس الجامعة المختص )</div>
          </CenteredDiv>
        </RightDiv>
        <LeftDiv>
          <CenteredDiv>
            <div>يعتمد ،،،،،،،،،،، </div>
            <div>(أ.د / رئيس الجامعة)</div>
          </CenteredDiv>
        </LeftDiv>
      </SignatureDiv>
      <div>
        مصادقة مجلس الجامعة بجلسته رقم ( <BlankSpan></BlankSpan>) بتاريخ
        <Span>/</Span>
        <Span>/ </Span>
        <Span>20 م</Span>
      </div>
    </>
  );
};

export default Footer;
