import React, { useState } from "react";
import Footer, { BlankSpan } from "./Footer";
import { Container } from "./Footer";
import { LineDiv } from "./Footer";
import { SignatureDiv } from "./Footer";
import { RightDiv } from "./Footer";
import { LeftDiv } from "./Footer";
import { CenteredDiv } from "./Footer";
import { InputDate } from "./Footer";
import { PrintButton } from "./Footer";
import { BoldDiv } from "./Footer";
import { MessageSubject } from "./Footer";
import { DivThesis } from "./Footer";
import { MarginDiv } from "./Footer";
import { ButtonDev } from "./Footer";
import { StyledInput } from "./Footer";
import { StatementSubmittedDiv } from "./Footer";
import { StyledTable } from "./Footer";
import { TableHeader } from "./Footer";
import { TableData } from "./Footer";
import AlertModal from "../../../../molecules/modals/AlertModal";
import DateInput from "../../../../atoms/form/dateInput/DateInput";
import { useSelector } from "react-redux";
import getSupervision from "../../../../services/supervision/getSupervision";
import SupervisorsCommitee from "../../../../organisms/supervisors-commitee/SupervisorsCommitee";
import { useParams } from "react-router-dom";
const FormationRulingReport = ({ StudentsDetails }) => {
  const id = useParams().id;
  const { students } = useSelector((state) => state.students);
  const student = students ? students[id] : {};
  const [filteredSupervisors, setFilteredSupervisors] = useState([]);
  const [filteredRole, setFilteredRole] = useState([]);
  const { supervisors } = useSelector((state) => state.supervisors);
  const [reason, setReason] = useState("");
  const [value, setValue] = useState("");
  const [finalCollegeDate, setFinalCollegeDate] = useState();
  const [finalDepartmentDate, setFinalDepartmentDate] = useState();
  const [suggestedSupervisors, setSuggestedSupervisors] = useState([]);
  const [suggestedRoles, setSuggestedRoles] = useState([]);
  const [showReport, setShowReport] = useState(false);
  const [islamicSubject, setIslamicSubject] = useState("");
  const [specializedSubject, setSpecializedSubject] = useState("");

  React.useEffect(() => {
    getSupervision({ thesis: student?.thesis?.id }).then((res) => {
      console.log("res", res);

      const filteredSupervisors = res?.map((item) =>
        supervisors.find((supervisor) => supervisor.user.id === item.id)
      );

      const filteredRole = res?.map((item) => item.supervising_type);
      Promise.all([filteredSupervisors, filteredRole]).then(
        ([supervisorsArray, indexesArray]) => {
          setFilteredSupervisors(supervisorsArray);
          setFilteredRole(indexesArray);
          console.log("supervisorsArray", supervisorsArray);
          console.log("indexesArray", indexesArray);
        }
      );
    });
  }, [student]);

  const handleIslamicSubject = (event) => {
    setIslamicSubject(event.target.value);
  };
  const handleSpecializedSubject = (event) => {
    setSpecializedSubject(event.target.value);
  };
  const [title, setTitle] = useState("");
  const [open, setOpen] = useState(false);
  const [content, setContent] = useState("");
  function convertDateFormat(dateString) {
    if (!dateString) return dateString;
    var parts = dateString?.split("-");
    var day = parts[0];
    var month = parts[1];
    var year = parts[2];

    // Concatenate the parts in the desired format
    var convertedDate = year + "-" + month + "-" + day;

    return convertedDate;
  }
  const [finalSuggestion, setFinalSuggestion] = useState([]);
  const [finalSuggestionRole, setFinalSuggestionRole] = useState([]);
  const [value2, setValue2] = useState("");

  const show = () => {
    if (
      islamicSubject !== "" &&
      specializedSubject !== "" &&
      collegeDate &&
      departmentDate &&
      suggestedSupervisors.length &&
      suggestedRoles !== null
    ) {
      setShowReport(true);
      setValue(islamicSubject);
      setValue2(specializedSubject);
      setFinalCollegeDate(collegeDate);
      setFinalDepartmentDate(departmentDate);
      setFinalSuggestion(suggestedSupervisors);
      setFinalSuggestionRole(suggestedRoles);
    } else {
      setOpen(true);
      setTitle("خطأ");
      setContent("من فضلك أدخل جميع البيانات");
    }
  };

  const Print = () => {
    //console.log('print');
    let printContents = document.getElementById("printablediv").innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    window.location.reload(false);
  };
  const [collegeDate, setcollegeDate] = useState();
  const [departmentDate, setdepartmentDate] = useState();

  return (
    <>
      <AlertModal
        open={open}
        setOpen={setOpen}
        title={title}
        content={content}
        buttonText={"حسناً"}
      />
      <SupervisorsCommitee
        supervisors={suggestedSupervisors}
        setSupervisors={(supervisors) => setSuggestedSupervisors(supervisors)}
        supervisionTypes={suggestedRoles}
        setSupervisionType={(supervisionTypes) =>
          setSuggestedRoles(supervisionTypes)
        }
      />
      <StatementSubmittedDiv
        style={{ display: "flex", flexDirection: "column" }}
      >
        <div style={{ display: "flex", width: "100%", marginBottom: "1rem" }}>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            المواد الاسلامية
          </div>
          <StyledInput type="text" onChange={handleIslamicSubject} />
        </div>
        <div style={{ display: "flex", width: "100%" }}>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            المواد التخصصية
          </div>
          <StyledInput type="text" onChange={handleSpecializedSubject} />
        </div>
      </StatementSubmittedDiv>

      <SignatureDiv className="margin">
        {/* <CenteredDiv> */}
        {/* <BoldDiv>مجلس الكلية</BoldDiv> */}
        <DateInput
          date={convertDateFormat(collegeDate)}
          setDate={(e) => setcollegeDate(e)}
          apply={true}
          label={"موافقة مجلس القسم على تعديل الرسالة"}
          // disabled={true}
        />
        {/* </CenteredDiv> */}
        {/* <CenteredDiv> */}
        <DateInput
          date={convertDateFormat(departmentDate)}
          setDate={(e) => setdepartmentDate(e)}
          label={"موافقة مجلس الكلية على تعديل الرسالة"}
          apply={true}
          // disabled={true}
        />
        {/* </CenteredDiv> */}
      </SignatureDiv>
      <ButtonDev onClick={show}>عرض</ButtonDev>

      {showReport && (
        <div>
          <PrintButton onClick={Print}>طباعه</PrintButton>
          <div id="printablediv">
            <Container className="smallFont">
              <LineDiv className="marginBottom">
                <RightDiv className="center">
                  <div> جامعة الأزهر</div>
                  <div>كلية الهندسة بنين - القاهرة</div>
                  <div>ادارة الدراسات العليا </div>
                </RightDiv>
              </LineDiv>
              <CenteredDiv>
                طلب الموافقة على تعديل لجنة الاشراف لدرجة :{" "}
                {student?.thesis?.degree}{" "}
              </CenteredDiv>
              <LineDiv>
                <RightDiv>اسم الباحث / {student?.user?.arabicName}</RightDiv>
                <LeftDiv>الجنسية / {student?.user?.nationality}</LeftDiv>
              </LineDiv>
              <LineDiv>
                <RightDiv>
                  القسم / {student?.last_qualification?.department.display_name}
                </RightDiv>
                <LeftDiv>
                  الشعبة / {student?.last_qualification?.branch.display_name}
                </LeftDiv>
              </LineDiv>
              <MessageSubject>
                <div>
                  موضوع الرسالة : <BlankSpan></BlankSpan>{" "}
                  {student?.thesis?.arabic_title}
                </div>
              </MessageSubject>
              <LineDiv className="margin">
                <RightDiv>
                  {" "}
                  الموقف من المواد الاسلامية / <BlankSpan /> {value}
                </RightDiv>
                <LeftDiv>
                  {" "}
                  الموقف من المواد التخصصية / <BlankSpan /> {value2}
                </LeftDiv>
              </LineDiv>
              <MarginDiv className="Line small">
                لجنة الاشراف المعتمدة :
              </MarginDiv>
              <StyledTable>
                <thead>
                  <TableHeader>الرقم</TableHeader>
                  <TableHeader>لجنة الاشراف</TableHeader>
                  <TableHeader>الدرجة</TableHeader>
                  <TableHeader>الجهة </TableHeader>
                </thead>
                <tbody>
                  {filteredSupervisors.map((supervisor, index) => (
                    <tr>
                      <TableData>{index + 1}</TableData>
                      <TableData> {supervisor?.user?.arabicName}</TableData>
                      <TableData>{filteredRole[index]}</TableData>
                      <TableData> {supervisor?.organization} </TableData>
                    </tr>
                  ))}
                </tbody>
              </StyledTable>
              <LineDiv className="margin">
                <RightDiv>
                  موافقة مجلس الكلية على التسجيل :{" "}
                  {student?.thesis?.college_agree_date}
                </RightDiv>
                <LeftDiv>
                  موافقة مجلس الجامعة على التسجيل :{" "}
                  {student?.thesis?.university_agree_date}
                </LeftDiv>
              </LineDiv>
              <MarginDiv className="Line small">
                {" "}
                أعضاء لجنة المناقشة :
              </MarginDiv>
              <StyledTable>
                <thead>
                  <TableHeader>الرقم</TableHeader>
                  <TableHeader>لجنة الاشراف</TableHeader>
                  <TableHeader>الدرجة</TableHeader>
                  <TableHeader>الجهة </TableHeader>
                </thead>
                <tbody>
                  {finalSuggestion.map((supervisor, index) => (
                    <tr>
                      <TableData>{index + 1}</TableData>
                      <TableData> {supervisor?.user?.arabicName}</TableData>
                      <TableData>{finalSuggestionRole[index]}</TableData>
                      <TableData> {supervisor?.organization} </TableData>
                    </tr>
                  ))}
                </tbody>
              </StyledTable>
              <LineDiv className="margin">
                <RightDiv>
                  موافقة مجلس القسم على تشكيل اللجنة : {finalCollegeDate}
                </RightDiv>
                <LeftDiv>
                  موافقة مجلس الكلية على تشكيل اللجنة : {finalDepartmentDate}
                </LeftDiv>
              </LineDiv>
              <CenteredDiv className="smallDistance">
                السيد الاستاذ الدكتور / نائب رئيس الجامعة{" "}
              </CenteredDiv>
              <CenteredDiv className="smallDistance">
                {" "}
                تحية طيبة ،،،،،،،،،،، وبعد{" "}
              </CenteredDiv>
              <CenteredDiv>
                يرجاء التكرم بالموافقة و اعتماد تتكيل اللجنة المذكورة ومرفق طيه
                صورة من تقرير صلاحية الرسالة للمناقشة والحكم المقدم من السيد
                وكذا إفادة بتسليم الباحت نسخة {} من الرسالة مغلفة ومجلدة الى
                مكتب أ.د وكيل الكلية وكتلك تسليم نسحة من الرسالة لإدارة الدراسات
                العليا بالجامعة
              </CenteredDiv>

              <SignatureDiv className="margin Signature">
                <div>الدراسات العليا </div>
                <div>مدير عام الكلية </div>
                <div> يعتمد أ.د / وكيل الكلية للدراسات العليا </div>
              </SignatureDiv>
              <Footer />
            </Container>
          </div>
        </div>
      )}
    </>
  );
};

export default FormationRulingReport;
