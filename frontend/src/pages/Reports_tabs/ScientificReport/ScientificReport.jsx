import styled from "@emotion/styled";
import React, { useEffect, useState } from "react";

import { useOutletContext, useParams, Navigate } from "react-router-dom";
import { useSelector } from "react-redux";
import getSupervision from "../../../services/supervision/getSupervision";
const Container = styled("div")(({ theme }) => ({
  border: "2px solid black",
  padding: "20px",
  margin: "auto",
  margin: "20px",
  fontWeight: "bold",
  width: "95%",
}));
const LineDiv = styled("div")(({ theme }) => ({
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  marginBottom: "5px",
  "&.Signature": {
    marginBottom: "45px",
  },
  "&.margin": {
    marginBottom: "40px",
  },
  "&.marginTop": {
    marginTop: "30px",
  },
}));
const SignatureDiv = styled(LineDiv)(({ theme }) => ({
  justifyContent: "space-around",
}));
const RightDiv = styled("div")(({ theme }) => ({
  "&.center": {
    textAlign: "center",
  },
}));
const LeftDiv = styled("div")(({ theme }) => ({
  width: "40%",
}));
const Span = styled("span")(({ theme }) => ({
  marginRight: "40px",
}));
const MarginDiv = styled("div")(({ theme }) => ({
  margin: "20px 0 ",
  width: "fit-content",
  "&.Line": {
    borderBottom: "2px solid black",
  },
  "&.Big": {
    fontSize: "17px",
  },
}));
const StyledTable = styled("table")(({ theme }) => ({
  textAlign: "center",
  width: "90%",
  border: "2px solid black",
  margin: "20px auto 10px",
}));
const TableHeader = styled("th")(({ theme }) => ({
  border: "1px solid black",
  borderBottom: "2px solid black",
  backgroundColor: "#e4e5e5",
  padding: "7px",
}));
const TableData = styled("td")(({ theme }) => ({
  border: "1px solid black",
  padding: "7px",
}));
const CenteredDiv = styled("div")(({ theme }) => ({
  margin: "5px auto",
  textAlign: "center",
  padding: "5px",
  "&.Line": {
    borderBottom: "2px solid black",
  },
}));
const PrintButton = styled("button")(({ theme }) => ({
  backgroundColor: "purple",
  color: "white",
  display: "block",
  margin: "0 auto",
  marginTop: "2rem",
  marginBottom: "2rem",
  borderRadius: "4rem",
  width: "15%",
  height: "3.5rem",
}));

const find_main_suoervisor = (supervisors, role) => {
  const main_supervisor = supervisors.find(
    (supervisor, index) => role[index] === "مشرف رئيسي"
  );
  console.log("main_supervisor", main_supervisor);
  return main_supervisor;
};
const ScientificReport = ({ StudentDetails }) => {
  const id = useParams()["id"];
  const { students } = useSelector((state) => state.students);
  const student = students ? students[id] : {};
  const [filteredSupervisors, setFilteredSupervisors] = useState([]);
  const [filteredRole, setFilteredRole] = useState([]);
  const { supervisors } = useSelector((state) => state.supervisors);
  const [showReport, setShowReport] = useState(false);
  const [title, setTitle] = useOutletContext();
  useEffect(() => {
    setTitle("تقرير علمي");
    getSupervision({ thesis: student?.thesis?.id }).then((res) => {
      console.log("res", res);

      const filteredSupervisors = res?.map((item) =>
        supervisors.find((supervisor) => supervisor.user.id === item.id)
      );

      const filteredRole = res?.map((item) => item.supervising_type);
      Promise.all([filteredSupervisors, filteredRole]).then(
        ([supervisorsArray, indexesArray]) => {
          setFilteredSupervisors(supervisorsArray);
          setFilteredRole(indexesArray);
          console.log("supervisorsArray", supervisorsArray);
          console.log("indexesArray", indexesArray);
        }
      );
    });
  }, [student]);

  if (id == null) {
    return (
      <Navigate to="/main_info/" state={{ modalOpen: true }} replace></Navigate>
    );
  }
  const Print = () => {
    //console.log('print');
    let printContents = document.getElementById("printablediv").innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    window.location.reload(false);
  };
  return (
    <>
      <div style={{ width: "95%" }}>
        <PrintButton onClick={Print}>طباعه</PrintButton>
      </div>
      <div id="printablediv">
        <Container>
          <LineDiv className="Signature">
            <RightDiv className="center">
              <div>جامعة الأزهر</div>
              <div>كلية الهندسة بنين - القاهرة</div>
              <div>ادارة الدراسات العليا </div>
            </RightDiv>
            <LeftDiv>
              التقرير العلمي عن عام
              <Span>20 /</Span>
              <Span>20</Span>
            </LeftDiv>
          </LineDiv>
          <LineDiv>
            <RightDiv>
              المقدم من السيد الاستاذ الدكتور /{" "}
              {
                find_main_suoervisor(filteredSupervisors, filteredRole)?.user
                  ?.arabicName
              }{" "}
            </RightDiv>
            <LeftDiv> المشرف الرئيسي على الرسالة </LeftDiv>
          </LineDiv>
          <LineDiv>
            <RightDiv>
              المشرف على رسالة / {student?.thesis?.degree} في الهندسة{" "}
            </RightDiv>
          </LineDiv>
          <LineDiv>
            <RightDiv>اسم الباحث / {student?.user?.arabicName}</RightDiv>
            <LeftDiv>الجنسية / {student?.user?.nationality}</LeftDiv>
          </LineDiv>
          <LineDiv>
            <RightDiv>
              القسم / {student?.last_qualification?.department?.display_name}
            </RightDiv>
            <LeftDiv>
              الشعبة / {student?.last_qualification?.branch?.display_name}
            </LeftDiv>
          </LineDiv>
          <LineDiv>
            <RightDiv>
              {" "}
              موضوع البحث باللغة العربية / {student?.thesis?.arabic_title}{" "}
            </RightDiv>
          </LineDiv>
          <LineDiv>
            <RightDiv>
              {" "}
              موضوع البحث باللغة الإنجليزية / {
                student?.thesis?.english_title
              }{" "}
            </RightDiv>
          </LineDiv>
          <LineDiv>
            <RightDiv>تاريخ التسجيل : {student?.registration_date}</RightDiv>
          </LineDiv>
          <LineDiv>
            <RightDiv>
              موافقة مجلس الكلية على التسجيل :{" "}
              {student?.thesis?.college_agree_date}
            </RightDiv>
            <LeftDiv>
              موافقة مجلس الجامعة على التسجيل :{" "}
              {student?.thesis?.university_agree_date}
            </LeftDiv>
          </LineDiv>
          <LineDiv>
            <RightDiv>تاريخ انتهاء المدة القانونية : </RightDiv>
            <LeftDiv>تاريخ انتهاء آخر مد للباحث : </LeftDiv>
          </LineDiv>
          <MarginDiv className="Line Big">لجنة الاشراف</MarginDiv>
          <StyledTable>
            <thead>
              <TableHeader>الرقم</TableHeader>
              <TableHeader>الاسم</TableHeader>
              <TableHeader>نوع الإشراف</TableHeader>
              <TableHeader>الدرجة</TableHeader>
              <TableHeader>جهة العمل </TableHeader>
            </thead>
            <tbody>
              {filteredSupervisors.map((supervisor, index) => (
                <tr>
                  <TableData>{index + 1}</TableData>
                  <TableData> {supervisor?.user?.arabicName}</TableData>
                  <TableData> {filteredRole[index]}</TableData>
                  <TableData>{supervisor?.degree}</TableData>
                  <TableData> {supervisor?.organization} </TableData>
                </tr>
              ))}
            </tbody>
          </StyledTable>
          <MarginDiv className="Line Big">حالة الباحث </MarginDiv>
          <StyledTable>
            <thead>
              <TableHeader>الرقم</TableHeader>
              <TableHeader>الحالة </TableHeader>
              <TableHeader>نعم</TableHeader>
              <TableHeader> لا </TableHeader>
            </thead>
            <tbody>
              <tr>
                <TableData>1</TableData>
                <TableData>
                  الباحث منتظم في الحضور ومثابلة السيد المشرف{" "}
                </TableData>
                <TableData></TableData>
                <TableData></TableData>
              </tr>
              <tr>
                <TableData>2</TableData>
                <TableData>الباحث مجتهد في جمع المادة العلمية </TableData>
                <TableData></TableData>
                <TableData></TableData>
              </tr>
              <tr>
                <TableData>3</TableData>
                <TableData>
                  الباحث جاد وقد انتهي من الكتابة في عدد ( ) باب من أبواب
                  الرسالة{" "}
                </TableData>
                <TableData></TableData>
                <TableData></TableData>
              </tr>
              <tr>
                <TableData>4</TableData>
                <TableData>
                  الباحث قارب على الانتهاء من رسالته خلال المدة القانونية{" "}
                </TableData>
                <TableData></TableData>
                <TableData></TableData>
              </tr>
              <tr>
                <TableData>5</TableData>
                <TableData>اوصى بانذار الباحث لعدم الجدية </TableData>
                <TableData></TableData>
                <TableData></TableData>
              </tr>
            </tbody>
          </StyledTable>
          <MarginDiv className="Line Big">توقيع لجنة الاشراف </MarginDiv>
          <StyledTable>
            <thead>
              <TableHeader>الرقم</TableHeader>
              <TableHeader>الاسم</TableHeader>
              <TableHeader>نوع الإشراف</TableHeader>
              <TableHeader>الدرجة</TableHeader>
              <TableHeader> التوقيع </TableHeader>
            </thead>
            <tbody>
              {filteredSupervisors.map((supervisor, index) => (
                <tr>
                  <TableData>{index + 1}</TableData>
                  <TableData> {supervisor?.user?.arabicName}</TableData>
                  <TableData> {filteredRole[index]}</TableData>
                  <TableData>{supervisor?.degree}</TableData>
                  <TableData></TableData>
                </tr>
              ))}
            </tbody>
          </StyledTable>
          <LineDiv className="margin">
            <RightDiv>تاريخ اعتماد مجلس القسم : </RightDiv>
            <LeftDiv> تاريخ اعتماد مجلس القسم : </LeftDiv>
          </LineDiv>
          <SignatureDiv className="Signature">
            <div>الدراسات العليا </div>
            <div>مدير الادارة </div>
            <div>مدير عام الكلية </div>
            <RightDiv>يعتمد أ.د/ عميد الكلية </RightDiv>
          </SignatureDiv>

          <CenteredDiv>
            <div>الادارة العامة للدراسات العليا والبحوث بالجامعة </div>
            <div>القيد صحيح والمدة القانونية سارية</div>
          </CenteredDiv>
          <div>
            تحرير في :<Span>/</Span>
            <Span>/ </Span>
            <Span>20 م</Span>
          </div>
          <SignatureDiv className="Signature">
            <div> الموظف المختص </div>
            <div> رئيس القسم </div>
            <div>مدير الادارة </div>
            <RightDiv>المدير العام </RightDiv>
          </SignatureDiv>

          <LineDiv className="marginTop margin">
            <RightDiv>
              <CenteredDiv>
                <div>موافقة :</div>
                <div>(أ.د / نائب رئيس الجامعة المختص )</div>
              </CenteredDiv>
            </RightDiv>
            <LeftDiv>
              <CenteredDiv>
                <div>يعتمد ،،،،،،،،،، </div>
                <div>(أ.د / رئيس الجامعة)</div>
              </CenteredDiv>
            </LeftDiv>
          </LineDiv>
          <div>
            مصادقة مجلس الجامعة بجلسته رقم ( ) بتاريخ
            <Span>/</Span>
            <Span>/ </Span>
            <Span>20 م</Span>
          </div>
        </Container>
      </div>
    </>
  );
};

export default ScientificReport;
