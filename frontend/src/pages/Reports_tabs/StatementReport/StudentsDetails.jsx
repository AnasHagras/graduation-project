const StudentsDetails = {
    name : "تامر علي صلاح ضبش"  ,
    nationality : "مصري" ,
    department : "هندسة الحاسبات والنظم" ,
    specification : "نظم المعلومات" ,
    facultyRegistration : "31-11-2017" ,
    universityRegistration : "29-3-2018" ,
    degree : "الماجستير" ,
    ResearchName : "نمذجة الطائرات لكي تطير الي مسافات بعيدة بدون الحاجة الي اي طيار اساسا" ,
    mainSupervisor : "محمد الفيصل عبد السلام احمد الرفاعي" ,
    mainSupervisorPlace : "هندسة الازهر" ,
    coSupervisor : "احمد محمد بهجت" ,
    coSupervisorPlace : "الفنية العسكرية" ,

}
export default StudentsDetails