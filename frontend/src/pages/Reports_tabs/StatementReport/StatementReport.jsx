import styled from "@emotion/styled";
import { makeStyles } from "@mui/material";
import React, { useEffect, useRef, useState } from "react";

import { useOutletContext, useParams, Navigate } from "react-router-dom";
import AlertModal from "../../../molecules/modals/AlertModal";
import { useSelector } from "react-redux";
import getSupervision from "../../../services/supervision/getSupervision";

const StatementSubmittedDiv = styled("div")(({ theme }) => ({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  fontWeight: "bold",
}));
const ButtonDev = styled("button")(({ theme }) => ({
  backgroundColor: "purple",
  color: "white",
  display: "block",
  margin: "0 auto",
  marginTop: "2rem",
  marginBottom: "2rem",
  borderRadius: "4rem",
  width: "15%",
  height: "3.5rem",
}));
const StyledInput = styled("input")(({ theme }) => ({
  width: "80%",
  height: "3.5rem",
  border: "2px solid black",
  borderRadius: "4rem",
  marginRight: "1rem",
  marginLeft: "1rem",
  // textAlign: "center",
  padding: "0 1rem",
  fontSize: "1.2rem",
  // fontWeight: "bold",
  "&:focus": {
    outline: "none",
  },
}));

const Container = styled("div")(({ theme }) => ({
  border: "2px solid black",
  padding: "30px",
  margin: "10px",
  margin: "auto",
  fontWeight: "bold",
  width: "95%",
}));

const LineDiv = styled("div")(({ theme }) => ({
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  marginBottom: "5px",
  "&.Signature": {
    marginBottom: "45px",
  },
}));
const RightDiv = styled("div")(({ theme }) => ({
  textAlign: "center",
}));
const LeftDiv = styled("div")(({ theme }) => ({
  width: "35%",
}));
const CenteredDiv = styled("div")(({ theme }) => ({
  margin: "20px auto",
  textAlign: "center",
  width: "fit-content",
  padding: "5px",
  "&.Line": {
    borderBottom: "2px solid black",
  },
}));
const MarginDiv = styled("div")(({ theme }) => ({
  margin: "20px 0 ",
  width: "fit-content",
  "&.Line": {
    borderBottom: "2px solid black",
  },
  "&.Big": {
    fontSize: "17px",
  },
}));
const StyledTable = styled("table")(({ theme }) => ({
  textAlign: "center",
  width: "80%",
  border: "2px solid black",
  margin: "20px auto 40px",
}));
const TableHeader = styled("th")(({ theme }) => ({
  border: "1px solid black",
  borderBottom: "2px solid black",
  backgroundColor: "#e4e5e5",
  padding: "7px",
}));
const TableData = styled("td")(({ theme }) => ({
  border: "1px solid black",
  padding: "7px",
}));
const PrintButton = styled("button")(({ theme }) => ({
  backgroundColor: "purple",
  color: "white",
  display: "block",
  margin: "0 auto",
  marginTop: "2rem",
  marginBottom: "2rem",
  borderRadius: "4rem",
  width: "15%",
  height: "3.5rem",
}));
function findCurrentEducationalYear() {
  var now = new Date();
  var currentMonth = now.getMonth() + 1; // Add 1 to get the month in the range 1-12
  var currentYear = now.getFullYear();

  var startYear, endYear;
  if (currentMonth < 9) {
    // The academic year is based on the previous year
    startYear = currentYear - 1;
    endYear = currentYear;
  } else {
    startYear = currentYear;
    endYear = currentYear + 1;
  }

  var academicYear = startYear + "-" + endYear;
  return academicYear;
}

const StatementReport = ({ StudentsDetails }) => {
  const [open, setOpen] = useState(false);
  const [title, setTit] = useState("");
  const [content, setContent] = useState("");
  const [_, setTitle] = useOutletContext();
  const id = useParams()["id"];
  const { students } = useSelector((state) => state.students);
  const student = students ? students[id] : {};
  const [filteredSupervisors, setFilteredSupervisors] = useState([]);
  const [filteredRole, setFilteredRole] = useState([]);
  const { supervisors } = useSelector((state) => state.supervisors);
  useEffect(() => {
    setTitle("إفادة");
    getSupervision({ thesis: student?.thesis?.id }).then((res) => {
      console.log("res", res);

      const filteredSupervisors = res?.map((item) =>
        supervisors.find((supervisor) => supervisor.user.id === item.id)
      );

      const filteredRole = res?.map((item) => item.supervising_type);
      Promise.all([filteredSupervisors, filteredRole]).then(
        ([supervisorsArray, indexesArray]) => {
          setFilteredSupervisors(supervisorsArray);
          setFilteredRole(indexesArray);
          console.log("supervisorsArray", supervisorsArray);
          console.log("indexesArray", indexesArray);
        }
      );
    });
  }, [student]);
  const [showReport, setShowReport] = useState(false);
  const [givenPerson, setGivenPerson] = useState("");

  const handleInputChange = (event) => {
    setGivenPerson(event.target.value);
  };
  const [value, setValue] = useState("");
  if (id == null) {
    return (
      <Navigate to="/main_info/" state={{ modalOpen: true }} replace></Navigate>
    );
  }
  const show = () => {
    console.log(showReport);
    if (givenPerson !== "") {
      setValue(givenPerson);
      setShowReport(true);
    } else {
      setTit("خطأ");
      setContent("برجاء ادخال الجهة المطلوب ارسال البيان اليها");
      setOpen(true);
    }
  };
  const Print = () => {
    //console.log('print');
    let printContents = document.getElementById("printablediv").innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    window.location.reload(false);
  };

  return (
    <>
      <AlertModal
        open={open}
        setOpen={setOpen}
        title={title}
        content={content}
        buttonText={"حسناً"}
      />
      <StatementSubmittedDiv>
        <div>يقدم البيان الي</div>
        <StyledInput type="text" onChange={handleInputChange} />
      </StatementSubmittedDiv>
      <ButtonDev onClick={show}>عرض</ButtonDev>
      {showReport && (
        <div>
          <PrintButton onClick={Print}>طباعه</PrintButton>
          <div id="printablediv">
            <Container>
              <LineDiv>
                <RightDiv>
                  <div>جامعة الازهر</div>
                  <div>كلية الهندسة بنين - القاهرة</div>
                  <div>إدارة الدراسات العليا </div>
                </RightDiv>
                <LeftDiv>سدد بالقسيمة رقم /</LeftDiv>
              </LineDiv>
              <CenteredDiv className="Line">
                <div>افادة</div>
                <div>قيد بالدراسات العليا</div>
              </CenteredDiv>
              <MarginDiv>تشهد كلية الهندسة بنين جامعة الازهر بأن : </MarginDiv>
              <LineDiv>
                <div> السيد المهندس / {student?.user?.arabicName} </div>
                <LeftDiv>الجنسية / {student?.user?.nationality} </LeftDiv>
              </LineDiv>
              <LineDiv>
                <div>
                  {" "}
                  بقسم / {
                    student?.last_qualification?.department.display_name
                  }{" "}
                  {student.last_qualification?.specialization
                    ? `(${student.last_qualification?.specialization})`
                    : ""}{" "}
                </div>
                <LeftDiv>
                  {" "}
                  في العام الجامعي / {findCurrentEducationalYear()}{" "}
                </LeftDiv>
              </LineDiv>
              <MarginDiv className="Line Big">
                {" "}
                تاريخ التسجيل : {student?.registration_date}
              </MarginDiv>
              <LineDiv>
                <div>الكلية / {student?.thesis?.college_agree_date} </div>
                <LeftDiv>
                  {" "}
                  الجامعة / {student?.thesis?.university_agree_date}{" "}
                </LeftDiv>
              </LineDiv>
              <MarginDiv>
                مقيد بالدراسات العليا لدرجة التخصص ( {student?.thesis?.degree} )
              </MarginDiv>
              <MarginDiv>
                {" "}
                في : {student?.last_qualification?.department.display_name}{" "}
                {student.last_qualification?.specialization
                  ? `(${student.last_qualification?.specialization})`
                  : ""}{" "}
              </MarginDiv>
              <MarginDiv>
                وقد أعطي الباحث هذا البيان بناء على طلبه وذلك لتقديمه الي :{" "}
                {value}{" "}
              </MarginDiv>
              <MarginDiv>
                {" "}
                موضوع البحث / {student?.thesis?.arabic_title}{" "}
              </MarginDiv>
              <MarginDiv className="Line Big"> تحت اشراف :</MarginDiv>
              <StyledTable>
                <thead>
                  <TableHeader>الرقم</TableHeader>
                  <TableHeader>لجنة الاشراف</TableHeader>
                  <TableHeader>الدرجة</TableHeader>
                  <TableHeader>الجهة </TableHeader>
                </thead>
                <tbody>
                  {filteredSupervisors.map((supervisor, index) => (
                    <tr>
                      <TableData>{index + 1}</TableData>
                      <TableData> {supervisor?.user?.arabicName}</TableData>
                      <TableData>{filteredRole[index]}</TableData>
                      <TableData> {supervisor?.organization} </TableData>
                    </tr>
                  ))}
                </tbody>
              </StyledTable>
              <MarginDiv>
                والكلية غير مسؤولة عن اساءة استخدام هذا البيان او اي قشط او
                تصحيح
              </MarginDiv>
              <CenteredDiv>وتفضلو بقبول وافر الاحترام والتقدير </CenteredDiv>

              <LineDiv className="Signature">
                <div>الدراسات العليا</div>
                <div>مدير عام للكلية </div>
                <div>وكيل الكلية للدراسات العليا </div>
              </LineDiv>
            </Container>
          </div>
        </div>
      )}
    </>
  );
};

export default StatementReport;
