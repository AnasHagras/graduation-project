import styled from "@emotion/styled";
import TableShowNumberOfThesis from "./TableShowNumberOfThesis";
import { useEffect, useState } from "react";
import SelectMenu from "../../../molecules/form/SelectMenu/SelectMenu";
import { useSelector } from "react-redux";
import getSupervisors from "../../../services/supervisors/listSupervisors";
import { useOutletContext } from "react-router-dom";

export const Container = styled("div")(({ theme }) => ({
  border: "2px solid black",
  padding: "20px",
  margin: "auto",
  fontWeight: "bold",
  width: "95%",
  fontSize: "14px",
  "&.smallFont": {
    fontSize: "12px",
  },
  "&.NotBold": {
    fontWeight: "normal",
  },
}));
const RightPart = styled("div")(({ theme }) => ({
  width: "fit-content",
  height: "20%",
  fontWeight: "bold",
  textAlign: "center",
}));

export const CenteredDiv = styled("div")(({ theme }) => ({
  margin: "5px auto",
  textAlign: "center",
  padding: "3px",
  "&.Line": {
    borderBottom: "2px solid black",
  },
  "&.smallDistance": {
    margin: "auto",
    padding: "1px",
  },
  width: "fit-content",
}));
const SelectMenuContainer = styled("div")(({ theme }) => ({
  display: "flex",
  marginLeft: "0px !important",
  marginBottom: "10px",
  justifyContent: "center",
  justifyItems: "center",
  alignItems: "center",
  // border: "1px solid green",
}));

const ShowButton = styled("button")(({ theme }) => ({
  backgroundColor: "purple",
  color: "white",
  display: "block",
  margin: "0 auto",
  marginTop: "2rem",
  marginBottom: "2rem",
  borderRadius: "4rem",
  width: "15%",
  height: "3.5rem",
}));

const NumberOfThesis = () => {
  const [_, setTitle] = useOutletContext();
  const [load, setLoad] = useState(true);
  useEffect(() => {
    setTitle("عدد الرسائل");
  }, []);
  const [data, setData] = useState({
    faculty: "",
    department: "",
    branch: "",
  });
  const { organization } = useSelector((state) => state.enums);

  const [loading, setLoading] = useState(true);

  const [TeachingMembers, setTeachingMembers] = useState([]);
  const { token } = useSelector((state) => state.auth);
  const [department, setDepartment] = useState("");
  const [branch, setBranch] = useState("");
  const handleClick = () => {
    setDepartment(
      getDisplayName("departments", data?.faculty, data?.department)
    );
    setBranch(getDisplayName("branches", data?.department, data?.branch));
    setLoading(false);
    getSupervisors({
      token: token,
      faculty: data.faculty,
      department: data.department,
      branch: data.branch,
    })
      .then((res) => {
        setLoad(true);
        setTeachingMembers(res);
        setLoad(false);
        console.log(load);
      })
      .catch((err) => {
        setLoad(false);
      });
  };

  // setShowContent(true);
  const getDisplayName = (name, parent, field) => {
    console.log(name, parent, field);
    let objects = [];
    objects = organization[name][parent];
    console.log("OBJ", objects);
    objects = objects?.filter((department) => {
      return department.name === field;
    });
    console.log("OBJ2", objects);
    return objects === undefined || !objects ? "" : objects[0]?.display_name;
  };
  return (
    <>
      <SelectMenuContainer>
        <SelectMenu
          label="الكلية"
          choices={organization["faculties"]}
          value={data.faculty}
          setValue={(faculty) => {
            setData({
              ...data,
              faculty: faculty,
              department: "",
              branch: "",
            });
          }}
          apply={true}
        />
        <SelectMenu
          label="القسم"
          choices={
            organization["departments"][data.faculty]
              ? organization["departments"][data.faculty]
              : []
          }
          value={data.department}
          setValue={(department) =>
            setData({
              ...data,
              department: department,
              branch: "",
            })
          }
          apply={true}
        />
        <SelectMenu
          label="الشعبة"
          choices={
            organization["branches"][data.department]
              ? organization["branches"][data.department]
              : []
          }
          value={data.branch}
          setValue={(branch) =>
            setData({ ...data, branch: branch, specialization: "" })
          }
          apply={true}
        />
      </SelectMenuContainer>
      <ShowButton onClick={handleClick}> عرض</ShowButton>
      <>
        {!loading ? (
          load ? (
            <div
              className="spinner-border"
              style={{
                color: "#5e72e4",
                width: "3rem",
                height: "3rem",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                textAlign: "center",
                margin: "auto",
              }}
            ></div>
          ) : (
            <Container>
              <div>
                <RightPart>
                  <p> جامعة الازهر </p>
                  <p> كلية الهندسة بنين - القاهرة </p>
                  <p>إدارة الدراسات العليا </p>
                </RightPart>
                <CenteredDiv>
                  <p>بيان بعدد الرسائل المشرف عليها اعضاء هيئة التدريس </p>
                  <div>
                    <p>
                      {`
                  ${department ? `قسم :${" "}` : ""}
                  ${department ? department : ""}
                `}
                    </p>
                    <p>
                      {`
                  ${branch ? `شعبة :${" "}` : ""}
                  ${branch ? branch : ""}
                  `}
                    </p>
                  </div>
                </CenteredDiv>
              </div>
              <TableShowNumberOfThesis TeachingMembers={TeachingMembers} />
            </Container>
          )
        ) : (
          <></>
        )}
      </>
    </>
  );
};
export default NumberOfThesis;
