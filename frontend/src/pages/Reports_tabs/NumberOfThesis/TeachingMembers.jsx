 const TeachingMembers = [
    {
        name: "بهاء محمد فتحي داغر السيد ",
        degree : "استاذ ",
        NumberOfMastersTheses : 5 ,
        NumberOfDoctoralTheses : 3 ,
    } ,
    {
        name: "محمد شوكت السيد عبد السلام ",
        degree : "استاذ",
        NumberOfMastersTheses : 5 ,
        NumberOfDoctoralTheses : 3 ,
    } ,
    {
        name: "كريم السباعي محمد ابراهيم علي ",
        degree : "دكتور",
        NumberOfMastersTheses : 5 ,
        NumberOfDoctoralTheses : 3 ,
    } ,
    {
        name: "علي السيد مرسي ابو عسل ",
        degree : "استاذ مساعد ",
        NumberOfMastersTheses : 5 ,
        NumberOfDoctoralTheses : 3 ,
    } ,
    {
        name: "محمد حسيني محمد السيد حسان  ",
        degree : "استاذ رئيس قسم ",
        NumberOfMastersTheses : 5 ,
        NumberOfDoctoralTheses : 3 ,
    } ,
    {
        name: "عمر يحيي يعقوب منتصر الغرابلي ",
        degree : "دكتوراه",
        NumberOfMastersTheses : 5 ,
        NumberOfDoctoralTheses : 3 ,
    } ,{
        name: "بهاء محمد فتحي داغر السيد ",
        degree : "دكتوراه",
        NumberOfMastersTheses : 5 ,
        NumberOfDoctoralTheses : 3 ,
    } ,
    {
        name: "محمد عبد العزيز السيد عوضين",
        degree : "دكتوراه",
        NumberOfMastersTheses : 5 , 
        NumberOfDoctoralTheses : 3 , 
    } , 
    {
        name: "السيد عبد العزيز حمدان سلام الدين",
        degree : "دكتوراه",
        NumberOfMastersTheses : 5 ,
        NumberOfDoctoralTheses : 3 ,
    } ,
    {
        name: "بهاء محمد فتحي داغر السيد ",
        degree : "دكتوراه",
        NumberOfMastersTheses : 5 ,
        NumberOfDoctoralTheses : 3 ,
    } ,


]
export default TeachingMembers;