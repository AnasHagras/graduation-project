import styled from "@emotion/styled";
import { Fragment } from "react";
import TeachingMembers from "./TeachingMembers";

export const StyledTable = styled("table")(({ theme }) => ({
  textAlign: "center",
  width: "98%",
  border: "2px solid black",
  margin: "5px auto",
}));
export const TableHeader = styled("th")(({ theme }) => ({
  border: "1px solid black",
  borderBottom: "2px solid black",
  backgroundColor: "#e4e5e5",
  padding: "4px",
}));
export const TableData = styled("td")(({ theme }) => ({
  border: "1px solid black",
  padding: "4px",
}));
const TableShowNumberOfThesis = ({ TeachingMembers }) => {
  console.log(TeachingMembers);
  var count = 1;
  return (
    <StyledTable>
      {/* <TeachingMembers /> */}
      <thead>
        <TableHeader>الرقم</TableHeader>
        <TableHeader>الاسم</TableHeader>
        <TableHeader>الدرجة</TableHeader>
        <TableHeader>عدد رسائل الماجيستير </TableHeader>
        <TableHeader>عدد رسائل الدكتوراه </TableHeader>
      </thead>
      <tbody>
        {TeachingMembers.map((member, i) => {
          return (
            <Fragment key={i}>
              <tr>
                <TableData>{count++}</TableData>
                <TableData>{member.user.arabicName}</TableData>
                <TableData>
                  {member.degree ? member.degree : "ـــــــــــ"}
                </TableData>
                <TableData>{member.number_of_master_theses}</TableData>
                <TableData>{member.number_of_PhD_theses}</TableData>
              </tr>
            </Fragment>
          );
        })}
      </tbody>
    </StyledTable>
  );
};

export default TableShowNumberOfThesis;
