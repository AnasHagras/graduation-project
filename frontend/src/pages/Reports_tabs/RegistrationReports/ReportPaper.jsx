import styled from "@emotion/styled";
import { Container } from "@mui/material";
import React, { Fragment, useEffect } from "react";
// import { useOutletContext } from "react-router-dom";
// import { useSelector } from "react-redux";
import { useState } from "react";
const StyledTable = styled("table")(({ theme }) => ({
  margin: "20px",
  textAlign: "center",
  width: "99%",
  border: "2px solid black",
}));
const TableHeader = styled("th")(({ theme }) => ({
  border: "1px solid black",
  borderBottom: "2px solid black",
  padding: "7px",
}));
const TableData = styled("td")(({ theme }) => ({
  border: "1px solid black",
  padding: "7px",
}));

const ContainerDiv = styled("div")(({ theme }) => ({
  width: "90%",
  margin: "auto",
  border: "solid 1px black",
  padding: "20px 50px",
  fontWeight: "bold",
}));
const CenterDiv = styled("div")(({ theme }) => ({
  width: "50%",
  margin: "auto",
  textAlign: "center",
}));
const InfoDiv = styled("div")(({ theme }) => ({
  display: "flex",
  justifyContent: "space-around",
  width: "100%",
  marginTop: "10px",
}));
const rightInfo = styled("div")(({ theme }) => ({
  width: "50%",
}));

const leftInfo = styled("div")(({ theme }) => ({
  width: "50%",
}));
const MessageSubject = styled("div")(({ theme }) => ({
  width: "90%",
  border: "solid 1px black",
  margin: "auto",
  marginTop: "10px",
  padding: "10px",
}));
const SpaceBetweenDiv = styled("div")(({ theme }) => ({
  display: "flex",
  justifyContent: "space-between",
  marginBottom: "40px",
}));
const DeanApproved = styled("div")(({ theme }) => ({
  display: "flex",
  marginBottom: "40px",
  flexDirection: "column",
  alignItems: "flex-end",
  marginTop: "20px",
}));
const ReportPaper = ({
  degree,
  department,
  ResearcherInformation,
  supervisor,
  role,
}) => {
  console.log(department);
  const [supervisors, setSupervisors] = useState([]);
  const [roles, setRoles] = useState([]);
  React.useEffect(() => {
    setSupervisors(supervisor);
    setRoles(role);
  }, [supervisor, role]);
  console.log("supervisors", supervisors);
  console.log("roles", roles);
  return (
    <ContainerDiv>
      <CenterDiv>
        <div>
          طلب الموافقة على تسجيل رسالة للحصول على{" "}
          {`
                  ${degree ? `درجة :${" "}` : ""}
                  ${degree ? degree : ""}
                `}
        </div>
        <div>{`
                  ${department ? `قسم :${" "}` : ""}
                  ${department ? department : ""}
                `}</div>
      </CenterDiv>
      <div>السيد الأستاذ الدكتور / عميد الكلية </div>
      <CenterDiv>تحية طيبة .... وبعد </CenterDiv>
      <div>
        المرجو التنبيه باتخاذ اللازم نحو اعتماد تسجيل موضوع الرسالة ولجنة
        الاشراف العلمي والافادة{" "}
      </div>
      <InfoDiv>
        <rightInfo>
          <div>اسم الباحث : {ResearcherInformation?.user?.arabicName}</div>
          <div>المؤهل الدراسي الحاصل عليه : {"بكالوريوس"} </div>
          <div>
            تاريخ الحصول : {ResearcherInformation?.last_qualification?.date}
          </div>
          <div>
            التخصص المطلوب التسجيل فيه :{" "}
            {ResearcherInformation?.last_qualification?.specialization}
          </div>
        </rightInfo>
        <leftInfo>
          <div> الجنسية : {ResearcherInformation?.user?.nationality}</div>
          <div>
            التقدير العام :{" "}
            {ResearcherInformation?.last_qualification?.grade.display_name}
          </div>
          <div>
            الجامعة : {ResearcherInformation?.last_qualification?.university}
          </div>
          <div>
            القسم :{" "}
            {
              ResearcherInformation?.last_qualification?.department
                ?.display_name
            }
          </div>
        </leftInfo>
      </InfoDiv>
      <MessageSubject>
        <CenterDiv>موضوع الرسالة</CenterDiv>
        <div>
          باللغة العربية : {ResearcherInformation?.thesis?.arabic_title}
        </div>
        <div>
          باللغة الانجليزية :{ResearcherInformation?.thesis?.english_title}
        </div>
      </MessageSubject>
      <StyledTable>
        <thead>
          <TableHeader>الرقم</TableHeader>
          <TableHeader>اسم المشرف</TableHeader>
          <TableHeader>نوع الاشراف</TableHeader>
          <TableHeader>الوظيفة</TableHeader>
          <TableHeader>جهة العمل </TableHeader>
        </thead>
        <tbody>
          {supervisors?.map((supervisor, index) => {
            return (
              <tr>
                <TableData>{index + 1}</TableData>
                <TableData>{supervisor.user.arabicName}</TableData>
                <TableData>{roles[index]}</TableData>
                <TableData>{supervisor?.degree}</TableData>
                <TableData>{supervisor?.organization}</TableData>
              </tr>
            );
          })}
        </tbody>
      </StyledTable>

      <SpaceBetweenDiv>
        <div>اعتبار التسجيل : </div>
        <div>رئيس القسم</div>
      </SpaceBetweenDiv>

      <div>
        {" "}
        روجعت البيانات والاجرائات صحيحة ومطابقة للوائح والطالب تقدم بجميع
        الاوراق والمستندات المطلوبة للتقديم كما تقدم باقرار بفيد عدم تسجيله في
        أي جامعة أخرى{" "}
      </div>

      <SpaceBetweenDiv>
        <div> الموظف المختص </div>
        <div>رئيس القسم</div>
        <div>مدير الادارة</div>
        <div>المدير العام</div>
      </SpaceBetweenDiv>
      <div>
        {" "}
        وافق مجلس الكلية على تسجيل الطالب المذكور عاليه بالعنوان المذكور ولجنة
        الأشراف في ضوء المبررات وقرار لجنة الدراسات العليا رقم (314){" "}
      </div>
      <div>
        وذلك بتاريخ ... / ... / ...... و على أن يتم مخاطبة السيد الأستاذ الدكتور
        نائب رئيس الجامعة للعرض على قضية الأستاذ الدكتور رئيس الجامعة لاتخاذ
        اللازم نحو تسجيل الطالب بعد سداد المصروفات
      </div>

      <DeanApproved>
        <div>يعتمد ...</div>
        <div>عميد الكلية</div>
      </DeanApproved>
    </ContainerDiv>
  );
};

export default ReportPaper;
