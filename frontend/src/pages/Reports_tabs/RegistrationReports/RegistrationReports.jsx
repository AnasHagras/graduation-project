import styled from "@emotion/styled";
import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useOutletContext, useParams } from "react-router-dom";
import SelectMenu from "../../../molecules/form/SelectMenu/SelectMenu";
import StyledSelectMenu from "../../../molecules/form/SelectMenu/StyledSelectMenu";
import ReportPaper from "./ReportPaper";
import { useSelector } from "react-redux";
import getSupervision from "../../../services/supervision/getSupervision";
import AlertModal from "../../../molecules/modals/AlertModal";
const SelectMenuContainer = styled("div")(({ theme }) => ({
  display: "flex",
  marginLeft: "0px !important",
  marginBottom: "10px",
  justifyContent: "center",
  justifyItems: "center",
  alignItems: "center",
}));

const ShowButton = styled("button")(({ theme }) => ({
  backgroundColor: "purple",
  color: "white",
  display: "block",
  margin: "0 auto",
  marginTop: "2rem",
  marginBottom: "2rem",
  borderRadius: "4rem",
  width: "15%",
  height: "3.5rem",
}));

const RegistrationReports = ({ ResearcherInformation }) => {
  const [open, setOpen] = useState(false);
  const [title, setTit] = useState("");
  const [content, setContent] = useState("");

  const id = useParams().id;
  const { students } = useSelector((state) => state.students);
  const student = students ? students[id] : {};
  const [filteredSupervisors, setFilteredSupervisors] = useState([]);
  const [filteredRole, setFilteredRole] = useState([]);
  const { supervisors } = useSelector((state) => state.supervisors);
  console.log(student);
  useEffect(() => {
    setTitle("مستندات باحث");
    getSupervision({ thesis: student?.thesis?.id }).then((res) => {
      console.log("res", res);

      const filteredSupervisors = res?.map((item) =>
        supervisors.find((supervisor) => supervisor.user.id === item.id)
      );

      const filteredRole = res?.map((item) => item.supervising_type);
      Promise.all([filteredSupervisors, filteredRole]).then(
        ([supervisorsArray, indexesArray]) => {
          setFilteredSupervisors(supervisorsArray);
          setFilteredRole(indexesArray);
          console.log("supervisorsArray", supervisorsArray);
          console.log("indexesArray", indexesArray);
        }
      );
    });
  }, [student]);
  const [data, setData] = useState([]);
  const [_, setTitle] = useOutletContext();
  useEffect(() => {
    setTitle("تقارير التسجيل");
  }, []);
  const [degreeValue, setDegreeValue] = useState("");
  const [departmentsValue, setDepartmentsValue] = useState("");
  const [showReport, setShowReport] = useState(false);

  const degreeHandlechange = (e) => {
    setDegreeValue(e);
  };
  const departmentsHandlechange = (e) => {
    setDepartmentsValue(e);
  };
  const toggleReport = () => {
    if (!showReport && (degreeValue === "" || departmentsValue === "")) {
      setOpen(true);
      setTit("خطأ");
      setContent("الرجاء اختيار الدرجة والقسم");
      return;
    }
    setShowReport(!showReport);
  };

  const getDisplayName = (name, parent, field) => {
    console.log(name, parent, field);
    let objects = [];
    objects = organization[name][parent];
    console.log("OBJ", objects);
    objects = objects?.filter((department) => {
      return department.name === field;
    });
    console.log("OBJ2", objects);
    return objects === undefined || !objects ? "" : objects[0]?.display_name;
  };

  const degrees = {
    master: "الماجستير",
    phd: "دكتوراه",
  };
  const { organization } = useSelector((state) => state.enums);

  return (
    <>
      <AlertModal
        open={open}
        setOpen={setOpen}
        title={title}
        content={content}
        buttonText={"حسناً"}
      />
      <SelectMenuContainer>
        <SelectMenu
          label="الدرجة"
          choices={[
            { id: 0, name: "master", display_name: "الماجستير" },
            { id: 1, name: "phd", display_name: "دكتوراه" },
          ]}
          value={degreeValue ? degreeValue : ""}
          setValue={degreeHandlechange}
          apply={true}
        />
        <SelectMenu
          label="القسم"
          choices={
            organization["departments"]["Engineering"]
              ? organization["departments"]["Engineering"]
              : []
          }
          value={departmentsValue ? departmentsValue : ""}
          setValue={departmentsHandlechange}
          apply={true}
        />
      </SelectMenuContainer>
      <>
        {!showReport && <ShowButton onClick={toggleReport}> عرض</ShowButton>}
        {showReport && (
          <ShowButton onClick={toggleReport} className="clear">
            {" "}
            إلغاء
          </ShowButton>
        )}
      </>
      {showReport && (
        <ReportPaper
          degree={degrees[degreeValue]}
          department={getDisplayName(
            "departments",
            "Engineering",
            departmentsValue
          )}
          ResearcherInformation={student}
          supervisor={filteredSupervisors}
          role={filteredRole}
        />
      )}
    </>
  );
};

export default RegistrationReports;
