export const ResearcherInformation  = 
{
    name : "محمد احمد ابو السعود حسن "  , 
    educationalQualification : "بكالوريوس"  , 
    date : "2021/10/10"  ,
    SpecializationRequired: "علوم حاسوب" ,
    Nationality: "فلسطيني" ,
    rate : "جيد جدا" ,
    university: "الازهر" ,
    department: "ميكانيكا الانتاج" ,
    thesisNameArabic: "تصميم وتصنيع ماكينة تشكيل البلاستيك" ,
    thesisNameEnglish: "Design and manufacture of a plastic forming machine" ,
    mainSupervisor:" أشرف محمد السيد المراكبي " ,
    coSupervisor: "جابر علي السيد محمد عوضين " ,
}
