import styled from "@emotion/styled";
import { Fragment } from "react";

export const StyledTable = styled("table")(({ theme }) => ({
  textAlign: "center",
  width: "98%",
  border: "2px solid black",
  margin: "5px auto",
}));
export const TableHeader = styled("th")(({ theme }) => ({
  border: "1px solid black",
  borderBottom: "2px solid black",
  backgroundColor: "#e4e5e5",
  padding: "4px",
}));
export const TableData = styled("td")(({ theme }) => ({
  border: "1px solid black",
  padding: "4px",
}));
const TableShowThesisNames = ({ ThesisNamesTable, load }) => {
  var count = 1;
  return (
    <>
      {load ? (
        <div
          className="spinner-border"
          style={{
            color: "#5e72e4",
            width: "3rem",
            height: "3rem",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            textAlign: "center",
            margin: "auto",
          }}
        ></div>
      ) : (
        <StyledTable>
          <thead>
            <TableHeader>الرقم</TableHeader>
            <TableHeader>عنوان الرساله </TableHeader>
            <TableHeader>عام التسجيل</TableHeader>
            <TableHeader>المشرف الرئيسي</TableHeader>
            <TableHeader>اسم الباحث</TableHeader>
          </thead>
          <tbody>
            {ThesisNamesTable.map((member, i) => {
              return (
                <Fragment key={i}>
                  <tr>
                    <TableData>{count++}</TableData>
                    <TableData>{member.title}</TableData>
                    <TableData>{member.date}</TableData>
                    <TableData>{member.supervisor}</TableData>
                    <TableData>{member.researcher}</TableData>
                  </tr>
                </Fragment>
              );
            })}
          </tbody>
        </StyledTable>
      )}
    </>
  );
};

export default TableShowThesisNames;
