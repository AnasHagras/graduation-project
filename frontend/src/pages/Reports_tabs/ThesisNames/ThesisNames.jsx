import React from "react";
import styled from "@emotion/styled";
import { useEffect, useState } from "react";
import SelectMenu from "../../../molecules/form/SelectMenu/SelectMenu";
import { useSelector } from "react-redux";
import TableShowThesisNames from "./TableShowThesisNames";
import StyledSelectMenu from "../../../molecules/form/SelectMenu/StyledSelectMenu";
import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import { useOutletContext } from "react-router-dom";
import AlertModal from "../../../molecules/modals/AlertModal";
import getSupervision from "../../../services/supervision/getSupervision";

export const Container = styled("div")(({ theme }) => ({
  border: "2px solid black",
  padding: "20px",
  margin: "auto",
  fontWeight: "bold",
  width: "95%",
  fontSize: "14px",
  "&.smallFont": {
    fontSize: "12px",
  },
  "&.NotBold": {
    fontWeight: "normal",
  },
}));
const RightPart = styled("div")(({ theme }) => ({
  width: "fit-content",
  height: "20%",
  fontWeight: "bold",
  textAlign: "center",
}));

export const CenteredDiv = styled("div")(({ theme }) => ({
  margin: "5px auto",
  textAlign: "center",
  padding: "3px",
  "&.Line": {
    borderBottom: "2px solid black",
  },
  "&.smallDistance": {
    margin: "auto",
    padding: "1px",
  },
  width: "fit-content",
}));
const SelectMenuContainer = styled("div")(({ theme }) => ({
  display: "flex",
  marginBottom: "10px",
  justifyContent: "center",
  alignContent: "center",
}));

const ShowButton = styled("button")(({ theme }) => ({
  backgroundColor: "purple",
  color: "white",
  display: "block",
  margin: "0 auto",
  marginTop: "2rem",
  marginBottom: "2rem",
  borderRadius: "4rem",
  width: "15%",
  height: "3.5rem",
}));
const ThesisNames = ({ ThesisNamesTable }) => {
  const { students } = useSelector((state) => state.students);
  const { supervisors } = useSelector((state) => state.supervisors);
  const [renderData, setRenderData] = useState([]);
  console.log("STUDENTS", students);
  const [open, setOpen] = useState(false);
  const [title, setTitle2] = useState("");
  const [content, setContent] = useState("");
  const [_, setTitle] = useOutletContext();
  const [degreeValue, setDegreeValue] = useState("");
  const [degree, setDegree] = useState(degreeValue);
  const [filteredSupervisors, setFilteredSupervisors] = useState([]);
  const [data, setData] = useState({
    type: "",
    faculty: "",
    department: "",
    branch: "",
  });
  const getDegreeName = (degree) => {
    if (degree === "master") {
      return "الماجستير";
    } else if (degree === "phd") {
      return "دكتوراه";
    }
    return "";
  };
  const filter = (degree) => {
    console.log("FILTERING");
    console.log("ST", students);
    const filter = students?.filter((student) => {
      // console.log(getDegreeName(degree));
      // console.log(student?.thesis?.degree);
      console.log("DEGREE", getDegreeName(degree));
      console.log(degree && student?.thesis?.degree);
      if (student?.thesis?.degree !== getDegreeName(degree)) {
        return false;
      }
      if (
        data?.faculty &&
        student?.last_qualification?.faculty?.name !== data?.faculty
      ) {
        return false;
      }
      if (
        data?.department &&
        student?.last_qualification?.department?.name !== data?.department
      ) {
        return false;
      }
      if (
        data?.branch &&
        student?.last_qualification?.branch?.name !== data?.branch
      ) {
        return false;
      }
      console.log(4);
      return true;
    });
    console.log("FILTERED", filter);
    // setFilteredStudents(filter);
    return filter;
  };
  const [filteredStudents, setFilteredStudents] = useState([]);
  const [filteredRole, setFilteredRole] = useState([]);
  const [load, setLoad] = useState(false);
  useEffect(() => {
    setTitle("اسماء الرسائل");
    setFilteredStudents(filter(degree));
    console.log("FILTERED STUDENTS", filteredStudents);
  }, [degree]);

  useEffect(() => {
    const fetchData = async () => {
      const render = [];
      for (const student of filteredStudents) {
        setLoad(true);
        const res = await getSupervision({ thesis: student?.thesis?.id });
        setLoad(false);
        const filteredSupervisors = res?.map((item) =>
          supervisors.find((supervisor) => supervisor.user.id === item.id)
        );

        const filteredRole = res?.map((item) => item.supervising_type);

        const supervisorsArray = await Promise.all(filteredSupervisors);
        const indexesArray = await Promise.all(filteredRole);

        render.push({
          title: student?.thesis?.arabic_title || "ـــــــــــــ",
          date: student?.registration_date || "ـــــــــــــ",
          supervisor:
            filteredSupervisors[
              filteredRole.findIndex((item) => item === "مشرف رئيسي")
            ]?.user?.arabicName || "ـــــــــــــ",
          researcher: student?.user?.arabicName || "ـــــــــــــ",
        });
      }
      setRenderData(render);
      // console.log("RENDER DATA", renderData);
    };
    // console.log("FFFF", filteredStudents);
    if (filteredStudents) fetchData();
  }, [filteredStudents]);
  const { organization } = useSelector((state) => state.enums);
  const [showContent, setShowContent] = useState(false);
  const handleClick = () => {
    if (!degreeValue) {
      setTitle2("خطأ");
      setContent("برجاء اختيار الدرجة");
      setOpen(true);
      return;
    }
    setShowContent(true);
    setDepartment(
      getDisplayName("departments", data?.faculty, data?.department)
    );
    setBranch(getDisplayName("branches", data?.department, data?.branch));
    setDegree(degreeValue);
    // setFilteredStudents(filter(degree));
  };
  const degrees = {
    master: "الماجستير",
    phd: "دكتوراه",
  };
  const degreeHandlechange = (e) => {
    setDegreeValue(e);
  };
  const [department, setDepartment] = useState("");
  const [branch, setBranch] = useState("");

  const getDisplayName = (name, parent, field) => {
    console.log(name, parent, field);
    let objects = [];
    objects = organization[name][parent];
    console.log("OBJ", objects);
    objects = objects?.filter((department) => {
      return department.name === field;
    });
    console.log("OBJ2", objects);
    return objects ? objects[0]?.display_name : "";
  };
  return (
    <>
      <AlertModal
        open={open}
        setOpen={setOpen}
        title={title}
        content={content}
        buttonText={"حسناً"}
      />
      <SelectMenuContainer>
        <SelectMenu
          label="الدرجة"
          choices={[
            { id: 0, name: "master", display_name: "الماجستير" },
            { id: 1, name: "phd", display_name: "الدكتوراه" },
          ]}
          value={degreeValue ? degreeValue : ""}
          setValue={degreeHandlechange}
          apply={true}
        />
        <SelectMenu
          label="الكلية"
          choices={organization["faculties"]}
          value={data.faculty}
          setValue={(faculty) => {
            setData({
              ...data,
              faculty: faculty,
              department: "",
              branch: "",
            });
          }}
          apply={true}
        />
        <SelectMenu
          label="القسم"
          choices={
            organization["departments"][data.faculty]
              ? organization["departments"][data.faculty]
              : []
          }
          value={data.department}
          setValue={(department) =>
            setData({
              ...data,
              department: department,
              branch: "",
            })
          }
          apply={true}
        />
        <SelectMenu
          label="الشعبة"
          choices={
            organization["branches"][data.department]
              ? organization["branches"][data.department]
              : []
          }
          value={data.branch}
          setValue={(branch) =>
            setData({ ...data, branch: branch, specialization: "" })
          }
          apply={true}
        />
      </SelectMenuContainer>
      <ShowButton onClick={handleClick}> عرض</ShowButton>
      {showContent && (
        <Container>
          <div>
            <RightPart>
              <p> جامعة الازهر </p>
              <p> كلية الهندسة بنين - القاهرة </p>
              <p>إدارة الدراسات العليا </p>
            </RightPart>
            <CenteredDiv>
              <p>
                بيانات بأسماء الرسائل المسجلة لدرجة : {degrees[degreeValue]}{" "}
              </p>
              <div>
                <p>
                  {`
                  ${department ? `قسم :${" "}` : ""}
                  ${department ? department : ""}
                `}
                </p>
                <p>
                  {`
                  ${branch ? `شعبة :${" "}` : ""}
                  ${branch ? branch : ""}
                  `}
                </p>
              </div>
            </CenteredDiv>
          </div>
          <TableShowThesisNames ThesisNamesTable={renderData} load={load} />
        </Container>
      )}
    </>
  );
};

export default ThesisNames;
