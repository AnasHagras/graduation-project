import React from "react";
import styled from "@emotion/styled";
import TableShowNumberOfThesis from "./TableShowInventoryReport";
import { useEffect, useState } from "react";
import SelectMenu from "../../../molecules/form/SelectMenu/SelectMenu";
import { useSelector } from "react-redux";
import TableShowInventoryReport from "./TableShowInventoryReport";
import TeachingMembers from "../NumberOfThesis/TeachingMembers";
import StyledSelectMenu from "../../../molecules/form/SelectMenu/StyledSelectMenu";
import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import { useOutletContext } from "react-router-dom";
import AlertModal from "../../../molecules/modals/AlertModal";
export const Container = styled("div")(({ theme }) => ({
  border: "2px solid black",
  padding: "20px",
  margin: "auto",
  fontWeight: "bold",
  width: "98%",
  fontSize: "14px",
  "&.smallFont": {
    fontSize: "12px",
  },
  "&.NotBold": {
    fontWeight: "normal",
  },
}));
const RightPart = styled("div")(({ theme }) => ({
  width: "fit-content",
  height: "20%",
  fontWeight: "bold",
  textAlign: "center",
}));
export const CenteredDiv = styled("div")(({ theme }) => ({
  margin: "5px auto",
  textAlign: "center",
  padding: "3px",
  "&.Line": {
    borderBottom: "2px solid black",
  },
  "&.smallDistance": {
    margin: "auto",
    padding: "1px",
  },
  width: "fit-content",
}));
const SelectMenuContainer = styled("div")(({ theme }) => ({
  display: "flex",
  marginLeft: "0px !important",
  marginBottom: "10px",
  justifyContent: "center",
  justifyItems: "center",
  alignItems: "center",
}));

const ShowButton = styled("button")(({ theme }) => ({
  backgroundColor: "purple",
  color: "white",
  display: "block",
  margin: "0 auto",
  marginTop: "2rem",
  marginBottom: "2rem",
  borderRadius: "4rem",
  width: "15%",
  height: "3.5rem",
}));

export const InventoryReport = ({ InventoryTable }) => {
  const { students } = useSelector((state) => state.students);
  const getDegreeName = (degree) => {
    if (degree === "master") {
      return "الماجستير";
    } else if (degree === "phd") {
      return "دكتوراه";
    }
    return "";
  };
  const [data, setData] = useState({
    type: "",
    faculty: "",
    department: "",
    branch: "",
  });
  const filter = (degree) => {
    // console.log("FILTERING");
    // console.log("ST", students);
    const filter = students?.filter((student) => {
      // console.log(getDegreeName(degree));
      // console.log(student?.thesis?.degree);
      console.log("DEGREE", getDegreeName(degree));
      console.log(degree && student?.thesis?.degree);
      if (student?.thesis?.degree !== getDegreeName(degree)) {
        return false;
      }
      if (
        data?.faculty &&
        student?.last_qualification?.faculty?.name !== data?.faculty
      ) {
        return false;
      }
      if (
        data?.department &&
        student?.last_qualification?.department?.name !== data?.department
      ) {
        return false;
      }
      if (
        data?.branch &&
        student?.last_qualification?.branch?.name !== data?.branch
      ) {
        return false;
      }
      console.log(4);
      return true;
    });
    console.log("FILTERED", filter);
    // setFilteredStudents(filter);
    return filter;
  };
  const [filteredStudents, setFilteredStudents] = useState([]);
  const [degree, setDegree] = useState("");
  useEffect(() => {
    setTitle("اسماء الرسائل");
    setFilteredStudents(filter(degree));
    console.log("FILTERED STUDENTS", filteredStudents);
  }, [degree]);
  const [load, setLoad] = useState(false);
  const [renderData, setRenderData] = useState({});
  useEffect(() => {
    const render = {};
    filteredStudents?.forEach((student) => {
      const registrationDate = student?.registration_date;
      if (render[registrationDate]) {
        // Increment existing values
        render[registrationDate].registered +=
          student?.thesis?.state === "مستمر" ? 1 : 0;
        render[registrationDate].Granted +=
          student?.thesis?.state === "منحت" ? 1 : 0;
        render[registrationDate].canceled +=
          student?.thesis?.state === "ملغي" ? 1 : 0;
      } else {
        // Insert new values
        render[registrationDate] = {
          year: registrationDate,
          registered: student?.thesis?.state === "مستمر" ? 1 : 0,
          Granted: student?.thesis?.state === "منحت" ? 1 : 0,
          canceled: student?.thesis?.state === "ملغي" ? 1 : 0,
        };
      }
    });
    setRenderData(render);
    console.log("RENDER", render);
  }, [filteredStudents]);

  const [_, setTitle] = useOutletContext();
  const [open, setOpen] = useState(false);
  const [content, setContent] = useState("");
  const [title, setTit] = useState("");
  useEffect(() => {
    setTitle("تقرير حصر");
  }, []);
  const { organization } = useSelector((state) => state.enums);

  const [showContent, setShowContent] = useState(false);
  const getDisplayName = (name, parent, field) => {
    console.log(name, parent, field);
    let objects = [];
    objects = organization[name][parent];
    console.log("OBJ", objects);
    objects = objects?.filter((department) => {
      return department.name === field;
    });
    console.log("OBJ2", objects);
    return objects ? objects[0]?.display_name : "";
  };
  const [department, setDepartment] = useState("");
  const [branch, setBranch] = useState("");
  const handleClick = () => {
    if (!degreeValue) {
      setTit("خطأ");
      setContent("برجاء اختيار الدرجة");
      setOpen(true);
      return;
    }
    setShowContent(true);
    setDepartment(
      getDisplayName("departments", data?.faculty, data?.department)
    );
    setBranch(getDisplayName("branches", data?.department, data?.branch));
    setDegree(degreeValue);
    setFilteredStudents(filter(degreeValue));
  };

  const [degreeValue, setDegreeValue] = useState("");
  const degrees = {
    master: "الماجستير",
    "Ph.D": "دكتوراه",
  };
  const degreeHandlechange = (e) => {
    setDegreeValue(e);
  };
  return (
    <>
      <AlertModal
        open={open}
        setOpen={setOpen}
        title={title}
        content={content}
        buttonText={"حسناً"}
      />
      <SelectMenuContainer>
        <SelectMenu
          label="الدرجة"
          choices={[
            { id: 0, name: "master", display_name: "الماجستير" },
            { id: 1, name: "phd", display_name: "الدكتوراه" },
          ]}
          value={degreeValue ? degreeValue : ""}
          setValue={degreeHandlechange}
          apply={true}
        />
        <SelectMenu
          label="الكلية"
          choices={organization["faculties"]}
          value={data.faculty}
          setValue={(faculty) => {
            setData({
              ...data,
              faculty: faculty,
              department: "",
              branch: "",
            });
          }}
          apply={true}
        />
        <SelectMenu
          label="القسم"
          choices={
            organization["departments"][data.faculty]
              ? organization["departments"][data.faculty]
              : []
          }
          value={data.department}
          setValue={(department) =>
            setData({
              ...data,
              department: department,
              branch: "",
            })
          }
          apply={true}
        />
        <SelectMenu
          label="الشعبة"
          choices={
            organization["branches"][data.department]
              ? organization["branches"][data.department]
              : []
          }
          value={data.branch}
          setValue={(branch) =>
            setData({ ...data, branch: branch, specialization: "" })
          }
          apply={true}
        />
      </SelectMenuContainer>
      <ShowButton onClick={handleClick}> عرض</ShowButton>

      {showContent && (
        <Container>
          <div>
            <RightPart>
              <p> جامعة الازهر </p>
              <p> كلية الهندسة بنين - القاهرة </p>
              <p>إدارة الدراسات العليا </p>
            </RightPart>
            <CenteredDiv>
              <p>بيان بعدد رسائل : {degrees[degreeValue]}</p>
              <div>
                <p>
                  {`
                  ${department ? `قسم :${" "}` : ""}
                  ${department ? department : ""}
                `}
                </p>
                <p>
                  {`
                  ${branch ? `شعبة :${" "}` : ""}
                  ${branch ? branch : ""}
                  `}
                </p>
              </div>
            </CenteredDiv>
          </div>
          <TableShowInventoryReport InventoryTable={renderData} />
        </Container>
      )}
    </>
  );
};
