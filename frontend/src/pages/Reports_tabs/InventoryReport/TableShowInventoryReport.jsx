import styled from "@emotion/styled";
import { Fragment } from "react";

export const StyledTable = styled("table")(({ theme }) => ({
  textAlign: "center",
  width: "90%",
  border: "2px solid black",
  margin: "5px auto",
}));
export const TableHeader = styled("th")(({ theme }) => ({
  border: "1px solid black",
  borderBottom: "2px solid black",
  backgroundColor: "#e4e5e5",
  padding: "4px",
}));
export const TableData = styled("td")(({ theme }) => ({
  border: "1px solid black",
  padding: "4px",
}));
const TableShowInventoryReport = ({ InventoryTable }) => {
  var count = 1;
  const inventoryArray = Object.values(InventoryTable);
  return (
    <StyledTable>
      <thead>
        <TableHeader>الرقم</TableHeader>
        <TableHeader>سنة التسجيل</TableHeader>
        <TableHeader>مسجل</TableHeader>
        <TableHeader>منح</TableHeader>
        <TableHeader>الغاء</TableHeader>
      </thead>
      <tbody>
        {inventoryArray.map((member, i) => {
          return (
            <Fragment key={i}>
              <tr>
                <TableData>{count++}</TableData>
                <TableData>{member.year}</TableData>
                <TableData>{member.registered}</TableData>
                <TableData>{member.Granted}</TableData>
                <TableData>{member.canceled}</TableData>
              </tr>
            </Fragment>
          );
        })}
      </tbody>
    </StyledTable>
  );
};

export default TableShowInventoryReport;
