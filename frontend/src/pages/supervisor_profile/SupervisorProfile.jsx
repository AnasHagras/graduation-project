import * as React from "react";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import EditIcon from "@mui/icons-material/Edit";
import SaveIcon from "@mui/icons-material/Save";
import { useSelector } from "react-redux";
import { useOutletContext, useParams } from "react-router-dom";
import SelectMenu from "../../molecules/form/SelectMenu/SelectMenu";
import { Button } from "@mui/material";
import updateSupervisor from "../../services/supervisors/updateSupervisor";
import ThreeDotsButton from "../students-table/ThreeDotsActions";
import AlertModal from "../../molecules/modals/AlertModal";
import { useDispatch } from "react-redux";
// import { listStudents } from "../../features/students/studentSlice";
import { listSupervisors } from "../../features/supervisor/supervisorSlice";
const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 20,
  },
}));
const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));
const StyledDiv = styled("div")(({ theme, index }) => ({
  fontSize: 20,
  backgroundColor:
    index % 2 === 0 ? theme.palette.common.white : theme.palette.grey[200],
  color: theme.palette.text.primary,
  padding: "8px",
  margin: "4px",
  textAlign: "center",
  display: "flex",
  justifyContent: "space-between",
  display: "flex",
  justifyContent: "space-between",
}));

export default function SupervisorProfile() {
  const dispatch = useDispatch();
  const { organization, cities } = useSelector((state) => state.enums);
  const [open, setOpen] = React.useState(false);
  const [content, setContent] = React.useState("");
  const [showButton, setShowButton] = React.useState(false);
  const [title, setTitle] = useOutletContext();
  console.log(cities);
  const degreeChoices = [
    { id: 0, name: "معيد", display_name: "معيد" },
    { id: 1, name: "باحث دراسات عليا", display_name: "باحث دراسات عليا" },
    { id: 2, name: "مدرس", display_name: "مدرس" },
    { id: 3, name: "مدرس مساعد", display_name: "مدرس مساعد" },
    { id: 4, name: "مدرس متفرغ", display_name: "مدرس متفرغ" },
    { id: 5, name: "استاذ", display_name: "استاذ" },
    { id: 6, name: "استاذ مساعد", display_name: "استاذ مساعد" },
    {
      id: 7,
      name: "استاذ مساعد متفرغ",
      display_name: "استاذ مساعد متفرغ",
    },
    { id: 8, name: "استاذ متفرغ", display_name: "استاذ متفرغ" },
  ];
  console.log(organization);
  const id = useParams().id;
  const [loading, setLoading] = React.useState(true);
  const supervisors = useSelector((state) => state.supervisors.supervisors);
  const [supervisor, setSupervisor] = React.useState(
    supervisors?.filter((supervisor) => supervisor.user.id == id)[0]
  );
  // React.useEffect(() => {
  //   if (!supervisor)
  //     setSupervisor(
  //       supervisors?.filter((supervisor) => supervisor.user.id == id)[0]
  //     );
  //   // setLoading(false);
  //   console.log("CC", supervisors);
  //   console.log(
  //     "SS",
  //     supervisors?.filter((supervisor) => supervisor.user.id == id)
  //   );
  // }, []);
  const students = [];
  supervisor?.supervised_students?.forEach((student) => {
    students.push(student.thesis.student.user);
  });
  const studentsList = useSelector((state) => state.students.students);
  const findIndex = (id) => {
    for (let i = 0; i < studentsList?.length; i++) {
      if (studentsList[i].user.id === id) {
        return i;
      }
    }
  };
  console.log(students);
  const [editing, setEditing] = React.useState(false);
  console.log("Degree : ", supervisor?.degree);
  React.useEffect(() => {
    setSupervisor(
      supervisors?.filter((supervisor) => supervisor.user.id == id)[0]
    );
  }, [supervisors]);
  React.useEffect(() => {
    setProfileData({
      name: ["الأسم", supervisor?.user?.arabicName],
      email: ["البريد الإلكتروني", supervisor?.user?.email],
      nationalID: ["الرقم القومي", supervisor?.user?.nationalID],
      degree: ["الدرجة", supervisor?.degree, degreeChoices],
      branch: ["الشعبة", supervisor?.branch?.name, organization?.branches],
      department: [
        "القسم",
        supervisor?.department?.name,
        organization["departments"]["Engineering"],
      ],
      // division: "هندسة النظم والحاسبات",
      specification: [
        "التخصص",
        supervisor?.specialization,
        organization?.specializations,
      ],
      // accurateSpecification: "",
      // organization: ["الجهة" , supervisor.organization],
      city: ["المدينة", cities[supervisor?.user.city - 1]?.name, cities],
      phone: ["رقم الهاتف", supervisor?.user.phoneNumber],
      masterThesis: [
        "عدد رسائل الماجستير",
        supervisor?.number_of_master_theses,
      ],
      phdThesis: ["عدد رسائل الدكتوراه", supervisor?.number_of_PhD_theses],
      supervised_students: ["الطلاب المشرف عليهم", students],
    });
    setTitle(
      " الملف الشخصي" +
        (" " + supervisor?.user.arabicName === undefined
          ? " "
          : supervisor?.user.arabicName
          ? " : " + supervisor?.user?.arabicName
          : "")
    );
  }, [supervisor]);
  const [profileData, setProfileData] = React.useState({
    name: ["الأسم", supervisor?.user?.arabicName],
    email: ["البريد الإلكتروني", supervisor?.user?.email],
    nationalID: ["الرقم القومي", supervisor?.user?.nationalID],
    degree: ["الدرجة", supervisor?.degree, degreeChoices],
    branch: ["الشعبة", supervisor?.branch?.name, organization?.branches],
    department: [
      "القسم",
      supervisor?.department?.name,
      organization["departments"]["Engineering"],
    ],
    // division: "هندسة النظم والحاسبات",
    specification: [
      "التخصص",
      supervisor?.specialization,
      organization?.specializations,
    ],
    // accurateSpecification: "",
    // organization: ["الجهة" , supervisor.organization],
    city: ["المدينة", cities[supervisor?.user.city - 1]?.name, cities],
    phone: ["رقم الهاتف", supervisor?.user.phoneNumber],
    masterThesis: ["عدد رسائل الماجستير", supervisor?.number_of_master_theses],
    phdThesis: ["عدد رسائل الدكتوراه", supervisor?.number_of_PhD_theses],
    supervised_students: ["الطلاب المشرف عليهم", students],
  });
  // console.log(supervisor);
  const thesisActions = [
    { to: "main_info/edit_researchers_data", title: "عرض الملف الشخصي" },
    { to: "main_info/message_cancel", title: "الغاء رسالة" },
    { to: "main_info/message_grant", title: "منح رسالة" },
    { to: "main_info/change_message", title: "تغير جوهري للرسالة" },
  ];

  // Docs
  // Statement
  // Scientific
  // Request approval
  const reportActions = [
    { to: "reports/researcher_document", title: "مستندات" },
    { to: "reports/statement", title: "افادة" },
    { to: "reports/scientific_report", title: "تقرير علمي" },
    { to: "reports/request_approvals", title: "طلب موافقات" },
  ];
  console.log(organization.branches);
  const handleEdit = () => {
    setEditing(true);
  };

  const handleSave = () => {
    setEditing(false);
    setShowButton(true);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setProfileData((prevState) => ({
      ...prevState,
      [name]: [prevState[[name]][0], value],
    }));
  };
  const getID = (list, target) => {
    if (list && Array.isArray(list)) {
      // Check if list is defined and an array
      for (let i = 0; i < list.length; i++) {
        if (list[i]?.name === target) {
          return list[i].id;
        }
      }
    }
    return null; // Return null if list is undefined or not an array, or if target is not found
  };

  const handleSaveData = (e) => {
    e.preventDefault();
    let data = {
      arabicName: profileData?.name[1],
      email: profileData?.email[1],
      nationalID: profileData?.nationalID[1],
      degree: profileData?.degree[1],
      branch: getID(
        organization?.branches?.[profileData.department[1]],
        profileData?.branch[1]
      ),
      department: getID(
        organization?.departments?.Engineering,
        profileData?.department[1]
      ),
      // division: profileData.division[1],
      specification: profileData?.specification[1],
      // accurateSpecification: profileData.accurateSpecification[1],
      // organization: profileData.organization[1],
      city: getID(cities, profileData?.city[1]),
      phoneNumber: profileData?.phone[1],
    };
    console.log(supervisor?.user.id);
    updateSupervisor({
      supervisor: data,
      supervisorID: supervisor?.user.id,
      token: localStorage.getItem("token"),
    })
      .then((res) => {
        console.log(res);
        setTitle("تم التعديل بنجاح");
        setContent("تم تعديل بيانات المشرف بنجاح");
        setOpen(true);
        dispatch(listSupervisors({ token: localStorage.getItem("token") }));
      })
      .catch((err) => {
        console.log(err);
        setTitle("حدث خطأ أثناء التعديل");
        setContent("حدث خطأ أثناء تعديل بيانات المشرف");
        setOpen(true);
      });
  };

  console.log(profileData.department[1]);
  const renderTableRows = () => {
    return Object.keys(profileData).map((key) => {
      if (
        key === "city" ||
        key === "degree" ||
        key === "department" ||
        key === "branch" ||
        key === "specification" ||
        key === "branch" ||
        key === "specification"
      ) {
        return (
          <StyledTableRow key={key}>
            <StyledTableCell component="th" scope="row" align="right">
              {profileData[key][0]}
            </StyledTableCell>
            <StyledTableCell align="right">
              <div
                className="section"
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <div className="section" style={{ width: "75%" }}>
                  <SelectMenu
                    label={profileData[key][0]}
                    choices={
                      key === "branch" &&
                      profileData[key][2][profileData.department[1]]
                        ? profileData[key][2][profileData.department[1]]
                        : key === "specification"
                        ? profileData[key][2][profileData.branch[1]]
                        : profileData[key][2]
                        ? profileData[key][2]
                        : []
                    }
                    apply={true}
                    value={profileData[key][1]}
                    disabled={!editing}
                    setValue={(value) =>
                      setProfileData((prevState) => ({
                        ...prevState,
                        [key]: [prevState[[key]][0], value, prevState[key][2]],
                      }))
                    }
                  />
                </div>
                {editing ? (
                  <SaveIcon
                    onClick={handleSave}
                    style={{ cursor: "pointer", marginLeft: "8px" }}
                  />
                ) : (
                  <EditIcon
                    onClick={handleEdit}
                    style={{ cursor: "pointer", marginLeft: "8px" }}
                  />
                )}
              </div>
            </StyledTableCell>
          </StyledTableRow>
        );
      } else if (key !== "supervised_students") {
        return (
          <StyledTableRow key={key}>
            <StyledTableCell component="th" scope="row" align="right">
              {profileData[key][0]}
            </StyledTableCell>
            <StyledTableCell align="right">
              {editing ? (
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <input
                    style={{
                      flex: 1,
                      border: "none",
                      background: "transparent",
                      color: "inherit",
                    }}
                    type="text"
                    name={key}
                    value={profileData[key][1]}
                    onChange={handleChange}
                  />
                  {key === "nationalID" ||
                  key === "masterThesis" ||
                  key === "phdThesis" ? (
                    <></>
                  ) : (
                    <SaveIcon
                      onClick={handleSave}
                      style={{ cursor: "pointer", marginLeft: "8px" }}
                    />
                  )}
                </div>
              ) : (
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  {profileData[key][1] === "" || profileData[key][1] === null
                    ? "ـــــــــــــــ"
                    : profileData[key][1]}
                  {key === "nationalID" ||
                  key === "masterThesis" ||
                  key === "phdThesis" ? (
                    <></>
                  ) : (
                    <EditIcon
                      onClick={handleEdit}
                      style={{ cursor: "pointer", marginLeft: "8px" }}
                    />
                  )}
                </div>
              )}
            </StyledTableCell>
          </StyledTableRow>
        );
      } else {
        return (
          <StyledTableRow key={key}>
            <StyledTableCell component="th" scope="row" align="right">
              {profileData[key][0]}
            </StyledTableCell>
            <StyledTableCell align="right">
              {profileData[key][1].length > 0 ? (
                profileData[key][1].map((student, i) => {
                  return (
                    <StyledDiv key={student.id} index={i}>
                      {student.arabicName}
                      <div>
                        <ThreeDotsButton
                          id={findIndex(student.id)}
                          items={thesisActions}
                        />
                        <ThreeDotsButton
                          id={findIndex(student.id)}
                          items={reportActions}
                        />
                      </div>
                    </StyledDiv>
                  );
                })
              ) : (
                <>لا يوجد طلاب</>
              )}
            </StyledTableCell>
          </StyledTableRow>
        );
      }
    });
  };
  return (
    <div>
      <AlertModal
        open={open}
        setOpen={setOpen}
        title={title}
        content={content}
        buttonText={"حسناً"}
      />
      {!supervisor ? (
        <div
          className="spinner-border"
          style={{
            color: "#5e72e4",
            width: "3rem",
            height: "3rem",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            textAlign: "center",
            margin: "auto",
          }}
        ></div>
      ) : (
        <>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 700 }} aria-label="customized table">
              <TableBody>{renderTableRows()}</TableBody>
            </Table>
          </TableContainer>
          {showButton && (
            <Button
              variant="contained"
              style={{
                backgroundColor: "purple",
                color: "white",
                display: "block",
                margin: "0 auto",
                marginTop: "2rem",
                marginBottom: "2rem",
                borderRadius: "4rem",
                width: "25%",
                height: "3.5rem",
              }}
              onClick={handleSaveData}
            >
              حفظ التعديلات
            </Button>
          )}
        </>
      )}
    </div>
  );
}
