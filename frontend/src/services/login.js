import axios from "axios";

const login = ({ email, password }) => {
  const REQUEST_URL = `${process.env.REACT_APP_API_URL}/authentication/login/`;
  console.log(process.env.REACT_APP_API_URL);
  const data = {
    email: email,
    password: password,
  };
  return new Promise((resolve, reject) => {
    axios
      .post(REQUEST_URL, data)
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export default login;
