import getStudent from "./getStudent";
import listStudents from "./listStudents";
import studentThesisRegister from "./studentThesisRegister";
const studentServices = {getStudent, listStudents, studentThesisRegister};
export { studentServices }