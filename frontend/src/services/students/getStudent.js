import axios from "axios";

const getStudent = ({ token, studentID }) => {
  token = localStorage.getItem("token");
  console.log("token: ", token);
  const REQUEST_URL = `${process.env.REACT_APP_API_URL}/students/retrieve/${studentID}/`;
  return new Promise((resolve, reject) => {
    axios
      .get(REQUEST_URL, {
        headers: {
          Authorization: `Token ${token}`,
        },
      })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export default getStudent;
