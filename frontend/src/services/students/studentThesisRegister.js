import axios from "axios";

const studentThesisRegister = ({ data, token }) => {
  const REQUEST_URL = `${process.env.REACT_APP_API_URL}/students/studentThesisRegister/`;
  return new Promise((resolve, reject) => {
    axios
      .post(
        REQUEST_URL, data,
        {
          headers: {
            Authorization: `Token ${token}`,
          },
        }
      )
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export default studentThesisRegister;
