import axios from "axios";

const getCanceledTheses = ({ token }) => {
  const REQUEST_URL = `${process.env.REACT_APP_API_URL}/thesis/canceled-theses/`;
  return new Promise((resolve, reject) => {
    axios
      .get(REQUEST_URL, {
        headers: {
          Authorization: `Token ${token}`,
        },
      })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export default getCanceledTheses;
