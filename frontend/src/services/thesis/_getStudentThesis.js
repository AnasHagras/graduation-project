import axios from "axios";

const getStudentThesis = (studentID) => {
  const REQUEST_URL = `${process.env.REACT_APP_API_URL}/thesis/${studentID}/`;
  const token = localStorage.getItem("token");
  return new Promise((resolve, reject) => {
    axios
      .get(REQUEST_URL, {
        headers: {
          Authorization: `Token ${token}`,
        },
      })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export default getStudentThesis;
