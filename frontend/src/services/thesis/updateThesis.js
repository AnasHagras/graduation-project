import axios from "axios";

const updateThesis = ({ token, thesisID, thesis }) => {
  const REQUEST_URL = `${process.env.REACT_APP_API_URL}/thesis/${thesisID}/`;
  return new Promise((resolve, reject) => {
    axios
      .patch(REQUEST_URL, thesis, {
        headers: {
          Authorization: `Token ${token}`,
        },
      })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export default updateThesis;
