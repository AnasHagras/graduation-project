import getBranches from "./getBranches";
import getCities from "./getCities";
import getDepartments from "./getDepartments";
import getFaculties from "./getFaculties";
import getGrades from "./getGrades";
import getMilitary from "./getMilitary";
import getSpecializations from "./getSpecializations";
import getOrganization from "./getOrganization";
const enumsServices = {
  getBranches,
  getCities,
  getDepartments,
  getFaculties,
  getGrades,
  getMilitary,
  getSpecializations,
  getOrganization,
};
export { enumsServices };
