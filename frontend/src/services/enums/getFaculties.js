import axios from "axios";

const getFaculties = ({ token }) => {
  // const token = localStorage.getItem("token");
  const REQUEST_URL = `${process.env.REACT_APP_API_URL}/faculty/`;
  return new Promise((resolve, reject) => {
    axios
      .get(REQUEST_URL, {
        headers: {
          Authorization: `Token ${token}`,
        },
      })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export default getFaculties;
