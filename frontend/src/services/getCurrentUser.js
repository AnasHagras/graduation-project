import axios from "axios";

const getCurrentUser = ({ token }) => {
  const REQUEST_URL = `${process.env.REACT_APP_API_URL}/users/get-current-user/`;
  // const token = localStorage.getItem("token");
  console.log("TOKEN : ", token);
  return new Promise((resolve, reject) => {
    axios
      .get(REQUEST_URL, {
        headers: {
          Authorization: `Token ${token}`,
        },
      })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export default getCurrentUser;
