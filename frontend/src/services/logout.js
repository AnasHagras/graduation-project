import axios from "axios";

const logout = ({ token }) => {
  const REQUEST_URL = `${process.env.REACT_APP_API_URL}/authentication/logout/`;
  // const token = localStorage.getItem("token");
  return new Promise((resolve, reject) => {
    axios
      .post(REQUEST_URL, null, {
        headers: {
          Authorization: `Token ${token}`,
        },
      })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export default logout;
