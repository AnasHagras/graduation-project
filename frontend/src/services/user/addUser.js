import axios from "axios";

const addUser = ({
  email,
  password,
  passwordConfirm,
  gender,
  type,
  arabicName,
  englishName,
  // token,
}) => {
  const REQUEST_URL = `${process.env.REACT_APP_API_URL}/users/`;
  const token = localStorage.getItem("token");
  const data = {
    email: email,
    password: password,
    passwordConfirm: passwordConfirm,
    gender: gender,
    role: type,
    arabicName: arabicName,
    englishName: englishName,
  };
  return new Promise((resolve, reject) => {
    axios
      .post(REQUEST_URL, data, {
        headers: {
          Authorization: `Token ${token}`,
        },
      })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export default addUser;
