import login from "./login";
import logout from "./logout";
import getCurrentUser from "./getCurrentUser";
const services = { login, logout, getCurrentUser };
export { services };
