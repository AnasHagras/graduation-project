import getStudentSupervisors from "./getSupervisors";
import getSupervisors from "./listSupervisors";
const supervisorServices = { getStudentSupervisors, getSupervisors };
export { supervisorServices };
