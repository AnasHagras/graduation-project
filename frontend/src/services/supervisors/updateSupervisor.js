import axios from "axios"

const updateSupervisor = ({ supervisor, supervisorID, token }) => {
    console.log(supervisorID)
    console.log(supervisor)
    console.log(token)
    const REQUEST_URL = `${process.env.REACT_APP_API_URL}/supervisors/update/${supervisorID}/`;
    return new Promise((resolve, reject) => {
      axios
        .patch(REQUEST_URL, supervisor, {
          headers: {
            Authorization: `Token ${token}`,
          },
        })
        .then((response) => {
          resolve(response.data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

export default updateSupervisor;