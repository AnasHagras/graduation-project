import axios from "axios";

const getStudentSupervisors = ({ studentID, token }) => {
  // const token = localStorage.getItem("token");
  const REQUEST_URL = `${process.env.REACT_APP_API_URL}/students/student-supervisors/`;
  return new Promise((resolve, reject) => {
    axios
      .get(REQUEST_URL, {
        params: {
          id: studentID,
        },
        headers: {
          Authorization: `Token ${token}`,
        },
      })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export default getStudentSupervisors;
