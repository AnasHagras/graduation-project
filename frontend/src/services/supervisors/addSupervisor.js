import axios from "axios";

const addSupervisor = ({ data, token }) => {
  const REQUEST_URL = `${process.env.REACT_APP_API_URL}/supervisors/register/`;
  return new Promise((resolve, reject) => {
    axios
      .post(
        REQUEST_URL,
        {
          arabicName: data.arabicName,
          englishName: data.englishName,
          email: data.email,
          gender: data.gender,
          password: data.password,
          password_confirmation: data.passwordConfirm,
          supervisor_profile: {
            degree: data.degree,
            organization: data.organization,
            faculty: data.faculty.id,
            department: data.department.id,
          },
        },
        {
          headers: {
            Authorization: `Token ${token}`,
          },
        }
      )
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export default addSupervisor;
