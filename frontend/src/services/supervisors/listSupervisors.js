import axios from "axios";

const getSupervisors = ({ faculty, department, branch, thesis }) => {
  const token = localStorage.getItem("token");
  const REQUEST_URL = `${process.env.REACT_APP_API_URL}/supervisors/`;

  let params = {}

  if (faculty){
    params = {...params, faculty: faculty}
  }
  
  if(department){
    params = {...params, department: department}
  }

  if(branch){
    params = {...params, branch: branch}
  }

  if(thesis){
    params = {...params, thesis: thesis}
  }

  return new Promise((resolve, reject) => {
    axios
      .get(REQUEST_URL, {
        headers: {
          Authorization: `Token ${token}`,
        },
        params: params,
      })
      .then((response) => {
        resolve(response.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export default getSupervisors;
