import "./App.css";
import {
  BrowserRouter,
  Navigate,
  Outlet,
  Route,
  Routes,
  useLocation,
} from "react-router-dom";
// import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Login from "./pages/Login/Login";
import SearchSupervisor from "./pages/Search/SearchSupervisor";
import {
  StudentRegistraionForm,
  Students,
  Supervisor,
  TestServices,
} from "../src/pages";

import SupervisorsTable from "./pages/supervisors-table/SupervisorsTable";
// import TestServices from "./pages/TestServices";
import StudentsTable from "./pages/students-table/StudentsTable";
import { MajorChangePage } from "../src/pages";

import Profile from "./pages/ResearcherProfile/ResearcherProfile";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { refreshAuth } from "./features/authentication/authSlice";
import RequireAuth from "./components/requireAuth";
import Testing from "./organisms/CNavbar";
import { Box, CircularProgress } from "@mui/material";
import CNavbar from "./organisms/CNavbar";
import CSidebar from "./organisms/CSidebar";
import Reports from "./routes/reports";
import MainInfoRoutes from "./routes/main_info";
import ReportsRoutes from "./routes/reports";
import UsersDataRoutes from "./routes/users_data";
// import {MajorChangePage} from "../src/pages/Major thesis change";
// import MajorChangePage from "../src/pages";
import "./index.css";
import { listSupervisors } from "./features/supervisor/supervisorSlice";
import { listStudents } from "./features/students/studentSlice";
import {
  listCities,
  listGrades,
  listMilitary,
  listOrganization,
} from "./features/enums/enumsSlice";
import { useHistory } from "react-router-dom";

function App() {
  const {
    initialLoading: authLoading,
    pending,
    refreshPending,
    token,
  } = useSelector((state) => state.auth);
  const { initialLoading: enumsLoading } = useSelector((state) => state.enums);
  console.log("TOKEN FROM APP: ", token);
  const dispatch = useDispatch();
  useEffect(() => {
    console.log("APP USE EFFECT CALLED");
    async function fetchData() {
      dispatch(refreshAuth({ token: localStorage.getItem("token") }));
      dispatch(listSupervisors({ token: token }));
      dispatch(listCities({ token: token }));
      dispatch(listMilitary({ token: token }));
      dispatch(listOrganization({ token: token }));
      dispatch(listGrades({ token: token }));
      dispatch(listStudents({ token: token }));
    }
    fetchData();
  }, [token]);
  return (
    <div className="App">
      {authLoading || enumsLoading ? (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignContent: "center",
            alignItems: "center",
            height: "90vh",
          }}
        >
          <CircularProgress></CircularProgress>
        </Box>
      ) : (
        <Routes>
          <Route path="/" element=<WithNavbar></WithNavbar>>
            <Route path="/home" element=<h1>Home</h1>></Route>

            {/* require auth pages*/}
            <Route path="/" element={<RequireAuth></RequireAuth>}>
              <Route path="/" element={<Home></Home>}></Route>
              {MainInfoRoutes}
              {ReportsRoutes}
              {UsersDataRoutes}
            </Route>
            <Route path="login" element={<Login />} />
          </Route>
          <Route
            path="studentRegister"
            element={<StudentRegistraionForm></StudentRegistraionForm>}
          ></Route>
        </Routes>
      )}
    </div>
  );
}

const Home = () => {
  return <Navigate to="/main_info"></Navigate>;
};

const WithNavbar = ({ fixedNavbar }) => {
  return (
    <>
      <CNavbar fixed={fixedNavbar} />
      {/* <Box sx={{ marginTop: "60px" }}></Box> */}
      <Outlet />
    </>
  );
};

export default App;
