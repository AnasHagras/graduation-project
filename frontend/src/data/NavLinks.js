const links = [
  { name: "البيانات الأساسية", to: "main_info" },
  { name: "التقارير", to: "reports" },
  { name: "بيانات المستخدمين", to: "users_data" },
];

export default links;
