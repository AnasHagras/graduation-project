export const studentMapping = {
  nationalID: "الرقم القومي",
  name: "الأسم",
  department: "القسم",
  division: "الشعبة",
  specialization: "التخصص",
  registrationCycleDate: "دورة التسجيل",
  departmentCouncilDate: "مجلس القسم",
  collegeCouncilDate: "مجلس الكلية",
  universityCouncilDate: "مجلس الجامعة",
};
