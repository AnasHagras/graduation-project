import StyledCheckGroup from "./StyledCheckGroup";
import CheckInput from "../../../molecules/form/CheckInput/CheckInput";
export const CheckGroup = ({ choices, values, setValues }) => {
  return (
    <StyledCheckGroup>
      {choices.map((choice, id) => {
        return (
          <CheckInput
            key={id}
            label={choice.display_name}
            checked={values.includes(choice.name)}
            setChecked={() => {
              if (values.includes(choice.name)) {
                setValues(values.filter((value) => value !== choice.name));
              } else {
                setValues([...values, choice.name]);
              }
            }}
          />
        );
      })}
    </StyledCheckGroup>
  );
};
