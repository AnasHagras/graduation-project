import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";

export const StyledCheckGroup = styled(Box)(({ theme }) => ({
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  justifyContent: "space-between",
}));

export default StyledCheckGroup;
