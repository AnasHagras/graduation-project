import * as React from "react";
import PropTypes from "prop-types";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import MenuIcon from "@mui/icons-material/Menu";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { Link } from "@mui/material";
import { NavLink, Outlet, useNavigate } from "react-router-dom";
import CNavLink from "../atoms/CNavLink";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../features/authentication/authSlice";
import links from "../data/NavLinks";

const drawerWidth = 240;

const pages = links;

function CNavbar(props) {
  const { wd, title } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const navigate = useNavigate();
  const [tab, setTab] = React.useState(window.location.pathname.split("/")[1]);

  const { isLoggedIn, token } = useSelector((state) => state.auth);
  const authDispatch = useDispatch();

  const handleLogout = () => {
    authDispatch(logout({ token: token }));
  };

  const handleChange = (e, tabName) => {
    setTab(tabName);
  };

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };

  const drawer = (
    <Box
      onClick={handleDrawerToggle}
      sx={{
        textAlign: "center",
        border: "none",
        outline: "none",
      }}
    >
      <List>
        {pages.map((item) => (
          <CNavLink
            key={item.to}
            link={item}
            className={item.to === tab ? "active" : ""}
            onClick={(e) => handleChange(e, item.to)}
          ></CNavLink>
        ))}
      </List>
    </Box>
  );

  const container = wd !== undefined ? () => window().document.body : undefined;

  return (
    <Box>
      {/* <AppBar component="nav" sx={{ position: "fixed" }}> */}
      <Toolbar
        sx={{
          display: "flex",
          justifyContent: "space-between",
          backgroundColor: "black",
          alignItems: "center",
          position: "fixed",
          width: "100%",
          top: 0,
          zIndex: "1515",
        }}
      >
        <IconButton
          color="black"
          aria-label="open drawer"
          edge="start"
          onClick={handleDrawerToggle}
          sx={{ mr: 2, display: { sm: "none" } }}
        >
          <MenuIcon />
        </IconButton>
        <Box sx={{ display: { xs: "none", sm: "flex" }, gap: "5px" }}>
          {pages.map((item) => (
            <CNavLink
              key={item.to}
              link={item}
              className={item.to === tab ? "active" : ""}
              onClick={(e) => handleChange(e, item.to)}
            ></CNavLink>
          ))}
        </Box>
        {isLoggedIn ? (
          <Button
            variant="contained"
            color="error"
            sx={{ position: "fixed", left: "25px" }}
            onClick={() => {
              handleLogout();
            }}
          >
            تسجيل الخروج
          </Button>
        ) : (
          <Button
            variant="contained"
            onClick={() => {
              navigate("/login");
            }}
          >
            تسجيل الدخول
          </Button>
        )}
      </Toolbar>
      {/* </AppBar> */}

      {/* for small screens */}
      <Box component="nav">
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: "block", sm: "none" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}
          anchor={"top"}
        >
          {drawer}
        </Drawer>
      </Box>
    </Box>
  );
}

CNavbar.propTypes = {
  window: PropTypes.func,
};

export default CNavbar;
