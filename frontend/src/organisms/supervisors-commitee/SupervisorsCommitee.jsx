import { Button } from "@mui/material";
import SupervisorsCommiteeTable from "../../molecules/supervisors-commite-table/SupervisorsCommiteeTable";
import StyledSupervisorsCommitee from "./StyledSupervisorsCommitee";
import { useState } from "react";
import SearchPickModal from "../../molecules/search-pick-modal/SearchPickModal";
import { useSelector } from "react-redux";

const SupervisorsCommitee = ({
  supervisors,
  setSupervisors,
  supervisionTypes,
  setSupervisionType,
  disabled,
  loading,
}) => {
  const [modalOpen, setModalOpen] = useState(false);
  const { supervisors: allSupervisors } = useSelector(
    (state) => state.supervisors
  );
  return (
    <StyledSupervisorsCommitee>
      <Button
        style={{
          margin: "10px",
          height: "55px",
          fontSize: "17px",
          fontWeight: "bold",
        }}
        disabled={disabled}
        variant="outlined"
        onClick={() => setModalOpen(true)}
      >
        أضف إلى لجنة الإشراف
      </Button>
      <SupervisorsCommiteeTable
        supervisors={supervisors}
        setSupervisors={setSupervisors}
        supervisionTypes={supervisionTypes}
        setSupervisionTypes={setSupervisionType}
        disabled={disabled}
        loading={loading}
      />
      <SearchPickModal
        open={modalOpen}
        handleClose={() => setModalOpen(false)}
        items={allSupervisors
          .filter(
            (supervisor) =>
              !supervisors ||
              !supervisors.some((s) => s.user.id === supervisor.user.id)
          )
          .map((supervisor) => ({
            name: supervisor.user.arabicName,
            value: supervisor,
          }))}
        pickedItems={supervisors}
        setPickedItems={setSupervisors}
      />
    </StyledSupervisorsCommitee>
  );
};
export default SupervisorsCommitee;
