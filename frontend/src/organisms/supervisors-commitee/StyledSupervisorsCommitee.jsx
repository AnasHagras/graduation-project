import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";

export const StyledSupervisorsCommitee = styled(Box)(({ theme }) => ({
  width: "100%",
  display: "flex",
  flexDirection: "column",
  marginBottom: "10px",
  padding: "10px",
}));

export default StyledSupervisorsCommitee;
