import React from "react";
import MyTableRow from "../../molecules/table-row/MyTableRow";
import { Table, TableBody, TableHead } from "@mui/material";
import TableRow from "@mui/material/TableRow";
import { useNavigate } from "react-router";
import TableContainer from "@mui/material/TableContainer";
import Paper from "@mui/material/Paper";
import TableFooter from "@mui/material/TableFooter";
import TablePagination from "@mui/material/TablePagination";
import TableCell from "@mui/material/TableCell";
import TabelPaginationActions from "../../atoms/Tabel Pagination/TabelPaginations";
import { useLocation } from "react-router";
import { makeStyles } from "@mui/styles";
// import { styled } from "@mui/material/styles";
function convertToArabicNumbers(number) {
  const arabicDigits = ["٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩"];
  return number.toString().replace(/\d/g, (digit) => arabicDigits[digit]);
}
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "flex-end",
    borderTop: `1px solid ${theme.palette.divider}`,
    padding: theme.spacing(2),
    borderBottom: "none",
    marginTop: theme.spacing(2),
    borderTop: "none",
  },
  toolbar: {
    minHeight: "unset",
  },
  spacer: {
    flex: "1 1 100%",
  },
  caption: {
    whiteSpace: "nowrap",
  },
  selectRoot: {
    marginRight: theme.spacing(1),
    marginLeft: theme.spacing(1),
  },
  selectIcon: {
    top: "50%",
    marginTop: "-12px",
  },
  input: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    "& input": {
      padding: "10px",
      textAlign: "right",
    },
  },
  displayedRows: {
    marginBottom: "0 !important",
  },
  selectLabel: {
    marginBottom: "0px !important",
  },
}));
function MyTable({ header, body, loading }) {
  const classes = useStyles();
  const navigate = useNavigate();
  const location = useLocation();
  const currentPath = location.pathname;
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  console.log(body);
  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - body.length) : 0;

  console.log(body.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage));
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <>
      <TableContainer component={Paper}>
        {loading ? (
          <div
            className="spinner-border"
            style={{
              color: "#5e72e4",
              width: "3rem",
              height: "3rem",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              textAlign: "center",
              margin: "auto",
            }}
          ></div>
        ) : (
          <Table>
            <TableHead>
              <MyTableRow cells={header}></MyTableRow>
            </TableHead>
            <TableBody>
              {(rowsPerPage > 0
                ? body.slice(
                    page * rowsPerPage,
                    page * rowsPerPage + rowsPerPage
                  )
                : body
              ).map((row, index) => (
                <>
                  {currentPath === "/main_info/supervisors_data" ? (
                    <MyTableRow
                      onClick={(e) =>
                        navigate(`/main_info/supervisor_profile/${[row[5]]}`)
                      }
                      cells={row}
                    >
                      {console.log()}
                    </MyTableRow>
                  ) : (
                    <MyTableRow cells={row} />
                  )}
                  {console.log(row, index)}
                </>
              ))}
              {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        )}
      </TableContainer>
      <TablePagination
        classes={{
          root: classes.root,
          toolbar: classes.toolbar,
          spacer: classes.spacer,
          caption: classes.caption,
          selectRoot: classes.selectRoot,
          selectIcon: classes.selectIcon,
          input: classes.input,
          displayedRows: classes.displayedRows,
          selectLabel: classes.selectLabel,
        }}
        rowsPerPageOptions={[5, 10, 25, { label: "الكل", value: -1 }]}
        colSpan={3}
        count={body.length}
        rowsPerPage={rowsPerPage}
        labelRowsPerPage="الصفوف في الصفحة"
        labelDisplayedRows={({ from, to, count }) =>
          `عرض ${convertToArabicNumbers(from)}-${convertToArabicNumbers(
            to
          )} من ${convertToArabicNumbers(count)} عنصر`
        }
        page={page}
        SelectProps={{
          inputProps: {
            "aria-label": "rows per page",
          },
          native: true,
        }}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
        ActionsComponent={TabelPaginationActions}
      />
    </>
  );
}

export default MyTable;
