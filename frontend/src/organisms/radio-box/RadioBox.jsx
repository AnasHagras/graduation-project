import React from 'react'
import { FormControl } from '@mui/material'
import {MyRadioGroup} from '../../molecules'
import radio_fields from "../../molecules/radio-buttons-group/RadioFields.json";
function RadioBox() {
  return (
    <FormControl>
        <MyRadioGroup dirction={"row"} radio_fields={radio_fields} type={"search"}/>
    </FormControl>
  )
}

export default RadioBox