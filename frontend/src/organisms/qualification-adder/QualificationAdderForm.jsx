import React, { useState } from "react";
import FormInput from "../../molecules/form/FormInput/FormInput";
import SelectMenu from "../../molecules/form/SelectMenu/SelectMenu";
import Button from "@mui/material/Button";
import { useSelector } from "react-redux";

export const QualificationAdderForm = ({
  qualifications,
  setQualifications,
}) => {
  const [qualification, setQualification] = useState({
    degree: "",
    university: "",
    grade: "",
    date: "",
  });

  const { grades } = useSelector((state) => state.enums);

  const handleSubmit = (event) => {
    event.preventDefault();
    setQualifications([...qualifications, qualification]);
    setQualification({
      degree: "",
      university: "",
      grade: "",
      date: "",
    });
  };

  return (
    <form onSubmit={handleSubmit} className={"section"}>
      <SelectMenu
        required={true}
        label="المؤهل "
        choices={[
          { name: "بكالوريوس", display_name: "بكالوريوس" },
          { name: "ماجستير", display_name: "ماجستير" },
          { name: "دكتوراه", display_name: "دكتوراه" },
        ]}
        value={qualification.degree}
        setValue={(degree) => {
          // console.log("degree : ", degree);
          setQualification({
            ...qualification,
            degree: degree,
          });
        }}
      />
      <FormInput
        required={true}
        label="جهة التخرج"
        value={qualification.university}
        setValue={(university) =>
          setQualification({ ...qualification, university: university })
        }
      />
      <SelectMenu
        required={true}
        label="التقدير"
        choices={grades}
        value={qualification.grade}
        setValue={(grade) =>
          setQualification({ ...qualification, grade: grade })
        }
      />
      <FormInput
        required={true}
        label="تاريخ التخرج"
        value={qualification.date}
        setValue={(date) => setQualification({ ...qualification, date: date })}
      />

      <Button
        style={{
          height: "50px",
          width: "100px",
          margin: "auto",
          fontSize: "17px",
        }}
        variant="contained"
        type="submit"
      >
        إضافة
      </Button>
    </form>
  );
};
export default QualificationAdderForm;
