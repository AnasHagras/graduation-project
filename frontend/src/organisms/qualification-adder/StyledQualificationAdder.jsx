import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";

export const StyledQualificationAdder = styled(Box)(({ theme }) => ({
  width: "100%",
  minHeight: "300px",
  border: "1px solid #000",
  borderRadius: "5px",
  backgroundColor: "lightgrey",
  margin: "10px",
  display: "flex",
  flexDirection: "column",
}));

export default StyledQualificationAdder;
