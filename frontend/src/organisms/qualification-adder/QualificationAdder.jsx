import QHeader from "../../molecules/qualification-adder/Header/QHeader";
import QRow from "../../molecules/qualification-adder/Row/QRow";
import { QualificationAdderForm } from "./QualificationAdderForm";
import StyledQualificationAdder from "./StyledQualificationAdder";

export const QualificationAdder = ({ qualifications, setQualifications }) => {
  return (
    <div>
      <QualificationAdderForm
        qualifications={qualifications}
        setQualifications={setQualifications}
      />
      <StyledQualificationAdder>
        <QHeader></QHeader>
        {qualifications.map((qualification, index) => (
          <QRow
            key={index}
            qualification={qualification}
            index={index}
            handleDelete={() =>
              setQualifications(
                qualifications.filter((q) => q !== qualification)
              )
            }
          ></QRow>
        ))}
      </StyledQualificationAdder>
    </div>
  );
};
export default QualificationAdder;
