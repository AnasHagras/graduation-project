import { Button, Grid, Snackbar, Typography } from "@mui/material";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { login } from "../features/authentication/authSlice";
import { Navigate } from "react-router-dom";
import {
  CAlert,
  CLoadingButton,
  CTextField,
  BorderedContainer,
  TextLink,
  CSnackBar,
} from "../atoms";

import homePage from "../data/Config";

const LoginForm = () => {
  const [state, setState] = useState({ email: "", password: "" });

  const [open, setOpen] = useState(false);
  const [snack, setSnack] = useState({ message: "", severity: "success" });

  const { isLoggedIn, pending, errorMessage } = useSelector(
    (state) => state.auth
  );

  const authDispatch = useDispatch();

  const handleLogin = (e) => {
    handleSnack("Logging you in", "warning");
    authDispatch(login({ state }));
  };

  const handleSnack = (message, severity) => {
    setSnack({ message: message, severity: severity });
    setOpen(true);
  };

  const handleChange = (e) => {
    setState({ ...state, [e.target.name]: e.target.value });
  };

  return isLoggedIn ? (
    <Navigate to={homePage} />
  ) : (
    <>
      <CSnackBar
        open={open}
        setOpen={setOpen}
        message={snack.message}
        severity={snack.severity}
      ></CSnackBar>
      <BorderedContainer direction={"column"} rowSpacing={3}>
        <Grid item>
          <Typography variant="h5">
            التسجيل عن طريق البريد الإلكتروني
          </Typography>
        </Grid>
        <Grid item>
          <CTextField
            label="البريد الإلكتروني"
            value={state["email"]}
            name="email"
            onChange={handleChange}
          ></CTextField>
        </Grid>
        <Grid item>
          <CTextField
            name="password"
            label="كلمة المرور"
            type="password"
            value={state["password"]}
            onChange={handleChange}
          ></CTextField>
        </Grid>
        {/* <Grid item>
          <TextLink href="password_reset">هل نسيت كلمة المرور ؟</TextLink>
        </Grid> */}
        {errorMessage && (
          <Grid item>
            <CAlert message={errorMessage} severity="error"></CAlert>
          </Grid>
        )}
        <Grid item xs={6}>
          <CLoadingButton
            onClick={handleLogin}
            disabled={pending}
            sx={{
              width: "100%",
            }}
            normalContent={"تسجيل الدخول"}
            loadingContent={"جاري تسجيل الدخول"}
          ></CLoadingButton>
        </Grid>
      </BorderedContainer>
    </>
  );
};

export default LoginForm;
