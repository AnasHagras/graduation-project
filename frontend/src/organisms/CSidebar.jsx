import {
  Box,
  Button,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  styled,
  ToggleButton,
  useMediaQuery,
} from "@mui/material";
import * as React from "react";
import { NavLink, Outlet, useNavigate } from "react-router-dom";
import { SideBarItem } from "../atoms";
import CNavLink from "../atoms/CNavLink";
import MenuIcon from "@mui/icons-material/Menu";
import { useState } from "react";
import { useTheme } from "@emotion/react";
import { NavHashLink } from "react-router-hash-link";
import { useDispatch } from "react-redux";
import { clearFilters } from "../features/search/searchSlice";
import AlertModal from "../molecules/modals/AlertModal";
const DesktopSidebar = styled(Box)(() => ({
  height: "100%",
  border: "1px solid rgb(0 0 0 / 20%)",
  borderBottom: "none",
  marginTop: "64px",
  "&.opened": {
    width: "300px",
  },
  "&.closed": {
    width: "60px",
    overflow: "hidden",
  },
  "flex-direction": "column",
  position: "fixed",
  top: "67px",
  // right: "0",
  bottom: "0",
  "z-index": "99",
  backgroundColor: "rgb(40 36 61)",
}));
const MobileSidebar = styled(Drawer)(({ theme }) => ({}));

const Header = styled(Box)(({ theme }) => ({
  backgroundColor: "rgb(40 36 61)",
  paddingTop: "5px",
  paddingBottom: "5px",
  paddingLeft: "8px",
  paddingRight: "10px",
  width: "100%",
  display: "flex",
  "flex-direction": "row",
  alignItems: "center",
  alignContent: "center",
  justifyContent: "space-between",
  position: "fixed",
  top: "64px",
  "z-index": "99",
  [theme.breakpoints.down("sm")]: {
    paddingLeft: "5px",
  },
}));

const Content = styled(Box)(({ theme }) => ({
  // flex: "1",
  // border: "1px solid green",
  width: "77%",
  // minHeight: "100vh",
  position: "absolute",

  // marginRight: "20px",
  "&.opened": {
    // marginRight: "320px",
    right: "330px",
  },
  "&.closed": {
    width: "90%",
    right: "100px",
    // marginRight: "80px",
  },
  "&.phone": {
    width: "100%",
  },
  overflow: "auto",
  marginTop: "150px",
  marginBottom: "20px",
  // marginLeft: "300px",
  [theme.breakpoints.down("sm")]: {
    margin: "70px 10px 10px 10px",
  },
}));

const Title = styled(Box)(() => ({
  color: "white",
  fontSize: "25px",
  padding: "10px",
}));

const CSidebarItem = styled(ListItem)(() => ({
  color: "#e7e3fc",
  "&:hover": {
    cursor: "pointer",
  },
  "&.activeSideBar": {
    background:
      "linear-gradient(76.8deg, rgb(158 96 165) 3.6%, rgb(75 71 122) 90.4%)",
    color: "white",
  },
  borderStartEndRadius: "30px",
  borderEndEndRadius: "30px",
  width: "90%",
  transitionDuration: "0.3s",
  margin: "10px",
}));

export default function CSidebar({ items }) {
  const [desktopOpen, setDesktopOpen] = useState(false);
  const [phoneOpen, setPhoneOpen] = useState(false);
  const [title, setTitle] = useState("");
  const handleDesktopToggle = () => {
    setDesktopOpen(!desktopOpen);
  };

  const handlePhoneToggle = () => {
    setPhoneOpen(!phoneOpen);
  };

  const navigate = useNavigate();

  const theme = useTheme();
  const phone = useMediaQuery(theme.breakpoints.down("sm"));
  const tablet = useMediaQuery(theme.breakpoints.down("md"));
  const desktop = useMediaQuery(theme.breakpoints.up("lg"));
  const [alertModalTitle, setAlertModalTitle] = useState("");
  const [alertModalContent, setAlertModalContent] = useState("");
  const [alertModalButtonText, setAlertModalButtonText] = useState("");
  const [alertModalOpen, setAlertModalOpen] = useState(false);
  const handleAlertModal = ({ title, content, buttonText }) => {
    setAlertModalTitle(title);
    setAlertModalContent(content);
    setAlertModalButtonText(buttonText);
    setAlertModalOpen(true);
  };
  const container =
    window !== undefined ? () => window.document.body : undefined;

  const handleItemClicked = (e, dispatch, to, name) => {
    // setTitle(name);
    dispatch(clearFilters());
    if (to === "supervisor_profile") {
      navigate("supervisors_data");
      handleAlertModal({
        title: "عفوا",
        content: "من فضلك قم بأختيار مشرف",
        buttonText: "حسنا",
      });
    } else if (
      to === "edit_researchers_data" ||
      to === "message_cancel" ||
      to === "message_grant" ||
      to === "change_message"
    ) {
      navigate("");
      handleAlertModal({
        title: "عفوا",
        content: "من فضلك قم بأختيار طالب",
        buttonText: "حسنا",
      });
    } else {
      // setTitle(name);
      navigate(to);
    }
  };
  const handlePhoneClicked = (e, dispatch, to, name) => {
    // setTitle(name);
    handleItemClicked(e, dispatch, to, name);
    setPhoneOpen(false);
  };

  React.useEffect(() => {
    setPhoneOpen(false);
    if (phone) {
      setDesktopOpen(false);
    }
    if (desktop) {
      setDesktopOpen(true);
    }
  }, [phone, desktop]);
  const dispatch = useDispatch();

  return (
    <>
      <AlertModal
        open={alertModalOpen}
        setOpen={setAlertModalOpen}
        title={alertModalTitle}
        content={alertModalContent}
        buttonText={alertModalButtonText}
      ></AlertModal>
      <Box
        sx={{
          width: "100%",
          display: "flex",
          backgroundColor: "white",
          boxShadow: "0px 2px 4px -1px rgb(0 0 0 / 20%)",
          position: "relative",
        }}
      ></Box>
      <Header>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <IconButton
            sx={{
              // border: "1px solid green",
              width: "fit-content",
              height: "fit-content",
              color: "rgb(194 119 110)",
              padding: "0px",
              marginLeft: phone ? "6px" : "10px",
            }}
            width="50%"
            disableRipple
            onClick={!phone ? handleDesktopToggle : handlePhoneToggle}
          >
            <MenuIcon />
          </IconButton>
          <Title sx={{ fontSize: phone ? "20px" : "25px" }}>{title}</Title>
        </div>
      </Header>
      <Box>
        <DesktopSidebar
          className={desktopOpen ? "opened" : "closed"}
          display={phone ? "none" : "block"}
        >
          <List sx={desktopOpen ? { paddingLeft: "15px" } : {}}>
            {items.map((item, index) => {
              return (
                <CSidebarItem
                  key={index}
                  onClick={(e) =>
                    handleItemClicked(e, dispatch, item.to, item.name)
                  }
                  className={
                    item.to === window.location.pathname.split("/")[2] ||
                    (!window.location.pathname.split("/")[2] && item.to == "")
                      ? "activeSideBar"
                      : ""
                  }
                  name={item.name}
                  sx={
                    desktopOpen ? {} : { padding: "5px", width: "fit-content" }
                  }
                >
                  <>
                    {item.icon ? item.icon : <MenuIcon></MenuIcon>}
                    {desktopOpen && (
                      <Box sx={{ marginRight: "5px" }} className="itemText">
                        {item.name}
                      </Box>
                    )}
                  </>
                </CSidebarItem>
              );
            })}
          </List>
        </DesktopSidebar>
      </Box>

      <MobileSidebar
        container={container}
        anchor="right"
        variant="temporary"
        open={phoneOpen}
        onClose={handlePhoneToggle}
        ModalProps={{
          keepMounted: true,
        }}
        sx={{
          display: { xs: "block", sm: "none" },
          "& .MuiDrawer-paper": { boxSizing: "border-box", width: "300px" },
        }}
      >
        <List sx={desktopOpen ? { paddingLeft: "15px" } : {}}>
          {items.map((item, index) => {
            return (
              <CSidebarItem
                key={index}
                onClick={(e) =>
                  handlePhoneClicked(e, dispatch, item.to, item.name)
                }
                name={item.name}
                className={
                  item.to === window.location.pathname.split("/")[2] ||
                  (!window.location.pathname.split("/")[2] && item.to == "")
                    ? "activeSideBar"
                    : ""
                }
              >
                <>
                  <MenuIcon></MenuIcon>
                  <Box sx={{ marginRight: "5px" }} className="itemText">
                    {item.name}
                  </Box>
                </>
              </CSidebarItem>
            );
          })}
        </List>
      </MobileSidebar>
      <Content className={desktopOpen ? "opened" : phone ? "phone" : "closed"}>
        <Outlet context={[title, setTitle]} />
      </Content>
    </>
  );
}
