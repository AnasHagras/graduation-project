import FormControl from '@mui/material/FormControl';
import { MyInputLabel } from '../../atoms';
import { Select } from '../../molecules';
import { makeStyles } from "@mui/styles";
import StyledSelectMenu from "../../molecules/form/SelectMenu/StyledSelectMenu";
const useStyles = makeStyles((theme) => ({
    selectMenuRoot: {
      direction: "rtl !important",
      "& legend": { textAlign: "right" },
      // border: "1px solid green",
      "& .MuiInputBase-input": {
        textAlign: "right",
        direction: "rtl",
        // marginRight: "10px",
        // margin: "0px",
      },
      "& label": {
        transformOrigin: "right !important",
        left: "inherit !important",
        right: "1.80rem !important",
        color: "#807D7B",
        fontWeight: "bolder",
        overflow: "hidden",
        textOverflow: "ellipsis", // Add this line to truncate the label text
        whiteSpace: "nowrap",
      },
      "& .MuiSelect-icon": {
        right: "auto", // Set the right position to auto
        left: "8px", // Set the left position to adjust the arrow
      },
      "& .MuiSelect-select, & .MuiSelect-outlined.MuiInputBase-input.MuiOutlinedInput-input": {
        paddingRight: "10px", // Update the paddingRight to 10px
      },
      "& .Mui-focused .MuiInputLabel-root": {
        marginRight: "0px",
      },
    },
  }));
export default function DropBox({items , type , data , setData}) {
    const tabel = {
        "Department": "من فضلك أختر القسم",
        "Degree" : "من فضلك أختر المؤهل",
        "Status" : "من فضلك أختر الحالة",
        "Edit" : "نوع التعديل"
    }
    const classes = useStyles();
    return (
        <StyledSelectMenu sx={{ minWidth: 120, flex: 1 }}
        className={classes.selectMenuRoot}
        >
        <FormControl sx={{ m: 1, width: 300 }}
        style={{ flex: 1 }}
        // className={classes.selectMenuRoot}
        >
            <MyInputLabel label={tabel[type]}></MyInputLabel>
            <Select items={items} type={type} data = {data} setData = {setData} label={tabel[type]}/>
        </FormControl>
        </StyledSelectMenu>
    );
}