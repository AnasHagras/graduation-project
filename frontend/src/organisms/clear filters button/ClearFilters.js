import React from "react";
import { useDispatch } from "react-redux";
import { IconButton } from "@mui/material";
import { Clear } from "@mui/icons-material";
import { clearFilters } from "../../features/search/searchSlice"; // Update with your actual file path

const ClearFiltersButton = () => {
  const dispatch = useDispatch();

  const handleClearFilters = () => {
    dispatch(clearFilters());
  };

  return (
    <IconButton style={{ color: "black", fontWeight: "bold" }} onClick={handleClearFilters} aria-label="Clear Filters">
      <Clear />
    </IconButton>
  );
};

export default ClearFiltersButton;
