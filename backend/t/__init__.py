import pyodbc
from users.models import User
from students.models import Student, StudentProfile, Qualification, postAttachment
from thesis.models import Thesis
import random
import string
from enums.models import *
import random
from supervisors.models import Supervisor, SupervisorProfile
from supervisory.models import Supervision


def generate_random_number(end, start=0):
    random_number = random.randint(start, end)
    return random_number


def generate_random_string(length):
    letters = string.ascii_letters
    random_string = "".join(random.choice(letters) for _ in range(length))
    return random_string


print("PRE CONNECTION")
server = ".\SQLEXPRESS"
database = "HANDASAPOSTSTAFF"  # Replace with the actual database name
conn_str = f"DRIVER={{SQL Server}};SERVER={server};DATABASE={database};Trusted_Connection=yes;"


def printQuery(query, nums=None):
    conn = pyodbc.connect(conn_str)
    cursor = conn.cursor()
    cursor.execute(query)
    headers = [column[0] for column in cursor.description]
    rows = cursor.fetchall()
    # print("Headers : ", headers)
    # print("Rows : ")
    # for row in range(nums if nums is not None else len(rows)):
    #     print(rows[row])
    return {"headers": headers, "rows": rows}


def get_department(oldID):
    return Department.objects.get(oldID=oldID)


def get_branch(oldID):
    return Branch.objects.get(oldID=oldID)


# def get_all_enums():
#     # faculty
#     engineering = Faculty.objects.create(display_name="engineering", oldID=1)
#     # departments
#     obj = printQuery("SELECT * FROM _Department;")
#     headers = obj["headers"]
#     rows = obj["rows"]
#     for row in rows:
#         Department.objects.create(display_name=row[1], oldID=row[0])
#     # branches
#     obj = printQuery("SELECT * FROM _Brances;")
#     headers = obj["headers"]
#     rows = obj["rows"]
#     for row in rows:
#         Specialization.objects.create(display_name=row[2], oldID=row[0],department=get_department(row[1]))
#     # specialization
#     obj = printQuery("SELECT * FROM _PostSpecialization;")
#     headers = obj["headers"]
#     rows = obj["rows"]
#     for row in rows:
#         Specialization.objects.create(display_name=row[2], oldID=row[0],branch=get_branch(row[1]))
from data import loadOrganization, loadGrades, loadCities, loadMilitary

if User.objects.filter(email="a@a.com").first() == None:
    admin = User.objects.create_superuser(email="a@a.com", password="a")


# get all students
def t():
    obj = printQuery("SELECT * FROM _PostStdData ORDER BY ID ASC;", 1)
    headers = obj["headers"]
    rows = obj["rows"]
    print("Loading Students.....")
    print("Loading Thesis.....")
    print("Loading Qualifications......")
    print("Loading Attachments......")
    for row in rows:
        randomText = generate_random_string(10)
        # Create Student First (USER)
        student = Student.objects.create_student(
            email=randomText + "@gmail.com",
            password=randomText,
            role="STUDENT",
            arabicName=row[1],
            englishName=row[2],
            gender="ذكر" if row[3] == 1 else "أنثي",
            nationality=row[5],
            date_of_birth=row[6].date() if row[6] is not None else None,
            birthPlace=row[7],
            currentPlace=row[11],
            phoneNumber=row[13],
            nationalID=row[20],
        )
        # Create Student Profile
        try:
            military = Military.objects.get(id=int(row[40]))
        except Exception:
            military = None
        studentProfile = StudentProfile.objects.create(
            user=student,
            oldID=row[0],
            job=row[15],
            jobPlace=row[16],
            jobPhone=row[17],
            military=military,
            azharian=True if row[4] == "Azhar" else False,
            registration_date=row[37].date() if row[37] is not None else None,
            file_number=generate_random_number(10154),
        )
        # Create Thesis
        thesis = Thesis.objects.create(
            student=studentProfile,
            arabic_title=row[23],
            english_title=row[24],
            state=Thesis.StateChoices.cancel
            if row[38]
            else Thesis.StateChoices.granted
            if row[43]
            else Thesis.StateChoices._continue,
            edit_type=Thesis.EditTypeChoices.main if row[38] or row[43] else Thesis.EditTypeChoices.noEdit,
            registration_period=row[37].date() if row[37] is not None else None,
            university_agree_date=row[36].date() if row[36] is not None else None,
            college_agree_date=row[34].date() if row[34] is not None else None,
            department_agree_date=row[33].date() if row[33] is not None else None,
        )
        # RANDOM
        specialization = None
        department = None
        branch = None

        try:
            all_departments = Department.objects.all()
            department = random.choice(all_departments)
            # Get random branch within the selected department
            branches_in_department = Branch.objects.filter(department=department)
            branch = random.choice(branches_in_department)

            # Get random specialization within the selected branch
            specializations_in_branch = Specialization.objects.filter(branch=branch)
            specialization = random.choice(specializations_in_branch)
        except:
            pass
        try:
            grade = Grade.objects.get(id=int(row[31]))
        except Exception:
            grade = None
        qualification = Qualification.objects.create(
            student=studentProfile,
            university=row[30],
            faculty=Faculty.objects.get(id=1),
            department=department if department else None,
            branch=branch if branch else None,
            specialization=specialization if specialization else None,
            date=row[29].date() if row[29] is not None else None,
            grade=grade,
        )
        obj = printQuery(f"SELECT * FROM _PostAttachments WHERE StdId = {row[0]};", 1)
        headers = obj["headers"]
        # print(headers)
        try:
            row = obj["rows"][0]
            attachments = postAttachment.objects.create(
                studentID=studentProfile,
                lastPillDate=row[8].date() if row[8] else None,
                lastPill=True if row[16] else False,
                militaryCertification=True if row[15] else False,
                bcsCertification=True if row[12] else False,
                masterCertification=True if row[18] else False,
                deplomaCertification=True if row[13] else False,
                stdPromise=True if row[17] else False,
                jobAcceptance=True if row[17] else False,
                nationalID=True if row[11] else False,
            )
        except:
            pass
        # print(row)
    print("Finished Loading Students!")
    print("Finished Loading Thesis!")
    print("Finished Loading Qualifications!")
    print("Finished Loading Attachments!")
    print("Loading Supervisors.....")
    obj = printQuery("SELECT * FROM _Poststaff ORDER BY Staffid ASC;", 1)
    headers = obj["headers"]
    rows = obj["rows"]
    for row in rows:
        randomText = generate_random_string(10)
        # Create Student First (USER)
        supervisor = Supervisor.objects.create_supervisor(
            email=randomText + "@gmail.com",
            password=randomText,
            role="SUPERVISOR",
            arabicName=row[1],
            nationalID=row[2],
        )
        specialization = None
        department = None
        branch = None

        try:
            all_departments = Department.objects.all()
            department = random.choice(all_departments)
            # Get random branch within the selected department
            branches_in_department = Branch.objects.filter(department=department)
            branch = random.choice(branches_in_department)

            # Get random specialization within the selected branch
            specializations_in_branch = Specialization.objects.filter(branch=branch)
            specialization = random.choice(specializations_in_branch)
        except:
            pass
        supervisorProfile = SupervisorProfile.objects.create(
            user=supervisor,
            oldID=row[0],
            organization=row[8],
            faculty=Faculty.objects.get(id=1),
            department=department if department else None,
            branch=branch if branch else None,
            specialization=specialization if specialization else None,
            degree=SupervisorProfile.DegreeChoices.choices[int(row[3] - 1)][0] if row[3] and isinstance(row[3] , int) and row[3] >= 0 and  row[3] < 9 else None,
        )
    print("Finished loading supervisors!")

    print("Loading Relationships.....")
    obj = printQuery("SELECT * FROM _PostSupervisory_lagna ORDER BY stdid ASC, staffid ASC;", 1)
    headers = obj["headers"]
    rows = obj["rows"]
    # print(headers)
    for row in rows:
        # print(row[1], " | ", row[2])
        try:
            student = StudentProfile.objects.filter(oldID=int(row[1])).first()
            thesis = Thesis.objects.filter(student=student).first()
        except Exception as e:
            thesis = None
        try:
            supervisor = SupervisorProfile.objects.filter(oldID=int(row[2])).first()
        except Exception as e:
            supervisor = None
        # print("____________________")
        supervisionType = (
            Supervision.SUPERVISING_TYPE_CHOICES.main_supervisor
            if row[3] == "مشرف رئيسى"
            else Supervision.SUPERVISING_TYPE_CHOICES.co_supervisor
        )
        if thesis and supervisor:
            try:
                Supervision.objects.create(thesis=thesis, supervisor=supervisor, supervising_type=supervisionType)
            except Exception as e:
                print("ERR : ", e)
    print("Finished loading relationships!")


t()
# Try fetch the enums as the same way as implemented (as possible)
# Create user for every student && supervisor

# For students Every email and password are auto generated with randomText@gmail.com and password randomText
# Alter student profile && supervisor profile to be have the old id
# Get the student data from > postStdData
# Get thesis data from > postStdData
# Get user data from > postStdData
# The enums that couldn't be generated will be randomized
# Change post attachments to booleans
# Create post attachments > post attachments
# Create supervisors > post staff (try fetch the enums in the same way as inserted)
# Create the supervisory relationship > supervisory committee
# Consider adding the users > from postUsers (manually)
# Consider adding all required not-filled models etc. last qualification (random && manually)
