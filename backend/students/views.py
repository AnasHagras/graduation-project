from rest_framework import generics, viewsets
from .serializers import (
    RegisterStudentSerializer,
    QualificationSerializer,
    StudentProfileSerializer,
    LastQualificationSerializer,
    postAttachmentSerializer,
)
from .models import *
from users.permissions import *
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAdminUser
from rest_framework.decorators import permission_classes
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from thesis.models import Thesis
from enums.models import *
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from supervisory.models import *
import json


@csrf_exempt 
def StudentThesisRegister(request):
    if request.method == 'POST':
        try:
            # Extract the JSON data from the request body
            data = json.loads(request.body)

            # Extract the required fields from the data dictionary
            student_data = data.get('student_data')
            student_profile_data = data.get('student_profile_data')
            thesis_data = data.get('thesis_data')
            supervisors = data.get('supervisors')
            supervisionTypes = data.get('supervisionTypes')
            degree = data.get('degree')
            qualification_data = data.get('qualification_data')
            qualifications = data.get('qualifications')

            checked_attachments = data.get('checked_attachments')
            checked_councils = data.get('checked_councils')

            print("supervision types: ", supervisionTypes)

            if len(supervisors) != len(supervisionTypes):
                return JsonResponse({'error': "make sure to set all supervision types"}, 
                                    status=400)

            student_data_required_fields = [
                'email',
                'arabicName',
                'englishName',
                'gender'
            ]
            thesis_data_required_fields = [
                'arabic_title',
                'english_title'
            ]

            missing_fields = [field for field in student_data_required_fields 
                              if field not in student_data] 
            
            missing_fields += [field for field in thesis_data_required_fields
                               if field not in thesis_data]
            
            if missing_fields or degree is None:
                error_message = 'Missing required fields: {}'.format(', '.join(missing_fields))
                return JsonResponse({'error': error_message}, status=400)
            
            if Student.objects.filter(email=student_data['email']).exists():
                return JsonResponse({'error': "User with that email exists"}, 
                                    status=400)

            if 'city' in student_data:
                student_data['city'] = City.objects.filter(name= student_data['city']).first()
            
            student = Student.objects.create_student(**student_data)
            student_profile_data['user'] = student
            if 'military' in student_profile_data:
                student_profile_data['military'] = Military.objects.filter(pk=student_profile_data['military']).first()

            student_profile = StudentProfile.objects.create(
                **student_profile_data)
            
            thesis_data['student'] = student_profile 
            if degree != 'دكتوراه':
                thesis_data['degree'] = 'الماجستير'
            else:
                thesis_data['degree'] = 'دكتوراه'
            
            thesis = Thesis.objects.create(**thesis_data)

            #create supervisions
            for i in range(len(supervisors)):
                supervisor_profile = User.objects.get(pk=supervisors[i]['user']['id']).supervisor_profile
                Supervision.objects.create(supervisor= supervisor_profile, 
                                           thesis= thesis, 
                                           supervising_type= supervisionTypes[str(i)])


            #create qualifications
            if 'faculty' in qualification_data:
                if Faculty.objects.filter(name = qualification_data['faculty']).exists():
                    qualification_data['faculty'] = Faculty.objects.filter(name = qualification_data['faculty']).first()
                else:
                    qualification_data['faculty'] = Faculty.objects.create(name = qualification_data['faculty'],
                                                                           display_name = qualification_data['faculty'])

            qualification_data['student'] = student_profile
            qualification_data['grade'] = Grade.objects.filter(name = qualification_data['grade']).first()
            Qualification.objects.create(**qualification_data)

            for qualification_data in qualifications:
                qualification_data['student'] = student_profile
                qualification_data['grade'] = Grade.objects.filter(name = qualification_data['grade']).first()
                Qualification.objects.create(**qualification_data)

            response_data = {
                'message': 'Data processed successfully',
            }
            return JsonResponse(response_data)
        except json.JSONDecodeError:
            # Return an error response if the request body is not valid JSON
            return JsonResponse({'error': 'Invalid JSON data'}, status=400)
    else:
        return JsonResponse({'error': 'Unsupported request method'}, status=405)



class StudentRegisterAPI(AdminAccess, generics.CreateAPIView):
    serializer_class = RegisterStudentSerializer
    queryset = Student.objects.all()


class StudentList(AdminAccess, generics.ListAPIView):
    serializer_class = StudentProfileSerializer
    queryset = StudentProfile.objects.all()

class StudentRetrieve(AdminAccess, generics.RetrieveAPIView):
    serializer_class = StudentProfileSerializer
    queryset = StudentProfile.objects.all()



class PermissionPolicyMixin:
    def check_permissions(self, request):
        try:
            handler = getattr(self, request.method.lower())
        except AttributeError:
            handler = None

        if handler and self.permission_classes_per_method and self.permission_classes_per_method.get(handler.__name__):
            self.permission_classes = self.permission_classes_per_method.get(handler.__name__)

        super().check_permissions(request)


class QualificationViewSet(AdminAccess, PermissionPolicyMixin, viewsets.ModelViewSet):
    serializer_class = QualificationSerializer
    model = Qualification
    queryset = Qualification.objects.all()
    permission_classes_per_method = {"list": [IsAuthenticated]}

    def list(self, request, *args, **kwargs):
        try:
            id = request.GET.get("id")
            if id is None:
                return Response({"error": "id should be provided"}, status=400)
            student = StudentProfile.objects.filter(pk=id).first()
            if student is None:
                return Response({"error": "id not found"}, status=404)
            # if user is admin or required user
            if request.user.role == User.Role.ADMIN or student.user.pk == request.user.pk:
                querySet = Qualification.objects.filter(student_profile=student)
                qualifications = LastQualificationSerializer(querySet, many=True).data
                return Response({"qualifications": qualifications}, status=200)
            else:
                return Response({"error": "you don't have permission to do that action"}, status=403)
        except Exception as e:
            return Response({"error": str(e)}, status=400)


class postAttachmentViewSet(viewsets.ModelViewSet):
    queryset = postAttachment.objects.all()
    serializer_class = postAttachmentSerializer
