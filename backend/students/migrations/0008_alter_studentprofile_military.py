# Generated by Django 4.1.3 on 2023-07-09 05:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("enums", "0001_initial"),
        ("students", "0007_alter_studentprofile_military"),
    ]

    operations = [
        migrations.AlterField(
            model_name="studentprofile",
            name="military",
            field=models.ForeignKey(
                blank=True,
                default=None,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="students",
                to="enums.military",
            ),
        ),
    ]
