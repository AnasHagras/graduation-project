# Generated by Django 4.1.3 on 2023-07-09 02:48

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("students", "0003_alter_studentprofile_user"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="postattachment",
            name="bscCertification",
        ),
        migrations.AddField(
            model_name="postattachment",
            name="bcsCertification",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="postattachment",
            name="militaryCertification",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="postattachment",
            name="nationalID",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="studentprofile",
            name="azharian",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="studentprofile",
            name="job",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name="postattachment",
            name="deplomaCertification",
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name="postattachment",
            name="jobAcceptance",
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name="postattachment",
            name="lastPill",
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name="postattachment",
            name="masterCertification",
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name="postattachment",
            name="stdPromise",
            field=models.BooleanField(default=False),
        ),
    ]
