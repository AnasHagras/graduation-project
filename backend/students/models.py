from django.db import models
from users.models import *
from django.contrib.auth.models import BaseUserManager
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.exceptions import ValidationError
from enums.models import *
from django.db.models import F
from datetime import datetime
from django.db.models import OuterRef, Subquery
from django.db.models import OuterRef, Subquery, IntegerField
from django.db.models.fields.related import ForeignKey


class StudentManager(UserManager):
    def get_queryset(self, *args, **kwargs):
        results = super().get_queryset(*args, **kwargs)
        return results.filter(role=User.Role.STUDENT)

    def create_student(self, **fields):
        fields["role"] = "STUDENT"
        return super().create_user(**fields)


class Student(User):
    base_role = User.Role.STUDENT
    objects = StudentManager()

    class Meta:
        proxy = True


def validateUser(user):
    if User.objects.get(id=user).role != User.Role.STUDENT:
        raise ValidationError("User's role isn't student")


class StudentProfileManager(UserManager):
    def get_queryset(self, *args, **kwargs):
        # subquery = Supervision.objects.filter(student=OuterRef("pk")).values("thesis")[:1]
        results = (
            super().get_queryset(*args, **kwargs)
            # .annotate(thesis=Subquery(subquery, output_field=models.ForeignKey(to=Thesis, on_delete=models.CASCADE)))
        )
        return results


class StudentProfile(models.Model):
    objects = StudentProfileManager()
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
        related_name="student_profile",
    )
    azharian = models.BooleanField(default=False)
    registration_date = models.DateField(null=True)
    expenses_paid = models.BooleanField(null=True)
    file_number = models.IntegerField(null=True)
    oldID = models.IntegerField(null=True)
    job = models.CharField(max_length=255, null=True, blank=True)
    jobPlace = models.CharField(max_length=255, null=True, blank=True)
    jobPhone = models.CharField(max_length=255, null=True, blank=True)
    military = models.ForeignKey(
        Military, on_delete=models.CASCADE, related_name="students", null=True, blank=True, default=None
    )

    @property
    def last_qualification(self):
        return Qualification.objects.filter(student=self.pk).order_by("-date").first()

    # @property
    # def thesis(self):
    #     return Thesis.objects.filter(pk__in=Supervision.objects.filter(student=self.pk).values_list("thesis")).first()


class Qualification(models.Model):
    class DegreeChoices(models.TextChoices):
        bachelor = "بكالوريوس"
        master = "ماجستير"
        phd = "دكتوراه"

    degree = models.CharField(max_length=255, choices=DegreeChoices.choices)
    student = models.ForeignKey(to=StudentProfile, on_delete=models.CASCADE)

    date = models.DateField()
    faculty = models.ForeignKey(to=Faculty, on_delete=models.CASCADE, blank=True, null=True)
    department = models.ForeignKey(to=Department, on_delete=models.CASCADE, blank=True, null=True)
    branch = models.ForeignKey(to=Branch, on_delete=models.CASCADE, blank=True, null=True)
    specialization = models.ForeignKey(to=Specialization, on_delete=models.CASCADE, blank=True, null=True)
    university = models.CharField(max_length=255, default="")
    grade = models.ForeignKey(to=Grade, on_delete=models.PROTECT, default=None, null=True, blank=True)

    def __str__(self):
        return str(self.pk)

    @property
    def place(self):
        return f"{self.university} - {self.faculty}"


class postAttachment(models.Model):
    studentID = models.ForeignKey(StudentProfile, on_delete=models.CASCADE, related_name="postattachments")
    lastPill = models.BooleanField(default=False, null=True, blank=True)
    militaryCertification = models.BooleanField(default=False, null=True, blank=True)
    lastPillDate = models.DateField(null=True, blank=True)
    bcsCertification = models.BooleanField(default=False, null=True, blank=True)
    masterCertification = models.BooleanField(default=False, null=True, blank=True)
    deplomaCertification = models.BooleanField(default=False, null=True, blank=True)
    stdPromise = models.BooleanField(default=False, null=True, blank=True)
    jobAcceptance = models.BooleanField(default=False, null=True, blank=True)
    nationalID = models.BooleanField(default=False, null=True, blank=True)
    # nationalID = models.BooleanField(default=False)
