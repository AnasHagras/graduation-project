from django.contrib import admin
from .models import StudentProfile, Qualification, Student


class StudentProfileAdmin(admin.ModelAdmin):
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "user",
                    "registration_date",
                    "expenses_paid",
                    "file_number",
                ),
            },
        ),
    )

    fieldsets = (
        (
            ("user_info"),
            {
                "fields": (
                    "user",
                    "registration_date",
                    "expenses_paid",
                    "file_number",
                )
            },
        ),
    )

    list_display = [
        "pk",
        "user",
        "registration_date",
        "expenses_paid",
        "file_number",
        "last_qualification",
    ]


class QualificationAdmin(admin.ModelAdmin):
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "student",
                    "university",
                    "faculty",
                    "department",
                    "branch",
                    "specialization",
                    "grade",
                    "date",
                ),
            },
        ),
    )

    fieldsets = (
        (
            ("user_info"),
            {
                "fields": (
                    "student",
                    "university",
                    "faculty",
                    "department",
                    "branch",
                    "specialization",
                    "grade",
                    "date",
                )
            },
        ),
    )

    list_display = [
        "pk",
        "student",
        "university",
        "faculty",
        "department",
        "branch",
        "specialization",
        "date",
    ]


admin.site.register(StudentProfile, StudentProfileAdmin)
admin.site.register(Qualification, QualificationAdmin)
