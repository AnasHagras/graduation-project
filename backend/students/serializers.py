from rest_framework import serializers
from .models import *
from django.contrib.auth import password_validation
from django.core import exceptions
from enums.serializers import (
    FacultySerializer,
    DepartmentSerializer,
    BranchSerializer,
    SpecializationSerializer,
    GradeSerializer,
)
from users.serializers import UserSerializer
from thesis.models import Thesis


class QualificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Qualification
        fields = "__all__"


class LastQualificationSerializer(serializers.ModelSerializer):
    place = serializers.CharField()
    faculty = FacultySerializer()
    department = DepartmentSerializer()
    specialization = SpecializationSerializer()
    branch = BranchSerializer()
    grade = GradeSerializer()

    class Meta:
        model = Qualification
        exclude = ("student",)
        extra_fields = ["place"]


class StudentProfileSerializer(serializers.ModelSerializer):
    last_qualification = LastQualificationSerializer(required=False)
    user = UserSerializer(required=False)
    thesis = serializers.SerializerMethodField()
    attachments = serializers.SerializerMethodField()

    class Meta:
        model = StudentProfile
        # exclude = ("user",)
        fields = "__all__"
        extra_fields = ["last_qualification", "thesis", "attachments"]
        # depth = 1

    def get_thesis(self, obj):
        return ThesisWithoutUserSerializer(Thesis.objects.filter(student=obj.pk).first()).data

    def get_attachments(self, obj):
        return postAttachmentSerializer(postAttachment.objects.filter(studentID=obj.pk).first()).data


class RegisterStudentSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    password_confirmation = serializers.CharField(write_only=True)
    student_profile = StudentProfileSerializer()

    class Meta:
        model = Student
        fields = [
            "pk",
            "email",
            "first_name",
            "last_name",
            "password",
            "password_confirmation",
            "date_of_birth",
            "student_profile",
        ]

    def validate_password(self, password):
        try:
            password_validation.validate_password(password=password, user=None)
        except exceptions.ValidationError as e:
            raise serializers.ValidationError(e.messages)

        return password

    def validate(self, data):
        if data["password"] != data["password_confirmation"]:
            raise serializers.ValidationError({"password": "Password fields didn't match."})
        return data

    def create(self, validated_data):
        student_profile_data = validated_data.pop("student_profile")
        validated_data.pop("password_confirmation")
        student = Student.objects.create_student(
            email=validated_data["email"],
            password=validated_data["password"],
            first_name=validated_data["first_name"],
            last_name=validated_data["last_name"],
            date_of_birth=validated_data["date_of_birth"],
        )
        StudentProfile.objects.create(user=student, **student_profile_data)
        return student


class postAttachmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = postAttachment
        fields = "__all__"


from thesis.serializers import ThesisWithoutUserSerializer
