from django.urls import path, include
from django.urls import path, include
from rest_framework import routers
from .views import *
from supervisory.views import StudentSupervisors

router = routers.DefaultRouter()
router.register(r"qualifications", QualificationViewSet, basename="qualifications")

router.register(r"PostAttachment", postAttachmentViewSet)

urlpatterns = [
    path("", include(router.urls)),
    path("register/", StudentRegisterAPI.as_view()),
    path("list/", StudentList.as_view()),
    path("student-supervisors/", StudentSupervisors.as_view()),
    path("retrieve/<int:pk>/", StudentRetrieve.as_view()),
    path("studentThesisRegister/", StudentThesisRegister, name= "studentThesisRegister")
]
