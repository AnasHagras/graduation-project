from django.db import models
from users.models import *
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.exceptions import ValidationError
from enums.models import *
from thesis.models import Thesis
import importlib


class SupervisorManager(UserManager):
    def get_queryset(self, *args, **kwargs):
        results = super().get_queryset(*args, **kwargs)
        return results.filter(role=User.Role.SUPERVISOR)

    def create_supervisor(self, **fields):
        fields["role"] = "SUPERVISOR"
        return super().create_user(**fields)


class Supervisor(User):
    base_role = User.Role.SUPERVISOR
    objects = SupervisorManager()

    class Meta:
        proxy = True


def validateUser(user):
    if User.objects.get(id=user).role != User.Role.SUPERVISOR:
        raise ValidationError("User's role isn't superviosr")


class SupervisorProfile(models.Model):
    class DegreeChoices(models.TextChoices):
        teachingAssistant = "معيد"
        assistantTeacher = "مدرس مساعد"
        teacher = "مدرس"
        fullTimeTeacher = "مدرس متفرغ"
        assistantProfessor = "استاذ مساعد"
        fullTimeAssistantProfessor = "استاذ مساعد متفرغ"
        professor = "استاذ"
        fullTimeProfessor = "استاذ متفرغ"
        postgraduateResearcher = "باحث دراسات عليا"

    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
        validators=[validateUser],
        related_name="supervisor_profile",
    )

    faculty = models.ForeignKey(to=Faculty, on_delete=models.CASCADE)
    department = models.ForeignKey(to=Department, on_delete=models.CASCADE)
    branch = models.ForeignKey(to=Branch, on_delete=models.CASCADE, blank=True, null=True)
    specialization = models.ForeignKey(to=Specialization, on_delete=models.CASCADE, blank=True, null=True)
    organization = models.CharField(max_length=255)

    degree = models.CharField(max_length=32, choices=DegreeChoices.choices,blank=True,null=True)
    oldID = models.IntegerField(null=True,blank=True,default=0)

    @property
    def number_of_master_theses(self):
        return Supervision.objects.filter(supervisor=self.pk, thesis__degree=Thesis.ThesisDegree.Master).count()

    @property
    def number_of_PhD_theses(self):
        return Supervision.objects.filter(supervisor=self.pk, thesis__degree=Thesis.ThesisDegree.PhD).count()

    # depertment = models.ForeignKey(Depertment, null=False, on_delete=models.CASCADE, default="dep")
    # English_Name = models.CharField(max_length=255, null=False, default="name")
    # Arabic_Name = models.CharField(null=False, max_length=255, default="اسم")
    # National_id = models.CharField(null=False, max_length=15, default="name")
    # organiztion = models.CharField(null=False, max_length=15, default="org")


from supervisory.models import Supervision
'''
[
    {
        "organization": "جامعة موسكو",
        "degree": "معيد",
    }
]
'''