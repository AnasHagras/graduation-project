from django.urls import path
from .views import SupervisorRegisterAPI, SupervisorListAPI , SupervisorUpdateAPI

urlpatterns = [
    path("", SupervisorListAPI.as_view()),
    path("register/", SupervisorRegisterAPI.as_view()),
    path("update/<int:pk>/", SupervisorUpdateAPI.as_view()),
]
