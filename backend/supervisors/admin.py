from django.contrib import admin
from .models import SupervisorProfile


class SupervisorProfileAdmin(admin.ModelAdmin):
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "user",
                    "degree",
                    "organization",
                    "faculty",
                    "department",
                    "branch",
                    "specialization",
                ),
            },
        ),
    )

    fieldsets = (
        (
            ("user_info"),
            {
                "fields": (
                    "user",
                    "degree",
                    "organization",
                    "faculty",
                    "department",
                    "branch",
                    "specialization",
                )
            },
        ),
    )

    list_display = [
        "user",
        "degree",
        "organization",
        "faculty",
        "department",
        "branch",
        "specialization",
    ]


# Register your models here.
admin.site.register(SupervisorProfile, SupervisorProfileAdmin)
