from rest_framework import serializers
from .models import *
from django.contrib.auth import password_validation
from django.core import exceptions
from users.serializers import UserSerializer
from enums.serializers import *
from students.models import StudentProfile


class SupervisorProfileSerializer(serializers.ModelSerializer):
    # faculty = FacultySerializer()
    # department = DepartmentSerializer()
    # branch = BranchSerializer()
    # specialization = SpecializationSerializer()

    class Meta:
        model = SupervisorProfile
        exclude = ["user"]

    # def validate(self, data):
    #     errors = {}
    #     if data.get("department") and data["faculty"] != data["department"].faculty:
    #         errors["department"] = "Department does not belong to this faculty."

    #     if data.get("branch") and data["department"] != data["branch"].department:
    #         errors["branch"] = "Branch does not belong to this department."

    #     if data.get("specialization") and data["branch"] != data["specialization"].branch:
    #         errors["specialization"] = "Specialization does not belong to this branch."

    #     if errors:
    #         raise serializers.ValidationError(errors)
    #     return data
    

class StudentSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    class Meta:
        model = StudentProfile
        fields = "__all__"

class ThesisSerializer(serializers.ModelSerializer):
    student = StudentSerializer()

    class Meta:
        model = Thesis
        fields = ("student",)

class SupervisionSerializer(serializers.ModelSerializer):
    thesis = ThesisSerializer()

    class Meta:
        model = Supervision
        fields = ("thesis",)

class SupervisorListSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    number_of_master_theses = serializers.ReadOnlyField()
    number_of_PhD_theses = serializers.ReadOnlyField()
    supervised_students = serializers.SerializerMethodField()

    class Meta:
        model = SupervisorProfile
        fields = "__all__"
        extra_fields = ["number_of_master_theses", "number_of_PhD_theses", "supervised_students"]
        depth = 1

    def get_supervised_students(self, obj):
        supervisions = Supervision.objects.filter(supervisor=obj)
        serializer = SupervisionSerializer(supervisions, many=True)
        return serializer.data



class RegisterSupervisorSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    password_confirmation = serializers.CharField(write_only=True)
    supervisor_profile = SupervisorProfileSerializer()
    nationalID = serializers.CharField(required=False)

    class Meta:
        model = Supervisor
        fields = [
            "email",
            "password",
            "password_confirmation",
            "date_of_birth",
            "gender",
            "arabicName",
            "englishName",
            "phoneNumber",
            "nationalID",
            "supervisor_profile",
        ]

    def validate_password(self, password):
        try:
            password_validation.validate_password(password=password, user=None)
        except exceptions.ValidationError as e:
            raise serializers.ValidationError(e.messages)

        return password

    def validate(self, data):
        if data["password"] != data["password_confirmation"]:
            raise serializers.ValidationError({"password": "Password fields didn't match."})
        return data

    def create(self, validated_data):
        supervisor_profile_data = validated_data.pop("supervisor_profile")
        validated_data.pop("password_confirmation")
        supervisor = Supervisor.objects.create_supervisor(**validated_data)
        SupervisorProfile.objects.create(user=supervisor, **supervisor_profile_data)
        return supervisor

class UpdateSupervisorsSerializer(serializers.ModelSerializer):
    supervisor_profile = SupervisorProfileSerializer()

    class Meta:
        model = Supervisor
        fields = "__all__"



