from django.shortcuts import render
from knox.auth import TokenAuthentication
from knox.models import AuthToken
from rest_framework import generics, permissions, views, viewsets
from rest_framework.response import Response
from .models import Supervisor, SupervisorProfile
from django.db.models import Q

from .serializers import (
    RegisterSupervisorSerializer,
    SupervisorProfileSerializer,
    SupervisorListSerializer,
    UpdateSupervisorsSerializer,
)

from users.models import User
from enums.models import City
from supervisory.models import Supervision

from users.permissions import AdminAccess
from enums.models import Faculty, Department, Specialization, Branch


class SupervisorRegisterAPI(AdminAccess, generics.CreateAPIView):
    serializer_class = RegisterSupervisorSerializer
    queryset = Supervisor.objects.all()


class SupervisorListAPI(AdminAccess, generics.ListAPIView):
    serializer_class = SupervisorListSerializer
    queryset = SupervisorProfile.objects.all()

    def get_queryset(self):
        thesis = self.request.query_params.get("thesis", None)
        queryset = SupervisorProfile.objects.all()
        faculty = self.request.query_params.get("faculty", None)
        department = self.request.query_params.get("department", None)
        branch = self.request.query_params.get("branch", None)
        specialization = self.request.query_params.get("specialization", None)

        if thesis:
            supervisions = Supervision.objects.filter(thesis=thesis)
            queryset = queryset.filter(
                Q(pk__in=supervisions.values("supervisor"))
            )

        if faculty:
            queryset = queryset.filter(
                faculty=Faculty.objects.filter(name=faculty).first(),
            )
        if department:
            queryset = queryset.filter(
                department=Department.objects.filter(name=department).first(),
            )
        if branch:
            queryset = queryset.filter(
                branch=Branch.objects.filter(name=branch).first(),
            )
        if specialization:
            queryset = queryset.filter(
                specialization=Specialization.objects.filter(
                    name=specialization
                ).first(),
            )

        return queryset

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class SupervisorUpdateAPI(generics.UpdateAPIView):
    serializer_class = UpdateSupervisorsSerializer
    queryset = Supervisor.objects.all()

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        # Update Supervisor fields
        supervisor = instance
        supervisor.email = request.data.get("email", supervisor.email)
        supervisor.arabicName = request.data.get("arabicName", supervisor.arabicName)
        supervisor.nationalID = request.data.get("nationalID", supervisor.nationalID)
        supervisor.phoneNumber = request.data.get("phoneNumber", supervisor.phoneNumber)

        city_id = request.data.get("city")
        if city_id:
            city = City.objects.get(id=city_id)
            supervisor.city = city

        supervisor.save()

        # Update SupervisorProfile fields
        supervisor_profile = supervisor.supervisor_profile
        departmet_id = request.data.get("department", supervisor_profile.department)
        if departmet_id:
            department = Department.objects.get(id=departmet_id)
            supervisor_profile.department = department

        branch_id = request.data.get("branch", supervisor_profile.branch)
        if branch_id:
            branch = Branch.objects.get(id=branch_id)
            supervisor_profile.branch = branch

        specialization_id = request.data.get(
            "specialization", supervisor_profile.specialization
        )
        if specialization_id:
            specialization = Specialization.objects.get(id=specialization_id)
            supervisor_profile.specialization = specialization

        supervisor_profile.degree = request.data.get(
            "degree", supervisor_profile.degree
        )
        supervisor_profile.save()

        self.perform_update(serializer)

        return Response(serializer.data)
