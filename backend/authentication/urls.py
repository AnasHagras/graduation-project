from django.urls import path
from knox.views import LogoutView, LogoutAllView
from .views import Login, Register

urlpatterns = [
    path("login/", Login.as_view(), name="login"),
    path("register/", Register.as_view(), name="register"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("logout-all/", LogoutAllView.as_view(), name="logoutAll"),
]
