from knox.models import AuthToken
from rest_framework import generics, permissions
from users.models import User
from django.contrib.auth import login
from knox.auth import TokenAuthentication
from rest_framework.response import Response
from users.serializers import UserSerializer
from .serializers import RegisterSerializer, LoginSerializer


class Login(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    authentication_classes = [
        TokenAuthentication,
    ]
    serializer_class = LoginSerializer

    def post(self, request):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        token = AuthToken.objects.create(user)
        login(request, user)
        return Response(
            {"token": token[1], "user": UserSerializer(user).data}, status=200
        )


class Register(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = RegisterSerializer
    authentication_classes = []
    lookup_field = User

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response(
            {
                "user": UserSerializer(
                    user, context=self.get_serializer_context()
                ).data,
                "token": AuthToken.objects.create(user)[1],
            },
            status=201,
        )
