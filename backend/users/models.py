from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.utils import timezone
from enums.models import City


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError("The given email must be set")
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("role", User.Role.ADMIN)
        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(email, password, **extra_fields)

class User(AbstractUser):
    username = None
    USERNAME_FIELD = "email"

    # TODO : add roles for data entry person and مدير الدراسات (later on)
    class Role(models.TextChoices):
        ADMIN = "ADMIN", "Admin"
        STUDENT = "STUDENT", "Student"
        SUPERVISOR = "SUPERVISOR", "Supervisor"

    class GenderChoices(models.TextChoices):
        male = "ذكر", "ذكر"
        female = "أنثي", "أنثي"
 
    base_role = Role.ADMIN
 
    role = models.CharField(max_length=50, choices=Role.choices)
    email = models.EmailField(_("email address"), unique=True)
    date_of_birth = models.DateField(null=True)
    arabicName = models.CharField(max_length=255)
    englishName = models.CharField(max_length=255)
    nationalID = models.CharField(max_length=255, null=True, blank=True)
    phoneNumber = models.CharField(max_length=255, null=True, blank=True)
    gender = models.CharField(max_length=32, choices=GenderChoices.choices)
    nationality = models.CharField(max_length=32, null=True, blank=True)
    city = models.ForeignKey(to=City, on_delete=models.DO_NOTHING, blank=True, null=True)
    birthPlace = models.CharField(max_length=255, null=True, blank=True)
    currentPlace = models.CharField(max_length=255,null=True,blank=True)
    phone_number = models.CharField(max_length=255,null=True,blank=True)
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return f"{self.email}"

    # def save(self, *args, **kwargs):
    #     if (not self.pk) or self.role == self.base_role:
    #         self.role = self.base_role
    #         return super().save(*args, **kwargs)
