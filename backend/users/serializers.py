from rest_framework import serializers
from .models import User
from django.contrib.auth.password_validation import *


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "id",
            "arabicName",
            "englishName",
            "email",
            "date_of_birth",
            "role",
            "gender",
            "date_of_birth",
            "nationalID",
            "phoneNumber",
            "city",
            "nationality",
            "city",
            "birthPlace",
            "currentPlace",
            "phone_number",
        ]
