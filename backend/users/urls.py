from .views import *
from django.urls import path, include
from .views import UserViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r"", UserViewSet)

urlpatterns = [
    path("get-current-user/", GetCurrentUser.as_view(), name="get_current_user"),
    path("", include(router.urls)),
]
