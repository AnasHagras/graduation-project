from django.contrib import admin
from .models import User
from django.contrib.auth.admin import UserAdmin


class CustomUserAdmin(UserAdmin):
    ordering = ("email",)

    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("email", "password1", "password2"),
            },
        ),
    )

    fieldsets = (
        (
            ("user_info"),
            {
                "fields": (
                    "email",
                    "arabicName",
                    "englishName",
                    "gender",
                    "phoneNumber",
                    "nationalID",
                    "city",
                    "last_login",
                    "role",
                )
            },
        ),
        (None, {"fields": ("date_joined",)}),
    )

    list_display = [
        "id",
        "email",
        "arabicName",
        "englishName",
        "gender",
        "phoneNumber",
        "role",
        "city",
    ]


admin.site.register(User, CustomUserAdmin)
