from knox.auth import TokenAuthentication
from rest_framework.permissions import IsAuthenticated, BasePermission
from .models import User


# access controll to all views (all users must be authenticated)
class BaseAccess:
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]


class AdminPermission(BasePermission):
    def has_permission(self, request, view):
        return request.user.role == "ADMIN"


class AdminAccess(BaseAccess):
    permission_classes = BaseAccess.permission_classes + [AdminPermission]
