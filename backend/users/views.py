from rest_framework import views, viewsets
from knox.auth import TokenAuthentication
from rest_framework import permissions
from rest_framework.response import Response
from .serializers import UserSerializer
from .permissions import AdminAccess
from .models import User


class GetCurrentUser(views.APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    def get(self, request, *args, **kwargs):
        return Response({"user": UserSerializer(request.user).data}, status=200)


class UserViewSet(AdminAccess, viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
