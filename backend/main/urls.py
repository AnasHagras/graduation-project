from django.contrib import admin
from django.urls import path, include, re_path
from users.views import *
from knox import views as knox_views
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from supervisory.views import *
from knox import views as knox_views 

schema_view = get_schema_view(
    openapi.Info(
        title="PostGraduation API",
        default_version="v1",
        description="PostGraduation Project ",
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
)
urlpatterns = [
    re_path(
        r"^swagger(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    re_path(
        r"^swagger/$",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    re_path(
        r"^redoc/$", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"
    ),
    path("admin/", admin.site.urls),
    path("api/authentication/", include("authentication.urls")),
    path("api/students/", include("students.urls")),
    path("api/users/", include("users.urls")),
    path("api/supervisors/", include("supervisors.urls")),
    path("api/supervisory/", include("supervisory.urls")),
    path("api/thesis/", include("thesis.urls")),
    path("api/", include("enums.urls")),
]
