from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status, generics, views
from .models import Supervision
from .serializers import SupervisionSerializer
from users.permissions import *
from supervisors.serializers import (
    SupervisorListSerializer,
    SupervisorProfileSerializer,
)
from supervisors.models import Supervisor, SupervisorProfile


class SupervisionViewSet(AdminAccess, viewsets.ModelViewSet):
    queryset = Supervision.objects.all()
    serializer_class = SupervisionSerializer

class SupervisionView(AdminAccess, generics.ListAPIView):
    serializer_class = SupervisionSerializer

    def get_queryset(self):
        thesis_id = self.request.query_params.get("thesis_id")
        supervisions = Supervision.objects.filter(thesis=thesis_id)
        
        supervisor_ids = supervisions.values_list("supervisor", flat=True)
        print(supervisor_ids)
        supervisors = User.objects.filter(id__in=supervisor_ids)
        print(supervisors)
        
        # Create a dictionary to map supervisor IDs to user IDs
        supervisor_id_to_user_id = [supervisor.id
            for supervisor in supervisors
        ]
        print(supervisor_id_to_user_id)
        
        # Replace supervisor IDs with user IDs in the supervisions queryset
        for supervision , user in zip(supervisions, supervisors):
            supervision.id = user.id
        
        return supervisions



# TODO : this API should be updated to be matched with the new flow
class StudentSupervisors(AdminAccess, generics.ListAPIView):
    queryset = Supervision.objects.all()

    def list(self, request):
        id = request.query_params.get("id")
        if id is None:
            return Response({"error": "id should be provided"}, status=400)

        supervisors = SupervisorProfile.objects.filter(
            pk__in=Supervision.objects.filter(student=id).values_list("supervisor")
        )
        serializer = SupervisorListSerializer(supervisors, many=True)
        return Response({"supervisors": serializer.data}, status=200)
