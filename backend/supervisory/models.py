from django.db import models
from supervisors.models import SupervisorProfile
from thesis.models import Thesis

['Supervisoryid', 'stdid', 'staffid', 'Stafftype']
class Supervision(models.Model):
    class SUPERVISING_TYPE_CHOICES(models.TextChoices):
        main_supervisor = "مشرف رئيسي"
        co_supervisor = "مشرف مشارك"

    supervisor = models.ForeignKey(
        SupervisorProfile,
        on_delete=models.CASCADE,
        related_name="supervisions_as_supervisor",
    )
    thesis = models.ForeignKey(Thesis, on_delete=models.CASCADE, related_name="supervisions")
    supervising_type = models.CharField(max_length=50, choices=SUPERVISING_TYPE_CHOICES.choices)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["thesis", "supervisor"], name="unique_supervision"),
            models.CheckConstraint(
                check=~models.Q(thesis=None) & ~models.Q(supervisor=None),
                name="not_null",
            ),
        ]

    def __str__(self):
        return f"{self.supervisor} is supervising {self.thesis.student} as {self.supervising_type}"
