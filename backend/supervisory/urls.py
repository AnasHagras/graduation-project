from django.urls import path, include
from . import views


urlpatterns = [
    # path("", views.SupervisionViewSet.as_view()),
    path("supervision/", views.SupervisionView.as_view()),
]
