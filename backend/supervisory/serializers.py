from rest_framework import serializers
from .models import Supervision


class SupervisionSerializer(serializers.ModelSerializer):
    supervisor = serializers.ReadOnlyField(source='supervisor.name')
    thesis = serializers.ReadOnlyField(source='thesis.title')
    class Meta:
        model = Supervision
        fields = "__all__"

    def validate(self, data):
        qs = Supervision.objects.filter(
            thesis=data["thesis"], supervisor=data["supervisor"]
        )
        if qs.exists():
            raise serializers.ValidationError("This supervision already exists")

        return data
