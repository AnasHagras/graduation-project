# Generated by Django 4.1.3 on 2023-06-30 18:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("thesis", "0001_initial"),
        ("supervisory", "0001_initial"),
        ("supervisors", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="supervision",
            name="supervisor",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="supervisions_as_supervisor",
                to="supervisors.supervisorprofile",
            ),
        ),
        migrations.AddField(
            model_name="supervision",
            name="thesis",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="supervisions",
                to="thesis.thesis",
            ),
        ),
        migrations.AddConstraint(
            model_name="supervision",
            constraint=models.UniqueConstraint(
                fields=("thesis", "supervisor"), name="unique_supervision"
            ),
        ),
        migrations.AddConstraint(
            model_name="supervision",
            constraint=models.CheckConstraint(
                check=models.Q(
                    models.Q(("thesis", None), _negated=True),
                    models.Q(("supervisor", None), _negated=True),
                ),
                name="not_null",
            ),
        ),
    ]
