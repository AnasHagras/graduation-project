from django.db import models
from students.models import StudentProfile


class Thesis(models.Model):
    class EditTypeChoices(models.TextChoices):
        main = "جوهري"
        notMain = "غير جوهري"
        noEdit = "لم يحصل تعديل"

    class StateChoices(models.TextChoices):
        _continue = "مستمر"
        cancel = "ملغي"
        granted = "منحت"

    class ThesisDegree(models.TextChoices):
        PhD = "دكتوراه"
        Master = "الماجستير"

    student = models.ForeignKey(to=StudentProfile, on_delete=models.CASCADE, related_name="thesis")
 
    degree = models.CharField(max_length=255, choices=ThesisDegree.choices, default=ThesisDegree.Master)
    _continue = models.BooleanField(default=True)
    cancel = models.BooleanField(default=False)
    accepted = models.BooleanField(default=False)
    edit = models.BooleanField(default=False) 
    arabic_title = models.CharField(max_length=255,null=True,blank=True,default="")
    english_title = models.CharField(max_length=255,null=True,blank=True,default="")
    notes = models.CharField(max_length=255, null=True,blank=True,default="")
    edit_type = models.CharField(max_length=255, choices=EditTypeChoices.choices, null=True)
    registration_period= models.DateField(null=True, blank=True)
    state = models.CharField(max_length=255, choices=StateChoices.choices, default=StateChoices._continue)
    cancel_reason = models.CharField(max_length=255,null=True,blank=True)
    # Dates
    university_agree_date= models.DateField(null=True, blank=True)
    college_agree_date= models.DateField(null=True, blank=True)
    department_agree_date= models.DateField(null=True, blank=True)
    department_edit_date = models.DateField(null=True, blank=True)
    college_edit_date = models.DateField(null=True, blank=True)
    university_edit_date = models.DateField(null=True, blank=True)
    department_grant_date = models.DateField(null=True, blank=True)
    college_grant_date = models.DateField(null=True, blank=True)
    university_grant_date = models.DateField(null=True, blank=True)
    department_cancel_date = models.DateField(null=True, blank=True)
    college_cancel_date = models.DateField(null=True, blank=True)
    university_cancel_date = models.DateField(null=True, blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)


class EditThesisLog(models.Model):
    class EditTypeChoices(models.TextChoices):
        main = "جوهري"
        notMain = "غير جوهري"

    thesis_Id = models.ForeignKey(Thesis, on_delete=models.CASCADE, related_name="thesis")
    log = models.TextField()

    @staticmethod
    def notify(**kwargs):
        message = ""
        obj_id = kwargs.get("pk")
        kwargs.pop("pk")
        try:
            print(obj_id)
            obj = Thesis.objects.get(pk=obj_id)
            for key, value in kwargs.items():
                if not value:
                    continue
                message += f"{getattr(obj, key)} --> {value}\n"
            EditThesisLog.objects.create(log=message, thesis_Id=obj)
        except Exception as e:
            print(e)
            pass
