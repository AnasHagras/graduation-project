from django.urls import path, include
from rest_framework import routers
from .views import ThesisViewSet,EditThesisLogViewSet

router = routers.DefaultRouter()
router.register(r"", ThesisViewSet)
router.register(r"EditThesisLog",EditThesisLogViewSet)

urlpatterns = [
    path(
        "canceled-theses/",
        ThesisViewSet.as_view({"get": "canceledTheses"}),
        name="canceled-theses",
    ),
    path(
        "granted-theses/",
        ThesisViewSet.as_view({"get": "grantedTheses"}),
        name="granted-theses",
    ),
    path("", include(router.urls)),
]
