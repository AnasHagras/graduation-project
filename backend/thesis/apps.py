from django.apps import AppConfig


class thesisConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "thesis"
