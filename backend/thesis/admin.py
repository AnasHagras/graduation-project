from django.contrib import admin
from .models import Thesis,EditThesisLog

# Register your models here.

admin.site.register(Thesis)
admin.site.register(EditThesisLog)