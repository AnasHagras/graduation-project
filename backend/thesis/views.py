from .models import Thesis, EditThesisLog
from .serializers import ThesisSerializer, EditThesisLogSerializer
from rest_framework.permissions import *
from rest_framework import viewsets, generics
from users.permissions import *
from rest_framework.response import Response
from supervisory.models import Supervision
from django.shortcuts import get_object_or_404
from django.http import Http404
from rest_framework.decorators import action
from students.models import StudentProfile
from students.serializers import StudentProfileSerializer
from django.db.models import Q


class ThesisViewSet(viewsets.ModelViewSet):
    queryset = Thesis.objects.all()
    serializer_class = ThesisSerializer

    def update(self, request, *args, **kwargs):
        print("request: ", request.data, " | kwargs : ", kwargs)
        EditThesisLog.notify(**request.data, pk=kwargs.get("pk"))
        return super(ThesisViewSet, self).update(request, *args, **kwargs)

    @action(detail=False, methods=["get"])
    def canceledTheses(self, request):
        queryset = Thesis.objects.filter(state="ملغي")
        results = ThesisSerializer(queryset, many=True).data
        return Response({"results": results}, status=200)

    @action(detail=False, methods=["get"])
    def grantedTheses(self, request):
        queryset = Thesis.objects.filter(state="منحت")
        results = ThesisSerializer(queryset, many=True).data
        return Response({"results": results}, status=200)


class EditThesisLogViewSet(viewsets.ModelViewSet):
    queryset = EditThesisLog.objects.all()
    serializer_class = EditThesisLogSerializer

    def get_object(self):
        pk = self.kwargs.get("pk")
        student_supervision = StudentProfile.objects.filter(student=pk).first()
        if student_supervision is not None:
            return student_supervision.thesis
        else:
            raise Http404()
