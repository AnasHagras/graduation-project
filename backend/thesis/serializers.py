from rest_framework import serializers
from .models import Thesis, EditThesisLog
from students.models import StudentProfile
from supervisory.models import Supervision

from students.serializers import StudentProfileSerializer
from supervisors.models import SupervisorProfile
from supervisors.serializers import SupervisorProfileSerializer


class ThesisSerializer(serializers.ModelSerializer):
    department_edit_date = serializers.DateField(input_formats=['%Y-%m-%d'], required=False, allow_null=True)
    college_edit_date = serializers.DateField(input_formats=['%Y-%m-%d'], required=False, allow_null=True)
    university_edit_date = serializers.DateField(input_formats=['%Y-%m-%d'], required=False, allow_null=True)
    department_grant_date = serializers.DateField(input_formats=['%Y-%m-%d'], required=False, allow_null=True)
    college_grant_date = serializers.DateField(input_formats=['%Y-%m-%d'], required=False, allow_null=True)
    university_grant_date = serializers.DateField(input_formats=['%Y-%m-%d'], required=False, allow_null=True)
    department_cancel_date = serializers.DateField(input_formats=['%Y-%m-%d'], required=False, allow_null=True)
    college_cancel_date = serializers.DateField(input_formats=['%Y-%m-%d'], required=False, allow_null=True)
    university_cancel_date = serializers.DateField(input_formats=['%Y-%m-%d'], required=False, allow_null=True)
    creation_date = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S', read_only=True)

    student = StudentProfileSerializer()

    class Meta:
        model = Thesis
        fields = "__all__"


    def get_supervisors(self, obj):
        supervisions = obj.supervisions.all()
        return SupervisorProfileSerializer(supervisions, many=True).data

    def update(self, instance, request):
        request = self.context.get('request')  # Get the request from the context
        print(request.data)
        supervisors_data = request.data.pop('supervisors', [])
        print(supervisors_data)

        supervisor_ids_in_request = [supervisor_data['id'] for supervisor_data in supervisors_data]

        # Delete supervisions for supervisors not in the request data
        instance.supervisions.exclude(supervisor_id__in=supervisor_ids_in_request).delete()

        for key, value in request.data.items():
            setattr(instance, key, value)
        instance.save()

        for supervisor_data in supervisors_data:
            supervisor_id = supervisor_data['id']
            supervising_type = supervisor_data['supervising_type']
            print(supervisor_id, supervising_type)
            if supervisor_id and supervising_type:
                supervisor = SupervisorProfile.objects.get(user_id=supervisor_id)
                supervision, created = instance.supervisions.get_or_create(
                    supervisor=supervisor,
                    defaults={'supervising_type': supervising_type}
                )
                print(supervision , created)
                if not created:
                    supervision.supervising_type = supervising_type
                    supervision.save()
                    supervision.refresh_from_db()
                    print(f'Type: {supervision.supervising_type}')
            else:
                raise serializers.ValidationError("Supervisor id or supervising type are required")
            
        return instance

class EditThesisLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = EditThesisLog
        fields = "__all__"


class ThesisWithoutUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Thesis
        exclude = ("student",)

    # def get_student(self, obj):
    #     from students.serializers import StudentProfileSerializer

    #     for attribute, value in obj.__dict__.items():
    #         # name bobbyhadz
    #         # salary 100
    #         print(attribute, value)
    #     return StudentProfileSerializer(StudentProfile.objects.get(obj.student)).data


# from students.serializers import StudentProfileSerializer
