from students.models import StudentProfile
from .loadUsers import std1, std2

studentProfile1 = StudentProfile.objects.create(
    user=std1,
)

studentProfile2 = StudentProfile.objects.create(
    user=std2,
)

print("Student profiles Created")
