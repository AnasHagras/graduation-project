from thesis.models import Thesis
from .loadStudents import studentProfile1, studentProfile2

thesis1 = Thesis.objects.create(
    student=studentProfile1,
    arabic_title="الرسالة الأولي",
    english_title="English",
    state="مستمر",
    registration_period=5,
)
thesis2 = Thesis.objects.create(
    student=studentProfile2,
    arabic_title="الرسالة الثانية",
    english_title="English",
    state="ملغي",
    registration_period=5,
)

print("Theses Loaded")
