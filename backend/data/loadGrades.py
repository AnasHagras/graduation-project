from enums.models import Grade

passGrade = Grade.objects.create(name="مقبول", display_name="مقبول")
goodGrade = Grade.objects.create(name="جيد", display_name="جيد")
veryGoodGrade = Grade.objects.create(name="جيد جدا", display_name="جيد جدا")
excellentGrade = Grade.objects.create(name="ممتاز", display_name="ممتاز")

print("Grades Loaded")
