from enums.models import Faculty, Department, Specialization, Branch

# Faculties
engineeringFaculty = Faculty.objects.create(name="Engineering", display_name="كلية الهندسة")

# Departments
urbanPlanningDepartment = Department.objects.create(
    name="urbanPlanningDepartment",
    display_name="هندسة التخطيط العمراني",
    faculty=engineeringFaculty,
)
civilDepartment = Department.objects.create(
    name="civilDepartment", display_name="الهندسة المدنية", faculty=engineeringFaculty
)
mechanicalDepartment = Department.objects.create(
    name="mechanicalDepartment",
    display_name="الهندسة المكانيكية",
    faculty=engineeringFaculty,
)
electricalDepartment = Department.objects.create(
    name="electricalDepartment",
    display_name="الهندسة الكهربية",
    faculty=engineeringFaculty,
)
miningAndPetroleumDepartment = Department.objects.create(
    name="miningAndPetroleumDepartment",
    display_name="هندسة التعدين والبترول",
    faculty=engineeringFaculty,
)
architectureDepartment = Department.objects.create(
    name="architectureDepartment",
    display_name="هندسة العمارة",
    faculty=engineeringFaculty,
)
computersAndSystemsDepartment = Department.objects.create(
    name="computersAndSystemsDepartment",
    display_name="هندسة النظم والحاسبات",
    faculty=engineeringFaculty,
)

# Civil Engineering Branches
workingBranch = Branch.objects.create(name="workingBranch", display_name="قسم الأشغال", department=civilDepartment)

constructionBranch = Branch.objects.create(
    name="constructionBranch", display_name="قسم الإنشاءات", department=civilDepartment
)

irrigationAndHydraulicsBranch = Branch.objects.create(
    name="irrigationAndHydraulicsBranch",
    display_name="قسم الري والهيدروليكا",
    department=civilDepartment,
)

# Mechanical Engineering Branches
powerBranch = Branch.objects.create(name="powerBranch", display_name="قسم القوي", department=mechanicalDepartment)

productionBranch = Branch.objects.create(
    name="productionBranch", display_name="قسم الأنتاج", department=mechanicalDepartment
)

# Electrical Engineering Branches
electricalPowerAndMachines = Branch.objects.create(
    name="electricalPowerAndMachines",
    display_name="قسم القوى والاّت الكهربية",
    department=electricalDepartment,
)
communicationBranch = Branch.objects.create(
    name="communicationBranch",
    display_name="قسم الاتصالات",
    department=electricalDepartment,
)

# Mining Engineering Branches
miningBranch = Branch.objects.create(
    name="miningBranch",
    display_name="قسم التعدين",
    department=miningAndPetroleumDepartment,
)
petroleumBranch = Branch.objects.create(
    name="petroleumBranch",
    display_name="قسم البترول",
    department=miningAndPetroleumDepartment,
)

print("Organization Loaded")
