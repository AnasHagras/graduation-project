from students.models import Qualification
from datetime import date
from .loadStudents import studentProfile1, studentProfile2
from .loadGrades import passGrade

Qualification.objects.create(student=studentProfile1, date=date.today(), grade=passGrade, degree="بكالوريوس")

Qualification.objects.create(student=studentProfile2, date=date.today(), grade=passGrade, degree="بكالوريوس")

print("Qualifications Loaded")
