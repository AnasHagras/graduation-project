from supervisory.models import Supervision
from .loadTheses import thesis1, thesis2
from .loadSupervisors import supervisorProfile1, supervisorProfile2

rel1 = Supervision.objects.create(thesis=thesis1, supervisor=supervisorProfile1)

rel2 = Supervision.objects.create(thesis=thesis1, supervisor=supervisorProfile2)

rel3 = Supervision.objects.create(thesis=thesis2, supervisor=supervisorProfile1)

rel4 = Supervision.objects.create(thesis=thesis2, supervisor=supervisorProfile2)

print("Relationships Created")
