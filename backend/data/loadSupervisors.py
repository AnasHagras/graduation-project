from supervisors.models import SupervisorProfile
from .loadUsers import sup1, sup2
from .loadOrganization import engineeringFaculty, electricalDepartment

supervisorProfile1 = SupervisorProfile.objects.create(
    user=sup1, faculty=engineeringFaculty, department=electricalDepartment
)

supervisorProfile2 = SupervisorProfile.objects.create(
    user=sup2, faculty=engineeringFaculty, department=electricalDepartment
)


print("Supervisor profiles Created")
