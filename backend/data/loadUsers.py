from users.models import User
from students.models import Student
from supervisors.models import Supervisor

if User.objects.filter(email="a@a.com").first() == None:
    admin = User.objects.create_superuser(email="a@a.com", password="a")
    print("Admin Created")
if Student.objects.filter(email="b@b.com").first() == None:
    std1 = Student.objects.create_student(email="b@b.com", password="b", role="STUDENT", arabicName="الطالب الأول")
    print("Student1 Created")
else:
    std1 = Student.objects.get(email="b@b.com")
if Student.objects.filter(email="c@c.com").first() == None:
    std2 = Student.objects.create_student(email="c@c.com", password="c", role="STUDENT", arabicName="الطالب الثاني")
    print("Student2 Created")
else:
    std2 = Student.objects.get(email="c@c.com")
if Supervisor.objects.filter(email="d@d.com").first() == None:
    sup1 = Supervisor.objects.create_supervisor(
        email="d@d.com", password="d", role="SUPERVISOR", arabicName="المشرف الأول"
    )
else:
    sup1 = Supervisor.objects.get(email="d@d.com")
    print("Supervisor1 Created")
if Supervisor.objects.filter(email="e@e.com").first() == None:
    sup2 = Supervisor.objects.create_supervisor(
        email="e@e.com", password="e", role="SUPERVISOR", arabicName="المشرف الثاني"
    )
else:
    sup2 = Supervisor.objects.get(email="e@e.com")
    print("Supervisor2 Created")

print("Users Loaded")
