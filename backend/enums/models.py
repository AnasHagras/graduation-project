from django.db import models


class Military(models.Model):
    name = models.CharField(
        max_length=50,
    )
    display_name = models.CharField(
        max_length=50,
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Militaries"


class Grade(models.Model):
    name = models.CharField(
        max_length=50,
    )
    display_name = models.CharField(
        max_length=50,
    )

    def __str__(self):
        return self.name


class City(models.Model):
    name = models.CharField(
        max_length=100,
    )
    display_name = models.CharField(
        max_length=100,
    )

    def __str__(self):
        return self.name 

    class Meta:
        verbose_name_plural = "Cities"


class Faculty(models.Model):
    name = models.CharField(
        max_length=128,
    )
    display_name = models.CharField(max_length=128)

    class Meta:
        verbose_name_plural = "Faculties"

    def __str__(self):
        return self.name


class Department(models.Model):
    name = models.CharField(max_length=128)
    display_name = models.CharField(
        max_length=128,
    )
    faculty = models.ForeignKey(to=Faculty, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Branch(models.Model):
    name = models.CharField(max_length=128)
    display_name = models.CharField(
        max_length=128,
    )
    department = models.ForeignKey(to=Department, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Specialization(models.Model):
    name = models.CharField(max_length=128)
    display_name = models.CharField(
        max_length=128,
    )
    branch = models.ForeignKey(to=Branch, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
