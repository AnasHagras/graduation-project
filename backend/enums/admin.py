from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Faculty)
admin.site.register(Branch)
admin.site.register(Department)
admin.site.register(Specialization)
admin.site.register(City)
admin.site.register(Grade)
admin.site.register(Military)
