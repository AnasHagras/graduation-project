from django.urls import path, include
from rest_framework import routers
from .views import *

router = routers.DefaultRouter()
router.register(r"faculty", FacultyViewSet, basename="faculty")
router.register(r"department", DepartmentViewSet, basename="department")
router.register(r"branch", BranchViewSet, basename="branch")
router.register(r"specialization", SpecializationViewSet, basename="specialization")
router.register(r"city", CityViewSet, basename="city")

urlpatterns = [
    path("", include(router.urls)),
    path("organization/", Organization.as_view()),
    path("qualification-grades/", GradeList.as_view()),
    path("military/", MilitaryList.as_view()),
]
