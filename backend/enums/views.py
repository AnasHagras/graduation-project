from rest_framework import viewsets, views, generics
from knox.auth import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from .models import *
from .serializers import *
from users.permissions import AdminAccess


class FacultyViewSet(AdminAccess, viewsets.ModelViewSet):
    model = Faculty
    queryset = Faculty.objects.all()
    serializer_class = FacultySerializer


class DepartmentViewSet(AdminAccess, viewsets.ModelViewSet):
    model = Department
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer


class BranchViewSet(AdminAccess, viewsets.ModelViewSet):
    model = Branch
    queryset = Branch.objects.all()
    serializer_class = BranchSerializer


class CityViewSet(AdminAccess, viewsets.ModelViewSet):
    model = City
    queryset = City.objects.all()
    serializer_class = CitySerializer


class SpecializationViewSet(AdminAccess, viewsets.ModelViewSet):
    model = Specialization
    queryset = Specialization.objects.all()
    serializer_class = SpecializationSerializer


class GradeList(AdminAccess, generics.ListAPIView):
    model = Grade
    queryset = Grade.objects.all()
    serializer_class = GradeSerializer


class MilitaryList(AdminAccess, generics.ListAPIView):
    model = Military
    queryset = Military.objects.all()
    serializer_class = MilitarySerializer


class Organization(AdminAccess, views.APIView):
    def get(self, request, *args, **kwargs):
        faculties = FacultySerializer(Faculty.objects.all(), many=True).data
        departments = {}
        for faculty in faculties:
            departments[faculty.get("name")] = DepartmentSerializer(
                Department.objects.filter(faculty=faculty.get("id")).all(), many=True
            ).data
        branches = {}
        for faculty in faculties:
            for department in departments[faculty.get("name")]:
                branches[department.get("name")] = BranchSerializer(
                    Branch.objects.filter(department=department.get("id")).all(),
                    many=True,
                ).data
        specializations = {}
        for faculty in faculties:
            for department in departments[faculty.get("name")]:
                for branch in branches[department.get("name")]:
                    specializations[branch.get("name")] = SpecializationSerializer(
                        Specialization.objects.filter(branch=branch.get("id")).all(),
                        many=True,
                    ).data
        return Response(
            {
                "faculties": faculties,
                "departments": departments,
                "branches": branches,
                "specializations": specializations,
            },
            status=200,
        )
