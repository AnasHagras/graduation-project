# Backend

## Student Registration

To register new students you must be logged in as Admin user, create one using the createsuperuser method

send a POST request to the following api "http://127.0.0.1:8000/api/students/register/"

The request body must include the following fields:

- email
- password
- password_confirmation
- date_of_birth (formatted YYYY-MM-DD)
- first_name
- last_name
- student profile:
  - degree (Master or PhD)
  - registration_date
  - expenses_paid (Boolean)
  - file_number: number
  - supervisor: (the id of the supervisor can be null)

Example:

```
{
    "email": "mograby6200@gmail.com",
    "password": "ASDCYEWRW123%!@#",
    "password_confirmation": "ASDCYEWRW123%!@#",
    "date_of_birth": "2000-2-1",
    "first_name": "Ahmad",
    "last_name": "Almaghraby",
    "student_profile":{
        "degree": "Master",
        "registration_date": "2000-2-11",
        "expenses_paid": true,
        "file_number": 5,
        "supervisor": null
    }
}
```

## get current user endpoint

takes token in `get` request and returns the associated user

# Frontend

## Services

calling backend endpoints should be done here in services directory

axios method returns a promise with the response resolved if request succeeded otherwise it returns a promise with error

REACT_APP_API_URL is the backend url stored in .env file

Deceleration Example:

```
    import axios from "axios";
    const login = (email, password) => {
    const REQUEST_URL = `${process.env.REACT_APP_API_URL}/authentication/login/`;
    const data = {
        email: email,
        password: password,
    };
    return new Promise((resolve, reject) => {
        axios
        .post(REQUEST_URL, data)
        .then((response) => {
            resolve(response.data);
        })
        .catch((error) => {
            reject(error);
        });
    });
    };

    export default login;
```

Usage Example (see TestServices Page):

```
    login(email, password)
      .then((res) => {
        // use the response
      })
      .catch((err) => {
        // handle the errors
      })
      .finally(() => {
        // do something after fetching
      })
```

## .env

this file should be included in .gitignore and shouldn't be in the global repo

prefix your variable with REACT_APP

Store the global environment variables in it i.e. `REACT_APP_VARIABLE_NAME="https://localhost:8000/api/"`

to import these variables use `process.env.REACT_APP_VARIABLE_NAME`

## .env.example

this file should be appear in the global repo, no issues, no secret data

this file should include the same variable names as the .env file but with the example values not the real

Usage:
this file is used to know that variables are stored in .env file so, any variable added to .env should be added to .env.example with no real data

Example:

```
  .env >> REACT_APP_DB_PASSWORD = 123456789
  .env.example >> REACT_APP_DB_PASSWORD = "PUT YOUR DATABASE PASSWORD HERE"
```

## redux toolkit

this is updated version of redux with the same functionality at all

Usage:
We create a new slice which represents a group of common things, for example authenticating slice, researcher slice and etc.
they are stored in the `features folder` see `features/authentication/authSlice.js`

- we create a new slice with `createSlice()`
- slice contains name , initial state , reducers and extra reducers
- both reducers and extra reducers are taking state and action and depending on the action they changing the state
- extra reducers deals with the states which calls a backend api
- for each endpoint we create a asyncThunk that takes a name and async function this async function takes state and thunkAPI and then
  the logic of the endpoint, usually we call the services method like services.login

```
  const login = createAsyncThunk("auth/login", async ({ state }, thunkAPI) => {
    try {
      console.log(state);
      const response = await services.login(state);
      return response;
    } catch (err) {
      let message = "something went wrong!";
      if (err.response.status === 400) {
        message = err.response.data["non_field_errors"][0];
      }
      return thunkAPI.rejectWithValue(message);
    }
  });
```

- in the extra reducers section we add 3 cases for each asyncThunk we have created above , they are login.pending which means that login request still pending , login.fulfilled which means that login request succeeded , login.rejected which means that login request failed , in each case we have a state and action or payload , payload is the return of the thunkAPI we created
  and state is changed according to this payload , example below
  handles the login cases , the addCase method takes a name which is asyncThunk name and a callback function which takes state and payload

```
  extraReducers: (builder) => {
    // login actions
    builder.addCase(login.pending, (state, {payload }) => {
      state.pending = true;
      state.errorMessage = "";
    });
    builder.addCase(login.fulfilled, (state, { payload }) => {
      state.pending = false;
      state.user = payload.user;
      state.token = payload.token;
      state.isLoggedIn = true;
      localStorage.setItem("token", payload.token);
      state.errorMessage = "";
      state.successMessage = "Welcome!" + payload.user.first_name;
    });
    builder.addCase(login.rejected, (state, { payload }) => {
      state.pending = false;
      state.errorMessage = payload;
    });
  }
```

- after creating a slice we export asyncThunks that we created earlier

```
  export { login, logout, refreshAuth };
```

- after that we add reduces to the store in `src/app/store.js`

```
  import { configureStore } from "@reduxjs/toolkit";
  import authenticationReducer from "../features/authentication/authSlice";
  export default configureStore({
    reducer: { auth: authenticationReducer },
  });
```

- to access the state we use that

```
  const { isLoggedIn, pending, errorMessage, successMessage } = useSelector(
      (state) => state.auth
    );
```

- then we call these actions in the react component as follows:
  initially we create a dispatch

```
const authDispatch = useDispatch();
```

then we call the function we want

```
authDispatch(login({ state }));
```

to use a state we can use that

```
{pending && <h1>Loading</h1>}
```
## Search-Pick Modal
source: `frontend/src/molecules/search-pick-modal/SearchPickModal.jsx`

### Functionality:
 - Displays a list of items and allows the user to choose one or more items.

### Parameters: 
  - open: Boolean, representing the state of the modal 
  - handleClose: function to handle close of the modal, in simplest forms it just set `open = false`
  - items: List of items to display, each item must have a name and id.
  - pickedItems: List of picked items, these items won't be displayed.
  - setPickedItems: Function to handle changing of `pickedItems`